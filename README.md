## [Play Market](https://play.google.com/store/apps/details?id=com.setnameinc.voenmeh)

## Модули и их ответственность

**ui** - view

**viewModel** - делает запросы в interactor и отображает в view

**domain** - содержит interactor который отвечает за model логику

**repository** - кэшированные данные

**api** - запросы к серверу

## Архитектура

![architecture](https://user-images.githubusercontent.com/62020596/120854216-86556e00-c585-11eb-84d3-8c56c7903730.png)
