package com.setnameinc.voenmeh.app

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.paging.ExperimentalPagingApi
import com.google.firebase.ktx.Firebase
import com.google.firebase.ktx.initialize
import com.setnameinc.voenmeh.app.di.Modules
import com.setnameinc.voenmeh.email.scheduler.CheckNewEmails.CHANNEL_ID
import com.setnameinc.voenmeh.email.scheduler.CheckNewEmailsService
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber


@ExperimentalPagingApi
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Firebase.initialize(this)
        startKoin {
            androidContext(this@App)
            modules(Modules.modules)
        }
        createEmailNotificationChannel()
        Timber.plant(Timber.DebugTree())
        try {
            startService(Intent(this, CheckNewEmailsService::class.java))
        } catch (e: Exception) {

        }
    }

    private fun createEmailNotificationChannel() {
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID,
                "My channel",
                NotificationManager.IMPORTANCE_HIGH
            )
            channel.description = "Новые письма на почте"
            channel.enableLights(true)
            channel.lightColor = Color.BLUE
            channel.enableVibration(false)
            notificationManager.createNotificationChannel(channel)
        }
    }

}