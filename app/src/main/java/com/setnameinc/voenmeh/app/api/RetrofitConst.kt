package com.setnameinc.voenmeh.app.api

object RetrofitConst {
    val VOENMEH_SERVER_BASE_URL = "https://voenmeh-server-812-ed-1.ru/"
    val CONNECTION_TIMEOUT_SECONDS: Long = 100
    val READ_TIMEOUT_SECONDS: Long = 60
    val WRITE_TIMEOUT_SECONDS: Long = 60

    val FIREBASE_BASE_URL = "https://voenmeh-841cf.firebaseapp.com/v1/"

    val FIREBASE_RETROFIT_INJECTION_KEY: String = "firebase"
    val VOENMEH_SERVER_RETROFIT_INJECTION_KEY: String = "voenmeh-server"
}