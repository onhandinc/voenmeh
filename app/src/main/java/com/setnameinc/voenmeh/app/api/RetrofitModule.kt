package com.setnameinc.voenmeh.app.api

import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.setnameinc.voenmeh.app.api.RetrofitConst.CONNECTION_TIMEOUT_SECONDS
import com.setnameinc.voenmeh.app.api.RetrofitConst.FIREBASE_BASE_URL
import com.setnameinc.voenmeh.app.api.RetrofitConst.FIREBASE_RETROFIT_INJECTION_KEY
import com.setnameinc.voenmeh.app.api.RetrofitConst.READ_TIMEOUT_SECONDS
import com.setnameinc.voenmeh.app.api.RetrofitConst.VOENMEH_SERVER_BASE_URL
import com.setnameinc.voenmeh.app.api.RetrofitConst.VOENMEH_SERVER_RETROFIT_INJECTION_KEY
import com.setnameinc.voenmeh.app.api.RetrofitConst.WRITE_TIMEOUT_SECONDS
import okhttp3.OkHttpClient
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitModule {
    val module = module {
        single<Retrofit>(named(VOENMEH_SERVER_RETROFIT_INJECTION_KEY)) {
            Retrofit.Builder()
                .baseUrl(VOENMEH_SERVER_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(okHttpClient)
                .build()
        }
        single<Retrofit>(named(FIREBASE_RETROFIT_INJECTION_KEY)) {
            Retrofit.Builder()
                .baseUrl(FIREBASE_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(okHttpClient)
                .build()
        }
    }
    private val okHttpClient = OkHttpClient()
        .newBuilder()
        .apply {
            connectTimeout(CONNECTION_TIMEOUT_SECONDS, TimeUnit.MILLISECONDS)
            readTimeout(READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            writeTimeout(WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
        }
        .build()
}