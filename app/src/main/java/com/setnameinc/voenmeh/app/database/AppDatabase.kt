package com.setnameinc.voenmeh.app.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.setnameinc.voenmeh.email.main.data.RemoteKeys
import com.setnameinc.voenmeh.email.main.data.RemoteKeysRepository
import com.setnameinc.voenmeh.email.main.repository.EmailEntity
import com.setnameinc.voenmeh.email.main.repository.EmailRepository
import com.setnameinc.voenmeh.news.main.repository.NewsEntity
import com.setnameinc.voenmeh.news.main.repository.NewsRemoteKey
import com.setnameinc.voenmeh.news.main.repository.NewsRemoteKeysRepository
import com.setnameinc.voenmeh.news.main.repository.NewsRepository
import com.setnameinc.voenmeh.timetable.common.repository.TimetableEntity
import com.setnameinc.voenmeh.timetable.common.repository.TimetableRepository

@Database(
    entities = [
        EmailEntity::class,
        TimetableEntity::class,
        RemoteKeys::class,
        NewsEntity::class,
        NewsRemoteKey::class],
    version = 14
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun emailRepository(): EmailRepository
    abstract fun timetableRepository(): TimetableRepository
    abstract fun remoteKeysRepository(): RemoteKeysRepository
    abstract fun newsRepository(): NewsRepository
    abstract fun newsRemoteKeysRepository(): NewsRemoteKeysRepository
}