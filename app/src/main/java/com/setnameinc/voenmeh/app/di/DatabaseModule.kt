package com.setnameinc.voenmeh.app.di

import androidx.room.Room
import com.setnameinc.voenmeh.app.database.AppDatabase
import org.koin.dsl.module

object DatabaseModule {
    val module = module {
        single {
            Room.databaseBuilder(
                get(),
                AppDatabase::class.java, "database-voenmeh"
            )
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}