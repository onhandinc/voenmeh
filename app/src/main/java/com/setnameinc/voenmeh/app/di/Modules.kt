package com.setnameinc.voenmeh.app.di

import androidx.paging.ExperimentalPagingApi
import com.setnameinc.voenmeh.app.api.RetrofitModule
import com.setnameinc.voenmeh.pages.classroompage.di.ClassroomPageModule
import com.setnameinc.voenmeh.email.main.di.EmailModule
import com.setnameinc.voenmeh.email.scheduler.CheckNewEmailsModule
import com.setnameinc.voenmeh.tools.firebase.di.FirebaseModule
import com.setnameinc.voenmeh.pages.grouppage.di.GroupPageModule
import com.setnameinc.voenmeh.pages.lecturerpage.di.LecturerPageModule
import com.setnameinc.voenmeh.loginsignup.login.main.di.LoginModule
import com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.di.VoenmehLoginFromEmailScreen
import com.setnameinc.voenmeh.news.di.NewsModule
import com.setnameinc.voenmeh.profile.di.ProfileModule
import com.setnameinc.voenmeh.search.di.SearchModule
import com.setnameinc.voenmeh.tools.sharedpreferences.di.SharedPreferencesModule
import com.setnameinc.voenmeh.email.show.di.ShowEmailModule
import com.setnameinc.voenmeh.loginsignup.signup.di.SignUpModule
import com.setnameinc.voenmeh.splash.di.SplashModule
import com.setnameinc.voenmeh.timetable.common.di.TimetableModules
import com.setnameinc.voenmeh.user.common.di.UserModule
import com.setnameinc.voenmeh.user.blocked.di.UserBlockedModule
import com.setnameinc.voenmeh.tools.vk.di.VKModule
import com.setnameinc.voenmeh.loginsignup.voenmehmail.di.VoenmehMailModule
import org.koin.core.module.Module

object Modules {
    @ExperimentalPagingApi
    val modules: List<Module> = mutableListOf(
        LoginModule.module,
        SignUpModule.module,
        RetrofitModule.module,
        FirebaseModule.module,
        SplashModule.module,
        UserModule.module,
        UserBlockedModule.module,
        SharedPreferencesModule.module,
        VKModule.module,
        VoenmehMailModule.module,
        ProfileModule.module,
        EmailModule.module,
        ShowEmailModule.module,
        DatabaseModule.module,
        VoenmehLoginFromEmailScreen.module,
        SearchModule.module,
        LecturerPageModule.module,
        GroupPageModule.module,
        ClassroomPageModule.module,
        CheckNewEmailsModule.module
    ).apply {
        addAll(NewsModule.modules)
        addAll(TimetableModules.modules)
    }
}