package com.setnameinc.voenmeh.app.ui.utils

import android.view.View
import timber.log.Timber

object DebounceClickListener {

    private val DEBOUNCE_TIME: Long = 800

    fun View.setDebounceClickListener(
        debounceTimeMillis: Long = DEBOUNCE_TIME,
        clickListener: View.OnClickListener
    ) {
        this.setOnClickListener(DebounceViewClickListener(debounceTimeMillis, clickListener))
    }

    fun View.setDebounceClickListener(
        debounceTimeMillis: Long = DEBOUNCE_TIME,
        clickListener: (View?) -> Unit
    ) {
        this.setOnClickListener(DebounceClickListener(debounceTimeMillis, clickListener))
    }

    class DebounceViewClickListener(
        private val debounceTimeMillis: Long,
        private val clickListener: View.OnClickListener
    ) : View.OnClickListener {

        var lastTimeClickedMillis: Long = 0

        override fun onClick(v: View?) {
            val currentTimeMillis: Long = System.currentTimeMillis()
            if (currentTimeMillis - lastTimeClickedMillis > debounceTimeMillis) {
                Timber.d("onClickListener")
                clickListener.onClick(v)
            } else {
                Timber.d("prevent action")
            }
            lastTimeClickedMillis = currentTimeMillis
        }
    }

    class DebounceClickListener(
        private val debounceTimeMillis: Long,
        private val clickListener: (View?) -> Unit
    ) : View.OnClickListener {

        var lastTimeClickedMillis: Long = 0

        override fun onClick(v: View?) {
            val currentTimeMillis: Long = System.currentTimeMillis()
            if (currentTimeMillis - lastTimeClickedMillis > debounceTimeMillis) {
                Timber.d("onClickListener")
                clickListener(v)
            } else {
                Timber.d("prevent action")
            }
            lastTimeClickedMillis = currentTimeMillis
        }
    }
}