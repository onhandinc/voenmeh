package com.setnameinc.voenmeh.app.ui.utils

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

object SupportFragmentManagerExtensions {
    fun FragmentManager.replace(active: Fragment, target: Fragment) {
        this.beginTransaction().hide(active).show(target).commit()
    }
}