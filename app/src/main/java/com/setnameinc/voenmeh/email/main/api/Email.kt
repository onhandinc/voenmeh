package com.setnameinc.voenmeh.email.main.api

import javax.mail.internet.MimeMessage

data class Email(
    val id: Int?,
    val message: MimeMessage
)