package com.setnameinc.voenmeh.email.main.api

import com.setnameinc.voenmeh.email.main.api.parcer.MimeMessageParser
import com.setnameinc.voenmeh.email.main.domain.EmailConverter
import com.setnameinc.voenmeh.email.main.domain.EmailModel
import com.setnameinc.voenmeh.email.main.repository.EmailClientSharedPreferences
import com.setnameinc.voenmeh.email.main.repository.EmailEntity
import com.setnameinc.voenmeh.loginsignup.voenmehmail.api.MailProtocolSettings
import com.setnameinc.voenmeh.tools.utils.flatMap
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import timber.log.Timber
import java.io.InputStream
import javax.activation.CommandMap
import javax.activation.MailcapCommandMap
import javax.mail.*
import javax.mail.internet.MimeMessage

class EmailApi(
    private val emailClientSharedPreferences: EmailClientSharedPreferences
) {

    val imapLockMutex: Mutex = Mutex()
    val connectImapMutex: Mutex = Mutex()

    private var imapStore: Store? = null

    private var inboxFolder: Folder? = null
    private var attachmentInboxFolder: Folder? = null

    private val imapLock: Any = Any()
    private val connectImapLock: Any = Any()

    suspend fun hasNewMessages(
        lastInvokedTime: Long,
        currentTime: Long,
        complete: (Long?) -> Unit,
        error: (Throwable) -> Unit
    ) {
        imapLockMutex.withLock(imapLock) {
            try {
                var lastMessageTime: Long? = null
                if (imapStore != null) {
                    if (inboxFolder == null) {
                        inboxFolder = imapStore?.getFolder("INBOX")
                    }
                    if (inboxFolder?.isOpen == false) {
                        inboxFolder?.open(Folder.READ_ONLY)
                    }
                    val to = inboxFolder?.messageCount ?: throw java.lang.Exception()
                    val from = Math.max(to.minus(5), 1)
                    Timber.d("range = ${lastInvokedTime..currentTime}")
                    lastMessageTime = inboxFolder?.getMessages(from, to)
                        ?.firstOrNull {
                            Timber.d("date = ${it.sentDate.time}")
                            it.sentDate.time in lastInvokedTime..currentTime
                        }?.sentDate?.time
                    inboxFolder?.close()
                } else {
                    connectToImapStore({}, {})
                    Timber.d("imapStore is null")
                }
                complete(lastMessageTime)
            } catch (e: Exception) {
                error(e)
            }
        }
    }

    suspend fun emailsFromNetworkToDatabase(
        page: Int = 0,
        complete: suspend (List<EmailEntity>) -> Unit,
        error: (Throwable) -> Unit
    ) {
        emails(
            page,
            complete = complete.flatMap {
                EmailConverter.convertToDatabase(it)
            },
            error = error
        )
    }

    private suspend fun emails(
        page: Int = 0,
        limit: Int = 10,
        complete: suspend (List<EmailModel>) -> Unit,
        error: (Throwable) -> Unit
    ) {
        Timber.d(
            "load emails, mutex = ${imapLockMutex.holdsLock(imapLock)}, = ${
                imapLockMutex.holdsLock(
                    connectImapLock
                )
            }"
        )
        imapLockMutex.withLock(imapLock) {
            Timber.d("going into imapLock")
            try {
                val resultList: MutableList<EmailModel> = mutableListOf()
                if (imapStore != null) {
                    val messages: MutableList<Email> = mutableListOf()
                    if (inboxFolder == null) {
                        inboxFolder = imapStore?.getFolder("INBOX")
                    }
                    if (inboxFolder?.isOpen == false) {
                        inboxFolder?.open(Folder.READ_ONLY)
                    }
                    inboxFolder?.let {
                        val uf = inboxFolder as? UIDFolder
                        val from = (it.messageCount - page * limit)
                        val to = Math.max(1, it.messageCount - limit * (page + 1))
                        Timber.d("from = $from")
                        Timber.d("to = $to")
                        for (i in from downTo to) {
                            val message: Message? = it.getMessage(i)
                            if (message != null) {
                                val id = uf?.getUID(message)?.toInt()
                                Timber.d("$id, message class = ${message::class.java}")
                                if (!message.isExpunged) {
                                    messages.add(
                                        Email(
                                            id,
                                            message as MimeMessage
                                        )
                                    )
                                }
                            }
                        }
                    } ?: throw Exception("empty inbox")
                    resultList.addAll(EmailConverter.convertFromNetwork(messages))
                    complete(resultList)
                } else {
                    Timber.e("empty login to password data")
                    throw Exception("empty login to password data")
                }
            } catch (e: Exception) {
                error(e)
            }
        }
    }

    suspend fun attachmentInputStream(
        messageId: Int,
        attachmentName: String,
        complete: (InputStream) -> Unit,
        error: (Throwable) -> Unit
    ) {
        try {
            if (imapStore != null) {
                if (attachmentInboxFolder == null) {
                    attachmentInboxFolder = imapStore?.getFolder("INBOX")
                }
                if (attachmentInboxFolder?.isOpen == false) {
                    attachmentInboxFolder?.open(Folder.READ_ONLY)
                }
                (attachmentInboxFolder as? UIDFolder)?.let {
                    complete(
                        MimeMessageParser(
                            it.getMessageByUID(messageId.toLong()) as MimeMessage
                        ).parse()
                            .findAttachmentByName(attachmentName)
                            .content
                    )
                } ?: throw Exception("imap store closed")
            } else {
                throw Exception("imap store closed")
            }
        } catch (e: Exception) {
            error(e)
        }
    }

    suspend fun messageRead(id: Int) {
        imapLockMutex.withLock(imapLock) {
            try {
                if (imapStore != null) {
                    if (inboxFolder == null) {
                        inboxFolder = imapStore?.getFolder("INBOX")
                    }
                    inboxFolder?.let {
                        if (it.isOpen) {
                            it.close()
                        }
                        it.open(Folder.READ_WRITE)
                        val uf = inboxFolder as? UIDFolder
                        uf?.getMessageByUID(id.toLong())
                            ?.apply {
                                setFlag(Flags.Flag.SEEN, true)
                                Timber.d("message marked as seen")
                            }
                        if (it.isOpen) {
                            it.close()
                        }
                    } ?: throw Exception("empty inbox")
                } else {
                    Timber.e("empty login to password data")
                    throw Exception("empty login to password data")
                }
            } catch (e: java.lang.Exception) {

            }
        }
    }

    suspend fun connectToImapStore(
        complete: suspend (Boolean) -> Unit,
        error: (Throwable) -> Unit
    ) {
        connectImapMutex.withLock(connectImapLock) {
            try {
                if (imapStore != null && imapStore?.isConnected == true) {
                    complete(true)
                } else {
                    emailClientSharedPreferences.getEmailClientData(
                        complete = {
                            val store: Store =
                                Session.getDefaultInstance(MailProtocolSettings.IMAPSettings).store
                            store.connect(it.email, it.password)
                            if (store.isConnected) {
                                imapStore = store
                                setHandlers()
                                complete(true)
                            } else {
                                imapStore = null
                                inboxFolder = null
                                attachmentInboxFolder = null
                                complete(false)
                            }
                        },
                        error = error
                    )
                }
            } catch (e: java.lang.Exception) {
                error(e)
            }
        }
    }

    fun closeStore() {
        if (attachmentInboxFolder?.isOpen == true) {
            attachmentInboxFolder?.close()
        }
        if (inboxFolder?.isOpen == true) {
            inboxFolder?.close()
        }
        if (imapStore?.isConnected == true) {
            imapStore?.close()
        }
        inboxFolder = null
        imapStore = null
    }

    private fun setHandlers() {
        val mc: MailcapCommandMap = CommandMap.getDefaultCommandMap() as MailcapCommandMap
        mc.addMailcap("text/html;; x-java-content-handler=com.sun.mail.handlers.text_html")
        mc.addMailcap("text/xml;; x-java-content-handler=com.sun.mail.handlers.text_xml")
        mc.addMailcap("text/plain;; x-java-content-handler=com.sun.mail.handlers.text_plain")
        mc.addMailcap("multipart/*;; x-java-content-handler=com.sun.mail.handlers.multipart_mixed")
        mc.addMailcap("message/rfc822;; x-java-content-handler=com.sun.mail.handlers.message_rfc822")
        CommandMap.setDefaultCommandMap(mc)
    }

}