package com.setnameinc.voenmeh.email.main.api.parcer;

import java.io.InputStream;

public class Attachment {
    InputStream content;
    String name;

    public Attachment(InputStream content, String name) {
        this.content = content;
        this.name = name;
    }

    public InputStream getContent() {
        return content;
    }

    public String getName() {
        return name;
    }
}
