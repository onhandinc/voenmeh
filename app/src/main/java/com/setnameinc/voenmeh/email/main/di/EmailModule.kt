package com.setnameinc.voenmeh.email.main.di

import androidx.paging.ExperimentalPagingApi
import com.setnameinc.voenmeh.app.database.AppDatabase
import com.setnameinc.voenmeh.email.main.api.EmailApi
import com.setnameinc.voenmeh.email.main.domain.EmailInteractor
import com.setnameinc.voenmeh.email.main.repository.EmailClientSharedPreferences
import com.setnameinc.voenmeh.email.main.ui.EmailFragment
import com.setnameinc.voenmeh.email.main.viewmodel.EmailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object EmailModule {
    @ExperimentalPagingApi
    val module = module {
        scope<EmailFragment> {
            viewModel {
                EmailViewModel(
                    emailRepository = get(),
                    remoteKeysRepository = get(),
                    emailApi = get()
                )
            }
        }
        single {
            get<AppDatabase>().remoteKeysRepository()
        }
        single {
            EmailInteractor(
                emailApi = get(),
                emailRepository = get(),
                emailClientSharedPreferences = get(),
                remoteKeysRepository = get()
            )
        }
        single {
            EmailClientSharedPreferences(sharedPreferences = get())
        }
        single {
            EmailApi(emailClientSharedPreferences = get())
        }
        single {
            get<AppDatabase>().emailRepository()
        }
    }
}