package com.setnameinc.voenmeh.email.main.domain

import kotlinx.serialization.Serializable

@Serializable
data class AttachmentListEntity(
    val items: List<AttachmentEntity>
)

@Serializable
data class AttachmentEntity(
    val name: String,
    val extension: String,
    val type: AttachmentType
)

@Serializable
enum class AttachmentType(val typeFileName: String) {
    PDF("pdf"),
    DOC("doc"),
    DOCX("docx"),
    XLSX("xlsx"),
    UNKNOWN("unknown")
}