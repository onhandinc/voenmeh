package com.setnameinc.voenmeh.email.main.domain

import com.setnameinc.voenmeh.email.main.api.Email
import com.setnameinc.voenmeh.email.main.api.parcer.MimeMessageParser
import com.setnameinc.voenmeh.email.main.repository.EmailEntity
import com.setnameinc.voenmeh.email.main.ui.EmailItem
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import timber.log.Timber
import java.util.*
import javax.mail.Flags
import javax.mail.Session

object EmailConverter {

    fun convertFromDatabase(entities: EmailEntity): EmailItem =
        EmailItem(
            id = entities.id,
            sender = entities.sender,
            subject = entities.subject,
            plainText = entities.plainText,
            shortText = entities.shortText,
            sentTime = entities.sentTime,
            hasAttachment = entities.hasAttachment,
            isRead = entities.isRead,
            senderIconLetter = entities.senderIconLetter.first(),
            senderIconColor = entities.senderIconColor,
            htmlText = entities.htmlText
        )

    fun convertFromNetwork(messages: List<Email>): List<EmailModel> {
        return messages.map { email ->
            EmailModel(
                id = email.id ?: 0,
                subject = email.message.subject ?: "(без заголовка)",
                sentTime = email.message.sentDate.time,
                isRead = email.message.isSet(Flags.Flag.SEEN),
            ).also {
                Thread.currentThread().contextClassLoader = Session::class.java.classLoader
                val parsedText: MimeMessageParser = MimeMessageParser(
                    email.message
                ).parse()
                Timber.d("parsedText.attachmentList = ${parsedText.attachmentList.map { it.name }}")
                it.sender = parsedText.from
                if (parsedText.hasHtmlContent()) {
                    it.htmlText = parsedText.htmlContent
                }
                if (parsedText.hasPlainContent()) {
                    it.plainText = parsedText.plainContent
                }
                it.hasAttachment = parsedText.hasAttachments()
                it.shortText = it.plainText.substring(0, Math.min(100, it.plainText.length))
                    .replace("\n", "")
                it.senderIconLetter = it.sender.first().toString()
                it.senderIconColor = generateColor(it.sender.first())
                it.attachmentNameJson = Json.encodeToString(
                    AttachmentListEntity(
                        parsedText.attachmentList.map { attachment ->
                            AttachmentEntity(
                                name = getName(attachment.name),
                                extension = getExtension(attachment.name),
                                generateFileTypeFromName(attachment.name)
                            )
                        }.toList()
                    )
                )
            }
        }
    }

    fun convertToDatabase(messages: List<EmailModel>): List<EmailEntity> =
        messages.map { email ->
            EmailEntity(
                id = email.id,
                subject = email.subject,
                sentTime = email.sentTime,
                isRead = email.isRead,
                sender = email.sender,
                attachmentNameJson = email.attachmentNameJson,
                htmlText = email.htmlText,
                plainText = email.plainText,
                senderIconColor = email.senderIconColor,
                senderIconLetter = email.senderIconLetter,
                hasAttachment = email.hasAttachment,
                shortText = email.shortText
            )
        }


    private fun getName(attachmentName: String): String =
        attachmentName.dropLastWhile { it != '.' }.dropLast(1)

    private fun getExtension(attachmentName: String): String =
        attachmentName.takeLastWhile { it != '.' }.toLowerCase()

    private fun generateFileTypeFromName(fileName: String): AttachmentType =
        when (fileName.takeLastWhile { it != '.' }.toLowerCase(Locale.ENGLISH)) {
            AttachmentType.PDF.typeFileName -> AttachmentType.PDF
            AttachmentType.DOCX.typeFileName -> AttachmentType.DOCX
            AttachmentType.DOC.typeFileName -> AttachmentType.DOC
            AttachmentType.XLSX.typeFileName -> AttachmentType.XLSX
            else -> AttachmentType.UNKNOWN
        }

    private fun generateColor(letter: Char): String =
        senderIconColors[letter.hashCode() % senderIconColors.size]

    private val senderIconColors: List<String> = listOf(
        "#6F9EAF",
        "#FF9F68",
        "#ED6663",
        "#167BF8",
        "#845EC2",
        "#FFC75F",
        "#AD6C80",
        "#E97878",
        "#FF7B54",
        "#0E918C"
    )

}