package com.setnameinc.voenmeh.email.main.domain

import android.util.Patterns
import com.setnameinc.voenmeh.email.main.api.EmailApi
import com.setnameinc.voenmeh.email.main.data.RemoteKeysRepository
import com.setnameinc.voenmeh.email.main.repository.EmailClientEntity
import com.setnameinc.voenmeh.email.main.repository.EmailClientSharedPreferences
import com.setnameinc.voenmeh.email.main.repository.EmailRepository
import timber.log.Timber
import java.lang.Exception

class EmailInteractor(
    private val emailApi: EmailApi,
    private val emailRepository: EmailRepository,
    private val emailClientSharedPreferences: EmailClientSharedPreferences,
    private val remoteKeysRepository: RemoteKeysRepository
) {

    suspend fun updateEmailClientData(
        email: String,
        password: String,
        complete: suspend (Boolean) -> Unit,
        error: (Throwable) -> Unit
    ) {
        if (email.isVoenmehMail()) {
            emailClientSharedPreferences.updateUserData(
                EmailClientEntity(
                    email = email,
                    password = password
                )
            )
            complete(true)
        } else {
            error(Exception("not voenmeh mail"))
        }
    }

    suspend fun deleteAll() {
        emailRepository.deleteAll()
        remoteKeysRepository.clearRemoteKeys()
        try {
            emailApi.closeStore()
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    private fun String.isVoenmehMail() = this.endsWith("@voenmeh.ru") &&
            Patterns.EMAIL_ADDRESS.matcher(this).matches()

}