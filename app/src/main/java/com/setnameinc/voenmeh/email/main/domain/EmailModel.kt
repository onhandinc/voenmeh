package com.setnameinc.voenmeh.email.main.domain

data class EmailModel(
    var id: Int = 0,
    var sender: String = "",
    var subject: String = "",
    var plainText: String = "",
    var htmlText: String = "",
    var shortText: String = "",
    var sentTime: Long = 0,
    var hasAttachment: Boolean = false,
    var isRead: Boolean = false,
    var senderIconLetter: String = "",
    var senderIconColor: String = "",
    var attachmentNameJson: String = ""
)