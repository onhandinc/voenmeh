package com.setnameinc.voenmeh.email.main.domain

import androidx.paging.*
import com.setnameinc.voenmeh.email.main.api.EmailApi
import com.setnameinc.voenmeh.email.main.data.RemoteKeys
import com.setnameinc.voenmeh.email.main.data.RemoteKeysRepository
import com.setnameinc.voenmeh.email.main.repository.EmailEntity
import com.setnameinc.voenmeh.email.main.repository.EmailRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException
import java.lang.NullPointerException

@OptIn(ExperimentalPagingApi::class)
class EmailRemoteMediator(
    private val emailRepository: EmailRepository,
    private val emailApi: EmailApi,
    private val remoteKeysRepository: RemoteKeysRepository
) : RemoteMediator<Int, EmailEntity>() {

    private val STARTING_PAGE = 0

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, EmailEntity>
    ): MediatorResult {
        try {
            val page: Int = when (loadType) {
                LoadType.REFRESH -> {
                    val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                    remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE
                }
                LoadType.PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
                LoadType.APPEND -> {
                    val remoteKeys = getRemoteKeyForLastItem(state)
                    if (remoteKeys == null) STARTING_PAGE
                    else remoteKeys.nextKey
                        ?: return MediatorResult.Success(endOfPaginationReached = true)
                }

            }

            // Suspending network load via Retrofit. This doesn't need to be wrapped in a
            // withContext(Dispatcher.IO) { ... } block since Retrofit's Coroutine CallAdapter
            // dispatches on a worker thread.

            var result: MediatorResult? = null
            withContext(Dispatchers.Default) {
                emailApi.connectToImapStore(
                    complete = {
                        withContext(Dispatchers.IO) {
                            emailApi.emailsFromNetworkToDatabase(
                                page = page,
                                complete = { repos ->
                                    val endOfPaginationReached = repos.isEmpty()
                                    // clear all tables in the database
                                    if (loadType == LoadType.REFRESH) {
                                        Timber.d("REFRESH, remove all")
                                        withContext(Dispatchers.IO) {
                                            remoteKeysRepository.clearRemoteKeys()
                                            emailRepository.deleteAll()
                                        }
                                    }
                                    val prevKey =
                                        if (page == STARTING_PAGE) null else page.minus(1)
                                    val nextKey =
                                        if (endOfPaginationReached) null else page.plus(1)
                                    val keys = repos.map {
                                        RemoteKeys(
                                            repoId = it.id,
                                            prevKey = prevKey,
                                            nextKey = nextKey
                                        )
                                    }
                                    Timber.d("prevKey = $prevKey, page = $page, nextKey = $nextKey")
                                    Timber.d("keys = $keys")
                                    withContext(Dispatchers.IO) {
                                        remoteKeysRepository.insertAll(keys)
                                        emailRepository.insertAll(repos)
                                    }
                                    Timber.d("repos = $repos")

                                    result = MediatorResult.Success(
                                        endOfPaginationReached = endOfPaginationReached
                                    )
                                },
                                error = {
                                    Timber.e(it)
                                    result = MediatorResult.Error(it)
                                }
                            )
                        }
                    },
                    error = {
                        Timber.e(it)
                        result = MediatorResult.Error(it)
                    }
                )

            }
            return result ?: MediatorResult.Error(NullPointerException())
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, EmailEntity>): RemoteKeys? {
        // Get the last page that was retrieved, that contained items.
        // From that last page, get the last item
        return state.pages
            .lastOrNull {
                it.data.isNotEmpty()
            }
            ?.data
            ?.lastOrNull()
            ?.let { repo ->
                // Get the remote keys of the last item retrieved
                remoteKeysRepository.remoteKeysRepoId(repo.id)
            }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, EmailEntity>): RemoteKeys? {
        // Get the first page that was retrieved, that contained items.
        // From that first page, get the first item
        return state.pages
            .firstOrNull {
                it.data.isNotEmpty()
            }
            ?.data
            ?.firstOrNull()
            ?.let { repo ->
                // Get the remote keys of the first items retrieved
                remoteKeysRepository.remoteKeysRepoId(repo.id)
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, EmailEntity>
    ): RemoteKeys? {
        // The paging library is trying to load data after the anchor position
        // Get the item closest to the anchor position
        return state.anchorPosition
            ?.let { position ->
                state.closestItemToPosition(position)
                    ?.id
                    ?.let { repoId ->
                        remoteKeysRepository.remoteKeysRepoId(repoId)
                    }
            }
    }

}