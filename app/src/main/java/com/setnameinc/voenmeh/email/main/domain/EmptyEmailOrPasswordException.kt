package com.setnameinc.voenmeh.email.main.domain

import java.lang.Exception

class EmptyEmailOrPasswordException : Exception() {

    override val message: String = "empty email or password"

}