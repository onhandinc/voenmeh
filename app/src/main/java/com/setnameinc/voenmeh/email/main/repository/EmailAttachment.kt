package com.setnameinc.voenmeh.email.main.repository

data class EmailAttachment(
    var id: Int = 0,
    var sender: String = "",
    var attachmentNameJson: String = ""
)