package com.setnameinc.voenmeh.email.main.repository

data class EmailClientEntity(
    val email: String,
    val password: String
)