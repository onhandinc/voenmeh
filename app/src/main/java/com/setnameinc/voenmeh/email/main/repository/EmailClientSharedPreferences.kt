package com.setnameinc.voenmeh.email.main.repository

import android.content.SharedPreferences
import com.setnameinc.voenmeh.email.main.domain.EmptyEmailOrPasswordException
import com.setnameinc.voenmeh.email.main.repository.EmailClientSharedPreferencesKeys.EMAIL
import com.setnameinc.voenmeh.email.main.repository.EmailClientSharedPreferencesKeys.PASSWORD

class EmailClientSharedPreferences(
    private val sharedPreferences: SharedPreferences
) {

    fun updateUserData(emailClientEntity: EmailClientEntity) {
        sharedPreferences.edit().apply {
            putString(EMAIL, emailClientEntity.email)
            putString(PASSWORD, emailClientEntity.password)
        }.apply()
    }

    suspend fun getEmailClientData(
        complete: suspend (EmailClientEntity) -> Unit,
        error: (Throwable) -> Unit
    ) {
        val email = sharedPreferences.getString(EMAIL, "") ?: ""
        val password = sharedPreferences.getString(PASSWORD, "") ?: ""
        if (email.isNotEmpty() && password.isNotEmpty()) {
            complete(
                EmailClientEntity(
                    email = email,
                    password = password
                )
            )
        } else {
            error(EmptyEmailOrPasswordException())
        }
    }

}