package com.setnameinc.voenmeh.email.main.repository

object EmailClientSharedPreferencesKeys {
    const val EMAIL: String = "emailClient.email"
    const val PASSWORD: String = "emailClient.password"
}