package com.setnameinc.voenmeh.email.main.repository

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "email")
open class EmailEntity(
    @PrimaryKey
    var id: Int = 0,
    var sender: String = "",
    var subject: String = "",
    var plainText: String = "",
    var htmlText: String = "",
    var shortText: String = "",
    var sentTime: Long = 0,
    var hasAttachment: Boolean = false,
    var isRead: Boolean = false,
    var senderIconLetter: String = "",
    var senderIconColor: String = "",
    var attachmentNameJson: String = ""
)