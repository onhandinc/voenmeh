package com.setnameinc.voenmeh.email.main.repository

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity
data class EmailEntityReadUpdateEntity(
    @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "isRead")
    val isRead: Boolean = true
)