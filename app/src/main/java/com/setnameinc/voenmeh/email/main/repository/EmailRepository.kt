package com.setnameinc.voenmeh.email.main.repository

import androidx.paging.PagingSource
import androidx.room.*

@Dao
interface EmailRepository {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(list: List<EmailEntity>)

    @Update(entity = EmailEntity::class)
    fun updateEmail(updateEmailEntity: EmailEntityReadUpdateEntity)

    @Query("SELECT * FROM email ORDER BY id DESC")
    fun getEmails(): PagingSource<Int, EmailEntity>

    @Query("SELECT * FROM email WHERE id = :id")
    suspend fun getEmailById(id: Int): EmailEntity

    @Query("SELECT attachmentNameJson, sender, id FROM email WHERE id = :id")
    suspend fun getEmailAttachmentsById(id: Int): EmailAttachment

    @Query("DELETE FROM email")
    suspend fun deleteAll()

}
