package com.setnameinc.voenmeh.email.main.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.setnameinc.voenmeh.R

class EmailAdapter(
    private val clickListener: (id: Int) -> Unit
) : PagingDataAdapter<EmailItem, EmailViewHolder>(
    object : DiffUtil.ItemCallback<EmailItem>() {
        override fun areItemsTheSame(
            oldItem: EmailItem,
            newItem: EmailItem
        ): Boolean = oldItem.id == newItem.id


        override fun areContentsTheSame(
            oldItem: EmailItem,
            newItem: EmailItem
        ): Boolean = oldItem == newItem
    }
) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): EmailViewHolder = EmailViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.item_email, parent, false)
    ) { position ->
        getItem(position)?.id?.let { clickListener(it) }
    }

    override fun onBindViewHolder(holder: EmailViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }
}