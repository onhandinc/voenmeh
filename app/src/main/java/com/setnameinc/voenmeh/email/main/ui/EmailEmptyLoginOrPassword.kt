package com.setnameinc.voenmeh.email.main.ui

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.ui.VoenmehMailLoginFromEmailActivity
import kotlinx.android.synthetic.main.fragment_email_empty_login_or_password.*
import timber.log.Timber

class EmailEmptyLoginOrPassword : Fragment(R.layout.fragment_email_empty_login_or_password) {

    private val VOENMEH_MAIL_LOGIN_REQUEST_CODE = 1314

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loginToVoenmehMailButton.setDebounceClickListener {
            startActivityForResult(
                Intent(
                    requireActivity(),
                    VoenmehMailLoginFromEmailActivity::class.java
                ),
                VOENMEH_MAIL_LOGIN_REQUEST_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == VOENMEH_MAIL_LOGIN_REQUEST_CODE && resultCode == RESULT_OK) {
            Timber.d("VOENMEH_MAIL_LOGIN_REQUEST_CODE onActivityResult")
            if (data?.getBooleanExtra("loggedIn", false) == true) {

                Timber.d("navigate back")
                Timber.d("prev ${findNavController().currentDestination}")
                findNavController().popBackStack()
                Timber.d("now ${findNavController().currentDestination}")

            } else {
                Timber.d("bad extra boolean")
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}