package com.setnameinc.voenmeh.email.main.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.email.main.domain.EmptyEmailOrPasswordException
import com.setnameinc.voenmeh.email.main.ui.loading.EmailLoadStateAdapter
import com.setnameinc.voenmeh.email.main.viewmodel.EmailViewModel
import com.setnameinc.voenmeh.email.main.viewmodel.model.EmailAction
import com.setnameinc.voenmeh.email.main.viewmodel.model.EmailViewState
import com.setnameinc.voenmeh.email.main.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.email.show.ui.ShowEmailActivity
import com.setnameinc.voenmeh.tools.utils.viewModel
import kotlinx.android.synthetic.main.fragment_emails.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import org.koin.androidx.scope.fragmentScope
import timber.log.Timber

@ExperimentalPagingApi
class EmailFragment : Fragment(R.layout.fragment_emails) {

    private val viewModel: EmailViewModel by fragmentScope().viewModel(this)

    private val emailAdapter: EmailAdapter = EmailAdapter {
        startActivity(
            Intent(
                activity,
                ShowEmailActivity::class.java
            ).apply {
                putExtra("emailId", it)
            }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewState(it)
                }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.viewActions()
                .filterNotNull()
                .collect {
                    bindViewAction(it)
                }
        }
        lifecycleScope.launchWhenStarted {
            emailAdapter.refresh()
        }
        emailAdapter.addLoadStateListener { loadState ->
            lifecycleScope.launchWhenStarted {
                refreshEmailsSwipeRefreshLayout.isRefreshing =
                    loadState.refresh is LoadState.Loading
                if (loadState.refresh is LoadState.Error) {
                    Timber.e((loadState.refresh as LoadState.Error).error)
                    if ((loadState.refresh as LoadState.Error).error is EmptyEmailOrPasswordException) {
                        CoroutineScope(Dispatchers.Main).launch {
                            Timber.e("error")
                            if (findNavController().currentDestination?.id == R.id.emailClientStartFragment) {
                                findNavController().navigate(R.id.toEmptyLoginOrPasswordFragment)
                            }
                        }
                    }
                }
                Timber.d("loadState.prepend = ${loadState.prepend}, scrollState = ${emailsRecyclerView.scrollState}")
                if (emailsRecyclerView.scrollState == SCROLL_STATE_IDLE) {
                    if (loadState.prepend is LoadState.NotLoading && loadState.prepend.endOfPaginationReached) {
                        if (
                            (emailsRecyclerView.layoutManager as? LinearLayoutManager)
                                ?.findFirstCompletelyVisibleItemPosition() == 0
                        ) {

                            Timber.d("scroll to top")
                            emailsRecyclerView.scrollToPosition(0)
                        } else {
                            Timber.d(
                                "pos = ${
                                    (emailsRecyclerView.layoutManager as? LinearLayoutManager)
                                        ?.findFirstCompletelyVisibleItemPosition()
                                }"
                            )
                        }
                    }
                } else {
                    Timber.d("emailsRecyclerView scroll is not idle")
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        emailsRecyclerView.apply {
            adapter = emailAdapter.withLoadStateFooter(
                EmailLoadStateAdapter(emailAdapter::retry)
            )
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
        refreshEmailsSwipeRefreshLayout.setOnRefreshListener {
            emailAdapter.refresh()
        }
    }

    private fun bindViewState(viewState: EmailViewState) {
        when (viewState.fetchStatus) {
            is FetchStatus.Success -> {
                lifecycleScope.launchWhenCreated {
                    emailAdapter.submitData(viewState.mails)
                }
            }
        }
    }

    private fun bindViewAction(action: EmailAction) {
        when (action) {
            is EmailAction.ShowSnackbar -> {

            }
        }
    }

}