package com.setnameinc.voenmeh.email.main.ui

data class EmailItem(
    val id: Int,
    val sender: String,
    val subject: String,
    val plainText: String,
    val htmlText: String,
    var shortText: String = "",
    val sentTime: Long,
    val hasAttachment: Boolean,
    var isRead: Boolean,
    var senderIconLetter: Char = ' ',
    var senderIconColor: String = ""
)