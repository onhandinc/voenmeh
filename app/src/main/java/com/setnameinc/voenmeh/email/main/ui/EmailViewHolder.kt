package com.setnameinc.voenmeh.email.main.ui

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import kotlinx.android.synthetic.main.item_email.view.*
import java.text.SimpleDateFormat
import java.util.*

class EmailViewHolder(
    private val view: View,
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.ViewHolder(view), View.OnClickListener {

    init {
        view.setDebounceClickListener(clickListener = this)
    }

    @SuppressLint("Range")
    fun bind(email: EmailItem) {
        view.apply {
            if (email.isRead) {
                ResourcesCompat.getFont(context, R.font.lato).let {
                    subjectTextView.typeface = it
                    senderTextView.typeface = it
                    dateTextView.typeface = it
                }
            } else {
                ResourcesCompat.getFont(context, R.font.lato_bold).let {
                    subjectTextView.typeface = it
                    dateTextView.typeface = it
                }
                senderTextView.typeface = ResourcesCompat.getFont(context, R.font.lato_black)
            }
            subjectTextView.text = email.subject
            textTextView.text = email.shortText
            senderTextView.text = email.sender
            dateTextView.text = SimpleDateFormat(
                "d MMM",
                Locale("ru")
            ).format(Date(email.sentTime))
            senderIconTextView.apply {
                text = email.senderIconLetter.toString()
                backgroundTintList = ColorStateList.valueOf(Color.parseColor(email.senderIconColor))
            }
            if (email.hasAttachment) {
                hasAttachmentsImageView.visibility = View.VISIBLE
            } else {
                hasAttachmentsImageView.visibility = View.GONE
            }
        }
    }

    override fun onClick(v: View?) {
        clickListener(absoluteAdapterPosition)
    }

}