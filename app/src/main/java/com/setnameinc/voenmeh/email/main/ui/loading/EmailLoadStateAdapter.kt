package com.setnameinc.voenmeh.email.main.ui.loading

import android.view.ViewGroup
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter

class EmailLoadStateAdapter(
    private val retry: () -> Unit
) : LoadStateAdapter<EmailLoadStateViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ) = EmailLoadStateViewHolder(parent, retry)

    override fun onBindViewHolder(
        holder: EmailLoadStateViewHolder,
        loadState: LoadState
    ) = holder.bind(loadState)
}