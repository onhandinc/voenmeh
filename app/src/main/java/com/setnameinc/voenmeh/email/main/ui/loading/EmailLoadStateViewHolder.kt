package com.setnameinc.voenmeh.email.main.ui.loading

import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.databinding.ItemLoadStateBinding
import com.setnameinc.voenmeh.tools.utils.inflateHolder
import timber.log.Timber

class EmailLoadStateViewHolder(
    parent: ViewGroup,
    retry: () -> Unit
) : RecyclerView.ViewHolder(
    parent.inflateHolder(R.layout.item_load_state)
) {

    private val binding: ItemLoadStateBinding by viewBinding()

    fun bind(loadState: LoadState) {
        Timber.d("loadState = $loadState")
        if (loadState is LoadState.Error) {
            //errorMsg.text = loadState.error.localizedMessage
        }

        //binding.progressBar.isVisible = loadState is LoadState.Loading
        /*retry.isVisible = loadState is LoadState.Error
        errorMsg.isVisible = loadState is LoadState.Error*/
    }
}