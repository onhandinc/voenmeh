package com.setnameinc.voenmeh.email.main.viewmodel

import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.setnameinc.voenmeh.email.main.api.EmailApi
import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.email.main.data.RemoteKeysRepository
import com.setnameinc.voenmeh.email.main.domain.EmailConverter
import com.setnameinc.voenmeh.email.main.domain.EmailRemoteMediator
import com.setnameinc.voenmeh.email.main.repository.EmailRepository
import com.setnameinc.voenmeh.email.main.viewmodel.model.EmailAction
import com.setnameinc.voenmeh.email.main.viewmodel.model.EmailEvent
import com.setnameinc.voenmeh.email.main.viewmodel.model.EmailViewState
import com.setnameinc.voenmeh.email.main.viewmodel.model.FetchStatus
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@ExperimentalPagingApi
class EmailViewModel(
    private val emailRepository: EmailRepository,
    private val emailApi: EmailApi,
    private val remoteKeysRepository: RemoteKeysRepository
) : BaseFlowViewModel<EmailViewState, EmailAction, EmailEvent>() {

    init {
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                Pager(
                    config = PagingConfig(pageSize = 10, enablePlaceholders = false),
                    remoteMediator = EmailRemoteMediator(
                        emailRepository = emailRepository,
                        emailApi = emailApi,
                        remoteKeysRepository = remoteKeysRepository
                    ),
                    pagingSourceFactory = { emailRepository.getEmails() },
                    initialKey = null
                )
                    .flow
                    .map { pagingData ->
                        pagingData.map { EmailConverter.convertFromDatabase(it) }
                    }
                    .cachedIn(viewModelScope)
                    .collectLatest {
                        viewState = EmailViewState(FetchStatus.Success, it)
                    }
            }
        }
    }

    override suspend fun obtainInternalEvent(viewEvent: EmailEvent) {
        when (viewEvent) {
            EmailEvent.ShowEmails -> {

            }
        }
    }

}