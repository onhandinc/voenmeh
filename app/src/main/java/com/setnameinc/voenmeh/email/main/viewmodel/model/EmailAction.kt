package com.setnameinc.voenmeh.email.main.viewmodel.model

sealed class EmailAction {
    data class ShowSnackbar(val message: String) : EmailAction()
    data class OpenEmail(val id: Int) : EmailAction()
}