package com.setnameinc.voenmeh.email.main.viewmodel.model

sealed class EmailEvent {
    object ShowEmails : EmailEvent()
}