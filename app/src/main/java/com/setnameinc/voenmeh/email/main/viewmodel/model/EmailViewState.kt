package com.setnameinc.voenmeh.email.main.viewmodel.model

import androidx.paging.PagingData
import com.setnameinc.voenmeh.email.main.ui.EmailItem

data class EmailViewState(
    val fetchStatus: FetchStatus,
    val mails: PagingData<EmailItem>
)

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}