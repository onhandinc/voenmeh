package com.setnameinc.voenmeh.email.scheduler

class CheckNewEmailsContainer {

    companion object {
        val DELAY: Long = 10000
    }

    val newEmailsId: MutableList<Long> = mutableListOf()

    var lastInvokedTime: Long = Long.MAX_VALUE

}