package com.setnameinc.voenmeh.email.scheduler

import org.koin.dsl.module

object CheckNewEmailsModule {
    val module = module {
        single {
            CheckNewEmailsContainer()
        }
    }
}