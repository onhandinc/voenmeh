package com.setnameinc.voenmeh.email.scheduler

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import timber.log.Timber

class CheckNewEmailsRestartBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        Timber.d("service started")
        context?.startService(Intent(context, CheckNewEmailsService::class.java))
    }
}