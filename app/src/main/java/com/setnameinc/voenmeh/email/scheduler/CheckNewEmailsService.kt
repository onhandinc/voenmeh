package com.setnameinc.voenmeh.email.scheduler

import android.app.Notification
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.email.main.api.EmailApi
import com.setnameinc.voenmeh.email.scheduler.CheckNewEmails.CHANNEL_ID
import com.setnameinc.voenmeh.email.scheduler.CheckNewEmailsContainer.Companion.DELAY
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import timber.log.Timber
import java.util.*

class CheckNewEmailsService : Service() {

    private var timer: Timer? = null
    private var timerTask: TimerTask? = null

    private val emailApi: EmailApi by inject()

    private val checkNewEmailsContainer: CheckNewEmailsContainer by inject()

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)
        timer = Timer()
        initialiseTimerTask()
        timer?.schedule(timerTask, 0, DELAY)
        return START_STICKY
    }

    override fun onDestroy() {
        Timber.d("service stopped")
        super.onDestroy()
    }

    private fun initialiseTimerTask() {
        timerTask = object : TimerTask() {
            override fun run() {
                /*CoroutineScope(Dispatchers.Default).launch {
                    val currentTime: Long = Date().time
                    emailApi.hasNewMessages(
                        checkNewEmailsContainer.lastInvokedTime - 60000,
                        currentTime,
                        complete = {
                            if (it != null &&
                                !checkNewEmailsContainer.newEmailsId.contains(it)
                            ) {
                                Timber.d("received new message")
                                checkNewEmailsContainer.newEmailsId.add(it)
                                notifyNewMessage()
                            } else {
                                Timber.d("hasn't new messages")
                            }
                        },
                        error = {
                            Timber.d("failed :(, ${it}")
                        }
                    )
                    checkNewEmailsContainer.lastInvokedTime = currentTime
                }*/
            }
        }
    }

    private fun notifyNewMessage() {
        val builder: NotificationCompat.Builder = NotificationCompat.Builder(this, CHANNEL_ID)
        builder.setSmallIcon(R.mipmap.ic_launcher)
        builder.setContentTitle("Пришло новое письмо на почту")
        builder.setAutoCancel(true)
        val notification: Notification = builder.build()
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.notify(1, notification)
    }

    override fun onBind(p0: Intent?): IBinder? = Binder()

}