package com.setnameinc.voenmeh.email.show.di

import com.setnameinc.voenmeh.email.show.domain.ShowEmailInteractor
import com.setnameinc.voenmeh.email.show.ui.ShowEmailActivity
import com.setnameinc.voenmeh.email.show.viewmodel.ShowEmailViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object ShowEmailModule {
    val module = module {
        scope<ShowEmailActivity> {
            scoped {
                ShowEmailInteractor(
                    emailRepository = get(),
                    emailApi = get(),
                    context = get()
                )
            }
            viewModel {
                ShowEmailViewModel(
                    interactor = get()
                )
            }
        }
    }
}