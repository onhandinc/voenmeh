package com.setnameinc.voenmeh.email.show.domain

import java.lang.Exception

/**
 * file is caching now in the [com.setnameinc.voenmeh.show.domain.DownloadsProvider]
 */
class AttachmentAccessDeniedCachingException : Exception("attachment caching")