package com.setnameinc.voenmeh.email.show.domain

enum class AttachmentCacheState {
    CACHED,
    CACHING,
    NOT_CACHED,
    CACHE_FAILED
}