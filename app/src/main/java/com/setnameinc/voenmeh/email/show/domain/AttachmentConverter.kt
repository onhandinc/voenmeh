package com.setnameinc.voenmeh.email.show.domain

import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.email.main.domain.AttachmentEntity
import com.setnameinc.voenmeh.email.main.domain.AttachmentType
import com.setnameinc.voenmeh.email.show.ui.Attachment
import timber.log.Timber

object AttachmentConverter {

    fun List<AttachmentEntity>.convert(
        sender: String,
        messageId: Int
    ): List<Attachment> = this.map {
        Attachment(
            AttachmentData(
                it.name,
                it.extension,
                sender,
                messageId
            ),
            getImageId(it.type)
        )
    }

    private fun getImageId(attachmentType: AttachmentType): Int = when (attachmentType) {
        AttachmentType.PDF -> R.drawable.ic_file_pdf
        AttachmentType.DOCX -> R.drawable.ic_file_docx
        AttachmentType.DOC -> R.drawable.ic_file_docx
        AttachmentType.XLSX -> R.drawable.ic_file_xlsx
        else -> R.drawable.ic_file_unknown
    }.also {
        Timber.d("attachmentType converted to ${attachmentType}")
    }

}