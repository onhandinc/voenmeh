package com.setnameinc.voenmeh.email.show.domain

data class AttachmentData(
    val name: String,
    val extension: String,
    val sender: String,
    val messageId: Int,
    var cacheState: AttachmentCacheState = AttachmentCacheState.NOT_CACHED
) {

    val filePath = "$name.$extension"

    val path: String = "${sender.hashCode()}_${messageId}_${name}.$extension"
}