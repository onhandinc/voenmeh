package com.setnameinc.voenmeh.email.show.domain

import android.content.Context
import android.net.Uri
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import java.io.InputStream
import kotlin.coroutines.CoroutineContext

class DownloadManager(
    private val context: Context
) : CoroutineScope {

    private val job: Job = Job()

    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    val viewActions: MutableSharedFlow<DownloadManagerAction> = MutableSharedFlow()

    private val downloadsProvider: DownloadsProvider = DownloadsProvider(context)

    init {
        launch {
            downloadsProvider.viewActions
                .filterNotNull()
                .collect { action ->
                    when (action) {
                        is DownloadsProviderAction.FileCached -> {
                            downloadsPathList.remove(action.path)
                            viewActions.emit(
                                DownloadManagerAction.AttachmentCached(action.path)
                            )
                        }
                    }
                }
        }
    }

    private val downloadsPathList: MutableList<String> = mutableListOf()

    fun buildUri(path: String): Uri = downloadsProvider.buildUri(path)

    fun cacheAttachment(path: String, inputStream: InputStream) {
        downloadsPathList.add(path)
        downloadsProvider.cacheAttachment(path, inputStream)
    }

    fun attachmentCacheState(
        attachmentPath: String
    ): AttachmentCacheState = when {
        downloadsPathList.contains(attachmentPath) -> {
            AttachmentCacheState.CACHING
        }
        downloadsProvider.isAttachmentFileExists(attachmentPath) -> {
            AttachmentCacheState.CACHED
        }
        else -> {
            AttachmentCacheState.NOT_CACHED
        }
    }

}