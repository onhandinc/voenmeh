package com.setnameinc.voenmeh.email.show.domain

sealed class DownloadManagerAction {
    data class AttachmentCached(val path: String) : DownloadManagerAction()
}