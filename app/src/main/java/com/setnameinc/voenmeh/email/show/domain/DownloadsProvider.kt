package com.setnameinc.voenmeh.email.show.domain

import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.Environment
import androidx.core.content.FileProvider
import com.setnameinc.voenmeh.BuildConfig
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import kotlin.coroutines.CoroutineContext

class DownloadsProvider(
    private val context: Context
) : CoroutineScope {

    private val job: Job = Job()

    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    val viewActions: MutableSharedFlow<DownloadsProviderAction> = MutableSharedFlow()

    private val downloadDirectory =
        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)

    fun cacheAttachment(
        attachmentPath: String,
        sourceInputStream: InputStream
    ) {
        downloadDirectory.mkdir()
        val destinationFile = File(downloadDirectory, attachmentPath)
        if (destinationFile.exists()) {
            launch {
                viewActions.emit(DownloadsProviderAction.FileCached(attachmentPath))
            }
            //file already cached
            return
        }
        destinationFile.createNewFile()
        launch {
            copyFile(sourceInputStream, destinationFile, attachmentPath)
        }
    }

    fun buildUri(path: String): Uri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        FileProvider.getUriForFile(
            context,
            BuildConfig.APPLICATION_ID + ".provider",
            File(downloadDirectory, path)
        )
    } else {
        Uri.fromFile(File(downloadDirectory, path))
    }

    fun isAttachmentFileExists(
        attachmentPath: String
    ): Boolean = File(downloadDirectory, attachmentPath).exists()

    private fun copyFile(
        sourceFile: InputStream,
        destinationFile: File,
        attachmentPath: String
    ) {
        val out = FileOutputStream(destinationFile)
        val buf = ByteArray(4096)
        var len: Int
        var totalLen = 0
        while (sourceFile.read(buf).also { len = it } > 0) {
            Thread.yield()
            out.write(buf, 0, len)
            totalLen += len
        }
        out.fd.sync()
        out.close()
        launch {
            viewActions.emit(DownloadsProviderAction.FileCached(attachmentPath))
        }
    }

}