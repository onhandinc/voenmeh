package com.setnameinc.voenmeh.email.show.domain

sealed class DownloadsProviderAction {
    data class FileCached(val path: String) : DownloadsProviderAction()
}