package com.setnameinc.voenmeh.email.show.domain

import com.setnameinc.voenmeh.email.main.domain.AttachmentListEntity
import com.setnameinc.voenmeh.email.main.repository.EmailAttachment
import com.setnameinc.voenmeh.email.main.repository.EmailEntity
import com.setnameinc.voenmeh.email.show.domain.AttachmentConverter.convert
import com.setnameinc.voenmeh.email.show.ui.Attachment
import com.setnameinc.voenmeh.email.show.ui.EmailItem
import kotlinx.serialization.json.Json

object ShowEmailConverter {

    fun convertFromDatabase(emailEntity: EmailEntity): EmailItem = EmailItem(
        id = emailEntity.id,
        sender = emailEntity.sender,
        subject = emailEntity.subject,
        plainText = emailEntity.plainText,
        htmlText = emailEntity.htmlText,
        sentTime = emailEntity.sentTime,
        hasAttachment = emailEntity.hasAttachment,
        isRead = emailEntity.isRead,
        senderIconLetter = emailEntity.senderIconLetter.first(),
        senderIconColor = emailEntity.senderIconColor
    )

    fun convertAttachmentsFromDatabase(emailAttachment: EmailAttachment): List<Attachment> =
        Json.decodeFromString(
            AttachmentListEntity.serializer(),
            emailAttachment.attachmentNameJson
        ).items.convert(emailAttachment.sender, emailAttachment.id)

}