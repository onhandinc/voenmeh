package com.setnameinc.voenmeh.email.show.domain

import android.content.Context
import android.net.Uri
import com.setnameinc.voenmeh.email.main.api.EmailApi
import com.setnameinc.voenmeh.email.main.repository.EmailEntityReadUpdateEntity
import com.setnameinc.voenmeh.email.main.repository.EmailRepository
import com.setnameinc.voenmeh.email.show.ui.Attachment
import com.setnameinc.voenmeh.email.show.ui.EmailItem
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import timber.log.Timber
import java.lang.Exception
import kotlin.coroutines.CoroutineContext

class ShowEmailInteractor(
    private val emailApi: EmailApi,
    private val emailRepository: EmailRepository,
    private val context: Context
) : CoroutineScope {

    private val job: Job = Job()

    override val coroutineContext: CoroutineContext = Dispatchers.IO + job

    val viewActions: MutableSharedFlow<ShowEmailInteractorAction> = MutableSharedFlow()

    private val downloadsManager: DownloadManager = DownloadManager(context)

    init {
        launch {
            downloadsManager.viewActions
                .collect { action ->
                    when (action) {
                        is DownloadManagerAction.AttachmentCached -> {
                            viewActions.emit(
                                ShowEmailInteractorAction.AttachmentCached(action.path)
                            )
                        }
                    }
                }
        }
    }

    suspend fun cacheAttachment(
        attachmentData: AttachmentData,
        complete: (Any) -> Unit,
        error: (Throwable) -> Unit
    ) {
        complete(
            emailApi.attachmentInputStream(
                attachmentData.messageId,
                attachmentData.filePath,
                complete = {
                    downloadsManager.cacheAttachment(
                        attachmentData.path,
                        it
                    )
                },
                error = error
            )
        )
    }

    suspend fun getAttachmentUri(
        attachmentData: AttachmentData,
        complete: (Uri) -> Unit,
        error: (Throwable) -> Unit
    ) {
        when (downloadsManager.attachmentCacheState(attachmentData.path)) {
            AttachmentCacheState.CACHED -> {
                Timber.d("file cached")
                complete(downloadsManager.buildUri(attachmentData.path))
            }
            else -> {
                error(AttachmentAccessDeniedCachingException())
            }
        }
    }

    suspend fun getAttachments(
        id: Int,
        complete: (List<Attachment>) -> Unit,
        error: (Throwable) -> Unit
    ) {
        try {
            complete(
                ShowEmailConverter.convertAttachmentsFromDatabase(
                    emailRepository.getEmailAttachmentsById(id)
                ).onEach {
                    it.data.cacheState = downloadsManager.attachmentCacheState(it.data.path)
                    Timber.d("attachment ${it.data.name} state = ${it.data.cacheState}")
                }
            )
        } catch (e: Exception) {
            error(e)
        }
    }

    suspend fun getEmail(
        id: Int,
        complete: suspend (EmailItem) -> Unit,
        error: (Throwable) -> Unit
    ) {
        try {
            complete(
                ShowEmailConverter.convertFromDatabase(emailRepository.getEmailById(id))
            )
        } catch (e: Exception) {
            error(e)
        }
    }

    suspend fun messageRead(id: Int) {
        emailRepository.updateEmail(EmailEntityReadUpdateEntity(id))
        emailApi.messageRead(id)
    }

    fun closeFolder() {
        /*launch {
            emailApi.closeFolder()
        }*/
    }

}