package com.setnameinc.voenmeh.email.show.domain

sealed class ShowEmailInteractorAction {
    data class AttachmentCached(val path: String) : ShowEmailInteractorAction()
}