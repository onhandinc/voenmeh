package com.setnameinc.voenmeh.email.show.ui

import androidx.annotation.DrawableRes
import com.setnameinc.voenmeh.email.show.domain.AttachmentData

data class Attachment(
    val data: AttachmentData,
    @DrawableRes val imageId: Int
)