package com.setnameinc.voenmeh.email.show.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.email.show.domain.AttachmentCacheState
import com.setnameinc.voenmeh.email.show.domain.AttachmentData
import timber.log.Timber

class AttachmentAdapter(
    private val clickListener: (attachment: AttachmentData) -> Unit
) : ListAdapter<Attachment, AttachmentViewHolder>(
    object : DiffUtil.ItemCallback<Attachment>() {
        override fun areItemsTheSame(
            oldItem: Attachment,
            newItem: Attachment
        ): Boolean = oldItem.data.name == newItem.data.name

        override fun areContentsTheSame(
            oldItem: Attachment,
            newItem: Attachment
        ): Boolean = oldItem == newItem

    }
) {

    private val PAYLOAD_CACHED: String = "PAYLOAD_CACHED"
    private val PAYLOAD_CACHING: String = "PAYLOAD_CACHING"
    private val PAYLOAD_CACHE_FAILED: String = "PAYLOAD_CACHE_FAILED"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AttachmentViewHolder =
        AttachmentViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_email_attachment, parent, false)
        ) {
            clickListener(getItem(it).data)
        }

    override fun onBindViewHolder(holder: AttachmentViewHolder, position: Int) {
        Timber.d("onBindViewHolder, $position")
        holder.bind(getItem(position))
    }

    override fun onBindViewHolder(
        holder: AttachmentViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        Timber.d("onBindViewHolder with payloads")
        if (payloads.isEmpty()) {
            Timber.d("pos $position, payloads is empty")
            super.onBindViewHolder(holder, position, payloads)
        } else {
            payloads.filterIsInstance<String>()
                .forEach {
                    Timber.d("pos $position payload = $it")
                    when (it) {
                        PAYLOAD_CACHED -> {
                            holder.bindCached(getItem(position).imageId)
                        }
                        PAYLOAD_CACHE_FAILED -> {
                            holder.bindCacheError()
                        }
                        PAYLOAD_CACHING -> {
                            holder.bindCaching()
                        }
                    }
                }
        }
    }

    fun itemCaching(path: String) {
        val position = currentList.indexOfFirst { it.data.path == path }
        currentList.getOrNull(position)?.data?.cacheState = AttachmentCacheState.CACHING
        notifyItemChanged(position, PAYLOAD_CACHING)
    }

    fun itemCached(path: String) {
        val position = currentList.indexOfFirst { it.data.path == path }
        currentList.getOrNull(position)?.data?.cacheState = AttachmentCacheState.CACHED
        notifyItemChanged(position, PAYLOAD_CACHED)
    }

    fun itemCacheFailed(path: String) {
        val position = currentList.indexOfFirst { it.data.path == path }
        currentList.getOrNull(position)?.data?.cacheState = AttachmentCacheState.CACHE_FAILED
        notifyItemChanged(position, PAYLOAD_CACHE_FAILED)
    }

}