package com.setnameinc.voenmeh.email.show.ui

import android.view.View
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.email.show.domain.AttachmentCacheState
import kotlinx.android.synthetic.main.item_email_attachment.view.*
import timber.log.Timber

class AttachmentViewHolder(
    private val view: View,
    private val onClickListener: (position: Int) -> Unit
) : RecyclerView.ViewHolder(view), View.OnClickListener {

    init {
        view.setDebounceClickListener(clickListener = this)
    }

    override fun onClick(p0: View?) {
        onClickListener(absoluteAdapterPosition)
    }

    private fun bindNotCached() {
        view.apply {
            cacheProgressBar.visibility = View.GONE
            iconFileTypeImageView.visibility = View.INVISIBLE
            downloadImageView.visibility = View.VISIBLE
        }
    }

    fun bindCached(@DrawableRes imageId: Int) {
        view.apply {
            cacheProgressBar.visibility = View.GONE
            downloadImageView.visibility = View.GONE
            iconFileTypeImageView.visibility = View.VISIBLE
            iconFileTypeImageView.setBackgroundResource(imageId)
        }
    }

    fun bindCaching() {
        view.apply {
            cacheProgressBar.visibility = View.VISIBLE
            downloadImageView.visibility = View.INVISIBLE
            iconFileTypeImageView.visibility = View.INVISIBLE
        }
    }

    fun bindCacheError() {
        view.apply {
            cacheProgressBar.visibility = View.GONE
            iconFileTypeImageView.visibility = View.INVISIBLE
            downloadImageView.visibility = View.VISIBLE
        }
    }

    fun bind(attachment: Attachment) {
        Timber.d("bind $attachment")
        view.apply {
            nameTextView.text = attachment.data.name
            when (attachment.data.cacheState) {
                AttachmentCacheState.CACHED -> {
                    bindCached(attachment.imageId)
                }
                AttachmentCacheState.CACHING -> {
                    bindCaching()
                }
                AttachmentCacheState.NOT_CACHED -> {
                    bindNotCached()
                }
                AttachmentCacheState.CACHE_FAILED -> {
                    bindCacheError()
                }
            }
        }
    }

}