package com.setnameinc.voenmeh.email.show.ui

data class EmailItem(
    val id: Int,
    val sender: String,
    val subject: String,
    val plainText: String,
    val htmlText: String,
    val sentTime: Long,
    val hasAttachment: Boolean,
    var isRead: Boolean,
    var senderIconLetter: Char = ' ',
    var senderIconColor: String = ""
)