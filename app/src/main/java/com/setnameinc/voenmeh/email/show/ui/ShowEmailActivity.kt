package com.setnameinc.voenmeh.email.show.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.email.show.viewmodel.ShowEmailViewModel
import com.setnameinc.voenmeh.email.show.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.email.show.viewmodel.model.ShowEmailAction
import com.setnameinc.voenmeh.email.show.viewmodel.model.ShowEmailEvent
import com.setnameinc.voenmeh.email.show.viewmodel.model.ShowEmailViewState
import com.setnameinc.voenmeh.tools.base.ScopedAppCompatActivity
import com.setnameinc.voenmeh.tools.utils.viewModel
import kotlinx.android.synthetic.main.activity_show_email.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.scope.activityScope
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class ShowEmailActivity : ScopedAppCompatActivity(R.layout.activity_show_email) {

    private val viewModel: ShowEmailViewModel by activityScope.viewModel(this)

    private val attachmentAdapter: AttachmentAdapter =
        AttachmentAdapter { attachment ->
            Timber.d("attachment = $attachment")
            if (isStoragePermissionGranted()) {
                viewModel.openAttachment(attachment)
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenCreated {
            viewModel.viewStates()
                .filterNotNull()
                .collect { bindViewState(it) }
        }
        lifecycleScope.launchWhenCreated {
            viewModel.viewActions()
                .filterNotNull()
                .collect { bindViewAction(it) }
        }

        intent.extras?.getInt("emailId")?.let {
            viewModel.obtainEvent(ShowEmailEvent.LoadEmail(it))
        }

        attachmentsRecyclerView.apply {
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            adapter = attachmentAdapter
        }

    }

    @SuppressLint("Range")
    private fun bindViewState(viewState: ShowEmailViewState) {
        when (viewState) {
            is ShowEmailViewState.LoadEmail -> {
                resolveLoadEmailViewState(viewState)
            }
            is ShowEmailViewState.OpenAttachment -> {
                resolveOpenAttachmentViewState(viewState)
            }
            is ShowEmailViewState.LoadAttachments -> {
                resolveLoadAttachmentsViewState(viewState)
            }
            is ShowEmailViewState.CacheAttachment -> {
                resolveCacheAttachmentViewState(viewState)
            }
        }
    }

    @SuppressLint("Range")
    private fun resolveLoadEmailViewState(viewState: ShowEmailViewState.LoadEmail) {
        when (viewState.fetchStatus) {
            is FetchStatus.Success -> {
                Timber.d("loaded ${viewState.email?.id}")
                viewState.email?.apply {
                    titleTextView.text = subject
                    senderIconTextView.text = senderIconLetter.toString()
                    senderIconTextView.backgroundTintList = ColorStateList.valueOf(
                        Color.parseColor(senderIconColor)
                    )
                    senderTextView.text = sender
                    dateTextView.text = SimpleDateFormat(
                        "HH:mm dd/MM/yyyy",
                        Locale("ru")
                    ).format(Date(sentTime))
                    if (hasAttachment) {
                        viewModel.obtainEvent(ShowEmailEvent.LoadAttachments(id))
                    }
                    if (htmlText.isNotEmpty()) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            textTextView.text = Html.fromHtml(
                                htmlText,
                                Html.FROM_HTML_MODE_LEGACY
                            )
                        } else {
                            textTextView.text = Html.fromHtml(
                                htmlText
                            )
                        }
                    } else {
                        textTextView.text = plainText
                    }
                }
            }
        }
    }

    private fun resolveOpenAttachmentViewState(viewState: ShowEmailViewState.OpenAttachment) {
        when (viewState.fetchStatus) {
            is FetchStatus.Success -> {
                val intent = Intent(Intent.ACTION_VIEW).apply {
                    addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    data = viewState.uri
                }
                startActivity(
                    Intent.createChooser(
                        intent,
                        "Открыть ${viewState.filePath}"
                    )
                )
            }
            is FetchStatus.Error -> {
                /*viewState.adapterPosition?.let {
                    attachmentAdapter.itemCacheFailed(it)
                }*/
            }
        }
    }

    private fun resolveLoadAttachmentsViewState(viewState: ShowEmailViewState.LoadAttachments) {
        when (viewState.fetchStatus) {
            is FetchStatus.Success -> {
                attachmentsLoadingProgressBar.visibility = View.GONE
                attachmentAdapter.submitList(viewState.attachments)
            }
            is FetchStatus.Loading -> {
                textSpace.visibility = View.VISIBLE
                attachmentsRecyclerView.visibility = View.VISIBLE
                attachmentsLoadingProgressBar.visibility = View.VISIBLE
            }
            is FetchStatus.Error -> {
                attachmentsLoadingProgressBar.visibility = View.GONE
                Timber.e("error happend")
            }
        }
    }

    private fun resolveCacheAttachmentViewState(viewState: ShowEmailViewState.CacheAttachment) {
        when (viewState.fetchStatus) {
            FetchStatus.Success -> {
                viewState.path?.let { attachmentAdapter.itemCached(it) }
            }
            FetchStatus.Loading -> {
                viewState.path?.let { attachmentAdapter.itemCaching(it) }
            }
            FetchStatus.Error -> {
                viewState.path?.let { attachmentAdapter.itemCacheFailed(it) }
            }
        }
    }

    private fun bindViewAction(action: ShowEmailAction) {
        when (action) {
            is ShowEmailAction.ShowSnackbar -> {
                Timber.e(action.message)
            }
            is ShowEmailAction.UpdateItem -> {
                attachmentAdapter.itemCached(action.path)
            }
            is ShowEmailAction.AttachmentCaching -> {
                attachmentAdapter.itemCaching(action.path)
            }
        }
    }

    private fun isStoragePermissionGranted(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                Timber.d("Permission is granted")
                true
            } else {
                Timber.d("Permission is revoked")
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    1
                )
                false
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            Timber.d("Permission is granted")
            true
        }
    }

}