package com.setnameinc.voenmeh.email.show.viewmodel

import androidx.lifecycle.viewModelScope
import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.email.show.domain.*
import com.setnameinc.voenmeh.email.show.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.email.show.viewmodel.model.ShowEmailAction
import com.setnameinc.voenmeh.email.show.viewmodel.model.ShowEmailEvent
import com.setnameinc.voenmeh.email.show.viewmodel.model.ShowEmailViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class ShowEmailViewModel(
    private val interactor: ShowEmailInteractor
) : BaseFlowViewModel<ShowEmailViewState, ShowEmailAction, ShowEmailEvent>() {

    init {
        viewModelScope.launch {
            interactor.viewActions
                .collect { action ->
                    when (action) {
                        is ShowEmailInteractorAction.AttachmentCached -> {
                            viewState = ShowEmailViewState.CacheAttachment(
                                FetchStatus.Success,
                                action.path
                            )
                        }
                    }
                }
        }
    }

    override suspend fun obtainInternalEvent(viewEvent: ShowEmailEvent) {
        viewModelScope.launch {
            when (viewEvent) {
                is ShowEmailEvent.LoadEmail -> {
                    viewState = ShowEmailViewState.LoadEmail(FetchStatus.Loading)
                    withContext(Dispatchers.IO) {
                        interactor.getEmail(
                            viewEvent.id,
                            complete = {
                                viewState = ShowEmailViewState.LoadEmail(
                                    FetchStatus.Success,
                                    it
                                )
                                withContext(Dispatchers.IO) {
                                    if (!it.isRead) {
                                        interactor.messageRead(it.id)
                                    }
                                }
                            },
                            error = {
                                viewState = ShowEmailViewState.LoadEmail(FetchStatus.Error)
                                viewAction = ShowEmailAction.ShowSnackbar("error happened")
                            }
                        )
                    }
                }
                is ShowEmailEvent.LoadAttachments -> {
                    viewState = ShowEmailViewState.LoadAttachments(FetchStatus.Loading)
                    withContext(Dispatchers.IO) {
                        interactor.getAttachments(
                            viewEvent.id,
                            complete = {
                                viewState = ShowEmailViewState.LoadAttachments(
                                    FetchStatus.Success,
                                    it
                                )
                            },
                            error = {
                                Timber.e(it)
                                viewState = ShowEmailViewState.LoadAttachments(FetchStatus.Error)
                            }
                        )
                    }

                }
                is ShowEmailEvent.CacheAttachment -> {
                    viewState = ShowEmailViewState.CacheAttachment(
                        FetchStatus.Loading,
                        viewEvent.attachmentData.path
                    )
                    withContext(Dispatchers.IO) {
                        interactor.cacheAttachment(
                            viewEvent.attachmentData,
                            complete = {},
                            error = {
                                Timber.e(it)
                                Timber.e("cache failed")
                                viewState = ShowEmailViewState.CacheAttachment(
                                    FetchStatus.Error,
                                    viewEvent.attachmentData.path
                                )
                            }
                        )
                    }
                }
                is ShowEmailEvent.OpenAttachment -> {
                    viewState = ShowEmailViewState.OpenAttachment(FetchStatus.Loading)
                    withContext(Dispatchers.IO) {
                        interactor.getAttachmentUri(
                            viewEvent.attachmentData,
                            complete = {
                                viewState = ShowEmailViewState.OpenAttachment(
                                    FetchStatus.Success,
                                    it,
                                    filePath = viewEvent.attachmentData.filePath
                                )
                            },
                            error = {
                                Timber.e(it)
                                when (it) {
                                    is AttachmentAccessDeniedCachingException -> {
                                        viewAction = ShowEmailAction.AttachmentCaching(
                                            viewEvent.attachmentData.path
                                        )
                                    }
                                    else -> {
                                        viewState =
                                            ShowEmailViewState.OpenAttachment(FetchStatus.Error)
                                    }
                                }
                            }
                        )
                    }
                }
            }
        }
    }

    fun openAttachment(attachmentData: AttachmentData) {
        when (attachmentData.cacheState) {
            AttachmentCacheState.CACHE_FAILED -> {
                obtainEvent(ShowEmailEvent.CacheAttachment(attachmentData))
            }
            AttachmentCacheState.NOT_CACHED -> {
                obtainEvent(ShowEmailEvent.CacheAttachment(attachmentData))
            }
            AttachmentCacheState.CACHED -> {
                obtainEvent(ShowEmailEvent.OpenAttachment(attachmentData))
            }
            AttachmentCacheState.CACHING -> {

            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        interactor.closeFolder()
    }

}