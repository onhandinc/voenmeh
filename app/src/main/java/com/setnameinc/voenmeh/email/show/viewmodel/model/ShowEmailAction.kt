package com.setnameinc.voenmeh.email.show.viewmodel.model

sealed class ShowEmailAction {
    data class ShowSnackbar(val message: String) : ShowEmailAction()
    data class UpdateItem(val path: String) : ShowEmailAction()
    data class AttachmentCaching(val path: String) : ShowEmailAction()
}