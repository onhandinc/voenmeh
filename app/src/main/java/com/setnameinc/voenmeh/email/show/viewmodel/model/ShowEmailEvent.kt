package com.setnameinc.voenmeh.email.show.viewmodel.model

import com.setnameinc.voenmeh.email.show.domain.AttachmentData

sealed class ShowEmailEvent {
    data class LoadEmail(val id: Int) : ShowEmailEvent()
    data class LoadAttachments(val id: Int) : ShowEmailEvent()
    data class OpenAttachment(
        val attachmentData: AttachmentData
    ) : ShowEmailEvent()

    data class CacheAttachment(
        val attachmentData: AttachmentData
    ) : ShowEmailEvent()
}