package com.setnameinc.voenmeh.email.show.viewmodel.model

import android.net.Uri
import com.setnameinc.voenmeh.email.show.ui.Attachment
import com.setnameinc.voenmeh.email.show.ui.EmailItem

sealed class ShowEmailViewState(open val fetchStatus: FetchStatus) {
    data class LoadEmail(
        override val fetchStatus: FetchStatus,
        val email: EmailItem? = null
    ) : ShowEmailViewState(fetchStatus)

    data class LoadAttachments(
        override val fetchStatus: FetchStatus,
        val attachments: List<Attachment> = listOf()
    ) : ShowEmailViewState(fetchStatus)

    data class OpenAttachment(
        override val fetchStatus: FetchStatus,
        val uri: Uri? = null,
        val filePath: String = "",
        val adapterPosition: Int? = null
    ) : ShowEmailViewState(fetchStatus)

    data class CacheAttachment(
        override val fetchStatus: FetchStatus,
        val path: String? = null
    ) : ShowEmailViewState(fetchStatus)
}


sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}