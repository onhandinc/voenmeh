package com.setnameinc.voenmeh.loginsignup.login.main.api

import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.tasks.await
import java.lang.Exception

class LoginApi(
    private val firebaseAuth: FirebaseAuth
) {

    suspend fun loginAsync(
        requestBody: LoginRequestBody,
        complete: suspend (AuthResult?) -> Unit,
        error: suspend (Throwable) -> Unit
    ) {
        try {
            complete(
                firebaseAuth.signInWithEmailAndPassword(
                    requestBody.email,
                    requestBody.password
                ).await()
            )
        } catch (e: Exception) {
            error(e)
        }
    }

}