package com.setnameinc.voenmeh.loginsignup.login.main.api

data class LoginRequestBody(
    val email: String,
    val password: String
)