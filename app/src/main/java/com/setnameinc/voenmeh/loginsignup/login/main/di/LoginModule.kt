package com.setnameinc.voenmeh.loginsignup.login.main.di

import com.setnameinc.voenmeh.loginsignup.login.main.api.LoginApi
import com.setnameinc.voenmeh.loginsignup.login.main.domain.LoginInteractor
import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object LoginModule {
    val module = module {
        single { LoginApi(firebaseAuth = get()) }
        single {
            LoginInteractor(
                loginApi = get(),
                userInteractor = get(),
                userBlockedInteractor = get(),
                emailInteractor = get()
            )
        }
        viewModel {
            LoginViewModel(
                loginInteractor = get()
            )
        }
    }
}