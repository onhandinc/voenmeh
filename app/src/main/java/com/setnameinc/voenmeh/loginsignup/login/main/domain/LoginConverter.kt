package com.setnameinc.voenmeh.loginsignup.login.main.domain

import com.setnameinc.voenmeh.loginsignup.login.main.api.LoginRequestBody

object LoginConverter {
    fun convertToNetwork(
        email: String,
        password: String
    ): LoginRequestBody = LoginRequestBody(
        email = email,
        password = password
    )
}