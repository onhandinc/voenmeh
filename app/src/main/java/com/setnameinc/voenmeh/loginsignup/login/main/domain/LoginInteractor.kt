package com.setnameinc.voenmeh.loginsignup.login.main.domain

import android.text.TextUtils
import android.util.Patterns
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.setnameinc.voenmeh.email.main.domain.EmailInteractor
import com.setnameinc.voenmeh.loginsignup.login.main.api.LoginApi
import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.LoginViewResponseFailed.*
import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.LoginViewResponseFailed
import com.setnameinc.voenmeh.user.common.domain.UserInteractor

import com.setnameinc.voenmeh.user.blocked.domain.UserBlockedInteractor
import kotlinx.coroutines.delay
import timber.log.Timber
import java.lang.NullPointerException

class LoginInteractor(
    private val emailInteractor: EmailInteractor,
    private val loginApi: LoginApi,
    private val userInteractor: UserInteractor,
    private val userBlockedInteractor: UserBlockedInteractor
) {

    suspend fun login(
        email: String,
        password: String,
        complete: suspend () -> Unit,
        error: suspend (LoginViewResponseFailed) -> Unit
    ) {
        delay(500)
        loginApi.loginAsync(
            LoginConverter.convertToNetwork(
                email = email,
                password = password
            ),
            complete = {
                emailInteractor.updateEmailClientData(email, password, {}, { Timber.e(it) })
                userInteractor.updateUserData()
                userBlockedInteractor.isUserBlocked(
                    complete = {
                        if (it) {
                            error(USER_BLOCKED)
                        } else {
                            complete()
                        }
                    },
                    error = {
                        if (it is NullPointerException) {
                            error(UNKNOWN)
                        }
                    }
                )
            },
            error = {
                when (it) {
                    is FirebaseAuthInvalidCredentialsException -> {
                        error(PASSWORD_INCORRECT)
                    }
                    is FirebaseAuthInvalidUserException -> {
                        error(EMAIL_INCORRECT)
                    }
                    else -> {
                        Timber.e(it)
                        error(UNKNOWN)
                    }
                }
            }
        )
    }


    suspend fun isEmailInvalid(email: String): Boolean =
        TextUtils.isEmpty(email)
                || !Patterns.EMAIL_ADDRESS.matcher(email).matches()

    suspend fun isPasswordInvalid(password: String): Boolean =
        password.isEmpty() || password.length < 6

}