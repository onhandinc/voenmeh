package com.setnameinc.voenmeh.loginsignup.login.main.domain

sealed class LoginModelResponse<T> {
    data class Success<T>(val data: T) : LoginModelResponse<T>()
    data class Failed<T>(val reason: LoginModelResponseFailed) :
        LoginModelResponse<T>()

    data class Error<T>(val throwable: Throwable) : LoginModelResponse<T>()
}