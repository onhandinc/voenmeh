package com.setnameinc.voenmeh.loginsignup.login.main.domain

enum class LoginModelResponseFailed {
    EMAIL_NOT_FOUND,
    PASSWORD_INCORRECT
}