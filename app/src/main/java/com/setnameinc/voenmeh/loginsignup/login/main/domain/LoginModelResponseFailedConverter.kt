package com.setnameinc.voenmeh.loginsignup.login.main.domain

import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.LoginViewResponseFailed

object LoginModelResponseFailedConverter {
    fun convert(reason: LoginModelResponseFailed): LoginViewResponseFailed = when (reason) {
        LoginModelResponseFailed.EMAIL_NOT_FOUND -> {
            LoginViewResponseFailed.EMAIL_INCORRECT
        }
        LoginModelResponseFailed.PASSWORD_INCORRECT -> {
            LoginViewResponseFailed.PASSWORD_INCORRECT
        }
    }
}