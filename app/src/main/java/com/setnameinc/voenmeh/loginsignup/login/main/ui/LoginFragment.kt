package com.setnameinc.voenmeh.loginsignup.login.main.ui

import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.StyleSpan
import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.setnameinc.voenmeh.main.ui.MainActivity
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.LoginViewModel
import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.model.LoginAction
import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.model.LoginEvent
import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.model.LoginViewState
import com.setnameinc.voenmeh.user.blocked.ui.UserBlockedActivity
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class LoginFragment : Fragment(R.layout.fragment_login) {

    private val LINK_TO_VK: String = "https://vk.com/appvoenmeh"

    private val viewModel: LoginViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewState(it)
                }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.viewActions()
                .filterNotNull()
                .collect {
                    bindViewAction(it)
                }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        signUpTextView.apply {
            setDebounceClickListener {
                findNavController().navigate(R.id.toSelectSignUp)
            }
            text = getColoredText(R.string.login_has_not_account, R.color.tundora)
            append(" ")
            append(getColoredBoldText(R.string.login_sign_up, R.color.dodger_blue))
        }
        loginButton.setDebounceClickListener(
            debounceTimeMillis = 2000
        ) {
            Timber.d("clicked")
            viewModel.obtainEvent(LoginEvent.Login)
        }
        emailEditText.doAfterTextChanged {
            it?.toString()?.let { email ->
                viewModel.obtainEvent(LoginEvent.EditEmail(email))
            }
        }
        passwordEditText.doAfterTextChanged {
            it?.toString()?.let { password ->
                viewModel.obtainEvent(LoginEvent.EditPassword(password))
            }
        }
        logoImageView.setDebounceClickListener(debounceTimeMillis = 2000) {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(LINK_TO_VK)
                )
            )
        }
    }

    private fun bindViewState(viewState: LoginViewState) {
        Timber.d("bindViewState, viewState = $viewState")
        when (viewState.fetchStatus) {
            FetchStatus.Loading -> {
                Timber.d("current destionation = ${findNavController().currentDestination}")
                findNavController().navigate(R.id.loginToLoginInProgress)
            }
            FetchStatus.Success -> {
                startActivity(Intent(activity, MainActivity::class.java))
                activity?.finish()
            }
            FetchStatus.Error -> {
                Timber.d("error")
                findNavController().popBackStack()
                //todo show error happened message
            }
        }
    }

    private fun bindViewAction(action: LoginAction) {
        when (action) {
            LoginAction.ShowErrorIncorrectEmail -> {
                if (findNavController().currentDestination?.id != R.id.loginFragment) {
                    findNavController().popBackStack(R.id.loginFragment, false)
                }
                emailEditText.error = resources.getString(R.string.login_incorrect_login)
            }
            LoginAction.ShowErrorIncorrectPassword -> {
                if (findNavController().currentDestination?.id != R.id.loginFragment) {
                    findNavController().popBackStack(R.id.loginFragment, false)
                }
                passwordEditText.error =
                    resources.getString(R.string.login_incorrect_password)
            }
            LoginAction.ShowErrorUserBlocked -> {
                startActivity(Intent(activity, UserBlockedActivity::class.java))
                activity?.finish()
            }
        }
    }

    private fun getColoredText(
        textId: Int,
        colorId: Int
    ) = SpannableString(
        resources.getString(textId)
    ).apply {
        setSpan(
            ForegroundColorSpan(resources.getColor(colorId)),
            0,
            length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }

    private fun getColoredBoldText(
        textId: Int,
        colorId: Int
    ) = SpannableString(
        resources.getString(textId)
    ).apply {
        setSpan(
            StyleSpan(Typeface.BOLD),
            0,
            length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        setSpan(
            ForegroundColorSpan(resources.getColor(colorId)),
            0,
            length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }

}