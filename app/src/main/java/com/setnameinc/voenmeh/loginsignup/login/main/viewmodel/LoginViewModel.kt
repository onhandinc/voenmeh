package com.setnameinc.voenmeh.loginsignup.login.main.viewmodel

import androidx.lifecycle.viewModelScope
import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.loginsignup.login.main.domain.LoginInteractor
import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.LoginViewResponseFailed.*
import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.model.LoginAction
import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.model.LoginEvent
import com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.model.LoginViewState
import kotlinx.coroutines.*

class LoginViewModel(
    private val loginInteractor: LoginInteractor
) : BaseFlowViewModel<LoginViewState, LoginAction, LoginEvent>() {

    private var email: String = ""
    private var password: String = ""

    override suspend fun obtainInternalEvent(viewEvent: LoginEvent) {
        when (viewEvent) {
            is LoginEvent.EditEmail -> {
                email = viewEvent.value
            }
            is LoginEvent.EditPassword -> {
                password = viewEvent.value
            }
            LoginEvent.Login -> {
                viewModelScope.launch {
                    when {
                        loginInteractor.isEmailInvalid(email) -> {
                            viewAction = LoginAction.ShowErrorIncorrectEmail
                        }
                        loginInteractor.isPasswordInvalid(password) -> {
                            viewAction = LoginAction.ShowErrorIncorrectPassword
                        }
                        else -> {
                            viewState = LoginViewState(FetchStatus.Loading)
                            withContext(Dispatchers.IO) {
                                loginInteractor.login(
                                    email = email,
                                    password = password,
                                    complete = {
                                        viewState = LoginViewState(FetchStatus.Success)
                                    },
                                    error = {
                                        when (it) {
                                            EMAIL_INCORRECT -> {
                                                viewAction = LoginAction.ShowErrorIncorrectEmail
                                            }
                                            PASSWORD_INCORRECT -> {
                                                viewAction = LoginAction.ShowErrorIncorrectPassword
                                            }
                                            USER_BLOCKED -> {
                                                viewAction = LoginAction.ShowErrorUserBlocked
                                            }
                                            UNKNOWN -> {
                                                viewState = LoginViewState(FetchStatus.Error)
                                            }
                                        }
                                    }
                                )
                            }
                        }
                    }
                }
            }
        }
    }

}