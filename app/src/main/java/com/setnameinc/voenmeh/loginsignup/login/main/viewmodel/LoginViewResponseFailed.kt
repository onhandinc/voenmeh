package com.setnameinc.voenmeh.loginsignup.login.main.viewmodel

enum class LoginViewResponseFailed {
    EMAIL_INCORRECT,
    PASSWORD_INCORRECT,
    USER_BLOCKED,
    UNKNOWN
}