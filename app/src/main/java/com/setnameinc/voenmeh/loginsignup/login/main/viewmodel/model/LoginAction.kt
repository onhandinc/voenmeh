package com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.model

sealed class LoginAction {
    object ShowErrorIncorrectEmail : LoginAction()
    object ShowErrorIncorrectPassword : LoginAction()
    object ShowErrorUserBlocked : LoginAction()
}