package com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.model

sealed class LoginEvent {
    data class EditEmail(val value: String) : LoginEvent()
    data class EditPassword(val value: String) : LoginEvent()
    object Login : LoginEvent()
}