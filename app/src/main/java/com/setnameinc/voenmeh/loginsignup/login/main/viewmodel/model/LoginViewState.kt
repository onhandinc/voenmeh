package com.setnameinc.voenmeh.loginsignup.login.main.viewmodel.model

data class LoginViewState(
    val fetchStatus: FetchStatus
)

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}