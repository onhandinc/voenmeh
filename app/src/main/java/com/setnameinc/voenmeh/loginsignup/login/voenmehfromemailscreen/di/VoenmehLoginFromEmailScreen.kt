package com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.di

import com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel.VoenmehLoginFromEmailScreenViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object VoenmehLoginFromEmailScreen {
    val module = module {
        viewModel {
            VoenmehLoginFromEmailScreenViewModel(
                voenmehMailInteractor = get()
            )
        }
    }
}