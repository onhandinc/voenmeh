package com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.ui

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel.VoenmehLoginFromEmailScreenViewModel
import com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel.model.VoenmehLoginFromEmailScreenAction
import com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel.model.VoenmehLoginFromEmailScreenEvent
import com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel.model.VoenmehMailLoginFromEmailScreenViewState

import kotlinx.android.synthetic.main.fragment_login_voenmeh_mail.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class VoenmehMailLoginFromEmailActivity : AppCompatActivity(R.layout.fragment_login_voenmeh_mail) {

    private val viewModel: VoenmehLoginFromEmailScreenViewModel by viewModel()

    private var loadingSnackbarList: MutableList<Snackbar?> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        emailEditText.doAfterTextChanged {
            emailEditText.error = null
            it?.toString()?.let { email ->
                viewModel.obtainEvent(VoenmehLoginFromEmailScreenEvent.EditEmail(email))
            }
        }
        passwordEditText.doAfterTextChanged {
            it?.toString()?.let { password ->
                viewModel.obtainEvent(VoenmehLoginFromEmailScreenEvent.EditPassword(password))
            }
        }
        loginButton.setDebounceClickListener(debounceTimeMillis = 2000) {
            viewModel.obtainEvent(VoenmehLoginFromEmailScreenEvent.Login)
        }
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewState(it)
                }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.viewActions()
                .filterNotNull()
                .collect {
                    bindViewAction(it)
                }
        }
    }

    private fun bindViewAction(action: VoenmehLoginFromEmailScreenAction) {
        when (action) {
            VoenmehLoginFromEmailScreenAction.ShowErrorIncorrectEmail -> {
                emailEditText.error =
                    resources.getString(R.string.login_mail_voenmeh_incorrect_email)
                loadingSnackbarList.forEach {
                    it?.dismiss()
                }
                loadingSnackbarList.clear()
            }
        }
    }

    private fun bindViewState(viewState: VoenmehMailLoginFromEmailScreenViewState) {
        when (viewState.fetchStatus) {
            is FetchStatus.Loading -> {
                loadingSnackbarList.add(
                    createLoadingSnackbar()?.apply { show() }
                )
            }
            is FetchStatus.Success -> {
                CoroutineScope(Dispatchers.Main).launch {
                    setResult(RESULT_OK, Intent().putExtra("loggedIn", true))
                    delay(1000)
                    Timber.d("completely logged in")
                    this@VoenmehMailLoginFromEmailActivity.finish()
                }
            }
            FetchStatus.Error -> {
                CoroutineScope(Dispatchers.Main).launch {
                    delay(2000)
                    passwordEditText.setText("")
                    showLoginFailedSnackbar()
                }
            }
        }
    }

    private fun createLoadingSnackbar(): Snackbar? {
        val snackbar =
            Snackbar.make(rootConstraintLayout, "Loading...", Snackbar.LENGTH_INDEFINITE)
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(resources.getColor(R.color.rolling_stone))
        snackbarView.layoutParams =
            (snackbarView.layoutParams as FrameLayout.LayoutParams).apply {
                setMargins(
                    0,
                    this.topMargin,
                    0,
                    0
                )
            }
        val textView =
            snackbarView.findViewById<androidx.appcompat.widget.AppCompatTextView>(com.google.android.material.R.id.snackbar_text)
        textView.setTextColor(Color.WHITE)
        textView.textSize = 24f
        return snackbar
    }

    private fun showLoginFailedSnackbar(): Snackbar? {
        val snackbar =
            Snackbar.make(rootConstraintLayout, "Login failed.", Snackbar.LENGTH_LONG)
        snackbar.duration = 10000
        val snackbarView = snackbar.view
        snackbarView.setBackgroundColor(resources.getColor(R.color.dandelion))
        snackbarView.layoutParams =
            (snackbarView.layoutParams as FrameLayout.LayoutParams).apply {
                setMargins(
                    0,
                    this.topMargin,
                    0,
                    0
                )
            }
        (snackbar.view).layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        val textView =
            snackbarView.findViewById<androidx.appcompat.widget.AppCompatTextView>(com.google.android.material.R.id.snackbar_text)
        textView.setTextColor(Color.WHITE)
        textView.textSize = 24f
        snackbar.show()
        return snackbar
    }

}