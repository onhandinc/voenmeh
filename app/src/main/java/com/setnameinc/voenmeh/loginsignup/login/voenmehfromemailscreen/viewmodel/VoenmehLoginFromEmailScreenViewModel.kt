package com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel

import android.util.Patterns
import androidx.lifecycle.viewModelScope
import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel.model.VoenmehLoginFromEmailScreenAction
import com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel.model.VoenmehLoginFromEmailScreenEvent
import com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel.model.VoenmehMailLoginFromEmailScreenViewState
import com.setnameinc.voenmeh.loginsignup.voenmehmail.domain.VoenmehMailInteractor
import kotlinx.coroutines.*
import timber.log.Timber

class VoenmehLoginFromEmailScreenViewModel(
    private val voenmehMailInteractor: VoenmehMailInteractor
) : BaseFlowViewModel<VoenmehMailLoginFromEmailScreenViewState, VoenmehLoginFromEmailScreenAction, VoenmehLoginFromEmailScreenEvent>() {

    var email: String = ""
        get() = when {
            field.endsWith("@voenmeh.ru") &&
                    Patterns.EMAIL_ADDRESS.matcher(field)
                        .matches() -> {
                field
            }
            !field.contains('@') -> {
                "$field@voenmeh.ru"
            }
            else -> {
                ""
            }
        }

    var password: String = ""

    override suspend fun obtainInternalEvent(viewEvent: VoenmehLoginFromEmailScreenEvent) {
        when (viewEvent) {
            is VoenmehLoginFromEmailScreenEvent.EditEmail -> {
                email = viewEvent.value
            }
            is VoenmehLoginFromEmailScreenEvent.EditPassword -> {
                password = viewEvent.value
            }
            is VoenmehLoginFromEmailScreenEvent.Login -> {
                login()
            }
        }
    }

    private fun login() = viewModelScope.launch {
        when {
            email.isEmpty() or voenmehMailInteractor.isEmailInvalid(email) -> {
                viewAction = VoenmehLoginFromEmailScreenAction.ShowErrorIncorrectEmail
            }
            else -> {
                viewState = VoenmehMailLoginFromEmailScreenViewState(FetchStatus.Loading)
                withContext(Dispatchers.IO) {
                    voenmehMailInteractor.voenmehMailLogin(
                        email,
                        password,
                        complete = {
                            if (it) {
                                Timber.d("loggen in from voenmeh mail screen")
                                voenmehMailInteractor.saveUserEmailAndPassword(
                                    email,
                                    password,
                                    complete = {
                                        viewState =
                                            VoenmehMailLoginFromEmailScreenViewState(FetchStatus.Success)
                                    },
                                    error = {
                                        Timber.e(it)
                                    }
                                )
                            } else {
                                viewState =
                                    VoenmehMailLoginFromEmailScreenViewState(FetchStatus.Error)
                            }
                        },
                        error = {
                            viewState = VoenmehMailLoginFromEmailScreenViewState(FetchStatus.Error)
                        }
                    )
                }
            }
        }
    }

}