package com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel.model

sealed class VoenmehLoginFromEmailScreenAction {
    object ShowErrorIncorrectEmail : VoenmehLoginFromEmailScreenAction()
}