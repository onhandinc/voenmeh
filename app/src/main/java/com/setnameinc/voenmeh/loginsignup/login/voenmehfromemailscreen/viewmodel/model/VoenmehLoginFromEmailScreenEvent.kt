package com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel.model

sealed class VoenmehLoginFromEmailScreenEvent {
    data class EditEmail(val value: String) : VoenmehLoginFromEmailScreenEvent()
    data class EditPassword(val value: String) : VoenmehLoginFromEmailScreenEvent()
    object Login : VoenmehLoginFromEmailScreenEvent()
}