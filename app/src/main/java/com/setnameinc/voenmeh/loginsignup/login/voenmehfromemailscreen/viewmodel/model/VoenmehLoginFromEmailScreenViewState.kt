package com.setnameinc.voenmeh.loginsignup.login.voenmehfromemailscreen.viewmodel.model

data class VoenmehMailLoginFromEmailScreenViewState(
    val fetchStatus: FetchStatus
)

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}