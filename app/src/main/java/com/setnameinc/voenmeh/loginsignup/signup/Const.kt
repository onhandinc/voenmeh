package com.setnameinc.voenmeh.loginsignup.signup

import com.setnameinc.voenmeh.profile.settings.groupselector.SelectGroupItem

object Const {

    val GROUPS_ID_TO_NAME: List<SelectGroupItem> = listOf(
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1c5",
            number = 2481,
            name = "А101С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1c7",
            number = 2485,
            name = "А103Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1ca",
            number = 2616,
            name = "А111С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1cb",
            number = 2618,
            name = "А112С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1cd",
            number = 2619,
            name = "А114Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1cf",
            number = 1996,
            name = "А161",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1d0",
            number = 2152,
            name = "А171",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1d1",
            number = 2161,
            name = "А172",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1d2",
            number = 2248,
            name = "А181",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1d3",
            number = 2249,
            name = "А182",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1d4",
            number = 2250,
            name = "А183",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1d6",
            number = 2376,
            name = "А191",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1db",
            number = 2567,
            name = "А1М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1dc",
            number = 2694,
            name = "А1М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1de",
            number = 2486,
            name = "А301С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1e0",
            number = 2488,
            name = "А303Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1e3",
            number = 2624,
            name = "А313Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1e4",
            number = 1999,
            name = "А361",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1e5",
            number = 2153,
            name = "А371",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1e6",
            number = 2251,
            name = "А381",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1e9",
            number = 2378,
            name = "А391",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1ee",
            number = 2569,
            name = "А3М63",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1f6",
            number = 2002,
            name = "А461",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1f7",
            number = 2154,
            name = "А471",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce200",
            number = 2627,
            name = "А511С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce203",
            number = 2256,
            name = "А581",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce204",
            number = 2257,
            name = "А582",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce205",
            number = 2382,
            name = "А591",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce20e",
            number = 2631,
            name = "А813Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce20f",
            number = 2158,
            name = "А871",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce214",
            number = 2380,
            name = "А891",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce225",
            number = 2691,
            name = "ВР111Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce228",
            number = 2498,
            name = "Е101С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce22a",
            number = 2501,
            name = "Е103Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce22b",
            number = 2634,
            name = "Е111С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce22d",
            number = 2636,
            name = "Е113Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce22e",
            number = 2150,
            name = "Е171",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce230",
            number = 2258,
            name = "Е181",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce231",
            number = 2372,
            name = "Е191",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce233",
            number = 2502,
            name = "Е201Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce237",
            number = 2261,
            name = "Е281",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce239",
            number = 2401,
            name = "Е291",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce23c",
            number = 2702,
            name = "Е2М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce23f",
            number = 2639,
            name = "Е311С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce241",
            number = 2145,
            name = "Е371",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce243",
            number = 2370,
            name = "Е391",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce245",
            number = 2506,
            name = "Е401С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce24b",
            number = 2147,
            name = "Е471",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce24c",
            number = 2265,
            name = "Е481",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce24f",
            number = 2368,
            name = "Е491",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce251",
            number = 2562,
            name = "Е4М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce252",
            number = 2703,
            name = "Е4М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce255",
            number = 2566,
            name = "Е5М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce266",
            number = 2271,
            name = "Е781",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce26b",
            number = 2614,
            name = "Е7М62",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce26d",
            number = 2741,
            name = "Е7М72",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce271",
            number = 2650,
            name = "И111Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce275",
            number = 2272,
            name = "И181",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce278",
            number = 2395,
            name = "И191",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce27d",
            number = 2275,
            name = "И281",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce281",
            number = 2517,
            name = "И401С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce284",
            number = 2520,
            name = "И404Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce287",
            number = 2536,
            name = "И407Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce288",
            number = 2653,
            name = "И411С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce28a",
            number = 2655,
            name = "И413Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce28c",
            number = 2657,
            name = "И415Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce28e",
            number = 2143,
            name = "И471",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce290",
            number = 2277,
            name = "И481",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce293",
            number = 2365,
            name = "И491",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce299",
            number = 2523,
            name = "И501Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce29f",
            number = 2529,
            name = "И507Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2a7",
            number = 2420,
            name = "И591",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2b5",
            number = 2403,
            name = "И891",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2c2",
            number = 2670,
            name = "И913Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2c4",
            number = 2113,
            name = "И971",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2d8",
            number = 2605,
            name = "КПР781",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2d9",
            number = 2603,
            name = "КПР81",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2dc",
            number = 2722,
            name = "КПР91",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2de",
            number = 2290,
            name = "О181",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2e9",
            number = 2291,
            name = "О481",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2eb",
            number = 2659,
            name = "О711Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2ee",
            number = 2662,
            name = "О714Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2f1",
            number = 2665,
            name = "О717Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2f5",
            number = 2538,
            name = "Р101Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2f9",
            number = 2537,
            name = "Р107С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2fa",
            number = 2675,
            name = "Р111Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2fe",
            number = 2294,
            name = "Р181",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce301",
            number = 2422,
            name = "Р191",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce303",
            number = 2577,
            name = "Р1М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce305",
            number = 2606,
            name = "Р1М63",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce306",
            number = 2715,
            name = "Р1М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce308",
            number = 2717,
            name = "Р1М73",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce30c",
            number = 2678,
            name = "Р411Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce315",
            number = 2547,
            name = "Р701Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce318",
            number = 2550,
            name = "Р704С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce31d",
            number = 2681,
            name = "Р711Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce31f",
            number = 2684,
            name = "Р714С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce326",
            number = 2300,
            name = "Р781",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce328",
            number = 2302,
            name = "Р783",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce32c",
            number = 2426,
            name = "Р791",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2fc",
            number = 2674,
            name = "Р113С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1ff",
            number = 2492,
            name = "А502Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2ab",
            number = 2414,
            name = "И595",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce25e",
            number = 2367,
            name = "Е691",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1fa",
            number = 2379,
            name = "А491",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce21f",
            number = 2410,
            name = "А991",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce26e",
            number = 2514,
            name = "И101Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce29c",
            number = 2526,
            name = "И504Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce24a",
            number = 2643,
            name = "Е413Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce30f",
            number = 2297,
            name = "Р481",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1f9",
            number = 2255,
            name = "А482",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1e2",
            number = 2623,
            name = "А312Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2b3",
            number = 2281,
            name = "И881",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce211",
            number = 2310,
            name = "А881",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce25b",
            number = 2149,
            name = "Е671",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce27b",
            number = 2561,
            name = "И1М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1fc",
            number = 2570,
            name = "А4М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2e4",
            number = 2560,
            name = "О2М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce250",
            number = 2416,
            name = "Е493",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce27f",
            number = 2392,
            name = "И291",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce242",
            number = 2263,
            name = "Е381",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2ea",
            number = 2393,
            name = "О491",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1f8",
            number = 2254,
            name = "А481",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1fb",
            number = 2408,
            name = "А492",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce209",
            number = 2493,
            name = "А801С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce23b",
            number = 2564,
            name = "Е2М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce26a",
            number = 2563,
            name = "Е7М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1f4",
            number = 2625,
            name = "А411С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce268",
            number = 2399,
            name = "Е791",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1ec",
            number = 2559,
            name = "А3М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce207",
            number = 2594,
            name = "А5М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce221",
            number = 2573,
            name = "А9М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce202",
            number = 2160,
            name = "А571",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2ae",
            number = 2557,
            name = "И5М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2b8",
            number = 2576,
            name = "И8М62",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2c9",
            number = 2386,
            name = "И992",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2be",
            number = 2532,
            name = "И903Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2bf",
            number = 2533,
            name = "И904Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce25f",
            number = 2412,
            name = "Е692",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2af",
            number = 2534,
            name = "И801Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce28f",
            number = 2144,
            name = "И472",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2df",
            number = 2405,
            name = "О191",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce274",
            number = 2690,
            name = "И114Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2e2",
            number = 2688,
            name = "О211Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2fb",
            number = 2676,
            name = "Р112Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2fd",
            number = 2677,
            name = "Р114Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1e7",
            number = 2252,
            name = "А382",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce212",
            number = 2311,
            name = "А882",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1c6",
            number = 2484,
            name = "А102С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce217",
            number = 2572,
            name = "А8М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2dd",
            number = 2724,
            name = "КПР92",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2d4",
            number = 2306,
            name = "КВ81",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce216",
            number = 2398,
            name = "А893",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1df",
            number = 2487,
            name = "А302Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1f3",
            number = 2490,
            name = "А402Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce220",
            number = 2397,
            name = "А992",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce20b",
            number = 2495,
            name = "А803Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce219",
            number = 2496,
            name = "А901Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1e8",
            number = 2253,
            name = "А383",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2b4",
            number = 2282,
            name = "И882",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2ca",
            number = 2385,
            name = "И993",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce302",
            number = 2424,
            name = "Р193",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce31a",
            number = 2612,
            name = "Р706С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2bc",
            number = 2530,
            name = "И901С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2bd",
            number = 2531,
            name = "И902С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce226",
            number = 2692,
            name = "ВР112Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce29d",
            number = 2527,
            name = "И505Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce29e",
            number = 2528,
            name = "И506Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce316",
            number = 2548,
            name = "Р702Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce317",
            number = 2549,
            name = "Р703Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce31c",
            number = 2611,
            name = "Р708Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce31e",
            number = 2682,
            name = "Р712Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce319",
            number = 2551,
            name = "Р705С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce31b",
            number = 2613,
            name = "Р707С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce320",
            number = 2685,
            name = "Р715С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce321",
            number = 2686,
            name = "Р716С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce329",
            number = 2303,
            name = "Р784",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce32a",
            number = 2304,
            name = "Р785",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce32b",
            number = 2305,
            name = "Р786",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce229",
            number = 2499,
            name = "Е102С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce23d",
            number = 2504,
            name = "Е301С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce23e",
            number = 2505,
            name = "Е302С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2cb",
            number = 2556,
            name = "И9М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce260",
            number = 2575,
            name = "Е6М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2b7",
            number = 2565,
            name = "И8М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce246",
            number = 2507,
            name = "Е402С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce257",
            number = 2510,
            name = "Е601С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce269",
            number = 2400,
            name = "Е792",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce213",
            number = 2312,
            name = "А883",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce21d",
            number = 2313,
            name = "А981",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce21e",
            number = 2314,
            name = "А982",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce327",
            number = 2301,
            name = "Р782",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1c9",
            number = 2483,
            name = "А105С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1c8",
            number = 2482,
            name = "А104С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce20a",
            number = 2494,
            name = "А802С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce29a",
            number = 2524,
            name = "И502Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce29b",
            number = 2525,
            name = "И503Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce23a",
            number = 2402,
            name = "Е292",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2d1",
            number = 2155,
            name = "КВ71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2d3",
            number = 2156,
            name = "КВ73",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce22f",
            number = 2151,
            name = "Е172",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2d2",
            number = 2157,
            name = "КВ72",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce294",
            number = 2366,
            name = "И492",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce295",
            number = 2389,
            name = "И493",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce296",
            number = 2390,
            name = "И494",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce291",
            number = 2279,
            name = "И483",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce292",
            number = 2280,
            name = "И484",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce311",
            number = 2425,
            name = "Р491",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1eb",
            number = 2391,
            name = "А393",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1ed",
            number = 2568,
            name = "А3М62",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2cd",
            number = 2552,
            name = "КВ01",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce258",
            number = 2511,
            name = "Е602Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce312",
            number = 2454,
            name = "Р492",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2c8",
            number = 2383,
            name = "И991",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2d5",
            number = 2307,
            name = "КВ82",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1ea",
            number = 2407,
            name = "А392",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce32d",
            number = 2427,
            name = "Р792",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce32e",
            number = 2428,
            name = "Р793",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce32f",
            number = 2429,
            name = "Р794",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce330",
            number = 2430,
            name = "Р795",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce234",
            number = 2503,
            name = "Е202Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce262",
            number = 2512,
            name = "Е701Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce263",
            number = 2513,
            name = "Е702Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2b0",
            number = 2535,
            name = "И802Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2e1",
            number = 2554,
            name = "О202Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2cf",
            number = 2050,
            name = "КВ61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1d7",
            number = 2384,
            name = "А192",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1d8",
            number = 2406,
            name = "А193",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce208",
            number = 2698,
            name = "А5М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2b9",
            number = 2710,
            name = "И8М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce27c",
            number = 2707,
            name = "И1М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2bb",
            number = 2740,
            name = "И8М73",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2d0",
            number = 2051,
            name = "КВ63",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce26f",
            number = 2515,
            name = "И102Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce270",
            number = 2516,
            name = "И103Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce276",
            number = 2309,
            name = "И182",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce277",
            number = 2274,
            name = "И183",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce279",
            number = 2396,
            name = "И192",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce27a",
            number = 2394,
            name = "И193",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1d9",
            number = 2377,
            name = "А194",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1da",
            number = 2418,
            name = "А195",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2d7",
            number = 2456,
            name = "КВ91",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce232",
            number = 2419,
            name = "Е192",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2db",
            number = 2604,
            name = "КПР83",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce30a",
            number = 2544,
            name = "Р401Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2c5",
            number = 2283,
            name = "И981",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce24d",
            number = 2267,
            name = "Е483",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce24e",
            number = 2268,
            name = "Е484",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce25c",
            number = 2269,
            name = "Е681",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce25d",
            number = 2270,
            name = "Е682",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce307",
            number = 2716,
            name = "Р1М72",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce244",
            number = 2371,
            name = "Е392",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce297",
            number = 2558,
            name = "И4М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2da",
            number = 2609,
            name = "КПР82",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2ec",
            number = 2660,
            name = "О712Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2ed",
            number = 2661,
            name = "О713Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2ef",
            number = 2663,
            name = "О715Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2f0",
            number = 2664,
            name = "О716Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce300",
            number = 2296,
            name = "Р183",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2ff",
            number = 2295,
            name = "Р182",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce280",
            number = 2411,
            name = "И292",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2b6",
            number = 2404,
            name = "И892",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1cc",
            number = 2617,
            name = "А113С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1e1",
            number = 2622,
            name = "А311С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce201",
            number = 2628,
            name = "А512Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce25a",
            number = 2647,
            name = "Е612Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2e3",
            number = 2689,
            name = "О212Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce20c",
            number = 2630,
            name = "А811С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce20d",
            number = 2629,
            name = "А812С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2c0",
            number = 2668,
            name = "И911С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2c1",
            number = 2669,
            name = "И912С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce272",
            number = 2651,
            name = "И112Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce273",
            number = 2652,
            name = "И113Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce289",
            number = 2654,
            name = "И412С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce30e",
            number = 2680,
            name = "Р413Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce222",
            number = 2571,
            name = "А9М62",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2f2",
            number = 2666,
            name = "О718Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2f3",
            number = 2667,
            name = "О719Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce310",
            number = 2298,
            name = "Р482",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce21c",
            number = 2633,
            name = "А912Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce28b",
            number = 2656,
            name = "И414Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce28d",
            number = 2658,
            name = "И416Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce215",
            number = 2381,
            name = "А892",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce309",
            number = 2720,
            name = "Р1М74",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2a2",
            number = 2284,
            name = "И582",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2c6",
            number = 2285,
            name = "И983",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2c7",
            number = 2287,
            name = "И985",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2a8",
            number = 2387,
            name = "И592",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2a9",
            number = 2421,
            name = "И593",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2aa",
            number = 2413,
            name = "И594",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2c3",
            number = 2671,
            name = "И914Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce247",
            number = 2508,
            name = "Е403Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce253",
            number = 2509,
            name = "Е501Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce227",
            number = 2693,
            name = "ВР411Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce322",
            number = 2169,
            name = "Р773",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce323",
            number = 2170,
            name = "Р774",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce325",
            number = 2242,
            name = "Р776",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce324",
            number = 2241,
            name = "Р775",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1f2",
            number = 2489,
            name = "А401С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1fe",
            number = 2491,
            name = "А501С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1ce",
            number = 2620,
            name = "А115Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1dd",
            number = 2621,
            name = "А211Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce240",
            number = 2640,
            name = "Е312С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce248",
            number = 2641,
            name = "Е411С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce249",
            number = 2642,
            name = "Е412С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce259",
            number = 2646,
            name = "Е611С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2a0",
            number = 2608,
            name = "И508Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2a1",
            number = 2610,
            name = "И509Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2a3",
            number = 2286,
            name = "И584",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2a4",
            number = 2358,
            name = "И585",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2a5",
            number = 2288,
            name = "И586",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2a6",
            number = 2289,
            name = "И587",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2ac",
            number = 2415,
            name = "И596",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2ad",
            number = 2474,
            name = "И597",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce238",
            number = 2262,
            name = "Е282",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce206",
            number = 2409,
            name = "А592",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1ef",
            number = 2695,
            name = "А3М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1f0",
            number = 2696,
            name = "А3М72",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1f1",
            number = 2742,
            name = "А3М73",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1fd",
            number = 2697,
            name = "А4М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce218",
            number = 2699,
            name = "А8М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce223",
            number = 2700,
            name = "А9М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce224",
            number = 2701,
            name = "А9М72",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2e6",
            number = 2713,
            name = "О2М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce261",
            number = 2705,
            name = "Е6М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2e7",
            number = 2714,
            name = "О2М72",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce256",
            number = 2704,
            name = "Е5М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce26c",
            number = 2706,
            name = "Е7М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce298",
            number = 2708,
            name = "И4М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2f4",
            number = 2709,
            name = "О7М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2ba",
            number = 2711,
            name = "И8М72",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2cc",
            number = 2712,
            name = "И9М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2d6",
            number = 2308,
            name = "КВ83",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce314",
            number = 2718,
            name = "Р4М71",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1d5",
            number = 2340,
            name = "А184",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2e0",
            number = 2553,
            name = "О201Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2e8",
            number = 2555,
            name = "О401Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce282",
            number = 2518,
            name = "И402С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce283",
            number = 2519,
            name = "И403С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2e5",
            number = 2574,
            name = "О2М62",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce210",
            number = 2159,
            name = "А872",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce285",
            number = 2521,
            name = "И405Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce286",
            number = 2522,
            name = "И406Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce267",
            number = 2260,
            name = "Е782",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2f6",
            number = 2539,
            name = "Р102Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2f7",
            number = 2541,
            name = "Р104Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2f8",
            number = 2542,
            name = "Р105Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce30b",
            number = 2545,
            name = "Р402Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce27e",
            number = 2276,
            name = "И282",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce304",
            number = 2578,
            name = "Р1М62",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce313",
            number = 2579,
            name = "Р4М61",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce30d",
            number = 2679,
            name = "Р412Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce1f5",
            number = 2626,
            name = "А412Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce21b",
            number = 2632,
            name = "А911Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce22c",
            number = 2635,
            name = "Е112С",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2ce",
            number = 2687,
            name = "КВ11",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce235",
            number = 2637,
            name = "Е211Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce236",
            number = 2638,
            name = "Е212Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce264",
            number = 2648,
            name = "Е711Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce265",
            number = 2649,
            name = "Е712Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce254",
            number = 2644,
            name = "Е511Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2b1",
            number = 2672,
            name = "И811Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce2b2",
            number = 2673,
            name = "И812Б",
            isSelected = false
        ),
        SelectGroupItem(
            id = "613dfccef01cd43a538ce21a",
            number = 2497,
            name = "А902Б",
            isSelected = false
        )
    )

}