package com.setnameinc.voenmeh.loginsignup.signup.api

import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.setnameinc.voenmeh.user.common.api.UserApi
import com.setnameinc.voenmeh.user.common.api.UserPostRequest
import com.setnameinc.voenmeh.user.common.api.UserResponse
import kotlinx.coroutines.tasks.await
import java.lang.Exception

class SignUpApi(
    private val firebaseAuth: FirebaseAuth,
    private val userApi: UserApi
) {

    suspend fun saveAdditionalData(
        requestBody: SignUpSaveAdditionalDataRequestBody,
        complete: suspend (UserResponse?) -> Unit,
        error: (Throwable) -> Unit
    ) {
        try {
            complete(
                userApi.create(
                    UserPostRequest(
                        requestBody.uid,
                        requestBody.firstName,
                        requestBody.lastName,
                        requestBody.groupId
                    )
                ).await().body()
            )
        } catch (e: Exception) {
            error(e)
        }
    }

    suspend fun createAccount(
        requestBody: SignUpCreateWithEmailAndPasswordRequestBody,
        complete: suspend (AuthResult?) -> Unit,
        error: (Throwable) -> Unit
    ) {
        try {
            complete(
                firebaseAuth.createUserWithEmailAndPassword(
                    requestBody.email,
                    requestBody.password
                ).await()
            )
        } catch (e: Exception) {
            error(e)

        }
    }
}