package com.setnameinc.voenmeh.loginsignup.signup.api

data class SignUpCreateWithEmailAndPasswordRequestBody(
    val email: String,
    val password: String
)