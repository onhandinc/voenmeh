package com.setnameinc.voenmeh.loginsignup.signup.api

data class SignUpSaveAdditionalDataRequestBody(
    val uid: String,
    val firstName: String,
    val lastName: String,
    val groupId: String
)