package com.setnameinc.voenmeh.loginsignup.signup.di

import com.setnameinc.voenmeh.loginsignup.signup.api.SignUpApi
import com.setnameinc.voenmeh.loginsignup.signup.interactor.SignUpInteractor
import com.setnameinc.voenmeh.loginsignup.signup.viewmodel.SignUpViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object SignUpModule {
    val module = module {
        single { SignUpApi(firebaseAuth = get(), userApi = get()) }
        single { SignUpInteractor(signUpApi = get(), userInteractor = get()) }
        viewModel {
            SignUpViewModel(
                signUpInteractor = get()
            )
        }
    }
}