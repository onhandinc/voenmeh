package com.setnameinc.voenmeh.loginsignup.signup.interactor

import com.setnameinc.voenmeh.loginsignup.signup.api.SignUpCreateWithEmailAndPasswordRequestBody
import com.setnameinc.voenmeh.loginsignup.signup.api.SignUpSaveAdditionalDataRequestBody

object SignUpConverter {
    fun toNetworkSaveAdditionalData(
        uid: String,
        firstName: String,
        lastName: String
    ): SignUpSaveAdditionalDataRequestBody = SignUpSaveAdditionalDataRequestBody(
        uid = uid,
        firstName = firstName,
        lastName = lastName,
        groupId = ""
    )

    fun toNetworkCreateWithEmailAndPassword(
        email:String,
        password:String
    ): SignUpCreateWithEmailAndPasswordRequestBody =
        SignUpCreateWithEmailAndPasswordRequestBody(
            email = email,
            password = password
        )

}