package com.setnameinc.voenmeh.loginsignup.signup.interactor

import android.text.TextUtils
import android.util.Patterns
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.setnameinc.voenmeh.loginsignup.signup.api.SignUpApi
import com.setnameinc.voenmeh.loginsignup.signup.viewmodel.SingUpViewResponseFailed
import com.setnameinc.voenmeh.loginsignup.signup.viewmodel.SingUpViewResponseFailed.*
import com.setnameinc.voenmeh.user.common.api.UserResponse
import com.setnameinc.voenmeh.user.common.domain.UserInteractor

import timber.log.Timber

class SignUpInteractor(
    private val signUpApi: SignUpApi,
    private val userInteractor: UserInteractor
) {

    suspend fun signUp(
        firstName: String,
        lastName: String,
        email: String,
        password: String,
        complete: () -> Unit,
        error: (SingUpViewResponseFailed) -> Unit
    ) {
        createAccount(
            email,
            password,
            complete = {
                Timber.d("completely created account")
                it?.user?.uid?.let { uid ->
                    val responseSaveAdditionalData = saveAdditionalData(
                        uid = uid,
                        firstName = firstName,
                        lastName = lastName,
                        complete = {

                        },
                        error = {

                        }
                    )
                    userInteractor.updateUserData()
                    Timber.d("saved data ")
                }
                complete()
            },
            error = {
                if (it is FirebaseAuthUserCollisionException) {
                    error(EMAIL_ALREADY_REGISTERED)
                } else {
                    error(UNKNOWN)
                }
            }
        )
    }

    fun isFirstNameInvalid(firstName: String): Boolean = firstName.isEmpty()

    fun isLastNameInvalid(firstName: String): Boolean = firstName.isEmpty()

    suspend fun saveAdditionalData(
        uid: String,
        firstName: String,
        lastName: String,
        complete: suspend (UserResponse?) -> Unit,
        error: (Throwable) -> Unit
    ) {
        signUpApi.saveAdditionalData(
            SignUpConverter.toNetworkSaveAdditionalData(
                uid = uid,
                firstName = firstName,
                lastName = lastName
            ),
            complete,
            error
        )
    }

    suspend fun createAccount(
        email: String,
        password: String,
        complete: suspend (AuthResult?) -> Unit,
        error: (Throwable) -> Unit
    ) {
        signUpApi.createAccount(
            SignUpConverter.toNetworkCreateWithEmailAndPassword(
                email = email,
                password = password
            ),
            complete,
            error
        )
    }

    fun isEmailInvalid(email: String): Boolean =
        TextUtils.isEmpty(email)
                || !Patterns.EMAIL_ADDRESS.matcher(email).matches()

    fun isPasswordInvalid(password: String) = password.isEmpty() or (password.length < 6)

}