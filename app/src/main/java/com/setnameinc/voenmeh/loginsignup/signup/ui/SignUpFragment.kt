package com.setnameinc.voenmeh.loginsignup.signup.ui

import android.content.Intent
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.setnameinc.voenmeh.main.ui.MainActivity
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.loginsignup.signup.viewmodel.SignUpViewModel
import com.setnameinc.voenmeh.loginsignup.signup.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.loginsignup.signup.viewmodel.model.SignUpAction
import com.setnameinc.voenmeh.loginsignup.signup.viewmodel.model.SignUpEvent
import com.setnameinc.voenmeh.loginsignup.signup.viewmodel.model.SignUpViewState
import kotlinx.android.synthetic.main.fragment_sign_up.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class SignUpFragment : Fragment(R.layout.fragment_sign_up) {

    private val viewModel: SignUpViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewState(it)
                }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.viewActions()
                .filterNotNull()
                .collect {
                    bindViewAction(it)
                }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        firstNameEditText.doAfterTextChanged {
            it?.toString()?.let { firstName ->
                viewModel.obtainEvent(SignUpEvent.EditFirstName(firstName))
            }
        }
        lastNameEditText.doAfterTextChanged {
            it?.toString()?.let { lastName ->
                viewModel.obtainEvent(SignUpEvent.EditLastName(lastName))
            }
        }
        emailEditText.doAfterTextChanged {
            it?.toString()?.let { email ->
                viewModel.obtainEvent(SignUpEvent.EditEmail(email))
            }
        }
        passwordEditText.doAfterTextChanged {
            it?.toString()?.let { password ->
                viewModel.obtainEvent(SignUpEvent.EditPassword(password))
            }
        }
        singUpButton.setDebounceClickListener(
            debounceTimeMillis = 2000
        ) {
            viewModel.obtainEvent(SignUpEvent.SignUp)
        }
        loginBackTextView.apply {
            setDebounceClickListener {
                findNavController().popBackStack(R.id.loginFragment, false)
            }
            text = getColoredText(R.string.sign_up_already_has_account, R.color.tundora)
            append(" ")
            append(getColoredText(R.string.sign_up_login, R.color.dodger_blue))
        }
    }

    private fun bindViewState(viewState: SignUpViewState) {
        when (viewState.fetchStatus) {
            is FetchStatus.Success -> {
                startActivity(Intent(activity, MainActivity::class.java))
                activity?.finish()
            }
            is FetchStatus.Error -> {
                findNavController().popBackStack()
            }
            FetchStatus.Loading -> {
                findNavController().navigate(R.id.signUpToSignUpInProgress)
            }
        }
    }

    private fun bindViewAction(action: SignUpAction) {
        when (action) {
            SignUpAction.ShowErrorIncorrectFirstName -> {
                firstNameEditText.error =
                    resources.getString(R.string.sign_up_incorrect_first_name)
            }
            SignUpAction.ShowErrorIncorrectLastName -> {
                lastNameEditText.error =
                    resources.getString(R.string.sign_up_incorrect_last_name)
            }
            SignUpAction.ShowErrorIncorrectEmail -> {
                emailEditText.error = resources.getString(R.string.sign_up_incorrect_email)
            }
            SignUpAction.ShowErrorWeakPassword -> {
                passwordEditText.error =
                    resources.getString(R.string.sign_up_weak_password)
            }
            SignUpAction.ShowErrorEmailAlreadyRegistered -> {
                Timber.d("email already registered")
                findNavController().popBackStack()
                emailEditText.error =
                    resources.getString(R.string.sign_up_already_registered_email)
            }
        }
    }

    private fun getColoredText(
        textId: Int,
        colorId: Int
    ) = SpannableString(
        resources.getString(textId)
    ).apply {
        setSpan(
            ForegroundColorSpan(resources.getColor(colorId)),
            0,
            length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }

}