package com.setnameinc.voenmeh.loginsignup.signup.ui

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import kotlinx.android.synthetic.main.fragment_sign_up_select_type.*


class SignUpSelectTypeFragment : Fragment(R.layout.fragment_sign_up_select_type) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        vkLoginButton.setDebounceClickListener {
            Toast.makeText(
                context,
                R.string.login_in_developing_text,
                Toast.LENGTH_SHORT
            ).show()
            //activity?.let { activity -> VK.login(activity, arrayListOf()) }
        }
        voenmehMailLoginButton.setDebounceClickListener {
            findNavController().navigate(R.id.toMailLogin)
        }
        signUpTextView.setDebounceClickListener {
            /*Toast.makeText(
                context,
                R.string.login_in_temporary_unavailable_text,
                Toast.LENGTH_SHORT
            ).show()*/
            findNavController().navigate(R.id.toSignUp)
        }
    }

}