package com.setnameinc.voenmeh.loginsignup.signup.viewmodel

import androidx.lifecycle.viewModelScope
import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.loginsignup.signup.interactor.SignUpInteractor
import com.setnameinc.voenmeh.loginsignup.signup.viewmodel.SingUpViewResponseFailed.*
import com.setnameinc.voenmeh.loginsignup.signup.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.loginsignup.signup.viewmodel.model.SignUpAction
import com.setnameinc.voenmeh.loginsignup.signup.viewmodel.model.SignUpEvent
import com.setnameinc.voenmeh.loginsignup.signup.viewmodel.model.SignUpViewState
import kotlinx.coroutines.*
import timber.log.Timber

class SignUpViewModel(
    private val signUpInteractor: SignUpInteractor
) : BaseFlowViewModel<SignUpViewState, SignUpAction, SignUpEvent>() {

    private var firstName: String = ""
    private var lastName: String = ""
    private var email: String = ""
    private var password: String = ""

    private fun signUp() = viewModelScope.launch {
        when {
            signUpInteractor.isFirstNameInvalid(firstName) -> {
                viewAction = SignUpAction.ShowErrorIncorrectFirstName
            }
            signUpInteractor.isLastNameInvalid(lastName) -> {
                viewAction = SignUpAction.ShowErrorIncorrectLastName
            }
            signUpInteractor.isEmailInvalid(email) -> {
                viewAction = SignUpAction.ShowErrorIncorrectEmail
            }
            signUpInteractor.isPasswordInvalid(password) -> {
                viewAction = SignUpAction.ShowErrorWeakPassword
            }
            else -> {
                viewState = SignUpViewState(FetchStatus.Loading)
                withContext(Dispatchers.IO) {
                    signUpInteractor.signUp(
                        email = email,
                        password = password,
                        firstName = firstName,
                        lastName = lastName,
                        complete = {
                            viewState = SignUpViewState(FetchStatus.Success)
                        },
                        error = {
                            when (it) {
                                EMAIL_ALREADY_REGISTERED -> {
                                    viewAction = SignUpAction.ShowErrorEmailAlreadyRegistered
                                }
                                UNKNOWN -> {
                                    viewState = SignUpViewState(FetchStatus.Error)
                                }
                            }
                        }
                    )
                }
            }
        }
    }

    override suspend fun obtainInternalEvent(viewEvent: SignUpEvent) {
        when (viewEvent) {
            is SignUpEvent.EditEmail -> {
                email = viewEvent.value
            }
            is SignUpEvent.EditPassword -> {
                password = viewEvent.value
            }
            is SignUpEvent.EditFirstName -> {
                firstName = viewEvent.value
            }
            is SignUpEvent.EditLastName -> {
                lastName = viewEvent.value
            }
            is SignUpEvent.SignUp -> {
                signUp()
            }
        }
    }

}