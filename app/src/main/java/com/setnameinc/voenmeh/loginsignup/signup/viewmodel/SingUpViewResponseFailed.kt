package com.setnameinc.voenmeh.loginsignup.signup.viewmodel

/**
 * [EMAIL_ALREADY_REGISTERED] exists account with this email address
 */

enum class SingUpViewResponseFailed {
    EMAIL_ALREADY_REGISTERED,
    UNKNOWN
}