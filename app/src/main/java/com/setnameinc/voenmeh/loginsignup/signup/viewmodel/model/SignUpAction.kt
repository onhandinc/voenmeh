package com.setnameinc.voenmeh.loginsignup.signup.viewmodel.model

sealed class SignUpAction {
    object ShowErrorIncorrectFirstName : SignUpAction()
    object ShowErrorIncorrectLastName : SignUpAction()
    object ShowErrorIncorrectEmail : SignUpAction()
    object ShowErrorWeakPassword : SignUpAction()
    object ShowErrorEmailAlreadyRegistered : SignUpAction()
}