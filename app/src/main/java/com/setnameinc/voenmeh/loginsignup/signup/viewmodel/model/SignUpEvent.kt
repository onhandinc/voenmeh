package com.setnameinc.voenmeh.loginsignup.signup.viewmodel.model

sealed class SignUpEvent {
    data class EditFirstName(val value: String) : SignUpEvent()
    data class EditLastName(val value: String) : SignUpEvent()
    data class EditEmail(val value: String) : SignUpEvent()
    data class EditPassword(val value: String) : SignUpEvent()
    object SignUp : SignUpEvent()
}