package com.setnameinc.voenmeh.loginsignup.signup.viewmodel.model

data class SignUpViewState(
    val fetchStatus: FetchStatus
)

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}