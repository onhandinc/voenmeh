package com.setnameinc.voenmeh.loginsignup.voenmehmail.api

import java.util.*

object MailProtocolSettings {
    val IMAPSettings: Properties = Properties().apply {
        put("mail.imap.ssl.enable", true)
        put("mail.imap.host", "mail.voenmeh.ru")
        put("mail.store.protocol", "imap")
        put("mail.imap.port", 993)
        put("mail.imap.auth", true)
    }
}