package com.setnameinc.voenmeh.loginsignup.voenmehmail.api

import java.lang.Exception
import javax.mail.Session
import javax.mail.Store

class VoenmehMailApi {

    suspend fun login(
        login: String,
        password: String,
        complete: suspend (Boolean) -> Unit,
        error: (Throwable) -> Unit
    ) {
        try {
            val store: Store = Session.getDefaultInstance(MailProtocolSettings.IMAPSettings).store
            store.connect(login, password)
            complete(store.isConnected)
        } catch (e: Exception) {
            error(e)
        }
    }

}