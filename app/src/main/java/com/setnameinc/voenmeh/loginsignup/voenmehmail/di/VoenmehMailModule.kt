package com.setnameinc.voenmeh.loginsignup.voenmehmail.di

import com.setnameinc.voenmeh.loginsignup.voenmehmail.api.VoenmehMailApi
import com.setnameinc.voenmeh.loginsignup.voenmehmail.domain.VoenmehMailInteractor
import com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel.VoenmehMailLoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object VoenmehMailModule {
    val module = module {
        single { VoenmehMailApi() }
        single {
            VoenmehMailInteractor(
                voenmehMailApi = get(),
                signUpInteractor = get(),
                loginInteractor = get(),
                emailInteractor = get()
            )
        }
        viewModel {
            VoenmehMailLoginViewModel(
                voenmehMailInteractor = get(),
                userInteractor = get()
            )
        }
    }
}