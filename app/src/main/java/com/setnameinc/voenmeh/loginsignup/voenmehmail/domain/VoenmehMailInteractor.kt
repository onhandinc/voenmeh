package com.setnameinc.voenmeh.loginsignup.voenmehmail.domain

import com.google.firebase.auth.AuthResult
import com.setnameinc.voenmeh.email.main.domain.EmailInteractor
import com.setnameinc.voenmeh.loginsignup.login.main.domain.LoginInteractor
import com.setnameinc.voenmeh.loginsignup.signup.interactor.SignUpInteractor
import com.setnameinc.voenmeh.loginsignup.voenmehmail.api.VoenmehMailApi
import com.setnameinc.voenmeh.user.common.api.UserResponse
import timber.log.Timber

class VoenmehMailInteractor(
    private val emailInteractor: EmailInteractor,
    private val signUpInteractor: SignUpInteractor,
    private val loginInteractor: LoginInteractor,
    private val voenmehMailApi: VoenmehMailApi
) {

    suspend fun login(
        email: String,
        password: String,
        complete: suspend (Boolean) -> Unit
    ) {
        loginInteractor.login(
            email,
            password,
            complete = {
                complete(true)
            },
            error = {
                Timber.e("$it")
                complete(false)
            }
        )
    }

    suspend fun voenmehMailLogin(
        login: String,
        password: String,
        complete: suspend (Boolean) -> Unit,
        error: (Throwable) -> Unit
    ) {
        voenmehMailApi.login(login = login, password = password, complete = complete, error = error)
    }


    suspend fun createAccount(
        email: String,
        password: String,
        complete: suspend (AuthResult?) -> Unit,
        error: (Throwable) -> Unit
    ) = signUpInteractor.createAccount(
        email = email,
        password = password,
        complete = complete,
        error = error
    )

    suspend fun saveAdditionalData(
        uid: String,
        firstName: String,
        lastName: String,
        complete: suspend (UserResponse?) -> Unit,
        error: (Throwable) -> Unit
    ) {
        signUpInteractor.saveAdditionalData(
            uid = uid,
            firstName = firstName,
            lastName = lastName,
            complete = complete,
            error = error
        )
    }

    suspend fun isEmailInvalid(email: String) = loginInteractor.isEmailInvalid(email)

    suspend fun saveUserEmailAndPassword(
        email: String,
        password: String,
        complete: suspend (Boolean) -> Unit,
        error: (Throwable) -> Unit
    ) {
        emailInteractor.updateEmailClientData(
            email,
            password,
            complete,
            error
        )
    }


}