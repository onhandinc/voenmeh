package com.setnameinc.voenmeh.loginsignup.voenmehmail.ui

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.setnameinc.voenmeh.main.ui.MainActivity
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel.VoenmehMailLoginViewModel
import com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel.model.VoenmehMailAction
import com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel.model.VoenmehMailEvent
import com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel.model.VoenmehMailViewState
import kotlinx.android.synthetic.main.fragment_login_voenmeh_mail.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class VoenmehMailLoginFragment : Fragment(R.layout.fragment_login_voenmeh_mail) {

    private val viewModel: VoenmehMailLoginViewModel by viewModel()

    private var loadingSnackbarList: MutableList<Snackbar?> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewState(it)
                }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.viewActions()
                .filterNotNull()
                .collect {
                    bindViewAction(it)
                }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        emailEditText.doAfterTextChanged {
            emailEditText.error = null
            it?.toString()?.let { email ->
                viewModel.obtainEvent(VoenmehMailEvent.EditEmail(email))
            }
        }
        passwordEditText.doAfterTextChanged {
            it?.toString()?.let { password ->
                viewModel.obtainEvent(VoenmehMailEvent.EditPassword(password))
            }
        }
        loginButton.setDebounceClickListener(debounceTimeMillis = 2000) {
            viewModel.obtainEvent(VoenmehMailEvent.Login)
        }
    }

    private fun bindViewAction(action: VoenmehMailAction) {
        when (action) {
            VoenmehMailAction.ShowErrorIncorrectEmail -> {
                emailEditText.error =
                    resources.getString(R.string.login_mail_voenmeh_incorrect_email)
                loadingSnackbarList.forEach {
                    it?.dismiss()
                }
                loadingSnackbarList.clear()
            }
        }
    }

    private fun bindViewState(viewState: VoenmehMailViewState) {
        when (viewState.fetchStatus) {
            is FetchStatus.Loading -> {
                loadingSnackbarList.add(
                    createLoadingSnackbar()?.apply { show() }
                )
            }
            is FetchStatus.Success -> {
                CoroutineScope(Dispatchers.Main).launch {
                    delay(1000)
                    Timber.d("completely logged in")
                    startActivity(Intent(activity, MainActivity::class.java))
                    activity?.finish()
                }
            }
            FetchStatus.Error -> {
                CoroutineScope(Dispatchers.Main).launch {
                    delay(2000)
                    passwordEditText.setText("")
                    showLoginFailedSnackbar()
                }
            }
        }
    }

    private fun createLoadingSnackbar(): Snackbar? {
        if (view != null) {
            val snackbar =
                Snackbar.make(requireView(), "Loading...", Snackbar.LENGTH_INDEFINITE)
            val snackbarView = snackbar.view
            snackbarView.setBackgroundColor(resources.getColor(R.color.rolling_stone))
            snackbarView.layoutParams =
                (snackbarView.layoutParams as FrameLayout.LayoutParams).apply {
                    setMargins(
                        0,
                        this.topMargin,
                        0,
                        0
                    )
                }
            val textView =
                snackbarView.findViewById<androidx.appcompat.widget.AppCompatTextView>(com.google.android.material.R.id.snackbar_text)
            textView.setTextColor(Color.WHITE)
            textView.textSize = 24f
            return snackbar
        }
        return null
    }

    private fun showLoginFailedSnackbar(): Snackbar? {
        if (view != null) {
            val snackbar =
                Snackbar.make(requireView(), "Login failed.", Snackbar.LENGTH_LONG)
            snackbar.duration = 10000
            val snackbarView = snackbar.view
            snackbarView.setBackgroundColor(resources.getColor(R.color.dandelion))
            snackbarView.layoutParams =
                (snackbarView.layoutParams as FrameLayout.LayoutParams).apply {
                    setMargins(
                        0,
                        this.topMargin,
                        0,
                        0
                    )
                }
            (snackbar.view).layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
            val textView =
                snackbarView.findViewById<androidx.appcompat.widget.AppCompatTextView>(com.google.android.material.R.id.snackbar_text)
            textView.setTextColor(Color.WHITE)
            textView.textSize = 24f
            snackbar.show()
            return snackbar
        }
        return null
    }

}