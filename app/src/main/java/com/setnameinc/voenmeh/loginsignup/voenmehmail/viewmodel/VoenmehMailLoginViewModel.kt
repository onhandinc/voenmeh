package com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel

import android.util.Patterns
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.user.common.domain.UserInteractor
import com.setnameinc.voenmeh.loginsignup.voenmehmail.domain.VoenmehMailInteractor
import com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel.model.VoenmehMailAction
import com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel.model.VoenmehMailEvent
import com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel.model.VoenmehMailViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber


class VoenmehMailLoginViewModel(
    private val voenmehMailInteractor: VoenmehMailInteractor,
    private val userInteractor: UserInteractor
) : BaseFlowViewModel<VoenmehMailViewState, VoenmehMailAction, VoenmehMailEvent>() {

    var email: String = ""
        get() = when {
            field.endsWith("@voenmeh.ru") &&
                    Patterns.EMAIL_ADDRESS.matcher(field)
                        .matches() -> {
                field
            }
            !field.contains('@') -> {
                "$field@voenmeh.ru"
            }
            else -> {
                ""
            }
        }

    var password: String = ""

    override suspend fun obtainInternalEvent(viewEvent: VoenmehMailEvent) {
        when (viewEvent) {
            is VoenmehMailEvent.EditEmail -> {
                email = viewEvent.value
            }
            is VoenmehMailEvent.EditPassword -> {
                password = viewEvent.value
            }
            is VoenmehMailEvent.Login -> {
                login()
            }
        }
    }

    private fun login() = viewModelScope.launch {
        when {
            email.isEmpty() or voenmehMailInteractor.isEmailInvalid(email) -> {
                viewAction = VoenmehMailAction.ShowErrorIncorrectEmail
            }
            else -> {
                viewState = VoenmehMailViewState(FetchStatus.Loading)
                withContext(Dispatchers.IO) {
                    voenmehMailInteractor.login(
                        email = email,
                        password = password,
                        complete = {
                            if (it) {
                                voenmehMailInteractor.saveUserEmailAndPassword(
                                    email,
                                    password,
                                    complete = {
                                        viewState = VoenmehMailViewState(FetchStatus.Success)
                                    },
                                    error = {
                                        Timber.e(it)
                                    }
                                )
                            } else {
                                voenmehMailInteractor.voenmehMailLogin(
                                    email,
                                    password,
                                    complete = {
                                        userInteractor.requestUserData(
                                            complete = { userData ->
                                                if (userData.uid.isNotEmpty()) {
                                                    voenmehMailInteractor.saveUserEmailAndPassword(
                                                        email,
                                                        password,
                                                        complete = {
                                                            viewState =
                                                                VoenmehMailViewState(FetchStatus.Success)
                                                        },
                                                        error = {
                                                            Timber.e(it)
                                                        }
                                                    )
                                                }
                                            },
                                            error = {
                                                Timber.d("a new one user, create account")
                                                createANewUser(email, password)
                                                viewState =
                                                    VoenmehMailViewState(FetchStatus.Success)
                                            }
                                        )
                                    },
                                    error = {
                                        Timber.e(it)
                                        viewState = VoenmehMailViewState(FetchStatus.Error)
                                    }
                                )
                            }
                        }
                    )
                }
            }
        }
    }

    private suspend fun createANewUser(email: String, password: String) {
        voenmehMailInteractor.saveUserEmailAndPassword(
            email,
            password,
            {

            },
            {
                Timber.e(it)
            }
        )
        voenmehMailInteractor.createAccount(
            email = email,
            password = password,
            complete = { authResult ->
                Timber.d("successfully create account")
                authResult?.user?.uid?.let {
                    val saveAdditionalDataResponse =
                        voenmehMailInteractor.saveAdditionalData(
                            uid = it,
                            firstName = "",
                            lastName = "",
                            complete = {
                                Timber.d("complete save additional data")
                            },
                            error = {
                                Timber.e(it)
                            }
                        )
                }
                viewState = VoenmehMailViewState(FetchStatus.Success)
            },
            error = {
                if (it is FirebaseAuthUserCollisionException) {
                    Timber.d("email already used")
                }
                Timber.e(it)
                viewState = VoenmehMailViewState(FetchStatus.Error)
            }
        )
    }
}