package com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel.model

sealed class VoenmehMailAction {
    object ShowErrorIncorrectEmail : VoenmehMailAction()
}