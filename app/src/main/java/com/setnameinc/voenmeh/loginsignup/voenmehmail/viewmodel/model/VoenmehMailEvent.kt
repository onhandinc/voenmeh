package com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel.model

sealed class VoenmehMailEvent {
    data class EditEmail(val value: String) : VoenmehMailEvent()
    data class EditPassword(val value: String) : VoenmehMailEvent()
    object Login : VoenmehMailEvent()
}