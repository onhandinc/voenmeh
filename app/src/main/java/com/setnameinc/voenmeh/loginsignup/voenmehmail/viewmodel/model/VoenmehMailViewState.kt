package com.setnameinc.voenmeh.loginsignup.voenmehmail.viewmodel.model

data class VoenmehMailViewState(
    val fetchStatus: FetchStatus
)

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}