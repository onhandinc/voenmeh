package com.setnameinc.voenmeh.main.ui

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.core.view.forEach
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.tools.utils.extensions.setupWithNavController
import kotlinx.android.synthetic.main.fragment_main.*
import timber.log.Timber

class MainFragment : Fragment(R.layout.fragment_main) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            setupBottomNavigationBar()
            bottomNavigationView.selectedItemId = R.id.timetableNavigation
        }
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        // Now that BottomNavigationBar has restored its instance state
        // and its selectedItemId, we can proceed with setting up the
        // BottomNavigationBar with Navigation
        setupBottomNavigationBar()
    }

    /**
     * Called on first creation and when restoring state.
     */
    private fun setupBottomNavigationBar() {
        val navGraphIds = listOf(
            R.navigation.news_navigation,
            R.navigation.search_navigation,
            R.navigation.timetable_navigation,
            R.navigation.email_client_navigation,
            R.navigation.profile_navigation
        )

        /*val groups: List<BottomNavigationViewItem> = listOf(
            BottomNavigationViewItem(
                itemId = R.id.timetableEditSubmenu,
                rootItemId = R.id.timetableNavigation,
                icon = R.drawable.ic_email_attachment_download,
                title = "Изменить"
            )
        )*/

        // Setup the bottom navigation view with a list of navigation graphs
        bottomNavigationView.setupWithNavController(
            /*  groups = groups,*/
            navGraphIds = navGraphIds,
            fragmentManager = childFragmentManager,
            containerId = R.id.navHostContainer,
            intent = requireActivity().intent
        )

        bottomNavigationView.disableTooltip()


    }

    fun BottomNavigationView.disableTooltip() {
        val content: View = getChildAt(0)
        if (content is ViewGroup) {
            content.forEach {
                when (it.id) {
                    R.id.timetableNavigation -> {
                        it.setOnLongClickListener {
                            if (bottomNavigationView.selectedItemId == R.id.timetableNavigation) {
                                Timber.d("display bottom sheet view")
                                return@setOnLongClickListener true
                            } else {
                                return@setOnLongClickListener false
                            }
                        }
                    }
                    else -> {
                        it.setOnLongClickListener {
                            return@setOnLongClickListener false
                        }
                    }
                }

            }
        }
    }


}