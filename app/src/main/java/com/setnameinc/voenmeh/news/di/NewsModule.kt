package com.setnameinc.voenmeh.news.di

import androidx.paging.ExperimentalPagingApi
import com.setnameinc.voenmeh.news.main.preferences.NewsSharedPreferences
import com.setnameinc.voenmeh.news.selectsource.di.NewsManageSourceModule
import org.koin.dsl.module

object NewsModule {

    private val commonModule = module {
        single {
            NewsSharedPreferences(sharedPreferences = get())
        }
    }

    @ExperimentalPagingApi
    val modules = listOf(
        com.setnameinc.voenmeh.news.main.di.NewsModule.module,
        NewsManageSourceModule.module,
        commonModule
    )

}