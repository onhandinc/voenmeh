package com.setnameinc.voenmeh.news.hint.ui

import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.tools.base.BaseBottomRoundSheetDialogFragment

class NewsAboutBottomDialog : BaseBottomRoundSheetDialogFragment(R.layout.dialog_news_about)