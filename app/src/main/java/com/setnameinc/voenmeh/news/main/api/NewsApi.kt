package com.setnameinc.voenmeh.news.main.api

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {

    @GET("news")
    fun load(
        @Query("page") page: Int,
        @Query("sources") sources: List<String>
    ): Deferred<Response<NewsResponse>>
}