package com.setnameinc.voenmeh.news.main.api

import com.google.gson.annotations.SerializedName

data class NewsItemResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("text")
    val text: String,
    @SerializedName("date")
    val date: Long,
    @SerializedName("groupOwnerId")
    val groupOwnerId: String,
    @SerializedName("groupName")
    val groupName: String,
    @SerializedName("groupPhotoUrl")
    val groupPhotoUrl: String
)