package com.setnameinc.voenmeh.news.main.api

import com.google.gson.annotations.SerializedName

data class NewsResponse(
    @SerializedName("items")
    val items: List<NewsItemResponse>
)