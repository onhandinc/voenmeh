package com.setnameinc.voenmeh.news.main.di

import androidx.paging.ExperimentalPagingApi
import com.setnameinc.voenmeh.app.api.RetrofitConst
import com.setnameinc.voenmeh.app.database.AppDatabase
import com.setnameinc.voenmeh.news.main.api.NewsApi
import com.setnameinc.voenmeh.news.main.ui.NewsFragment
import com.setnameinc.voenmeh.news.main.viewmodel.NewsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

@ExperimentalPagingApi
object NewsModule {

    val module = module {
        scope<NewsFragment> {
            scoped {
                get<AppDatabase>().newsRepository()
            }
            scoped {
                get<AppDatabase>().newsRemoteKeysRepository()
            }
            scoped {
                get<Retrofit>(
                    named(RetrofitConst.VOENMEH_SERVER_RETROFIT_INJECTION_KEY)
                ).create(NewsApi::class.java)
            }
            viewModel {
                NewsViewModel(
                    application = get(),
                    newsApi = get(),
                    newsRepository = get(),
                    newsRemoteKeysRepository = get(),
                    newsSharedPreferences = get()
                )
            }
        }
    }
}