package com.setnameinc.voenmeh.news.main.domain

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.net.Uri
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ClickableSpan
import android.view.View
import com.setnameinc.voenmeh.news.main.api.NewsItemResponse
import com.setnameinc.voenmeh.news.main.repository.NewsEntity
import com.setnameinc.voenmeh.news.main.ui.NewsItem
import com.setnameinc.voenmeh.tools.utils.TimeAgo
import timber.log.Timber

object NewsConverter {

    fun NewsItemResponse.fromApi(): NewsModel = NewsModel(
        id = this.id,
        text = this.text,
        date = this.date,
        groupOwnerId = this.groupOwnerId,
        groupName = this.groupName,
        groupPhotoUrl = this.groupPhotoUrl
    )

    fun NewsModel.toDatabase(): NewsEntity = NewsEntity(
        id = this.id,
        text = this.text,
        date = this.date,
        groupOwnerId = this.groupOwnerId,
        groupName = this.groupName,
        groupPhotoUrl = this.groupPhotoUrl
    )

    fun NewsEntity.fromDatabase(): NewsModel = NewsModel(
        id = this.id,
        text = this.text,
        date = this.date,
        groupOwnerId = this.groupOwnerId,
        groupName = this.groupName,
        groupPhotoUrl = this.groupPhotoUrl
    )

    fun NewsModel.toItem(
        context: Context
    ): NewsItem {
        val spans: Pair<String, List<SpanItem>> = this.text.replaceSpans()
        val spannableStringBuilder = SpannableStringBuilder(spans.first)
        spans.second.forEach {
            spannableStringBuilder.setSpan(
                generateClickableSpan(context, it.link),
                it.startIndex,
                it.startIndex + it.text.length,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
        }
        return NewsItem(
            id = this.id,
            text = spannableStringBuilder,
            date = TimeAgo.getTimeAgo(this.date),
            groupOwnerId = this.groupOwnerId,
            groupName = this.groupName,
            groupPhotoUrl = this.groupPhotoUrl
        )
    }

    private fun generateClickableSpan(
        context: Context,
        link: String
    ): ClickableSpan = object : ClickableSpan() {
        override fun onClick(p0: View) {
            Timber.d("open $link")
            context.startActivity(
                Intent(Intent.ACTION_VIEW, Uri.parse(link)).apply {
                    flags = FLAG_ACTIVITY_NEW_TASK
                }
            )
        }

    }

    private fun String.fixLink(): String = if (this.contains("://")) {
        this
    } else {
        "https://vk.com/$this"
    }

    fun String.replaceSpans(): Pair<String, List<SpanItem>> {
        var movingCursor = 0
        val list: MutableList<SpanItem> = mutableListOf()
        val ans = SpannableStringBuilder(this).replace(
            Regex("\\[(.*?)\\|(.*?)\\]")
        ) { matchResult ->
            val item = matchResult.value.drop(1).dropLast(1).split('|')
            Timber.d("item = ${item}")
            list.add(
                SpanItem(
                    matchResult.range.first - movingCursor,
                    text = item[1],
                    link = item[0].fixLink()
                )
            )
            movingCursor += matchResult.value.length - item[1].length
            item[1]
        }
        return ans to list
    }

    data class SpanItem(
        val startIndex: Int,
        val text: String,
        val link: String
    )

}