package com.setnameinc.voenmeh.news.main.domain

data class NewsModel(
    val id: String,
    val text: String,
    val date: Long,
    val groupOwnerId: String,
    val groupName: String,
    val groupPhotoUrl: String
)