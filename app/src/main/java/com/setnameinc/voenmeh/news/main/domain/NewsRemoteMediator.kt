package com.setnameinc.voenmeh.news.main.domain

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.setnameinc.voenmeh.news.main.api.NewsApi
import com.setnameinc.voenmeh.news.main.domain.NewsConverter.fromApi
import com.setnameinc.voenmeh.news.main.domain.NewsConverter.toDatabase
import com.setnameinc.voenmeh.news.main.preferences.NewsSharedPreferences
import com.setnameinc.voenmeh.news.main.repository.NewsEntity
import com.setnameinc.voenmeh.news.main.repository.NewsRemoteKey
import com.setnameinc.voenmeh.news.main.repository.NewsRemoteKeysRepository
import com.setnameinc.voenmeh.news.main.repository.NewsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import timber.log.Timber
import java.io.IOException

@ExperimentalPagingApi
class NewsRemoteMediator(
    private val newsApi: NewsApi,
    private val newsRepository: NewsRepository,
    private val newsRemoteKeysRepository: NewsRemoteKeysRepository,
    private val newsSharedPreferences: NewsSharedPreferences
) : RemoteMediator<Int, NewsEntity>() {

    private val STARTING_PAGE = 0

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, NewsEntity>
    ): MediatorResult {
        return try {
            val page: Int = when (loadType) {
                LoadType.REFRESH -> {
                    val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                    remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE
                }
                LoadType.PREPEND -> return MediatorResult.Success(endOfPaginationReached = true)
                LoadType.APPEND -> {
                    val remoteKeys = getRemoteKeyForLastItem(state)
                    if (remoteKeys == null) STARTING_PAGE
                    else remoteKeys.nextKey
                        ?: return MediatorResult.Success(endOfPaginationReached = true)
                }

            }

            // Suspending network load via Retrofit. This doesn't need to be wrapped in a
            // withContext(Dispatcher.IO) { ... } block since Retrofit's Coroutine CallAdapter
            // dispatches on a worker thread.

            return withContext(Dispatchers.Default) {
                val response = newsApi.load(
                    page = page,
                    sources = newsSharedPreferences.getSourceIds().toList()
                ).await().body()?.items
                if (response != null) {
                    val items = response.map { it.fromApi().toDatabase() }
                    val endOfPaginationReached = items.isEmpty()
                    // clear all tables in the database
                    if (loadType == LoadType.REFRESH) {
                        Timber.d("REFRESH, remove all")
                        withContext(Dispatchers.IO) {
                            newsRemoteKeysRepository.clearRemoteKeys()
                            newsRepository.deleteAll()
                        }
                    }

                    val prevKey = if (page == STARTING_PAGE) null else page.minus(1)
                    val nextKey = if (endOfPaginationReached) null else page.plus(1)
                    val keys = items.map {
                        NewsRemoteKey(repoId = it.id, prevKey = prevKey, nextKey = nextKey)
                    }
                    Timber.d("prevKey = $prevKey, page = $page, nextKey = $nextKey")
                    Timber.d("keys = $keys")
                    withContext(Dispatchers.IO) {
                        newsRemoteKeysRepository.insertAll(keys)
                        newsRepository.saveAll(items)
                    }
                    Timber.d("repos = $items")

                    return@withContext MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
                } else {
                    Timber.e("response error")
                    return@withContext MediatorResult.Error(Exception("response error"))
                }
            }
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, NewsEntity>): NewsRemoteKey? {
        // Get the last page that was retrieved, that contained items.
        // From that last page, get the last item
        return state.pages
            .lastOrNull {
                it.data.isNotEmpty()
            }
            ?.data
            ?.lastOrNull()
            ?.let { repo ->
                // Get the remote keys of the last item retrieved
                newsRemoteKeysRepository.remoteKeysRepoId(repo.id)
            }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, NewsEntity>): NewsRemoteKey? {
        // Get the first page that was retrieved, that contained items.
        // From that first page, get the first item
        return state.pages
            .firstOrNull {
                it.data.isNotEmpty()
            }
            ?.data
            ?.firstOrNull()
            ?.let { repo ->
                // Get the remote keys of the first items retrieved
                newsRemoteKeysRepository.remoteKeysRepoId(repo.id)
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, NewsEntity>
    ): NewsRemoteKey? {
        // The paging library is trying to load data after the anchor position
        // Get the item closest to the anchor position
        return state.anchorPosition
            ?.let { position ->
                state.closestItemToPosition(position)
                    ?.id
                    ?.let { repoId ->
                        newsRemoteKeysRepository.remoteKeysRepoId(repoId)
                    }
            }
    }

}