package com.setnameinc.voenmeh.news.main.preferences

import android.content.SharedPreferences

class NewsSharedPreferences(
    private val sharedPreferences: SharedPreferences
) {

    private val SOURCES_ID_KEY = "news.sources.ids"

    fun updateSourceIds(ids: Set<String>) {
        sharedPreferences.edit().apply {
            putStringSet(SOURCES_ID_KEY, ids.toSet())
        }.apply()
    }

    fun getSourceIds(): Set<String> =
        sharedPreferences.getStringSet(SOURCES_ID_KEY, setOf()) ?: setOf()

}