package com.setnameinc.voenmeh.news.main.repository

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "news")
open class NewsEntity(
    @PrimaryKey
    var id: String = "",
    var text: String = "",
    var date: Long = 0,
    var groupOwnerId: String = "",
    var groupName: String = "",
    var groupPhotoUrl: String = ""
)