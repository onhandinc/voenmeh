package com.setnameinc.voenmeh.news.main.repository

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "newsRemoteKey")
data class NewsRemoteKey(
    @PrimaryKey var repoId: String = "",
    var prevKey: Int?,
    var nextKey: Int?
)