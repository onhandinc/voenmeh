package com.setnameinc.voenmeh.news.main.repository

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface NewsRemoteKeysRepository {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKey: List<NewsRemoteKey>)

    @Query("SELECT * FROM newsRemoteKey WHERE repoId = :repoId")
    suspend fun remoteKeysRepoId(repoId: String): NewsRemoteKey?

    @Query("DELETE FROM newsRemoteKey")
    suspend fun clearRemoteKeys()
}