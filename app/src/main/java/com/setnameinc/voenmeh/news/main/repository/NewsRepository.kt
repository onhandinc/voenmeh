package com.setnameinc.voenmeh.news.main.repository

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface NewsRepository {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(entries: List<NewsEntity>)

    @Query("SELECT * FROM news ORDER BY date DESC")
    fun getNews(): PagingSource<Int, NewsEntity>

    @Query("DELETE FROM news")
    fun deleteAll()

}