package com.setnameinc.voenmeh.news.main.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.setnameinc.voenmeh.R

class NewsAdapter(
    private val headedClickListener: (link: String) -> Unit,
    private val clickListener: (id: String) -> Unit
) : PagingDataAdapter<NewsItem, NewsViewHolder>(
    object : DiffUtil.ItemCallback<NewsItem>() {
        override fun areItemsTheSame(
            oldItem: NewsItem,
            newItem: NewsItem
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: NewsItem,
            newItem: NewsItem
        ): Boolean = oldItem == newItem

    }
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder =
        NewsViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_news, parent, false),
            headedClickListener = {
                getItem(it)?.let { item ->
                    headedClickListener(item.groupOwnerId)
                }
            }
        ) {
            getItem(it)?.let { item ->
                clickListener(item.id)
            }
        }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }
}