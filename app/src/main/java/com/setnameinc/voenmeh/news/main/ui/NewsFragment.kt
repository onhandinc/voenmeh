package com.setnameinc.voenmeh.news.main.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.news.main.viewmodel.*
import com.setnameinc.voenmeh.tools.utils.viewModel
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.scope.fragmentScope
import timber.log.Timber

@ExperimentalPagingApi
class NewsFragment : Fragment(R.layout.fragment_news) {

    private val viewModel: NewsViewModel by fragmentScope().viewModel(this)

    private val newsAdapter: NewsAdapter by lazy {
        NewsAdapter(
            headedClickListener = {
                val directTo = "https://vk.com/club${it}"
                Timber.d("direct to ${directTo}")
                startActivity(
                    Intent(Intent.ACTION_VIEW, Uri.parse(directTo))
                )
            }
        ) {
            val directTo = "https://vk.com/feed?w=wall-${it}"
            Timber.d("direct to ${directTo}")
            startActivity(
                Intent(Intent.ACTION_VIEW, Uri.parse(directTo))
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewState(it)
                }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.viewActions()
                .filterNotNull()
                .collect {
                    bindViewAction(it)
                }
        }
        newsAdapter.addLoadStateListener { loadState ->
            lifecycleScope.launchWhenStarted {
                swipeRefreshLayout.isRefreshing =
                    loadState.refresh is LoadState.Loading
                if (newsRecyclerView.scrollState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (loadState.prepend is LoadState.NotLoading && loadState.prepend.endOfPaginationReached) {
                        if (
                            (newsRecyclerView.layoutManager as? LinearLayoutManager)
                                ?.findFirstCompletelyVisibleItemPosition() == 0
                        ) {

                            Timber.d("scroll to top")
                            newsRecyclerView.scrollToPosition(0)
                        }
                    }
                } else {
                    Timber.d("newsRecyclerView scroll is not idle")
                }
            }
        }
        setFragmentResultListener("requestSelectedItems") { key, bundle ->
            Timber.d("receive data")
            bundle.getStringArray("selectedItems")?.let {
                viewModel.obtainEvent(NewsEvent.UpdateSources(it.toList()))
            }
        }
    }

    private suspend fun bindViewState(viewState: NewsViewState) {
        when (viewState) {
            is NewsViewState.NewsLoaded -> {
                when (viewState.fetchStatus) {
                    is FetchStatus.Success -> {
                        lifecycleScope.launchWhenStarted {
                            newsAdapter.submitData(viewState.news)
                        }
                    }
                }
            }
            is NewsViewState.SourcesUpdated -> {
                when (viewState.fetchStatus) {
                    is FetchStatus.Success -> {
                        newsAdapter.refresh()
                    }
                }
            }
        }
    }

    private fun bindViewAction(action: NewsAction) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        newsRecyclerView.apply {
            adapter = newsAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
        swipeRefreshLayout.setOnRefreshListener {
            newsAdapter.refresh()
        }
        sourcesImageButton.setDebounceClickListener(debounceTimeMillis = 1000) {
            findNavController().navigate(R.id.toSelectSourcesDialog)
        }
        infoImageButton.setDebounceClickListener(debounceTimeMillis = 1000) {
            findNavController().navigate(R.id.toNewsAbout)
        }
    }
}