package com.setnameinc.voenmeh.news.main.ui

import android.text.SpannableStringBuilder

data class NewsItem(
    val id: String,
    val text: SpannableStringBuilder,
    val date: String,
    val groupOwnerId: String,
    val groupName: String,
    val groupPhotoUrl: String
)