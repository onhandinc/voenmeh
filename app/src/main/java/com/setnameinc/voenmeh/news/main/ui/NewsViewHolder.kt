package com.setnameinc.voenmeh.news.main.ui

import android.text.method.LinkMovementMethod
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import kotlinx.android.synthetic.main.item_news.view.*

class NewsViewHolder(
    private val view: View,
    private val headedClickListener: (position: Int) -> Unit,
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.ViewHolder(view), View.OnLongClickListener {

    init {
        view.setOnLongClickListener(this)
    }

    fun bind(newsItem: NewsItem) {
        view.apply {
            groupNameTextView.text = newsItem.groupName
            dateTextView.text = newsItem.date
            textTextView.movementMethod = LinkMovementMethod.getInstance()
            textTextView.text = newsItem.text
            textTextView.setOnTouchListener { _, motionEvent ->
                view.onTouchEvent(motionEvent)
                return@setOnTouchListener false
            }
            Glide.with(context)
                .load(newsItem.groupPhotoUrl)
                .transform(CircleCrop())
                .into(groupIconImageView)
            headerRelativeLayout.setOnLongClickListener {
                headedClickListener(absoluteAdapterPosition)
                return@setOnLongClickListener true
            }
        }
    }

    override fun onLongClick(p0: View?): Boolean {
        clickListener(absoluteAdapterPosition)
        return true
    }

}