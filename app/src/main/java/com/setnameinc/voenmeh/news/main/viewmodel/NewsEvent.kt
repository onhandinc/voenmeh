package com.setnameinc.voenmeh.news.main.viewmodel

sealed class NewsEvent {
    data class UpdateSources(val ids: List<String>) : NewsEvent()
}