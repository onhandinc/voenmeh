package com.setnameinc.voenmeh.news.main.viewmodel

import android.app.Application
import androidx.lifecycle.viewModelScope
import androidx.paging.*
import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.news.main.api.NewsApi
import com.setnameinc.voenmeh.news.main.domain.NewsConverter.fromDatabase
import com.setnameinc.voenmeh.news.main.domain.NewsConverter.toItem
import com.setnameinc.voenmeh.news.main.domain.NewsRemoteMediator
import com.setnameinc.voenmeh.news.main.preferences.NewsSharedPreferences
import com.setnameinc.voenmeh.news.main.repository.NewsRemoteKeysRepository
import com.setnameinc.voenmeh.news.main.repository.NewsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@ExperimentalPagingApi
class NewsViewModel(
    private val application: Application,
    private val newsApi: NewsApi,
    private val newsRepository: NewsRepository,
    private val newsRemoteKeysRepository: NewsRemoteKeysRepository,
    private val newsSharedPreferences: NewsSharedPreferences
) : BaseFlowViewModel<NewsViewState, NewsAction, NewsEvent>() {

    init {
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                Pager(
                    config = PagingConfig(pageSize = 10, enablePlaceholders = false),
                    remoteMediator = NewsRemoteMediator(
                        newsApi = newsApi,
                        newsRepository = newsRepository,
                        newsRemoteKeysRepository = newsRemoteKeysRepository,
                        newsSharedPreferences = newsSharedPreferences
                    ),
                    pagingSourceFactory = { newsRepository.getNews() },
                    initialKey = null
                )
                    .flow
                    .map { pagingData ->
                        pagingData.map { it.fromDatabase().toItem(application) }
                    }
                    .cachedIn(viewModelScope)
                    .collectLatest {
                        viewState = NewsViewState.NewsLoaded(FetchStatus.Success, it)
                    }
            }
        }
    }

    override suspend fun obtainInternalEvent(viewEvent: NewsEvent) {
        when (viewEvent) {
            is NewsEvent.UpdateSources -> {
                newsSharedPreferences.updateSourceIds(viewEvent.ids.toSet())
                viewState = NewsViewState.SourcesUpdated(FetchStatus.Success)
            }
        }
    }
}