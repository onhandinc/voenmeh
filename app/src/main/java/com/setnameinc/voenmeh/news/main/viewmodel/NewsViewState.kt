package com.setnameinc.voenmeh.news.main.viewmodel

import androidx.paging.PagingData
import com.setnameinc.voenmeh.news.main.ui.NewsItem

sealed class NewsViewState {
    data class NewsLoaded(
        val fetchStatus: FetchStatus,
        val news: PagingData<NewsItem>
    ) : NewsViewState()

    data class SourcesUpdated(
        val fetchStatus: FetchStatus
    ) : NewsViewState()
}

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}