package com.setnameinc.voenmeh.news.selectsource.di

import com.setnameinc.voenmeh.news.selectsource.ui.NewsManageSourceBottomDialog
import com.setnameinc.voenmeh.news.selectsource.viewmodel.NewsManageSourceViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object NewsManageSourceModule {
    val module = module {
        scope<NewsManageSourceBottomDialog> {
            viewModel {
                NewsManageSourceViewModel(newSharedPreferences = get())
            }
        }
    }
}