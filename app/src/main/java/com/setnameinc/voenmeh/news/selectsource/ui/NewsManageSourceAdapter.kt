package com.setnameinc.voenmeh.news.selectsource.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.setnameinc.voenmeh.R
import timber.log.Timber

class NewsManageSourceAdapter(
    private val clickListener: (index: Int) -> Unit
) : ListAdapter<NewsManageSourceItem, NewsManageSourceViewHolder>(
    object : DiffUtil.ItemCallback<NewsManageSourceItem>() {
        override fun areItemsTheSame(
            oldItem: NewsManageSourceItem,
            newItem: NewsManageSourceItem
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: NewsManageSourceItem,
            newItem: NewsManageSourceItem
        ): Boolean = oldItem == newItem

    }
) {

    private val PAYLOAD_SELECTION_CHANGED = "PAYLOAD_SELECTION_CHANGED"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsManageSourceViewHolder =
        NewsManageSourceViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_news_select_source, parent, false),
            clickListener
        )

    override fun onBindViewHolder(
        holder: NewsManageSourceViewHolder,
        position: Int,
        payloads: MutableList<Any>
    ) {
        Timber.d("onBindViewHolder with payloads")
        if (payloads.isEmpty()) {
            Timber.d("pos $position, payloads is empty")
            super.onBindViewHolder(holder, position, payloads)
        } else {
            payloads.filterIsInstance<String>()
                .forEach {
                    Timber.d("pos $position payload = $it")
                    when (it) {
                        PAYLOAD_SELECTION_CHANGED -> {
                            holder.bindSelection(getItem(position).isSelected)
                        }
                    }
                }
        }
    }

    fun updateItemSelection(position: Int, isSelected: Boolean) {
        currentList[position].isSelected = isSelected
        notifyItemChanged(position, PAYLOAD_SELECTION_CHANGED)
    }

    override fun onBindViewHolder(holder: NewsManageSourceViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }
}