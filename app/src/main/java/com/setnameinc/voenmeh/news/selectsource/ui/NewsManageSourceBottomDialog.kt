package com.setnameinc.voenmeh.news.selectsource.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.tools.base.BaseBottomRoundSheetDialogFragment
import com.setnameinc.voenmeh.news.selectsource.viewmodel.NewsManageSourceAction
import com.setnameinc.voenmeh.news.selectsource.viewmodel.NewsManageSourceEvent
import com.setnameinc.voenmeh.news.selectsource.viewmodel.NewsManageSourceViewModel
import com.setnameinc.voenmeh.tools.utils.viewModel
import kotlinx.android.synthetic.main.dialog_news_select_sources.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.scope.fragmentScope

class NewsManageSourceBottomDialog :
    BaseBottomRoundSheetDialogFragment(R.layout.dialog_news_select_sources) {

    private val viewModel: NewsManageSourceViewModel by fragmentScope().viewModel(this)

    private val newsManageSourceAdapter: NewsManageSourceAdapter by lazy {
        NewsManageSourceAdapter {
            viewModel.obtainEvent(NewsManageSourceEvent.Select(it))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.viewActions()
                .filterNotNull()
                .collect {
                    bindAction(it)
                }
        }
    }

    private fun bindAction(action: NewsManageSourceAction) {
        when (action) {
            is NewsManageSourceAction.ChangeSelection -> {
                newsManageSourceAdapter.updateItemSelection(action.id, action.isSelected)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sourcesRecyclerView.apply {
            adapter = newsManageSourceAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
        newsManageSourceAdapter.submitList(viewModel.items)
        closeDialogImageView.setDebounceClickListener {
            dismiss()
        }
    }

    override fun onDestroyView() {
        setFragmentResult(
            "requestSelectedItems",
            Bundle().apply {
                putStringArray(
                    "selectedItems",
                    viewModel.items
                        .filter {
                            it.isSelected
                        }
                        .map {
                            it.id
                        }
                        .toTypedArray()
                )
            }
        )
        super.onDestroyView()
    }

}