package com.setnameinc.voenmeh.news.selectsource.ui

data class NewsManageSourceItem(
    val id: String,
    val iconUrl: String,
    val name: String,
    var isSelected: Boolean
)