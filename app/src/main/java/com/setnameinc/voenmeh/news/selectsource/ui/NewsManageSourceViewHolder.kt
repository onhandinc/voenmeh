package com.setnameinc.voenmeh.news.selectsource.ui

import android.view.View
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.setnameinc.voenmeh.R
import kotlinx.android.synthetic.main.item_news_select_source.view.*
import timber.log.Timber

class NewsManageSourceViewHolder(
    private val view: View,
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.ViewHolder(view), View.OnClickListener {

    init {
        view.setOnClickListener(this)
    }

    fun bind(newsItem: NewsManageSourceItem) {
        Timber.d("newsItem = $newsItem")
        view.apply {
            nameTextView.text = newsItem.name
            Glide.with(context)
                .load(newsItem.iconUrl)
                .transform(CircleCrop())
                .into(iconImageView)
            if (newsItem.isSelected) {
                selectedImageView.visibility = View.VISIBLE
            } else {
                selectedImageView.visibility = View.INVISIBLE
            }
        }
    }

    fun bindSelection(isSelected: Boolean) {
        view.apply {
            if (isSelected) {
                selectedImageView.visibility = View.VISIBLE
                selectedImageView
                    .startAnimation(
                        AnimationUtils.loadAnimation(
                            context,
                            R.anim.appear_visible
                        )
                    )
            } else {
                selectedImageView.startAnimation(
                    AnimationUtils.loadAnimation(
                        context,
                        R.anim.appear_invisible
                    )
                )
                selectedImageView.visibility = View.INVISIBLE
            }
        }
    }

    override fun onClick(p0: View?) {
        clickListener(absoluteAdapterPosition)
    }

}