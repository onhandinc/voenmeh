package com.setnameinc.voenmeh.news.selectsource.viewmodel

sealed class NewsManageSourceAction {
    class ChangeSelection(val id: Int, val isSelected: Boolean) : NewsManageSourceAction()
}