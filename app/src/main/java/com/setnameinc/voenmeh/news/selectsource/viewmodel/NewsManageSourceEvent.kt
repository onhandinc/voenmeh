package com.setnameinc.voenmeh.news.selectsource.viewmodel

sealed class NewsManageSourceEvent {
    class Select(val id: Int) : NewsManageSourceEvent()
}