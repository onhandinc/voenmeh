package com.setnameinc.voenmeh.news.selectsource.viewmodel

import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.news.main.preferences.NewsSharedPreferences
import com.setnameinc.voenmeh.news.selectsource.ui.NewsManageSourceItem

class NewsManageSourceViewModel(
    private val newSharedPreferences: NewsSharedPreferences
) : BaseFlowViewModel<NewsManageSourceViewState, NewsManageSourceAction, NewsManageSourceEvent>() {

    val items: List<NewsManageSourceItem> = listOf(
        NewsManageSourceItem(
            "150938772",
            "https://sun1-99.userapi.com/s/v1/ig2/h3aksIX6Ii7BSarniQks2BygasjkCVGEfXAJnKeqnnha8FsljGyREHSPCP4DjnmexNxxkFBGK_vQV6fn3SIykVS8.jpg?size=50x0&quality=96&crop=496,496,1178,1178&ava=1",
            "BSTU MEDIA | БГТУ «ВОЕНМЕХ»",
            true
        ),
        NewsManageSourceItem(
            "34484589",
            "https://sun1-98.userapi.com/s/v1/ig1/rX9-lifc-zJpjOQKkhoe8le8-anzNE1TYt_dbOY_aEOvdb81KYZH9BQax0TJNwlfcg4or0Ek.jpg?size=50x0&quality=96&crop=365,314,1281,1281&ava=1",
            "Студенческий Совет БГТУ «ВОЕНМЕХ»",
            true
        ),
        NewsManageSourceItem(
            "89863663",
            "https://sun1-47.userapi.com/s/v1/if1/h6o0u93vZ_JsGnxj72Y8SkJFhYOUDQ6jooZPNAazS3sMS9L69Sh6VhKQBkMLRcE0UAOPUBY4.jpg?size=50x0&quality=96&crop=670,278,570,570&ava=1",
            "Деканат «И» Факультета БГТУ «ВОЕНМЕХ»",
            true
        ),
        NewsManageSourceItem(
            "129029245",
            "https://sun1-83.userapi.com/s/v1/if1/AK35MCtWktRRGECATh_GAPpUstnUAWjmLJa81_Pm-OE5j3KuazRRfCJ5B-1_wgkh87C9vSx7.jpg?size=50x0&quality=96&crop=98,132,473,473&ava=1",
            "ДЕКАНАТ Е",
            true
        ),
        NewsManageSourceItem(
            "47398715",
            "https://sun1-95.userapi.com/s/v1/if2/58d1d6X6aNC8iCf7InlNCYuTbvBWbfSNkFKq4y62tvvleQRqcNiVW7A18zL4zKTEI5HWmKyuzTLMnuxuuiZqelEz.jpg?size=50x0&quality=96&crop=228,56,957,957&ava=1",
            "БГТУ «ВОЕНМЕХ» кафедра И5",
            true
        ),
        NewsManageSourceItem(
            "107415708",
            "https://sun1-20.userapi.com/s/v1/if1/coCVc_7nhTrk-oTqYcM_Ch74BTDJtdOMR9jIIPWMEwWMvr8QVd3ItHlk38fPG0pDcr6G7hy3.jpg?size=50x0&quality=96&crop=313,442,1502,1502&ava=1",
            "ПРОФЕССИОНАЛЬНЫЙ СОЮЗ РАБОТНИКОВ И ОБУЧАЮЩИХСЯ",
            true
        )
    ).apply {
        val selected = newSharedPreferences.getSourceIds()
        this.forEach {
            it.isSelected = selected.contains(it.id)
        }
    }

    override suspend fun obtainInternalEvent(viewEvent: NewsManageSourceEvent) {
        when (viewEvent) {
            is NewsManageSourceEvent.Select -> {
                items[viewEvent.id].isSelected = !items[viewEvent.id].isSelected
                viewAction = NewsManageSourceAction.ChangeSelection(
                    viewEvent.id,
                    items[viewEvent.id].isSelected
                )
            }
        }
    }

}