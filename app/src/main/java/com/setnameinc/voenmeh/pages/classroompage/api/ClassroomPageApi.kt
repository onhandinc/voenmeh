package com.setnameinc.voenmeh.pages.classroompage.api

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ClassroomPageApi {

    @GET("classroom/{classroomName}")
    fun loadByName(
        @Path("classroomName") classroomName: String
    ): Deferred<Response<List<LessonResponse>>>

}