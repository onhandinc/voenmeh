package com.setnameinc.voenmeh.pages.classroompage.api

import com.google.gson.annotations.SerializedName

data class LessonResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("classroom")
    val classroom: String,
    @SerializedName("classroomName")
    val classroomName: String,
    @SerializedName("numberDayInWeek")
    val numberDayInWeek: Int,
    @SerializedName("discipline")
    val discipline: String,
    @SerializedName("disciplineName")
    val disciplineName: String,
    @SerializedName("lecturers")
    val lecturers: String,
    @SerializedName("lecturersId")
    val lecturersId: List<String>,
    @SerializedName("startTime")
    val startTime: Long,
    @SerializedName("startTimeText")
    val startTimeText: String,
    @SerializedName("endTimeText")
    val endTimeText: String,
    @SerializedName("groupsId")
    val groupIds: List<String>,
    @SerializedName("groups")
    val groups: String,
    @SerializedName("weekCode")
    val weekCode: Int,
    @SerializedName("type")
    val type: String
)