package com.setnameinc.voenmeh.pages.classroompage.di

import com.setnameinc.voenmeh.app.api.RetrofitConst
import com.setnameinc.voenmeh.pages.classroompage.api.ClassroomPageApi
import com.setnameinc.voenmeh.pages.classroompage.domain.ClassroomPageInteractor
import com.setnameinc.voenmeh.pages.classroompage.ui.ClassroomPageFragment
import com.setnameinc.voenmeh.pages.classroompage.viewmodel.ClassroomPageViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

object ClassroomPageModule {
    val module = module {
        scope<ClassroomPageFragment> {
            viewModel {
                ClassroomPageViewModel(classroomPageInteractor = get())
            }
            scoped {
                ClassroomPageInteractor(classroomPageApi = get())
            }
            scoped {
                get<Retrofit>(
                    named(RetrofitConst.VOENMEH_SERVER_RETROFIT_INJECTION_KEY)
                ).create(ClassroomPageApi::class.java)
            }
        }
    }
}