package com.setnameinc.voenmeh.pages.classroompage.domain

import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.pages.classroompage.api.LessonResponse
import com.setnameinc.voenmeh.pages.classroompage.ui.ClassroomPageTimetableItem
import com.setnameinc.voenmeh.tools.utils.DayTitleFromWeekNumber

object ClassroomPageConverter {

    fun List<LessonResponse>.fromApi(): List<ClassroomPageLessonModel> =
        this.map {
            ClassroomPageLessonModel(
                id = it.id,
                classroom = it.classroom,
                classroomName = it.classroomName,
                numberDayInWeek = it.numberDayInWeek,
                discipline = it.discipline,
                disciplineName = it.disciplineName,
                lecturers = it.lecturers,
                lecturersId = it.lecturersId,
                startTime = it.startTime,
                startTimeText = it.startTimeText,
                endTimeText = it.endTimeText,
                groupIds = it.groupIds,
                groups = it.groups,
                weekCode = it.weekCode,
                type = it.type
            )
        }

    fun List<ClassroomPageLessonModel>.toItem(weekCode: Int): List<ClassroomPageTimetableItem> =
        this.filter {
            it.weekCode == weekCode
        }
            .sortedBy {
                it.numberDayInWeek
            }
            .groupBy {
                Day(
                    DayTitleFromWeekNumber.getDayTitleFromWeekNumber(it.numberDayInWeek),
                    it.numberDayInWeek
                )
            }
            .flatMap { pair ->
                createTimetableToDay(
                    pair.key,
                    pair.value.sortedBy { it.startTime }
                )
            }

    private fun createTimetableToDay(
        day: Day,
        list: List<ClassroomPageLessonModel>
    ): List<ClassroomPageTimetableItem> =
        mutableListOf<ClassroomPageTimetableItem>().apply {
            day.title?.let {
                add(ClassroomPageTimetableItem.TitleTimetable(it, day.numberDayInWeek))
            }
            addAll(
                list.map {
                    when {
                        it.discipline.isEmpty() -> {
                            convertLessonToBodyWithEmptySubject(it)
                        }
                        it.lecturers.isEmpty() -> {
                            convertToBodyWithEmptyLecturers(it)
                        }
                        else -> {
                            convertToBody(it)
                        }
                    }
                }
            )
        }

    private fun convertToBody(
        item: ClassroomPageLessonModel
    ): ClassroomPageTimetableItem.BodyTimetable =
        ClassroomPageTimetableItem.BodyTimetable(
            id = item.id,
            classroom = item.classroom,
            classroomName = item.classroomName,
            subject = item.discipline,
            subjectName = item.disciplineName,
            startTime = item.startTimeText,
            endTime = item.endTimeText,
            lecturers = item.lecturers,
            classroomBackgroundId = generateClassroomBackgroundId(
                item.type
            )
        )

    private fun convertToBodyWithEmptyLecturers(
        lesson: ClassroomPageLessonModel
    ): ClassroomPageTimetableItem.BodyWithEmptyLecturers =
        ClassroomPageTimetableItem.BodyWithEmptyLecturers(
            id = lesson.id,
            classroom = lesson.classroom,
            classroomName = lesson.classroomName,
            subject = lesson.discipline,
            subjectName = lesson.disciplineName,
            startTime = lesson.startTimeText,
            endTime = lesson.endTimeText,
            classroomBackgroundId = generateClassroomBackgroundId(
                lesson.type
            )
        )

    private fun convertLessonToBodyWithEmptySubject(
        lesson: ClassroomPageLessonModel
    ): ClassroomPageTimetableItem.BodyWithEmptySubject =
        ClassroomPageTimetableItem.BodyWithEmptySubject(
            id = lesson.id,
            classroom = lesson.classroom,
            classroomName = lesson.classroomName,
            startTime = lesson.startTimeText,
            endTime = lesson.endTimeText,
            lecturers = lesson.lecturers,
            classroomBackgroundId = generateClassroomBackgroundId(
                lesson.type
            )
        )

    private fun generateClassroomBackgroundId(subjectType: String): Int =
        when (subjectType) {
            "practise" -> {
                R.drawable.shape_timetable_class_type_practise_background
            }
            "laboratory" -> {
                R.drawable.shape_timetable_class_type_laboratory_background
            }
            "consultation" -> {
                R.drawable.shape_timetable_class_type_consultation_background
            }
            else -> {
                R.drawable.shape_timetable_class_type_lecture_background
            }
        }

    private data class Day(
        val title: String?,
        val numberDayInWeek: Int
    )
}