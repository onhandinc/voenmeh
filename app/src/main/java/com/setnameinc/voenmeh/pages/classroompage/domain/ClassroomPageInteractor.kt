package com.setnameinc.voenmeh.pages.classroompage.domain

import com.setnameinc.voenmeh.pages.classroompage.api.ClassroomPageApi
import com.setnameinc.voenmeh.pages.classroompage.api.LessonResponse
import com.setnameinc.voenmeh.pages.classroompage.domain.ClassroomPageConverter.fromApi
import com.setnameinc.voenmeh.pages.classroompage.domain.ClassroomPageConverter.toItem
import com.setnameinc.voenmeh.pages.classroompage.ui.ClassroomPageTimetableItem
import com.setnameinc.voenmeh.timetable.common.domain.TimetableWeekType

class ClassroomPageInteractor(private val classroomPageApi: ClassroomPageApi) {

    suspend fun load(
        name: String,
        completeEven: (List<ClassroomPageTimetableItem>) -> Unit,
        completeOdd: (List<ClassroomPageTimetableItem>) -> Unit,
        error: () -> Unit
    ) {
        val response: List<LessonResponse>? = classroomPageApi.loadByName(name).await().body()
        if (response != null) {
            completeEven(
                response.fromApi().toItem(TimetableWeekType.EVEN.weekCode)
            )
            completeOdd(
                response.fromApi().toItem(TimetableWeekType.ODD.weekCode)
            )
        } else {
            error()
        }
    }

}