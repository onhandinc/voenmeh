package com.setnameinc.voenmeh.pages.classroompage.domain

data class ClassroomPageLessonModel(
    val id: String,
    val classroom: String,
    val classroomName: String,
    val numberDayInWeek: Int,
    val discipline: String,
    val disciplineName: String,
    val lecturers: String,
    val lecturersId: List<String>,
    val startTime: Long,
    val startTimeText: String,
    val endTimeText: String,
    val groupIds: List<String>,
    val groups: String,
    val weekCode: Int,
    val type: String
)