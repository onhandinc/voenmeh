package com.setnameinc.voenmeh.pages.classroompage.ui

import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.item_timetable_body_empty_lecturers.view.*

class ClassroomPageBodyEmptyLecturersViewHolder(
    private val view: View
) : ClassroomPageTimetableViewHolder(view = view) {

    override fun bind(item: ClassroomPageTimetableItem) {
        item as ClassroomPageTimetableItem.BodyWithEmptyLecturers
        view.apply {
            timeTextView.text = item.startTime
            subjectTextView.text = item.subjectName
            classroomTextView.background = ContextCompat.getDrawable(
                context,
                item.classroomBackgroundId
            )
            classroomTextView.text = item.classroomName
            endTimeTextView.text = item.endTime
        }
    }

}