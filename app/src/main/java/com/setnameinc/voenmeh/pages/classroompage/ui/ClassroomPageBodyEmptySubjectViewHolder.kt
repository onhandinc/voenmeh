package com.setnameinc.voenmeh.pages.classroompage.ui

import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.item_timetable_body_empty_subject.view.*

class ClassroomPageBodyEmptySubjectViewHolder(
    private val view: View
) : ClassroomPageTimetableViewHolder(view = view) {

    override fun bind(item: ClassroomPageTimetableItem) {
        item as ClassroomPageTimetableItem.BodyWithEmptySubject
        view.apply {
            timeTextView.text = item.startTime
            classroomTextView.background = ContextCompat.getDrawable(
                context,
                item.classroomBackgroundId
            )
            classroomTextView.text = item.classroomName
            lecturerTextView.text = item.lecturers
            endTimeTextView.text = item.endTime
        }
    }
}