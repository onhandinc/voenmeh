package com.setnameinc.voenmeh.pages.classroompage.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.tools.base.BsaeProgressFragment
import com.setnameinc.voenmeh.pages.classroompage.viewmodel.ClassroomPageEvent
import com.setnameinc.voenmeh.pages.classroompage.viewmodel.ClassroomPageViewModel
import com.setnameinc.voenmeh.pages.classroompage.viewmodel.ClassroomPageViewState
import com.setnameinc.voenmeh.pages.classroompage.viewmodel.FetchStatus
import com.setnameinc.voenmeh.tools.base.BsaeScopedProgressFragment
import com.setnameinc.voenmeh.tools.utils.viewModel
import kotlinx.android.synthetic.main.fragment_classroom_page.*
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.scope.fragmentScope
import timber.log.Timber

class ClassroomPageFragment : BsaeScopedProgressFragment(R.layout.fragment_classroom_page) {

    private val EVEN_WEEK_POSITION = 0
    private val ODD_WEEK_POSITION = 1

    private val viewModel: ClassroomPageViewModel by fragmentScope.viewModel(this)

    private val evenLoadListener: CompletableDeferred<Boolean> = CompletableDeferred()
    private val oddLoadListener: CompletableDeferred<Boolean> = CompletableDeferred()

    private val timetableEvenFragment: ClassroomPageTimetableEvenFragment by lazy {
        ClassroomPageTimetableEvenFragment(evenLoadListener)
    }

    private val timetableOddFragment: ClassroomPageTimetableOddFragment by lazy {
        ClassroomPageTimetableOddFragment(oddLoadListener)
    }

    private val classroomName: String by lazy {
        arguments?.getString("classroomName") ?: ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewState(it)
                }
        }
        lifecycleScope.launchWhenStarted {
            awaitAll(evenLoadListener, oddLoadListener)
            hideProgressBar()
        }
    }

    private fun bindViewState(viewState: ClassroomPageViewState) {
        when (viewState) {
            is ClassroomPageViewState.EvenWeekLoaded -> {
                when (viewState.fetchStatus) {
                    is FetchStatus.Success -> {
                        viewState.lessons?.let {
                            Timber.d("even: $it")
                            timetableEvenFragment.submitList(it)
                        }
                    }
                    is FetchStatus.Loading -> {

                    }
                    is FetchStatus.Error -> {

                    }
                }
            }
            is ClassroomPageViewState.OddWeekLoaded -> {
                when (viewState.fetchStatus) {
                    is FetchStatus.Success -> {
                        viewState.lessons?.let {
                            Timber.d("odd: $it")
                            timetableOddFragment.submitList(it)
                        }
                    }
                    is FetchStatus.Loading -> {

                    }
                    is FetchStatus.Error -> {

                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        classroomNameTextView.text = "Кабинет ${classroomName}"
        if (viewModel.classroomName.isEmpty()) {
            viewModel.obtainEvent(ClassroomPageEvent.Load(classroomName))
        }
        timetableViewPager.apply {
            adapter = object : FragmentStateAdapter(
                this@ClassroomPageFragment.childFragmentManager,
                viewLifecycleOwner.lifecycle
            ) {

                override fun getItemCount(): Int = 2

                override fun createFragment(position: Int): Fragment =
                    when (position) {
                        EVEN_WEEK_POSITION -> timetableEvenFragment
                        ODD_WEEK_POSITION -> timetableOddFragment
                        else -> throw Exception()
                    }

            }
            offscreenPageLimit = 2
            (getChildAt(0) as RecyclerView).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
        }
        TabLayoutMediator(timetableWeekTabLayout, timetableViewPager) { tab, position ->
            when (position) {
                EVEN_WEEK_POSITION -> {
                    tab.text = "Четная"
                }
                ODD_WEEK_POSITION -> {
                    tab.text = "Нечетная"
                }
            }
        }.attach()
    }

}