package com.setnameinc.voenmeh.pages.classroompage.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.setnameinc.voenmeh.R

class ClassroomPageTimetableAdapter :
    ListAdapter<ClassroomPageTimetableItem, ClassroomPageTimetableViewHolder>(
        object : DiffUtil.ItemCallback<ClassroomPageTimetableItem>() {
            override fun areItemsTheSame(
                oldItem: ClassroomPageTimetableItem,
                newItem: ClassroomPageTimetableItem
            ): Boolean = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: ClassroomPageTimetableItem,
                newItem: ClassroomPageTimetableItem
            ): Boolean = oldItem.id == newItem.id && oldItem.type == newItem.type

        }
    ) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ClassroomPageTimetableViewHolder = when (viewType) {
        TimetableTypes.TITLE ->
            ClassroomPageTimetableTitleViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_timetable_title, parent, false)
            )
        TimetableTypes.BODY -> {
            ClassroomPageTimetableBodyViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_timetable_body, parent, false)
            )
        }
        TimetableTypes.BODY_WITH_EMPTY_SUBJECT -> {
            ClassroomPageBodyEmptySubjectViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_timetable_body_empty_subject, parent, false)
            )
        }
        else -> {
            ClassroomPageBodyEmptyLecturersViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(
                        R.layout.item_timetable_body_empty_lecturers,
                        parent,
                        false
                    )
            )
        }
    }

    override fun onBindViewHolder(holder: ClassroomPageTimetableViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    override fun getItemViewType(position: Int): Int = currentList[position].type

}