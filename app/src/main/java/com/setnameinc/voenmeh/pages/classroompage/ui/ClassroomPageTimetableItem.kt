package com.setnameinc.voenmeh.pages.classroompage.ui

sealed class ClassroomPageTimetableItem(
    open val id: String,
    open val type: Int
) {

    data class TitleTimetable(
        val title: String,
        val dayNumberInWeek: Int,
        val isSelected: Boolean = false
    ) : ClassroomPageTimetableItem(
        type = TimetableTypes.TITLE,
        id = dayNumberInWeek.toString()
    )

    data class BodyTimetable(
        override val id: String,
        val classroom: String,
        val classroomName: String,
        val classroomBackgroundId: Int,
        val subject: String,
        val subjectName: String,
        val lecturers: String,
        val startTime: String,
        val endTime: String
    ) : ClassroomPageTimetableItem(
        type = TimetableTypes.BODY,
        id = id
    )

    data class BodyWithEmptyLecturers(
        override val id: String,
        val classroom: String,
        val classroomName: String,
        val classroomBackgroundId: Int,
        val subject: String,
        val subjectName: String,
        val startTime: String,
        val endTime: String
    ) : ClassroomPageTimetableItem(
        type = TimetableTypes.BODY_WITH_EMPTY_LECTURERS,
        id = id
    )

    data class BodyWithEmptySubject(
        override val id: String,
        val classroom: String,
        val classroomName: String,
        val classroomBackgroundId: Int,
        val lecturers: String,
        val startTime: String,
        val endTime: String
    ) : ClassroomPageTimetableItem(
        type = TimetableTypes.BODY_WITH_EMPTY_SUBJECT,
        id = id
    )
}

object TimetableTypes {
    const val TITLE = 0
    const val BODY = 1
    const val BODY_WITH_EMPTY_LECTURERS = 2
    const val BODY_WITH_EMPTY_SUBJECT = 3
}