package com.setnameinc.voenmeh.pages.classroompage.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.setnameinc.voenmeh.R
import kotlinx.android.synthetic.main.fragment_classroom_page_timetable_odd_week.*
import kotlinx.coroutines.CompletableDeferred

class ClassroomPageTimetableOddFragment(
    private val loadListener: CompletableDeferred<Boolean>
) : Fragment(R.layout.fragment_classroom_page_timetable_odd_week) {

    private val timetableAdapter: ClassroomPageTimetableAdapter by lazy {
        ClassroomPageTimetableAdapter().apply {
            stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        timetableRecyclerView.apply {
            adapter = timetableAdapter
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            itemAnimator = null
        }
    }

    fun submitList(list: List<ClassroomPageTimetableItem>) {
        timetableAdapter.submitList(list) {
            loadListener.complete(true)
        }
    }

}