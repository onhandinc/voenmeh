package com.setnameinc.voenmeh.pages.classroompage.ui

import android.view.View
import kotlinx.android.synthetic.main.item_timetable_title.view.*

class ClassroomPageTimetableTitleViewHolder(
    private val view: View
) : ClassroomPageTimetableViewHolder(view = view) {

    override fun bind(item: ClassroomPageTimetableItem) {
        val localeItem = item as ClassroomPageTimetableItem.TitleTimetable
        view.apply {
            titleTextView.text = localeItem.title
            currentDayIndicatorView.visibility = if (item.isSelected) {
                View.VISIBLE
            } else {
                View.INVISIBLE
            }
        }
    }
}