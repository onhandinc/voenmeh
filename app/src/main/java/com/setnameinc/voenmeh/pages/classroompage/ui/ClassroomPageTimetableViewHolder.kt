package com.setnameinc.voenmeh.pages.classroompage.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class ClassroomPageTimetableViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bind(item: ClassroomPageTimetableItem)
}