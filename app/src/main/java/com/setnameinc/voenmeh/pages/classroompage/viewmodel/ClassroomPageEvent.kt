package com.setnameinc.voenmeh.pages.classroompage.viewmodel

sealed class ClassroomPageEvent {
    data class Load(val name: String) : ClassroomPageEvent()
}