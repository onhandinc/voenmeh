package com.setnameinc.voenmeh.pages.classroompage.viewmodel

import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.pages.classroompage.domain.ClassroomPageInteractor

class ClassroomPageViewModel(
    private val classroomPageInteractor: ClassroomPageInteractor
) : BaseFlowViewModel<ClassroomPageViewState, ClassroomPageAction, ClassroomPageEvent>() {

    var classroomName: String = ""

    override suspend fun obtainInternalEvent(viewEvent: ClassroomPageEvent) {
        when (viewEvent) {
            is ClassroomPageEvent.Load -> {
                if (viewEvent.name.isNotEmpty()) {
                    classroomPageInteractor.load(
                        viewEvent.name,
                        completeEven = {
                            viewState = ClassroomPageViewState.EvenWeekLoaded(
                                FetchStatus.Success,
                                it
                            )
                        },
                        completeOdd = {
                            viewState = ClassroomPageViewState.OddWeekLoaded(
                                FetchStatus.Success,
                                it
                            )
                        },
                        error = {
                            viewState = ClassroomPageViewState.EvenWeekLoaded(
                                FetchStatus.Error
                            )
                            viewState = ClassroomPageViewState.OddWeekLoaded(
                                FetchStatus.Error
                            )
                        }
                    )
                }
            }
        }
    }

}