package com.setnameinc.voenmeh.pages.classroompage.viewmodel

import com.setnameinc.voenmeh.pages.classroompage.ui.ClassroomPageTimetableItem

sealed class ClassroomPageViewState {

    data class EvenWeekLoaded(
        val fetchStatus: FetchStatus,
        val lessons: List<ClassroomPageTimetableItem>? = null
    ) : ClassroomPageViewState()

    data class OddWeekLoaded(
        val fetchStatus: FetchStatus,
        val lessons: List<ClassroomPageTimetableItem>? = null
    ) : ClassroomPageViewState()
}

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}