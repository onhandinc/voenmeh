package com.setnameinc.voenmeh.pages.grouppage.api

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface GroupPageApi {
    @GET("group/{groupId}")
    fun load(
        @Path("groupId") lecturerId: String
    ): Deferred<Response<GroupPageResponse>>
}