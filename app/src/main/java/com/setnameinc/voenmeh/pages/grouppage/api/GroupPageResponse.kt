package com.setnameinc.voenmeh.pages.grouppage.api

import com.google.gson.annotations.SerializedName

data class GroupPageResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("number")
    val number: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("lessons")
    val lessons: List<LessonResponse>
)