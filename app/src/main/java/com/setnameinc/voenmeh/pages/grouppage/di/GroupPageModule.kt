package com.setnameinc.voenmeh.pages.grouppage.di

import com.setnameinc.voenmeh.app.api.RetrofitConst
import com.setnameinc.voenmeh.pages.grouppage.api.GroupPageApi
import com.setnameinc.voenmeh.pages.grouppage.domain.GroupPageInteractor
import com.setnameinc.voenmeh.pages.grouppage.ui.GroupPageFragment
import com.setnameinc.voenmeh.pages.grouppage.viewmodel.GroupPageViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

object GroupPageModule {
    val module = module {
        scope<GroupPageFragment> {
            viewModel {
                GroupPageViewModel(
                    groupPageInteractor = get()
                )
            }
            scoped {
                GroupPageInteractor(
                    groupPageApi = get()
                )
            }
            scoped {
                get<Retrofit>(
                    named(RetrofitConst.VOENMEH_SERVER_RETROFIT_INJECTION_KEY)
                ).create(GroupPageApi::class.java)
            }
        }
    }
}