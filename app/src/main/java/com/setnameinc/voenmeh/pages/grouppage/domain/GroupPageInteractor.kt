package com.setnameinc.voenmeh.pages.grouppage.domain

import com.setnameinc.voenmeh.pages.grouppage.api.GroupPageApi
import com.setnameinc.voenmeh.pages.grouppage.domain.GroupPageConverter.fromApi
import com.setnameinc.voenmeh.pages.grouppage.domain.GroupPageConverter.toItem
import com.setnameinc.voenmeh.pages.grouppage.ui.GroupPageTimetableItem
import com.setnameinc.voenmeh.timetable.common.domain.TimetableWeekType

class GroupPageInteractor(private val groupPageApi: GroupPageApi) {

    suspend fun load(
        groupId: String,
        completeEven: (List<GroupPageTimetableItem>) -> Unit,
        completeOdd: (List<GroupPageTimetableItem>) -> Unit,
        completeName: (String) -> Unit,
        error: () -> Unit
    ) {
        val response = groupPageApi.load(groupId).await().body()
        if (response != null) {
            completeName(response.name)
            completeEven(response.lessons.fromApi().toItem(TimetableWeekType.EVEN.weekCode))
            completeOdd(response.lessons.fromApi().toItem(TimetableWeekType.ODD.weekCode))
        } else {
            error()
        }
    }
}