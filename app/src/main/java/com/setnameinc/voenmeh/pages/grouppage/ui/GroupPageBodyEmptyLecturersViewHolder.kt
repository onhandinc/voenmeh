package com.setnameinc.voenmeh.pages.grouppage.ui

import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.item_timetable_body_empty_lecturers.view.*

class GroupPageBodyEmptyLecturersViewHolder(
    private val view: View
) : GroupPageTimetableViewHolder(view = view) {

    override fun bind(item: GroupPageTimetableItem) {
        item as GroupPageTimetableItem.BodyWithEmptyLecturersTimetable
        view.apply {
            timeTextView.text = item.startTime
            subjectTextView.text = item.subjectName
            classroomTextView.background = ContextCompat.getDrawable(
                context,
                item.classroomBackgroundId
            )
            classroomTextView.text = item.classroomName
            endTimeTextView.text = item.endTime
        }
    }

}