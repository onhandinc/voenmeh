package com.setnameinc.voenmeh.pages.grouppage.ui

import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.item_timetable_body_empty_subject.view.*

class GroupPageBodyEmptySubjectViewHolder(
    private val view: View
) : GroupPageTimetableViewHolder(view = view) {

    override fun bind(item: GroupPageTimetableItem) {
        item as GroupPageTimetableItem.BodyWithEmptySubject
        view.apply {
            timeTextView.text = item.startTime
            classroomTextView.background = ContextCompat.getDrawable(
                context,
                item.classroomBackgroundId
            )
            classroomTextView.text = item.classroomName
            lecturerTextView.text = item.lecturers
            endTimeTextView.text = item.endTime
        }
    }
}