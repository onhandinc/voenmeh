package com.setnameinc.voenmeh.pages.grouppage.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.pages.grouppage.viewmodel.FetchStatus
import com.setnameinc.voenmeh.pages.grouppage.viewmodel.GroupPageEvent
import com.setnameinc.voenmeh.pages.grouppage.viewmodel.GroupPageViewModel
import com.setnameinc.voenmeh.pages.grouppage.viewmodel.GroupPageViewState
import com.setnameinc.voenmeh.tools.base.BsaeScopedProgressFragment
import com.setnameinc.voenmeh.tools.utils.viewModel
import kotlinx.android.synthetic.main.fragment_group_page.*
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import timber.log.Timber

class GroupPageFragment : BsaeScopedProgressFragment(R.layout.fragment_group_page) {

    private val EVEN_WEEK_POSITION = 0
    private val ODD_WEEK_POSITION = 1

    private val viewModel: GroupPageViewModel by fragmentScope.viewModel(this)

    private val evenLoadListener: CompletableDeferred<Boolean> = CompletableDeferred()
    private val oddLoadListener: CompletableDeferred<Boolean> = CompletableDeferred()

    private val timetableEvenFragment: GroupPageTimetableEvenFragment by lazy {
        GroupPageTimetableEvenFragment(evenLoadListener)
    }

    private val timetableOddFragment: GroupPageTimetableOddFragment by lazy {
        GroupPageTimetableOddFragment(oddLoadListener)
    }

    private val groupId: String by lazy {
        arguments?.getString("groupId") ?: ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewState(it)
                }
        }
        lifecycleScope.launchWhenStarted {
            awaitAll(evenLoadListener, oddLoadListener)
            hideProgressBar()
        }
    }

    private fun bindViewState(viewState: GroupPageViewState) {
        when (viewState) {
            is GroupPageViewState.NameLoaded -> {
                Timber.d("ClassroomPageViewState.NameLoaded, ${viewState.fetchStatus}")
                when (viewState.fetchStatus) {
                    is FetchStatus.Success -> {
                        groupNameTextView.text = "Группа ${viewState.groupName}"
                    }
                    is FetchStatus.Loading -> {

                    }
                    is FetchStatus.Error -> {

                    }
                }
            }
            is GroupPageViewState.EvenWeekLoaded -> {
                when (viewState.fetchStatus) {
                    is FetchStatus.Success -> {
                        viewState.lessons?.let {
                            Timber.d("even: $it")
                            timetableEvenFragment.submitList(it)
                        }
                    }
                    is FetchStatus.Loading -> {

                    }
                    is FetchStatus.Error -> {

                    }
                }
            }
            is GroupPageViewState.OddWeekLoaded -> {
                when (viewState.fetchStatus) {
                    is FetchStatus.Success -> {
                        viewState.lessons?.let {
                            Timber.d("odd: $it")
                            timetableOddFragment.submitList(it)
                        }
                    }
                    is FetchStatus.Loading -> {

                    }
                    is FetchStatus.Error -> {

                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (viewModel.groupName.isNotEmpty()) {
            Timber.d("use from repository")
            groupNameTextView.text = "Группа ${viewModel.groupName}"
        } else {
            Timber.d("use from api")
            viewModel.obtainEvent(GroupPageEvent.Load(groupId))
        }
        timetableViewPager.apply {
            adapter = object : FragmentStateAdapter(
                this@GroupPageFragment.childFragmentManager,
                viewLifecycleOwner.lifecycle
            ) {

                override fun getItemCount(): Int = 2

                override fun createFragment(position: Int): Fragment =
                    when (position) {
                        EVEN_WEEK_POSITION -> timetableEvenFragment
                        ODD_WEEK_POSITION -> timetableOddFragment
                        else -> throw Exception()
                    }

            }
            offscreenPageLimit = 2
            (getChildAt(0) as RecyclerView).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
        }
        TabLayoutMediator(timetableWeekTabLayout, timetableViewPager) { tab, position ->
            when (position) {
                EVEN_WEEK_POSITION -> {
                    tab.text = "Четная"
                }
                ODD_WEEK_POSITION -> {
                    tab.text = "Нечетная"
                }
            }
        }.attach()
    }

}