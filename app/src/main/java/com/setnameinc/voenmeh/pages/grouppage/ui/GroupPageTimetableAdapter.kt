package com.setnameinc.voenmeh.pages.grouppage.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.setnameinc.voenmeh.R

class GroupPageTimetableAdapter : ListAdapter<GroupPageTimetableItem, GroupPageTimetableViewHolder>(
    object : DiffUtil.ItemCallback<GroupPageTimetableItem>() {
        override fun areItemsTheSame(
            oldItem: GroupPageTimetableItem,
            newItem: GroupPageTimetableItem
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: GroupPageTimetableItem,
            newItem: GroupPageTimetableItem
        ): Boolean = oldItem.id == newItem.id && oldItem.type == newItem.type

    }
) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GroupPageTimetableViewHolder = when (viewType) {
        TimetableTypes.TITLE ->
            GroupPageTimetableTitleViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_timetable_title, parent, false)
            )
        TimetableTypes.BODY -> {
            GroupPageTimetableBodyViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_timetable_body, parent, false)
            )
        }
        TimetableTypes.BODY_WITH_EMPTY_SUBJECT -> {
            GroupPageBodyEmptySubjectViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_timetable_body_empty_subject, parent, false)
            )
        }
        else -> {
            GroupPageBodyEmptyLecturersViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(
                        R.layout.item_timetable_body_empty_lecturers,
                        parent,
                        false
                    )
            )
        }
    }

    override fun onBindViewHolder(holder: GroupPageTimetableViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    override fun getItemViewType(position: Int): Int = currentList[position].type

}