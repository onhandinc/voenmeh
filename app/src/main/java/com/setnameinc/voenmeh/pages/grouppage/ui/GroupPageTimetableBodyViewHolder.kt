package com.setnameinc.voenmeh.pages.grouppage.ui

import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.item_timetable_body.view.*

class GroupPageTimetableBodyViewHolder(
    private val view: View
) : GroupPageTimetableViewHolder(view = view) {

    override fun bind(item: GroupPageTimetableItem) {
        item as GroupPageTimetableItem.BodyTimetable
        view.apply {
            timeTextView.text = item.startTime
            subjectTextView.text = item.subjectName
            classroomTextView.background = ContextCompat.getDrawable(
                context,
                item.classroomBackgroundId
            )
            classroomTextView.text = item.classroomName
            lecturerTextView.text = item.lecturers
            endTimeTextView.text = item.endTime
        }
    }
}
