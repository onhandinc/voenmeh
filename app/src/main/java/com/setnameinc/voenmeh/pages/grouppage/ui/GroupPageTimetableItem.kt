package com.setnameinc.voenmeh.pages.grouppage.ui

sealed class GroupPageTimetableItem(
    open val id: String,
    open val type: Int
) {

    data class TitleTimetable(
        val title: String,
        val dayNumberInWeek: Int,
        val isSelected: Boolean = false
    ) : GroupPageTimetableItem(
        type = TimetableTypes.TITLE,
        id = dayNumberInWeek.toString()
    )

    data class BodyTimetable(
        override val id: String,
        val classroom: String,
        val classroomName: String,
        val classroomBackgroundId: Int,
        val subject: String,
        val subjectName: String,
        val lecturers: String,
        val startTime: String,
        val endTime: String
    ) : GroupPageTimetableItem(
        type = TimetableTypes.BODY,
        id = id
    )

    data class BodyWithEmptyLecturersTimetable(
        override val id: String,
        val classroom: String,
        val classroomName: String,
        val classroomBackgroundId: Int,
        val subject: String,
        val subjectName: String,
        val startTime: String,
        val endTime: String
    ) : GroupPageTimetableItem(
        type = TimetableTypes.BODY_WITH_EMPTY_LECTURERS,
        id = id
    )

    data class BodyWithEmptySubject(
        override val id: String,
        val classroom: String,
        val classroomName: String,
        val classroomBackgroundId: Int,
        val lecturers: String,
        val startTime: String,
        val endTime: String
    ) : GroupPageTimetableItem(
        type = TimetableTypes.BODY_WITH_EMPTY_SUBJECT,
        id = id
    )
}

object TimetableTypes {
    const val TITLE = 0
    const val BODY = 1
    const val BODY_WITH_EMPTY_LECTURERS = 2
    const val BODY_WITH_EMPTY_SUBJECT = 3
}