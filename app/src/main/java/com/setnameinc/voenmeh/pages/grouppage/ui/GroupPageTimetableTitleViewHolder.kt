package com.setnameinc.voenmeh.pages.grouppage.ui

import android.view.View
import kotlinx.android.synthetic.main.item_timetable_title.view.*

class GroupPageTimetableTitleViewHolder(
    private val view: View
) : GroupPageTimetableViewHolder(view = view) {

    override fun bind(item: GroupPageTimetableItem) {
        val localeItem = item as GroupPageTimetableItem.TitleTimetable
        view.apply {
            titleTextView.text = localeItem.title
            currentDayIndicatorView.visibility = if (item.isSelected) {
                View.VISIBLE
            } else {
                View.INVISIBLE
            }
        }
    }
}