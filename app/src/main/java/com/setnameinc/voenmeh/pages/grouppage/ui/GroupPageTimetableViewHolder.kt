package com.setnameinc.voenmeh.pages.grouppage.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class GroupPageTimetableViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bind(item: GroupPageTimetableItem)
}