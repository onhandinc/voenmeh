package com.setnameinc.voenmeh.pages.grouppage.viewmodel

sealed class GroupPageEvent {
    data class Load(val groupId: String) : GroupPageEvent()
}