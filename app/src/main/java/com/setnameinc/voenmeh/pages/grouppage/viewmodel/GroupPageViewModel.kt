package com.setnameinc.voenmeh.pages.grouppage.viewmodel

import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.pages.grouppage.domain.GroupPageInteractor

class GroupPageViewModel(
    private val groupPageInteractor: GroupPageInteractor
) : BaseFlowViewModel<GroupPageViewState, GroupPageAction, GroupPageEvent>() {

    var groupName: String = ""

    override suspend fun obtainInternalEvent(viewEvent: GroupPageEvent) {
        when (viewEvent) {
            is GroupPageEvent.Load -> {
                if (viewEvent.groupId.isNotEmpty()) {
                    groupPageInteractor.load(
                        viewEvent.groupId,
                        completeEven = {
                            viewState = GroupPageViewState.EvenWeekLoaded(
                                FetchStatus.Success,
                                it
                            )
                        },
                        completeOdd = {
                            viewState = GroupPageViewState.OddWeekLoaded(
                                FetchStatus.Success,
                                it
                            )
                        },
                        completeName = {
                            groupName = it
                            viewState = GroupPageViewState.NameLoaded(
                                FetchStatus.Success,
                                it
                            )
                        }, error = {
                            viewState = GroupPageViewState.EvenWeekLoaded(
                                FetchStatus.Error
                            )
                            viewState = GroupPageViewState.OddWeekLoaded(
                                FetchStatus.Error
                            )
                            viewState = GroupPageViewState.NameLoaded(
                                FetchStatus.Error
                            )
                        }
                    )
                }
            }
        }
    }

}