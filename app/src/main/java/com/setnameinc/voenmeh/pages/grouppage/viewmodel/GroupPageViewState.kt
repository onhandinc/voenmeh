package com.setnameinc.voenmeh.pages.grouppage.viewmodel

import com.setnameinc.voenmeh.pages.grouppage.ui.GroupPageTimetableItem

sealed class GroupPageViewState {
    data class NameLoaded(
        val fetchStatus: FetchStatus,
        val groupName: String? = null
    ) : GroupPageViewState()

    data class EvenWeekLoaded(
        val fetchStatus: FetchStatus,
        val lessons: List<GroupPageTimetableItem>? = null
    ) : GroupPageViewState()

    data class OddWeekLoaded(
        val fetchStatus: FetchStatus,
        val lessons: List<GroupPageTimetableItem>? = null
    ) : GroupPageViewState()
}

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}