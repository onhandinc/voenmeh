package com.setnameinc.voenmeh.pages.lecturerpage.api

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface LecturerPageApi {
    @GET("lecturer/{lecturerId}")
    fun load(
        @Path("lecturerId") lecturerId: String
    ): Deferred<Response<LecturerPageResponse>>

    @GET("lecturer/{lecturerId}/lessons")
    fun loadTimetable(
        @Path("lecturerId") lecturerId: String
    ): Deferred<Response<List<LecturerPageTimetableResponse>>>
}