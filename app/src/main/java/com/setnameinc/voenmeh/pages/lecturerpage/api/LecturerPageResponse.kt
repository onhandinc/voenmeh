package com.setnameinc.voenmeh.pages.lecturerpage.api

import com.google.gson.annotations.SerializedName

data class LecturerPageResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("number")
    val number: Int,
    @SerializedName("shortName")
    val shortName: String,
    @SerializedName("fullName")
    val fullName: String,
    @SerializedName("rank")
    val rank: String,
    @SerializedName("position")
    val position: String,
    @SerializedName("department")
    val department: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("photoUrl")
    val photoUrl: String,
    @SerializedName("photoSmallUrl")
    val photoSmallUrl: String
)