package com.setnameinc.voenmeh.pages.lecturerpage.di

import com.setnameinc.voenmeh.app.api.RetrofitConst
import com.setnameinc.voenmeh.pages.lecturerpage.api.LecturerPageApi
import com.setnameinc.voenmeh.pages.lecturerpage.domain.LecturerPageInteractor
import com.setnameinc.voenmeh.pages.lecturerpage.ui.LecturerPageFragment
import com.setnameinc.voenmeh.pages.lecturerpage.viewmodel.LecturerPageViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

object LecturerPageModule {
    val module = module {
        scope<LecturerPageFragment> {
            viewModel {
                LecturerPageViewModel(
                    lecturerPageInteractor = get()
                )
            }
            scoped {
                LecturerPageInteractor(
                    lecturerPageApi = get()
                )
            }
            scoped {
                get<Retrofit>(
                    named(RetrofitConst.VOENMEH_SERVER_RETROFIT_INJECTION_KEY)
                ).create(LecturerPageApi::class.java)
            }
        }
    }
}