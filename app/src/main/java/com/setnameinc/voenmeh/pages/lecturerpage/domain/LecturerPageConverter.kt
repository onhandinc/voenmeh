package com.setnameinc.voenmeh.pages.lecturerpage.domain

import com.setnameinc.voenmeh.pages.lecturerpage.api.LecturerPageResponse

object LecturerPageConverter {

    fun LecturerPageResponse.fromApi(): LecturerPageModel = LecturerPageModel(
        id = this.id,
        number = this.number,
        shortName = this.shortName,
        fullName = this.fullName,
        rank = this.rank,
        position = this.position,
        department = this.department,
        email = this.email,
        photoUrl = this.photoUrl,
        photoSmallUrl = this.photoSmallUrl
    )
}