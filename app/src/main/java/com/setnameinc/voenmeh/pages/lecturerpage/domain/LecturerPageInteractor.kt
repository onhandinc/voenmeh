package com.setnameinc.voenmeh.pages.lecturerpage.domain

import com.setnameinc.voenmeh.pages.lecturerpage.api.LecturerPageApi
import com.setnameinc.voenmeh.pages.lecturerpage.api.LecturerPageResponse
import com.setnameinc.voenmeh.pages.lecturerpage.api.LecturerPageTimetableResponse
import com.setnameinc.voenmeh.pages.lecturerpage.domain.LecturerPageConverter.fromApi
import com.setnameinc.voenmeh.pages.lecturerpage.domain.LecturerTimetableConverter.fromApi
import com.setnameinc.voenmeh.pages.lecturerpage.domain.LecturerTimetableConverter.toItem
import com.setnameinc.voenmeh.pages.lecturerpage.ui.LecturerPageTimetableItem
import com.setnameinc.voenmeh.timetable.common.domain.TimetableWeekType
import timber.log.Timber
import java.lang.NullPointerException

class LecturerPageInteractor(private val lecturerPageApi: LecturerPageApi) {

    suspend fun loadLecturerInfo(
        lecturerId: String,
        complete: (LecturerPageModel) -> Unit,
        error: (Throwable) -> Unit
    ) {
        val response: LecturerPageResponse? =
            lecturerPageApi.load(lecturerId).await().body()
        if (response != null) {
            complete(response.fromApi())
        } else {
            error(NullPointerException())
        }
    }

    suspend fun loadLecturerLessons(
        lecturerId: String,
        completeEven: (List<LecturerPageTimetableItem>) -> Unit,
        completeOdd: (List<LecturerPageTimetableItem>) -> Unit,
        error: (Throwable) -> Unit
    ) {
        val response: List<LecturerPageTimetableResponse>? =
            lecturerPageApi.loadTimetable(lecturerId).await().body()
        if (response != null) {
            Timber.d("LecturerPageInteractorState.OddLoaded")
            completeOdd(
                response.fromApi().toItem(TimetableWeekType.ODD.weekCode)
            )
            Timber.d("LecturerPageInteractorState.EvenLoaded")
            completeEven(
                response.fromApi().toItem(TimetableWeekType.EVEN.weekCode)
            )
        } else {
            error(NullPointerException())
        }
    }
}