package com.setnameinc.voenmeh.pages.lecturerpage.domain

data class LecturerPageModel(
    val id: String,
    val number: Int,
    val shortName: String,
    val fullName: String,
    val rank: String,
    val position: String,
    val department: String,
    val email: String,
    val photoUrl: String,
    val photoSmallUrl: String
)