package com.setnameinc.voenmeh.pages.lecturerpage.domain

import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.pages.lecturerpage.api.LecturerPageTimetableResponse
import com.setnameinc.voenmeh.pages.lecturerpage.ui.LecturerPageTimetableItem
import com.setnameinc.voenmeh.tools.utils.DayTitleFromWeekNumber

object LecturerTimetableConverter {

    fun List<LecturerPageTimetableResponse>.fromApi(): List<LecturerTimetableModel> =
        this.map {
            LecturerTimetableModel(
                id = it.id,
                classroom = it.classroom,
                classroomName = it.classroomName,
                numberDayInWeek = it.numberDayInWeek,
                discipline = it.discipline,
                disciplineName = it.disciplineName,
                lecturers = it.lecturers,
                lecturersId = it.lecturersId,
                startTime = it.startTime,
                startTimeText = it.startTimeText,
                endTimeText = it.endTimeText,
                groupIds = it.groupIds,
                groups = it.groups,
                weekCode = it.weekCode,
                type = it.type
            )
        }

    fun List<LecturerTimetableModel>.toItem(weekCode: Int): List<LecturerPageTimetableItem> =
        this.filter {
            it.weekCode == weekCode
        }
            .sortedBy {
                it.numberDayInWeek
            }
            .groupBy {
                Day(
                    DayTitleFromWeekNumber.getDayTitleFromWeekNumber(it.numberDayInWeek),
                    it.numberDayInWeek
                )
            }
            .flatMap { pair ->
                createTimetableToDay(
                    pair.key,
                    pair.value.sortedBy { it.startTime }
                )
            }

    private fun createTimetableToDay(
        day: Day,
        list: List<LecturerTimetableModel>
    ): List<LecturerPageTimetableItem> =
        mutableListOf<LecturerPageTimetableItem>().apply {
            day.title?.let {
                add(LecturerPageTimetableItem.TitleTimetable(it, day.numberDayInWeek))
            }
            addAll(
                list.map {
                    if (it.discipline.isEmpty()) {
                        convertEmptySubject(it)
                    } else {
                        convert(it)
                    }
                }
            )
        }

    private fun convert(
        item: LecturerTimetableModel
    ): LecturerPageTimetableItem.BodyTimetable =
        LecturerPageTimetableItem.BodyTimetable(
            id = item.id,
            classroom = item.classroom,
            classroomName = item.classroomName,
            subject = item.discipline,
            subjectName = item.disciplineName,
            startTime = item.startTimeText,
            endTime = item.endTimeText,
            groups = item.groups,
            classroomBackgroundId = generateClassroomBackgroundId(
                item.type
            )
        )

    private fun convertEmptySubject(
        item: LecturerTimetableModel
    ): LecturerPageTimetableItem.BodyWithEmptySubject =
        LecturerPageTimetableItem.BodyWithEmptySubject(
            id = item.id,
            classroom = item.classroom,
            classroomName = item.classroomName,
            startTime = item.startTimeText,
            endTime = item.endTimeText,
            groups = item.groups,
            classroomBackgroundId = generateClassroomBackgroundId(
                item.type
            )
        )

    private fun generateClassroomBackgroundId(subjectType: String): Int =
        when (subjectType) {
            "practise" -> {
                R.drawable.shape_timetable_class_type_practise_background
            }
            "laboratory" -> {
                R.drawable.shape_timetable_class_type_laboratory_background
            }
            "consultation" -> {
                R.drawable.shape_timetable_class_type_consultation_background
            }
            else -> {
                R.drawable.shape_timetable_class_type_lecture_background
            }
        }

    private data class Day(
        val title: String?,
        val numberDayInWeek: Int
    )
}