package com.setnameinc.voenmeh.pages.lecturerpage.ui

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.material.tabs.TabLayoutMediator
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.pages.lecturerpage.viewmodel.FetchStatus
import com.setnameinc.voenmeh.pages.lecturerpage.viewmodel.LecturerPageEvent
import com.setnameinc.voenmeh.pages.lecturerpage.viewmodel.LecturerPageViewModel
import com.setnameinc.voenmeh.pages.lecturerpage.viewmodel.LecturerPageViewState
import com.setnameinc.voenmeh.tools.base.BsaeScopedProgressFragment
import com.setnameinc.voenmeh.tools.utils.FaceCenterCrop
import com.setnameinc.voenmeh.tools.utils.GlideFaceDetector
import com.setnameinc.voenmeh.tools.utils.viewModel
import kotlinx.android.synthetic.main.fragment_lecturer_page.*
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import timber.log.Timber

class LecturerPageFragment : BsaeScopedProgressFragment(R.layout.fragment_lecturer_page) {

    private val EVEN_WEEK_POSITION = 0
    private val ODD_WEEK_POSITION = 1

    private val lecturerId: String by lazy {
        arguments?.getString("lecturerId") ?: ""
    }

    private val evenLoadListener: CompletableDeferred<Boolean> = CompletableDeferred()
    private val oddLoadListener: CompletableDeferred<Boolean> = CompletableDeferred()
    private val photoLoadListener: CompletableDeferred<Boolean> = CompletableDeferred()
    private val nameLoadListener: CompletableDeferred<Boolean> = CompletableDeferred()
    private val departmentLoadListener: CompletableDeferred<Boolean> = CompletableDeferred()
    private val statusLoadListener: CompletableDeferred<Boolean> = CompletableDeferred()

    private val timetableEvenFragment: LecturerPageTimetableEvenFragment by lazy {
        LecturerPageTimetableEvenFragment(evenLoadListener)
    }
    private val timetableOddFragment: LecturerPageTimetableOddFragment by lazy {
        LecturerPageTimetableOddFragment(oddLoadListener)
    }

    private val viewModel: LecturerPageViewModel by fragmentScope.viewModel(this)

    private val glideFaceDetector: GlideFaceDetector = GlideFaceDetector()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        glideFaceDetector.initialize(context)
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewState(it)
                }
        }
        lifecycleScope.launch {
            Timber.d("await all")
            awaitAll(
                evenLoadListener,
                oddLoadListener,
                nameLoadListener,
                departmentLoadListener,
                statusLoadListener,
                photoLoadListener
            )
            Timber.d("hideProgressBar")
            hideProgressBar()
        }
        viewModel.obtainEvent(LecturerPageEvent.LoadLecturerInfo(lecturerId))
        viewModel.obtainEvent(LecturerPageEvent.LoadTimetable(lecturerId))
    }

    private fun bindViewState(viewState: LecturerPageViewState) {
        when (viewState) {
            is LecturerPageViewState.LecturerInfo -> {
                when (viewState.fetchStatus) {
                    is FetchStatus.Success -> {
                        viewState.timetable?.let {
                            if (it.department.isEmpty()
                                && it.position.isEmpty()
                                && it.rank.isEmpty()
                                && it.photoUrl.isEmpty()
                            ) {
                                headerRelativeLayout.visibility = View.GONE
                                nameLoadListener.complete(false)
                                departmentLoadListener.complete(false)
                                statusLoadListener.complete(false)
                                photoLoadListener.complete(false)
                                Timber.d("empty data provides")
                                return
                            }
                            if (it.fullName.isEmpty()) {
                                nameTextView.text = it.shortName
                            } else {
                                nameTextView.text = it.fullName
                            }
                            Timber.d("nameLoadListener.complete")
                            nameLoadListener.complete(true)
                            if (it.department.isNotEmpty()) {
                                departmentTextView.text = "Кафедра ${it.department}"
                            } else {
                                departmentTextView.visibility = View.GONE
                                (statusTextView.layoutParams as RelativeLayout.LayoutParams).apply {
                                    removeRule(RelativeLayout.BELOW)
                                    addRule(RelativeLayout.ALIGN_TOP, R.id.iconImageView)
                                }
                            }
                            Timber.d("departmentLoadListener.complete")
                            departmentLoadListener.complete(true)
                            if (it.position.isNotEmpty()) {
                                statusTextView.text = "${it.rank}\n${it.position}"
                            } else {
                                statusTextView.text = it.rank
                            }
                            Timber.d("statusLoadListener.complete")
                            statusLoadListener.complete(true)
                            if (it.photoUrl.isNotEmpty()) {
                                Glide.with(this)
                                    .load(it.photoUrl.replace("http", "https"))
                                    .listener(
                                        object : RequestListener<Drawable> {
                                            override fun onLoadFailed(
                                                e: GlideException?,
                                                model: Any?,
                                                target: Target<Drawable>?,
                                                isFirstResource: Boolean
                                            ): Boolean {
                                                photoLoadListener.complete(false)
                                                Timber.d("photoLoadListener.complete")
                                                return false
                                            }

                                            override fun onResourceReady(
                                                resource: Drawable?,
                                                model: Any?,
                                                target: Target<Drawable>?,
                                                dataSource: DataSource?,
                                                isFirstResource: Boolean
                                            ): Boolean {
                                                photoLoadListener.complete(true)
                                                Timber.d("photoLoadListener.complete")
                                                return false
                                            }
                                        }
                                    )
                                    .transform(FaceCenterCrop(glideFaceDetector))
                                    .into(iconImageView)
                            } else {
                                Glide.with(this)
                                    .load(R.drawable.ic_user_unknown)
                                    .listener(
                                        object : RequestListener<Drawable> {
                                            override fun onLoadFailed(
                                                e: GlideException?,
                                                model: Any?,
                                                target: Target<Drawable>?,
                                                isFirstResource: Boolean
                                            ): Boolean {
                                                photoLoadListener.complete(false)
                                                Timber.d("photoLoadListener.complete")
                                                return false
                                            }

                                            override fun onResourceReady(
                                                resource: Drawable?,
                                                model: Any?,
                                                target: Target<Drawable>?,
                                                dataSource: DataSource?,
                                                isFirstResource: Boolean
                                            ): Boolean {
                                                photoLoadListener.complete(true)
                                                Timber.d("photoLoadListener.complete")
                                                return false
                                            }
                                        }
                                    )
                                    .centerCrop()
                                    .into(iconImageView)
                            }
                        }
                    }
                    is FetchStatus.Loading -> {

                    }
                    is FetchStatus.Error -> {

                    }
                }
            }
            is LecturerPageViewState.EvenWeek -> {
                when (viewState.fetchStatus) {
                    is FetchStatus.Success -> {
                        Timber.d("LecturerPageViewState.EvenWeek.Success")
                        timetableEvenFragment.submitList(viewState.lessons)
                    }
                    is FetchStatus.Loading -> {

                    }
                    is FetchStatus.Error -> {
                        Timber.d("evenLoadListener.complete")
                        evenLoadListener.complete(false)
                    }
                }
            }
            is LecturerPageViewState.OddWeek -> {
                when (viewState.fetchStatus) {
                    is FetchStatus.Success -> {
                        Timber.d("LecturerPageViewState.OddWeek.Success")
                        timetableOddFragment.submitList(viewState.lessons)
                    }
                    is FetchStatus.Loading -> {

                    }
                    is FetchStatus.Error -> {
                        Timber.d("oddLoadListener.error")
                        oddLoadListener.complete(false)
                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (viewModel.lecturerName.isNotEmpty()) {
            nameTextView.text = viewModel.lecturerName
        }
        timetableViewPager.apply {
            adapter = object : FragmentStateAdapter(
                this@LecturerPageFragment.childFragmentManager,
                viewLifecycleOwner.lifecycle
            ) {

                override fun getItemCount(): Int = 2

                override fun createFragment(position: Int): Fragment =
                    when (position) {
                        EVEN_WEEK_POSITION -> timetableEvenFragment
                        ODD_WEEK_POSITION -> timetableOddFragment
                        else -> throw Exception()
                    }

            }
            offscreenPageLimit = 2
            (getChildAt(0) as RecyclerView).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
        }
        TabLayoutMediator(timetableWeekTabLayout, timetableViewPager) { tab, position ->
            when (position) {
                EVEN_WEEK_POSITION -> {
                    tab.text = "Четная"
                }
                ODD_WEEK_POSITION -> {
                    tab.text = "Нечетная"
                }
            }
        }.attach()
    }

    override fun onDestroy() {
        glideFaceDetector.releaseDetector()
        super.onDestroy()
    }

}