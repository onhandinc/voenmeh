package com.setnameinc.voenmeh.pages.lecturerpage.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.timetable.lists.ui.TimetableAction

class LecturerPageTimetableAdapter(
    private val clickListener: (action: TimetableAction, value: String) -> Unit
) : ListAdapter<LecturerPageTimetableItem, LecturerPageTimetableViewHolder>(
    object : DiffUtil.ItemCallback<LecturerPageTimetableItem>() {
        override fun areItemsTheSame(
            oldItem: LecturerPageTimetableItem,
            newItem: LecturerPageTimetableItem
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: LecturerPageTimetableItem,
            newItem: LecturerPageTimetableItem
        ): Boolean = oldItem.id == newItem.id && oldItem.type == newItem.type

    }
) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): LecturerPageTimetableViewHolder = when (viewType) {
        TimetableTypes.TITLE ->
            LecturerPageTimetableTitleViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_timetable_title, parent, false)
            )
        TimetableTypes.BODY_WITH_EMPTY_SUBJECT -> {
            LecturerPageTimetableBodyEmptySubjectViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_timetable_body_empty_subject, parent, false)
            )
        }
        else -> {
            LecturerPageTimetableBodyViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_body_lecturer_timetable, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: LecturerPageTimetableViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    override fun getItemViewType(position: Int): Int = currentList[position].type

}