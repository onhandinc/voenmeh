package com.setnameinc.voenmeh.pages.lecturerpage.ui

import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.item_body_lecturer_timetable.view.*

class LecturerPageTimetableBodyViewHolder(
    private val view: View
) : LecturerPageTimetableViewHolder(view = view) {

    override fun bind(item: LecturerPageTimetableItem) {
        item as LecturerPageTimetableItem.BodyTimetable
        view.apply {
            timeTextView.text = item.startTime
            subjectTextView.text = item.subjectName
            classroomTextView.background = ContextCompat.getDrawable(
                context,
                item.classroomBackgroundId
            )
            classroomTextView.text = item.classroomName
            groupsTextView.text = item.groups
            endTimeTextView.text = item.endTime
        }
    }
}