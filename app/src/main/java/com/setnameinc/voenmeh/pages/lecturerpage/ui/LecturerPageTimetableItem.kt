package com.setnameinc.voenmeh.pages.lecturerpage.ui

sealed class LecturerPageTimetableItem(
    open val id: String,
    open val type: Int
) {

    data class TitleTimetable(
        val title: String,
        val dayNumberInWeek: Int,
        val isSelected: Boolean = false
    ) : LecturerPageTimetableItem(
        type = TimetableTypes.TITLE,
        id = dayNumberInWeek.toString()
    )

    data class BodyTimetable(
        override val id: String,
        val classroom: String,
        val classroomName: String,
        val classroomBackgroundId: Int,
        val subject: String,
        val subjectName: String,
        val groups: String,
        val startTime: String,
        val endTime: String
    ) : LecturerPageTimetableItem(
        type = TimetableTypes.BODY,
        id = id
    )

    data class BodyWithEmptySubject(
        override val id: String,
        val classroom: String,
        val classroomName: String,
        val classroomBackgroundId: Int,
        val groups: String,
        val startTime: String,
        val endTime: String
    ) : LecturerPageTimetableItem(
        type = TimetableTypes.BODY,
        id = id
    )
}

object TimetableTypes {
    const val TITLE = 0
    const val BODY = 1
    const val BODY_WITH_EMPTY_SUBJECT = 2
}