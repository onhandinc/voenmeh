package com.setnameinc.voenmeh.pages.lecturerpage.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.setnameinc.voenmeh.R
import kotlinx.android.synthetic.main.fragment_lecturer_page_timetable_odd_week.*
import kotlinx.coroutines.CompletableDeferred
import timber.log.Timber

class LecturerPageTimetableOddFragment(
    private val loadListener: CompletableDeferred<Boolean>
) : Fragment(R.layout.fragment_lecturer_page_timetable_odd_week) {

    private val lecturerPageTimetableAdapter: LecturerPageTimetableAdapter by lazy {
        LecturerPageTimetableAdapter { action, value ->

        }.apply {
            stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        timetableRecyclerView.apply {
            this.adapter = lecturerPageTimetableAdapter
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
        }
    }

    fun submitList(list: List<LecturerPageTimetableItem>?) {
        lecturerPageTimetableAdapter.submitList(list) {
            Timber.d("oddLoadListener.complete")
            loadListener.complete(true)
        }
    }

}