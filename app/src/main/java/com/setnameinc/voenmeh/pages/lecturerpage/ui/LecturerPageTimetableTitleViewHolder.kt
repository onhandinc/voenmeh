package com.setnameinc.voenmeh.pages.lecturerpage.ui

import android.view.View
import kotlinx.android.synthetic.main.item_timetable_title.view.*

class LecturerPageTimetableTitleViewHolder(
    private val view: View
) : LecturerPageTimetableViewHolder(view = view) {

    override fun bind(item: LecturerPageTimetableItem) {
        val localeItem = item as LecturerPageTimetableItem.TitleTimetable
        view.apply {
            titleTextView.text = localeItem.title
            currentDayIndicatorView.visibility = if (item.isSelected) {
                View.VISIBLE
            } else {
                View.INVISIBLE
            }
        }
    }
}