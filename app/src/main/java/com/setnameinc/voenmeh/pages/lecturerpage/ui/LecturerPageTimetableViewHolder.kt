package com.setnameinc.voenmeh.pages.lecturerpage.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class LecturerPageTimetableViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bind(item: LecturerPageTimetableItem)
}