package com.setnameinc.voenmeh.pages.lecturerpage.viewmodel

sealed class LecturerPageEvent {
    data class LoadLecturerInfo(val lecturerId: String) : LecturerPageEvent()
    data class LoadTimetable(val lecturerId: String) : LecturerPageEvent()
}