package com.setnameinc.voenmeh.pages.lecturerpage.viewmodel

import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.pages.lecturerpage.domain.LecturerPageInteractor
import timber.log.Timber

class LecturerPageViewModel(
    private val lecturerPageInteractor: LecturerPageInteractor
) : BaseFlowViewModel<LecturerPageViewState, LecturerPageAction, LecturerPageEvent>() {

    var lecturerName: String = ""
    var rank: String = ""
    var department: String = ""
    var position: String = ""

    override suspend fun obtainInternalEvent(viewEvent: LecturerPageEvent) {
        when (viewEvent) {
            is LecturerPageEvent.LoadLecturerInfo -> {
                lecturerPageInteractor.loadLecturerInfo(
                    viewEvent.lecturerId,
                    complete = {
                        lecturerName = if (it.fullName.isEmpty()) {
                            it.shortName
                        } else {
                            it.fullName
                        }
                        rank = it.rank
                        department = it.department
                        position = it.position
                        viewState = LecturerPageViewState.LecturerInfo(
                            FetchStatus.Success,
                            it
                        )
                    },
                    error = {
                        viewState = LecturerPageViewState.LecturerInfo(FetchStatus.Error)
                    }
                )
            }
            is LecturerPageEvent.LoadTimetable -> {
                lecturerPageInteractor.loadLecturerLessons(
                    viewEvent.lecturerId,
                    completeEven = {
                        viewState = LecturerPageViewState.EvenWeek(
                            FetchStatus.Success,
                            it
                        )
                    },
                    completeOdd = {
                        viewState = LecturerPageViewState.OddWeek(
                            FetchStatus.Success,
                            it
                        )
                    },
                    error = {
                        Timber.e(it)
                        viewState = LecturerPageViewState.EvenWeek(FetchStatus.Error)
                        viewState = LecturerPageViewState.OddWeek(FetchStatus.Error)
                    }
                )
            }
        }
    }
}