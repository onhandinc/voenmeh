package com.setnameinc.voenmeh.pages.lecturerpage.viewmodel

import com.setnameinc.voenmeh.pages.lecturerpage.domain.LecturerPageModel
import com.setnameinc.voenmeh.pages.lecturerpage.ui.LecturerPageTimetableItem

sealed class LecturerPageViewState {
    data class LecturerInfo(
        val fetchStatus: FetchStatus,
        val timetable: LecturerPageModel? = null
    ) : LecturerPageViewState()

    data class EvenWeek(
        val fetchStatus: FetchStatus,
        val lessons: List<LecturerPageTimetableItem>? = null
    ) : LecturerPageViewState()

    data class OddWeek(
        val fetchStatus: FetchStatus,
        val lessons: List<LecturerPageTimetableItem>? = null
    ) : LecturerPageViewState()
}

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}