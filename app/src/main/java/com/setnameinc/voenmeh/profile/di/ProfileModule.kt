package com.setnameinc.voenmeh.profile.di

import com.setnameinc.voenmeh.profile.domain.ProfileInteractor
import com.setnameinc.voenmeh.profile.viewmodel.profile.ProfileViewModel
import com.setnameinc.voenmeh.profile.viewmodel.select.SelectGroupViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object ProfileModule {
    val module = module {
        single {
            ProfileInteractor(
                firebaseAuth = get(),
                emailInteractor = get(),
                sharedPreferences = get()
            )
        }
        viewModel {
            SelectGroupViewModel(
                userInteractor = get()
            )
        }
        viewModel {
            ProfileViewModel(profileInteractor = get())
        }
    }
}