package com.setnameinc.voenmeh.profile.domain

import android.content.SharedPreferences
import com.google.firebase.auth.FirebaseAuth
import com.setnameinc.voenmeh.email.main.domain.EmailInteractor
import java.lang.Exception

class ProfileInteractor(
    private val firebaseAuth: FirebaseAuth,
    private val emailInteractor: EmailInteractor,
    private val sharedPreferences: SharedPreferences
) {

    suspend fun signOut(
        complete: () -> Unit,
        error: () -> Unit
    ) {
        try {
            firebaseAuth.signOut()
            emailInteractor.deleteAll()
            sharedPreferences.edit().clear().apply()
            complete()
        } catch (e: Exception) {
            error()
        }
    }

}