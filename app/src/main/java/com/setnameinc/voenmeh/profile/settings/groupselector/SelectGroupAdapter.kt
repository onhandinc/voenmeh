package com.setnameinc.voenmeh.profile.settings.groupselector

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.setnameinc.voenmeh.R

class SelectGroupAdapter(
    private val sourceSelectGroups: List<SelectGroupItem>,
    private val clickListener: (SelectGroupItem) -> Unit
) : ListAdapter<SelectGroupItem, SelectGroupViewHolder>(
    object : DiffUtil.ItemCallback<SelectGroupItem>() {
        override fun areItemsTheSame(
            oldItem: SelectGroupItem,
            newItem: SelectGroupItem
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: SelectGroupItem,
            newItem: SelectGroupItem
        ): Boolean = oldItem == newItem

    }
) {

    init {
        submitList(sourceSelectGroups)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SelectGroupViewHolder = SelectGroupViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(
                R.layout.item_dialog_sign_up_group_selector,
                parent,
                false
            )
    ) {
        clickListener(currentList[it])
    }

    override fun onBindViewHolder(holder: SelectGroupViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    fun filterName(name: String, commitCallback: Runnable) {
        val newList = sourceSelectGroups.filter {
            it.name.startsWith(name.toUpperCase())
        }
        submitList(newList, commitCallback)
    }

}