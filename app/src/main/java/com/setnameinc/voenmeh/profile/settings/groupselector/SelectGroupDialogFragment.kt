package com.setnameinc.voenmeh.profile.settings.groupselector

import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.tools.base.BaseBottomRoundSheetDialogFragment
import com.setnameinc.voenmeh.loginsignup.signup.Const.GROUPS_ID_TO_NAME
import com.setnameinc.voenmeh.tools.utils.startOnUi
import com.setnameinc.voenmeh.user.common.domain.UserInteractor
import kotlinx.android.synthetic.main.dialog_select_group.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import timber.log.Timber

class SelectGroupDialogFragment : BaseBottomRoundSheetDialogFragment(R.layout.dialog_select_group) {

    private val userInteractor: UserInteractor by inject()

    private val signUpSelectGroupAdapter: SelectGroupAdapter by lazy {
        SelectGroupAdapter(GROUPS_ID_TO_NAME) {
            Timber.d("item $it clicked")
            CoroutineScope(Dispatchers.Default).launch {
                userInteractor.updateUserGroupId(
                    it.id,
                    complete = {
                        startOnUi {
                            if (findNavController().currentDestination?.id == R.id.timetableSelectGroup) {
                                findNavController().popBackStack(R.id.timetableLoading, false)
                            }
                            dismiss()
                        }
                    }
                )
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        groupsRecyclerView.apply {
            adapter = signUpSelectGroupAdapter
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            setHasFixedSize(true)
            itemAnimator = null
        }
        closeDialogImageView.setDebounceClickListener {
            dismiss()
        }
        searchEditText.doAfterTextChanged {
            signUpSelectGroupAdapter.filterName(it.toString()) {
                groupsRecyclerView.scrollToPosition(0)
            }
        }
        if (dialog?.window != null) {
            dialog?.window
                ?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        }

    }

}