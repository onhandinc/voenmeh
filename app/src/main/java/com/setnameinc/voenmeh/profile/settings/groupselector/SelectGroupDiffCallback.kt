package com.setnameinc.voenmeh.profile.settings.groupselector

import androidx.recyclerview.widget.DiffUtil

class SelectGroupDiffCallback(
    private val newList: List<SelectGroupItem>,
    private val oldlist: List<SelectGroupItem>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return oldlist.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldlist[oldItemPosition].id === newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldlist[oldItemPosition] == newList[newItemPosition]
    }

}