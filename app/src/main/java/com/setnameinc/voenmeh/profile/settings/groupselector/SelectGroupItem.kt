package com.setnameinc.voenmeh.profile.settings.groupselector

data class SelectGroupItem(
    val id: String,
    val number: Int,
    var name: String,
    var isSelected: Boolean = false
)