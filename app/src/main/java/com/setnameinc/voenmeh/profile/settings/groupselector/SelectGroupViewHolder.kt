package com.setnameinc.voenmeh.profile.settings.groupselector

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import kotlinx.android.synthetic.main.item_dialog_sign_up_group_selector.view.*

class SelectGroupViewHolder(
    val view: View,
    val clickListener: (position: Int) -> Unit
) : RecyclerView.ViewHolder(view), View.OnClickListener {

    init {
        view.setDebounceClickListener(clickListener = this)
    }

    fun bind(model: SelectGroupItem) {
        view.apply {
            groupNameTextView.text = model.name
            if (model.isSelected) {
                selectedImageView.visibility = View.VISIBLE
            } else {
                selectedImageView.visibility = View.GONE
            }
        }
    }

    override fun onClick(v: View?) {
        clickListener(absoluteAdapterPosition)
    }

}