package com.setnameinc.voenmeh.profile.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.loginsignup.login.main.ui.LoginActivity
import com.setnameinc.voenmeh.profile.viewmodel.profile.ProfileViewModel
import com.setnameinc.voenmeh.profile.viewmodel.profile.model.FetchStatus
import com.setnameinc.voenmeh.profile.viewmodel.profile.model.ProfileEvent
import com.setnameinc.voenmeh.profile.viewmodel.profile.model.ProfileViewState
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : Fragment(R.layout.fragment_profile) {

    private val viewModel: ProfileViewModel by viewModel()

    private val dialog = SelectGroupDialogFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewState(it)
                }
        }
    }

    private fun bindViewState(state: ProfileViewState) {
        when (state) {
            is ProfileViewState.LogOut -> {
                when (state.fetchStatus) {
                    FetchStatus.Loading -> {

                    }
                    FetchStatus.Success -> {
                        startActivity(Intent(this.activity, LoginActivity::class.java))
                        this.activity?.finish()
                    }
                    FetchStatus.Error -> {

                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        signOutTextView.setDebounceClickListener(debounceTimeMillis = 2000) {
            viewModel.obtainEvent(ProfileEvent.LogOut)
        }
        selectGroupTextView.setDebounceClickListener(debounceTimeMillis = 2000) {
            dialog.show(parentFragmentManager, dialog.tag)
        }
    }
}