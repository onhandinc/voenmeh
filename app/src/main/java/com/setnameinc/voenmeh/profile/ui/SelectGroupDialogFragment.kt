package com.setnameinc.voenmeh.profile.ui

import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.tools.base.BaseBottomRoundSheetDialogFragment
import com.setnameinc.voenmeh.profile.viewmodel.select.SelectGroupViewModel
import com.setnameinc.voenmeh.profile.viewmodel.select.model.FetchStatus
import com.setnameinc.voenmeh.profile.viewmodel.select.model.SelectGroupAction
import com.setnameinc.voenmeh.profile.viewmodel.select.model.SelectGroupEvent
import com.setnameinc.voenmeh.profile.viewmodel.select.model.SelectGroupViewState
import com.setnameinc.voenmeh.profile.settings.groupselector.SelectGroupAdapter
import kotlinx.android.synthetic.main.dialog_select_group.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class SelectGroupDialogFragment : BaseBottomRoundSheetDialogFragment(R.layout.dialog_select_group) {

    private val selectGroupViewModel: SelectGroupViewModel by viewModel()

    private val signUpSelectGroupAdapter: SelectGroupAdapter by lazy {
        SelectGroupAdapter(selectGroupViewModel.listOfSelectGroups) {
            Timber.d("item $it clicked")
            selectGroupViewModel.obtainEvent(SelectGroupEvent.SaveGroupId(it.id))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            selectGroupViewModel.viewStates()
                .filterNotNull()
                .collect {
                    when (it) {
                        is SelectGroupViewState.SaveGroupId -> {
                            when (it.fetchStatus) {
                                FetchStatus.Success -> {
                                    selectGroupViewModel.obtainEvent(SelectGroupEvent.ResetSaveGroupId)
                                    dismiss()
                                }
                            }
                        }
                    }
                }
        }
        lifecycleScope.launchWhenStarted {
            selectGroupViewModel.viewActions()
                .filterNotNull()
                .collect {
                    when (it) {
                        is SelectGroupAction.UpdateList -> {
                            signUpSelectGroupAdapter.submitList(selectGroupViewModel.listOfSelectGroups)
                        }
                    }
                }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        groupsRecyclerView.apply {
            adapter = signUpSelectGroupAdapter
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            itemAnimator = null
            setHasFixedSize(true)
        }
        closeDialogImageView.setDebounceClickListener {
            dismiss()
        }
        searchEditText.doAfterTextChanged {
            signUpSelectGroupAdapter.filterName(it.toString()) {
                lifecycleScope.launchWhenStarted {
                    groupsRecyclerView.scrollToPosition(0)
                }
            }
        }
        if (dialog?.window != null) {
            dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        searchEditText.setText("")
        super.onDismiss(dialog)
    }

}