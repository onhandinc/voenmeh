package com.setnameinc.voenmeh.profile.viewmodel.profile

import com.setnameinc.voenmeh.profile.domain.ProfileInteractor
import com.setnameinc.voenmeh.profile.viewmodel.profile.model.FetchStatus
import com.setnameinc.voenmeh.profile.viewmodel.profile.model.ProfileEvent
import com.setnameinc.voenmeh.profile.viewmodel.profile.model.ProfileViewState
import com.setnameinc.voenmeh.tools.base.BaseSimpleFlowViewModel

class ProfileViewModel(
    private val profileInteractor: ProfileInteractor
) : BaseSimpleFlowViewModel<ProfileViewState, ProfileEvent>() {

    override suspend fun obtainInternalEvent(viewEvent: ProfileEvent) {
        when (viewEvent) {
            ProfileEvent.LogOut -> {
                viewState = ProfileViewState.LogOut(FetchStatus.Loading)
                profileInteractor.signOut(
                    complete = {
                        viewState = ProfileViewState.LogOut(FetchStatus.Success)
                    },
                    error = {
                        viewState = ProfileViewState.LogOut(FetchStatus.Error)
                    }
                )
            }
        }
    }

}