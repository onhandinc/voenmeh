package com.setnameinc.voenmeh.profile.viewmodel.profile.model

sealed class ProfileEvent {
    object LogOut : ProfileEvent()
}