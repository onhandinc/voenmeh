package com.setnameinc.voenmeh.profile.viewmodel.profile.model

sealed class ProfileViewState {
    data class LogOut(val fetchStatus: FetchStatus) : ProfileViewState()
}

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}