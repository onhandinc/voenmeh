package com.setnameinc.voenmeh.profile.viewmodel.select

import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.profile.viewmodel.select.model.FetchStatus
import com.setnameinc.voenmeh.profile.viewmodel.select.model.SelectGroupAction
import com.setnameinc.voenmeh.profile.viewmodel.select.model.SelectGroupEvent
import com.setnameinc.voenmeh.profile.viewmodel.select.model.SelectGroupViewState
import com.setnameinc.voenmeh.profile.settings.groupselector.SelectGroupItem
import com.setnameinc.voenmeh.loginsignup.signup.Const
import com.setnameinc.voenmeh.user.common.domain.UserInteractor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

class SelectGroupViewModel(
    private val userInteractor: UserInteractor
) : BaseFlowViewModel<SelectGroupViewState, SelectGroupAction, SelectGroupEvent>() {

    val listOfSelectGroups: List<SelectGroupItem> = Const.GROUPS_ID_TO_NAME

    init {
        CoroutineScope(Dispatchers.Default).launch {
            userInteractor.requestUserData(
                complete = { userData ->
                    Timber.d("UserInteractorState.ResponseUserData, id = ${userData.groupId}")
                    listOfSelectGroups.forEach { it.isSelected = false }
                    val changeIndex =
                        listOfSelectGroups.indexOfFirst { it.id == userData.groupId }
                    if (changeIndex != -1) {
                        listOfSelectGroups[changeIndex].isSelected = true
                    }
                    viewAction = SelectGroupAction.UpdateList
                },
                error = {

                }
            )
        }
    }

    override suspend fun obtainInternalEvent(viewEvent: SelectGroupEvent) {
        when (viewEvent) {
            is SelectGroupEvent.SaveGroupId -> {
                userInteractor.updateUserGroupId(viewEvent.id) { groupId ->
                    Timber.d("UserInteractorState.UserGroupUpdated")
                    viewState = SelectGroupViewState.SaveGroupId(FetchStatus.Success)
                    listOfSelectGroups.forEach { it.isSelected = false }
                    val changeIndex =
                        listOfSelectGroups.indexOfFirst { it.id == groupId }
                    if (changeIndex != -1) {
                        listOfSelectGroups[changeIndex].isSelected = true
                    }
                    viewAction = SelectGroupAction.UpdateList
                }
            }
            SelectGroupEvent.ResetSaveGroupId -> {
                viewState = SelectGroupViewState.SaveGroupId(FetchStatus.Loading)
            }
        }
    }

}