package com.setnameinc.voenmeh.profile.viewmodel.select.model

sealed class SelectGroupAction {
    object UpdateList : SelectGroupAction()
}