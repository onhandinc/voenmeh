package com.setnameinc.voenmeh.profile.viewmodel.select.model

sealed class SelectGroupEvent {
    data class SaveGroupId(val id: String) : SelectGroupEvent()
    object ResetSaveGroupId : SelectGroupEvent()
}