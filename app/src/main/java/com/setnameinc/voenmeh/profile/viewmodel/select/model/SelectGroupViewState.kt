package com.setnameinc.voenmeh.profile.viewmodel.select.model

sealed class SelectGroupViewState {
    data class SaveGroupId(
        val fetchStatus: FetchStatus,
        val result: Boolean? = null
    ) : SelectGroupViewState()
}

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}