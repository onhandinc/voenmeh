package com.setnameinc.voenmeh.search.api

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SearchApi {

    @GET("search")
    fun search(
        @Query("startWith") startWith: String
    ): Deferred<Response<List<SearchResponse>>>

}