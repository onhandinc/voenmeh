package com.setnameinc.voenmeh.search.api

import com.google.gson.annotations.SerializedName

data class SearchResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("type")
    val type: Int,
    @SerializedName("name")
    val name: String
)