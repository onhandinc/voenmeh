package com.setnameinc.voenmeh.search.di

import com.setnameinc.voenmeh.app.api.RetrofitConst
import com.setnameinc.voenmeh.search.api.SearchApi
import com.setnameinc.voenmeh.search.domain.SearchInteractor
import com.setnameinc.voenmeh.search.ui.SearchFragment
import com.setnameinc.voenmeh.search.viewmodel.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

object SearchModule {
    val module = module {
        scope<SearchFragment> {
            viewModel {
                SearchViewModel(
                    searchInteractor = get()
                )
            }
            scoped {
                SearchInteractor(searchApi = get())
            }
            scoped {
                get<Retrofit>(
                    named(RetrofitConst.VOENMEH_SERVER_RETROFIT_INJECTION_KEY)
                ).create(SearchApi::class.java)
            }
        }
    }
}