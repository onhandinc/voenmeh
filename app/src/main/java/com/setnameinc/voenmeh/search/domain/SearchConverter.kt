package com.setnameinc.voenmeh.search.domain

import com.setnameinc.voenmeh.search.api.SearchResponse

object SearchConverter {
    fun List<SearchResponse>.convertFromApi(): List<SearchModel> = this.map {
        SearchModel(
            it.id,
            it.type,
            it.name
        )
    }
}