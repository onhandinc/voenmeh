package com.setnameinc.voenmeh.search.domain

import com.setnameinc.voenmeh.search.api.SearchApi
import com.setnameinc.voenmeh.search.domain.SearchConverter.convertFromApi
import timber.log.Timber
import java.lang.NullPointerException

class SearchInteractor(private val searchApi: SearchApi) {

    suspend fun search(
        query: String,
        complete: (String, List<SearchModel>) -> Unit,
        error: (Throwable) -> Unit
    ) {
        val res = searchApi.search(query)
        Timber.d("res = ${res.await()}")
        val response = searchApi.search(query).await().body()?.convertFromApi()
        if (response != null) {
            complete(
                query,
                response
            )
        } else {
            error(NullPointerException())
        }
    }

}