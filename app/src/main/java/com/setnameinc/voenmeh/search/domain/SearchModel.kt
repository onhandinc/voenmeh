package com.setnameinc.voenmeh.search.domain

data class SearchModel(
    val id: String,
    val type: Int,
    val name: String
)