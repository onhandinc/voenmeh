package com.setnameinc.voenmeh.search.domain

enum class SearchModelType(
    val type: Int
) {
    LECTURER(1),
    GROUP(2),
    CLASSROOM(3)
}