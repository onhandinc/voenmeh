package com.setnameinc.voenmeh.search.ui

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.search.domain.SearchModelType
import com.setnameinc.voenmeh.search.viewmodel.*
import com.setnameinc.voenmeh.tools.utils.viewModel
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.scope.fragmentScope
import timber.log.Timber

class SearchFragment : Fragment(R.layout.fragment_search) {

    private val searchViewModel: SearchViewModel by fragmentScope().viewModel(this)

    private val searchAdapter: SearchAdapter by lazy {
        SearchAdapter { type, id ->
            when (type) {
                SearchModelType.LECTURER.type -> {
                    findNavController().navigate(
                        R.id.toLecturerPage,
                        Bundle().apply {
                            putString("lecturerId", id)
                        }
                    )
                }
                SearchModelType.GROUP.type -> {
                    findNavController().navigate(
                        R.id.toGroupPage,
                        Bundle().apply {
                            putString("groupId", id)
                        }
                    )
                }
                SearchModelType.CLASSROOM.type -> {
                    findNavController().navigate(
                        R.id.toClassroomPage,
                        Bundle().apply {
                            putString("classroomName", id)
                        }
                    )
                }
            }
        }.apply {
            stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.ALLOW
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            searchViewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewState(it)
                }
        }
        lifecycleScope.launchWhenStarted {
            searchEditText.requestKeyboardFocus()
        }
    }

    private var isSearchBlocked: Boolean = true

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        searchEditText.doOnTextChanged { text, _, _, _ ->
            Timber.d("try 1, isSearchBlocked = $isSearchBlocked")
            if (isSearchBlocked) {
                isSearchBlocked = false
                return@doOnTextChanged
            }
            Timber.d("check")
            if (text.toString().isNotEmpty()) {
                searchViewModel.obtainEvent(SearchEvent.SaveQuery(text.toString()))
                searchViewModel.obtainEvent(SearchEvent.Search)
            } else {
                resultRecyclerView.visibility = View.GONE
            }
        }
        resultRecyclerView.apply {
            this.adapter = searchAdapter
            layoutManager = LinearLayoutManager(
                context,
                LinearLayoutManager.VERTICAL,
                false
            )
            setHasFixedSize(true)
            itemAnimator = null
        }
        if (searchViewModel.searchResult.isNotEmpty()) {
            errorQueueTextView.visibility = View.GONE
            searchingProgressBar.visibility = View.GONE
            noFoundRecordsTextView.visibility = View.GONE
            searchAdapter.submitList(searchViewModel.searchResult) {
                resultRecyclerView.visibility = View.VISIBLE
            }
        } else {
            isSearchBlocked = false
        }
    }

    private fun bindViewState(viewState: SearchViewState) {
        when (viewState) {
            is SearchViewState.Search -> {
                when (viewState.fetchStatus) {
                    FetchStatus.Success -> {
                        Timber.d("success, list = ${viewState.findResult}")
                        searchingProgressBar.visibility = View.GONE
                        errorQueueTextView.visibility = View.GONE
                        when {
                            searchEditText.text.toString().isEmpty() -> {
                                resultRecyclerView.visibility = View.GONE
                            }
                            viewState.findResult?.isEmpty() == true -> {
                                noFoundRecordsTextView.visibility = View.VISIBLE
                                resultRecyclerView.visibility = View.GONE
                            }
                            else -> {
                                noFoundRecordsTextView.visibility = View.GONE
                                searchAdapter.submitList(viewState.findResult) {
                                    resultRecyclerView.scrollToPosition(0)
                                    resultRecyclerView.visibility = View.VISIBLE
                                }
                            }
                        }
                    }
                    FetchStatus.Loading -> {
                        Timber.d("loading")
                        noFoundRecordsTextView.visibility = View.GONE
                        resultRecyclerView.visibility = View.GONE
                        errorQueueTextView.visibility = View.GONE
                        searchingProgressBar.visibility = View.VISIBLE
                        //loading
                    }
                    FetchStatus.Error -> {
                        Timber.d("error")
                        noFoundRecordsTextView.visibility = View.GONE
                        resultRecyclerView.visibility = View.GONE
                        searchingProgressBar.visibility = View.GONE
                        errorQueueTextView.visibility = View.VISIBLE
                    }
                }
            }
        }
    }

    override fun onStop() {
        super.onStop()
        searchEditText.clearKeyboardFocus()
        isSearchBlocked = true
    }

    private fun EditText.clearKeyboardFocus() {
        this.clearFocus()
        val imm: InputMethodManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(this.windowToken, 0)
    }

    private fun EditText.requestKeyboardFocus() {
        this.requestFocus()
        val imm: InputMethodManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    }

}