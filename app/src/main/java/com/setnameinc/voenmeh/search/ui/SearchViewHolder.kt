package com.setnameinc.voenmeh.search.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.setnameinc.voenmeh.search.domain.SearchModel
import kotlinx.android.synthetic.main.item_search.view.*

class SearchViewHolder(
    private val view: View,
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.ViewHolder(view), View.OnClickListener {

    init {
        view.setOnClickListener(this)
    }

    fun bind(searchModel: SearchModel) {
        view.apply {
            nameTextView.text = ""
            nameTextView.text = searchModel.name
        }
    }

    override fun onClick(p0: View?) {
        clickListener(absoluteAdapterPosition)
    }

}