package com.setnameinc.voenmeh.search.viewmodel

sealed class SearchEvent {
    object Search : SearchEvent()
    data class SaveQuery(
        val query: String
    ) : SearchEvent()
}