package com.setnameinc.voenmeh.search.viewmodel

import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.search.domain.SearchInteractor
import com.setnameinc.voenmeh.search.domain.SearchModel
import timber.log.Timber

class SearchViewModel(
    private val searchInteractor: SearchInteractor
) : BaseFlowViewModel<SearchViewState, SearchAction, SearchEvent>() {

    private var query: String = ""
    var searchResult: MutableList<SearchModel> = mutableListOf()

    override suspend fun obtainInternalEvent(viewEvent: SearchEvent) {
        when (viewEvent) {
            is SearchEvent.Search -> {
                viewState = SearchViewState.Search(
                    fetchStatus = FetchStatus.Loading
                )
                searchInteractor.search(
                    query,
                    complete = { queryResp, listResp ->
                        if (query == queryResp) {
                            searchResult.apply {
                                clear()
                                addAll(listResp)
                            }
                            viewState = SearchViewState.Search(
                                fetchStatus = FetchStatus.Success,
                                findResult = listResp
                            )
                        } else {
                            //ignore
                        }
                    },
                    error = {
                        Timber.e(it)
                        viewState = SearchViewState.Search(
                            fetchStatus = FetchStatus.Error
                        )
                    }
                )
            }
            is SearchEvent.SaveQuery -> {
                query = viewEvent.query
            }
        }
    }

}