package com.setnameinc.voenmeh.search.viewmodel

import com.setnameinc.voenmeh.search.domain.SearchModel

sealed class SearchViewState(
    open val fetchStatus: FetchStatus
) {
    data class Search(
        override val fetchStatus: FetchStatus,
        val findResult: List<SearchModel>? = null
    ) : SearchViewState(fetchStatus)
}

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}