package com.setnameinc.voenmeh.splash.api

import com.google.firebase.auth.FirebaseAuth
import java.lang.Exception

class SplashApi(private val firebaseAuth: FirebaseAuth) {

    suspend fun isUserLoggedIn(
        complete: (Boolean) -> Unit,
        error: (Throwable) -> Unit
    ) {
        try {
            complete(firebaseAuth.currentUser != null)
        } catch (e: Exception) {
            error(e)
        }
    }

}