package com.setnameinc.voenmeh.splash.di

import com.setnameinc.voenmeh.splash.api.SplashApi
import com.setnameinc.voenmeh.splash.domain.SplashInteractor
import com.setnameinc.voenmeh.splash.viewmodel.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object SplashModule {
    val module = module {
        single {
            SplashInteractor(
                splashApi = get(),
                userInteractor = get(),
                vkInteractor = get()
            )
        }
        viewModel {
            SplashViewModel(
                splashInteractor = get()
            )
        }
        single { SplashApi(firebaseAuth = get()) }
    }
}