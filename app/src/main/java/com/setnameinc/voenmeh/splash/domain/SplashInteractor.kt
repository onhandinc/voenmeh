package com.setnameinc.voenmeh.splash.domain

import com.setnameinc.voenmeh.splash.api.SplashApi
import com.setnameinc.voenmeh.user.common.domain.UserInteractor
import com.setnameinc.voenmeh.tools.vk.domain.VKInteractor
import timber.log.Timber

class SplashInteractor(
    private val splashApi: SplashApi,
    private val userInteractor: UserInteractor,
    private val vkInteractor: VKInteractor
) {

    suspend fun requestUserBlocked(
        complete: suspend (Boolean) -> Unit,
        error: () -> Unit
    ) {
        userInteractor.requestUserBlocked(
            complete = complete,
            error = error
        )
    }

    suspend fun requestUserLoggedIn(
        complete: (Boolean) -> Unit
    ) {
        splashApi.isUserLoggedIn(
            complete = {
                complete(it || vkInteractor.getToken().isNotEmpty())
            },
            error = {
                complete(false)
                Timber.e(it)
            }
        )
    }

}