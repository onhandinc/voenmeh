package com.setnameinc.voenmeh.splash.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.loginsignup.login.main.ui.LoginActivity
import com.setnameinc.voenmeh.main.ui.MainActivity
import com.setnameinc.voenmeh.splash.viewmodel.SplashViewModel
import com.setnameinc.voenmeh.splash.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.splash.viewmodel.model.SplashEvent
import com.setnameinc.voenmeh.splash.viewmodel.model.SplashViewState
import com.setnameinc.voenmeh.user.blocked.ui.UserBlockedActivity
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class SplashActivity : AppCompatActivity() {

    private val splashViewModel: SplashViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            splashViewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindState(it)
                }
        }
        splashViewModel.obtainEvent(SplashEvent.Login)
    }

    private fun bindState(state: SplashViewState) {
        when (state) {
            is SplashViewState.Login -> {
                when (state.status) {
                    FetchStatus.Success -> {
                        startActivity(MainActivity::class.java)
                    }
                    FetchStatus.Error -> {
                        startActivity(LoginActivity::class.java)
                    }
                }
            }
            is SplashViewState.UserBlocked -> {
                when (state.status) {
                    FetchStatus.Success -> {
                        startActivity(UserBlockedActivity::class.java)
                    }
                    FetchStatus.Error -> {
                        Timber.e("unknown error")
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        //just ignore
    }

    private fun startActivity(className: Class<*>) {
        startActivity(Intent(this@SplashActivity, className))
        overridePendingTransition(0, R.anim.disapperar_in_center)
        this@SplashActivity.finish()
    }

}