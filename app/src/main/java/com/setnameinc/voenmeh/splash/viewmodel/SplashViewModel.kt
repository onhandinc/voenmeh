package com.setnameinc.voenmeh.splash.viewmodel

import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.splash.domain.SplashInteractor
import com.setnameinc.voenmeh.splash.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.splash.viewmodel.model.SplashAction
import com.setnameinc.voenmeh.splash.viewmodel.model.SplashEvent
import com.setnameinc.voenmeh.splash.viewmodel.model.SplashViewState

class SplashViewModel(
    private val splashInteractor: SplashInteractor
) : BaseFlowViewModel<SplashViewState, SplashAction, SplashEvent>() {

    override suspend fun obtainInternalEvent(viewEvent: SplashEvent) {
        when (viewEvent) {
            SplashEvent.Login -> {
                splashInteractor.requestUserBlocked(
                    complete = { isBlocked ->
                        if (isBlocked) {
                            viewState = SplashViewState.UserBlocked(FetchStatus.Success)
                        } else {
                            splashInteractor.requestUserLoggedIn { isLoggedId ->
                                if (isLoggedId) {
                                    viewState = SplashViewState.Login(FetchStatus.Success)
                                } else {
                                    viewState = SplashViewState.Login(FetchStatus.Error)
                                }
                            }
                        }
                    },
                    error = {
                        viewState = SplashViewState.UserBlocked(FetchStatus.Error)
                    }
                )
            }
        }
    }
}