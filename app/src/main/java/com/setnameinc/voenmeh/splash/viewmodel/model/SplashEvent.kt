package com.setnameinc.voenmeh.splash.viewmodel.model

sealed class SplashEvent {
    object Login : SplashEvent()
}