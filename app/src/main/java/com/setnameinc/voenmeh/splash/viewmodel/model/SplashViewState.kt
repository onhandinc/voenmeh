package com.setnameinc.voenmeh.splash.viewmodel.model

sealed class SplashViewState {
    data class Login(
        val status: FetchStatus
    ) : SplashViewState()

    data class UserBlocked(
        val status: FetchStatus
    ) : SplashViewState()
}

sealed class FetchStatus {
    object Success : FetchStatus()
    object Error : FetchStatus()
}