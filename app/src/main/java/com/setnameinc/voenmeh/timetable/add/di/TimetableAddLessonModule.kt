package com.setnameinc.voenmeh.timetable.add.di

import com.setnameinc.voenmeh.timetable.add.domain.TimetableAddLessonInteractor
import com.setnameinc.voenmeh.timetable.add.ui.TimetableAddLessonActivity
import com.setnameinc.voenmeh.timetable.add.viewmodel.TimetableAddLessonViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object TimetableAddLessonModule {
    val module = module {
        scope<TimetableAddLessonActivity> {
            scoped {
                TimetableAddLessonInteractor(timetableRepository = get())
            }
            viewModel {
                TimetableAddLessonViewModel(interactor = get())
            }
        }
    }
}