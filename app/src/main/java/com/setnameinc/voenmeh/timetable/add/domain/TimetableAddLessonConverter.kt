package com.setnameinc.voenmeh.timetable.add.domain

import com.setnameinc.voenmeh.timetable.add.viewmodel.TimetableItem
import com.setnameinc.voenmeh.timetable.common.repository.TimetableEntity
import com.setnameinc.voenmeh.tools.utils.Id.generateStringId

object TimetableAddLessonConverter {

    fun TimetableItem.toDatabase(): List<TimetableEntity> =
        this.timetableWeekDayList.flatMap { weekDay ->
            this.timetableWeekTypeList.map { weekType ->
                TimetableEntity(
                    id = generateStringId(),
                    groupId = this.groupId,
                    groupNumber = this.groupNumber,
                    groupName = this.groupName,
                    classroom = this.classroom,
                    classroomName = "${this.classroom} ${this.timetableLessonType.lessonName}",
                    numberDayInWeek = weekDay.numberDayInWeek,
                    discipline = this.discipline,
                    disciplineName = this.discipline,
                    lecturers = this.lecturers,
                    startTime = this.startTime,
                    startTimeText = this.startTimeText,
                    endTimeText = this.endTimeText,
                    weekCode = weekType.weekCode,
                    type = this.timetableLessonType.type
                )
            }
        }

}