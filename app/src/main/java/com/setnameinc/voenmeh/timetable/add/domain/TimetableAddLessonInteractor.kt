package com.setnameinc.voenmeh.timetable.add.domain

import com.setnameinc.voenmeh.timetable.add.domain.TimetableAddLessonConverter.toDatabase
import com.setnameinc.voenmeh.timetable.add.viewmodel.TimetableItem
import com.setnameinc.voenmeh.timetable.common.repository.TimetableRepository
import com.setnameinc.voenmeh.user.common.domain.GroupInfoModel

class TimetableAddLessonInteractor(private val timetableRepository: TimetableRepository) {

    fun save(
        lesson: TimetableItem,
        complete: () -> Unit,
        error: () -> Unit
    ) {
        timetableRepository.saveAll(lesson.toDatabase())
        complete()
    }

    fun requestUserInfo(
        groupId: String,
        complete: (GroupInfoModel) -> Unit
    ) {
        complete(timetableRepository.groupInfoByGroupId(groupId))
    }
}