package com.setnameinc.voenmeh.timetable.add.ui

import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.lifecycleScope
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.snackbar.Snackbar
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.timetable.add.viewmodel.TimetableAddLessonViewModel
import com.setnameinc.voenmeh.timetable.add.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.timetable.add.viewmodel.model.TimetableAddLessonAction
import com.setnameinc.voenmeh.timetable.add.viewmodel.model.TimetableAddLessonActionViewState
import com.setnameinc.voenmeh.timetable.add.viewmodel.model.TimetableAddLessonEvent
import com.setnameinc.voenmeh.tools.utils.viewModel
import kotlinx.android.synthetic.main.activity_add_lesson.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.scope.activityScope
import timber.log.Timber

class TimetableAddLessonActivity : AppCompatActivity(R.layout.activity_add_lesson) {

    private val viewModel: TimetableAddLessonViewModel by activityScope().viewModel(this)

    private val groupId: String by lazy {
        intent.extras?.getString("groupId") ?: ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("groupId = $groupId")
        viewModel.obtainEvent(TimetableAddLessonEvent.RequestGroupInfo(groupId))
        startTimeTextView.setDebounceClickListener {
            Timber.d("clicked startTimeTextView")
            viewModel.obtainEvent(TimetableAddLessonEvent.EditStartTime)
        }
        endTimeTextView.setDebounceClickListener {
            Timber.d("clicked endTimeTextView")
            viewModel.obtainEvent(TimetableAddLessonEvent.EditEndTime)
        }
        saveButton.setDebounceClickListener {
            viewModel.obtainEvent(TimetableAddLessonEvent.Save)
        }
        closeImageButton.setDebounceClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
        lessonNameTextView.doAfterTextChanged {
            viewModel.obtainEvent(
                TimetableAddLessonEvent.SaveTitle(it.toString())
            )
            errorSnackbar?.dismiss()
        }
        classroomTextView.doAfterTextChanged {
            viewModel.obtainEvent(
                TimetableAddLessonEvent.SaveClassroom(it.toString())
            )
            errorSnackbar?.dismiss()
        }
        lecturerTextView.doAfterTextChanged {
            viewModel.obtainEvent(
                TimetableAddLessonEvent.SaveLecturer(it.toString())
            )
        }
        lessonTypeChipGroup.setOnCheckedChangeListener { group, checkedId ->
            Timber.d("checkedId = $checkedId")
            viewModel.obtainEvent(
                TimetableAddLessonEvent.SaveLessonType(checkedId)
            )
        }
        weekDayChipGroup.setOnCheckedListener(
            checked = {
                viewModel.obtainEvent(TimetableAddLessonEvent.WeekDaySelected(it))
            },
            unchecked = {
                viewModel.obtainEvent(TimetableAddLessonEvent.WeekDayUnselected(it))
            }
        )
        weekTypeChipGroup.setOnCheckedListener(
            checked = {
                viewModel.obtainEvent(TimetableAddLessonEvent.WeekTypeSelected(it))
            },
            unchecked = {
                viewModel.obtainEvent(TimetableAddLessonEvent.WeekTypeUnselected(it))
            }
        )
        lifecycleScope.launchWhenStarted {
            viewModel.viewActions()
                .filterNotNull()
                .collect {
                    bindViewAction(it)
                }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewStates(it)
                }
        }
    }

    private fun bindViewAction(action: TimetableAddLessonAction) {
        when (action) {
            is TimetableAddLessonAction.ShowStartTimePicker -> {
                TimePickerDialog(
                    this,
                    action.listener,
                    action.hour,
                    action.minute,
                    true
                ).show()
            }
            is TimetableAddLessonAction.ShowEndTimePicker -> {
                TimePickerDialog(
                    this,
                    action.listener,
                    action.hour,
                    action.minute,
                    true
                ).show()
            }
            is TimetableAddLessonAction.UpdateStartTime -> {
                startTimeTextView.text = action.time
                errorSnackbar?.dismiss()
            }
            is TimetableAddLessonAction.UpdateEndTime -> {
                endTimeTextView.text = action.time
                errorSnackbar?.dismiss()
            }
            is TimetableAddLessonAction.ShowSnackbar -> {
                showSnackbar(action.message)
            }
        }
    }

    private fun bindViewStates(viewState: TimetableAddLessonActionViewState) {
        when (viewState.fetchStatus) {
            FetchStatus.Success -> {
                //update list in mainActivity by passing back argument
                setResult(
                    Activity.RESULT_OK,
                    Intent().apply {
                        putExtra("added", true)
                    }
                )
                finish()
            }
            FetchStatus.Loading -> {
                //show snackbar
            }
            is FetchStatus.Error -> {
                //
            }
        }
    }

    private fun ChipGroup.setOnCheckedListener(
        checked: (id: Int) -> Unit,
        unchecked: (id: Int) -> Unit
    ) {
        for (i in 0 until this.childCount) {
            (this.getChildAt(i) as? Chip)
                ?.setOnCheckedChangeListener { view, isChecked ->
                    if (isChecked) {
                        checked(view.id)
                    } else {
                        unchecked(view.id)
                    }
                }
        }
    }

    private var errorSnackbar: Snackbar? = null

    private fun showSnackbar(message: String) {
        errorSnackbar?.dismiss()
        errorSnackbar = Snackbar.make(rootRelativeLayout, message, Snackbar.LENGTH_LONG)
        errorSnackbar?.show()
    }

}