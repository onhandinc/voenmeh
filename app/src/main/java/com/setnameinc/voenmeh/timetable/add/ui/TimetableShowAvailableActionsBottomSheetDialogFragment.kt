package com.setnameinc.voenmeh.timetable.add.ui

import android.os.Bundle
import android.view.View
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.tools.base.BaseBottomRoundSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_timetable_show_available_actions.*

class TimetableShowAvailableActionsBottomSheetDialogFragment(
    private val addLesson: () -> Unit
) : BaseBottomRoundSheetDialogFragment(R.layout.dialog_timetable_show_available_actions) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        closeDialogImageButton.setDebounceClickListener {
            dismiss()
        }
        addExerciseRelativeLayout.setDebounceClickListener {
            addLesson()
            dismiss()
        }
    }


}