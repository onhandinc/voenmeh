package com.setnameinc.voenmeh.timetable.add.viewmodel

import android.app.TimePickerDialog
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.timetable.add.domain.TimetableAddLessonInteractor
import com.setnameinc.voenmeh.timetable.add.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.timetable.add.viewmodel.model.TimetableAddLessonAction
import com.setnameinc.voenmeh.timetable.add.viewmodel.model.TimetableAddLessonActionViewState
import com.setnameinc.voenmeh.timetable.add.viewmodel.model.TimetableAddLessonEvent
import com.setnameinc.voenmeh.timetable.add.domain.TimetableLessonType
import com.setnameinc.voenmeh.timetable.add.domain.TimetableWeekDay
import com.setnameinc.voenmeh.timetable.add.domain.TimetableWeekType
import com.setnameinc.voenmeh.tools.utils.TimeUtils
import com.setnameinc.voenmeh.tools.utils.TimeUtils.simpleDateFormat
import com.setnameinc.voenmeh.tools.utils.TimeUtils.syncCalendarToCurrentUTC
import com.setnameinc.voenmeh.tools.utils.TimeUtils.syncCalendarToUTC0
import timber.log.Timber
import java.util.*

class TimetableAddLessonViewModel(
    private val interactor: TimetableAddLessonInteractor
) : BaseFlowViewModel<TimetableAddLessonActionViewState, TimetableAddLessonAction,
        TimetableAddLessonEvent>() {

    private val startDateCalendar: Calendar = TimeUtils.calendarFactory().apply {
        this.syncCalendarToCurrentUTC()
    }
    private val endDateCalendar: Calendar = TimeUtils.calendarFactory()

    private var groupId: String = ""
    private var groupNumber: Int = -1
    private var groupName: String = ""
    private var startTime: Long = -1
    private var startTimeSimpleText: String = ""
    private var startTimeText: String = ""
    private var endTime: Long = -1
    private var endTimeSimpleText: String = ""
    private var endTimeText: String = ""
    private var lecturers: String = ""
    private var title: String = ""
    private var classroom: String = ""
    private var lessonTypeId: Int = R.id.lectureChip

    private val weekDaysIds: MutableList<Int> = mutableListOf(R.id.mondayChip)
    private val weekTypeIds: MutableList<Int> = mutableListOf(R.id.evenChip)

    override suspend fun obtainInternalEvent(viewEvent: TimetableAddLessonEvent) {
        when (viewEvent) {
            TimetableAddLessonEvent.Save -> {
                if (isBodyType() || isEmptyLecturerType() || isEmptySubjectType()) {
                    when {
                        groupNumber == -1 || groupName.isEmpty() -> {
                            viewState = TimetableAddLessonActionViewState(FetchStatus.Error)
                            viewAction = TimetableAddLessonAction.ShowSnackbar(
                                "Ваша группа не найдена :("
                            )
                            return
                        }
                        startTime == -1L || endTime == -1L -> {
                            viewState = TimetableAddLessonActionViewState(FetchStatus.Error)
                            viewAction = TimetableAddLessonAction.ShowSnackbar(
                                "Укажите время начала и конца пары"
                            )
                            return
                        }
                    }
                    val weekDays = weekDaysIds.map { id ->
                        TimetableWeekDay.values()
                            .first {
                                it.id == id
                            }
                    }
                    val weekTypes = weekTypeIds.map { id ->
                        TimetableWeekType.values()
                            .first {
                                it.id == id
                            }
                    }
                    if (weekDaysIds.size != weekDays.size || weekTypeIds.size != weekTypes.size) {
                        viewState = TimetableAddLessonActionViewState(FetchStatus.Error)
                        return
                        //TODO("unknown exception")
                    }
                    viewState = TimetableAddLessonActionViewState(FetchStatus.Loading)
                    interactor.save(
                        lesson = TimetableItem(
                            classroom = classroom,
                            discipline = title,
                            lecturers = lecturers,
                            startTime = convertStartTime(),
                            startTimeText = startTimeSimpleText,
                            endTimeText = endTimeSimpleText,
                            timetableWeekTypeList = weekTypes,
                            timetableWeekDayList = weekDays,
                            timetableLessonType = TimetableLessonType.values()
                                .first {
                                    it.id == lessonTypeId
                                },
                            groupId = groupId,
                            groupName = groupName,
                            groupNumber = groupNumber
                        ),
                        complete = {
                            viewState = TimetableAddLessonActionViewState(FetchStatus.Success)
                        },
                        error = {
                            viewState = TimetableAddLessonActionViewState(FetchStatus.Error)
                        }
                    )
                } else {
                    when {
                        title.isEmpty() -> {
                            viewState = TimetableAddLessonActionViewState(FetchStatus.Error)
                            viewAction = TimetableAddLessonAction.ShowSnackbar(
                                "Укажите преподавателя или название пары"
                            )
                            return
                        }
                    }
                }
            }
            is TimetableAddLessonEvent.SaveTitle -> {
                title = viewEvent.title
            }
            is TimetableAddLessonEvent.SaveLessonType -> {
                lessonTypeId = viewEvent.viewId
            }
            is TimetableAddLessonEvent.WeekDaySelected -> {
                weekDaysIds.add(viewEvent.viewId)
            }
            is TimetableAddLessonEvent.WeekDayUnselected -> {
                weekDaysIds.remove(viewEvent.viewId)
            }
            is TimetableAddLessonEvent.WeekTypeSelected -> {
                weekTypeIds.add(viewEvent.viewId)
            }
            is TimetableAddLessonEvent.WeekTypeUnselected -> {
                weekTypeIds.remove(viewEvent.viewId)
            }
            TimetableAddLessonEvent.EditStartTime -> {
                viewAction = TimetableAddLessonAction.ShowStartTimePicker(
                    startDateCalendar.get(Calendar.HOUR_OF_DAY),
                    startDateCalendar.get(Calendar.MINUTE),
                    startTimePickerListener
                )
            }
            TimetableAddLessonEvent.EditEndTime -> {
                viewAction = TimetableAddLessonAction.ShowEndTimePicker(
                    endDateCalendar.get(Calendar.HOUR_OF_DAY),
                    endDateCalendar.get(Calendar.MINUTE),
                    endTimePickerListener
                )
            }
            is TimetableAddLessonEvent.SaveClassroom -> {
                classroom = viewEvent.classroom
            }
            is TimetableAddLessonEvent.SaveLecturer -> {
                lecturers = viewEvent.lecturers
            }
            is TimetableAddLessonEvent.RequestGroupInfo -> {
                Timber.d("RequestGroupInfo with groupId = ${viewEvent.groupId}")
                interactor.requestUserInfo(
                    viewEvent.groupId,
                    complete = { groupInfo ->
                        groupId = groupInfo.groupId
                        groupName = groupInfo.groupName
                        groupNumber = groupInfo.groupNumber
                    }
                )
            }
        }
    }

    private val startTimePickerListener =
        TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
            if (endTimeText.isEmpty() || isDifferentOneLesson()) {
                startDateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                startDateCalendar.set(Calendar.MINUTE, minute)
                updateStartDate()
                endDateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                endDateCalendar.set(Calendar.MINUTE, minute)
                endDateCalendar.add(Calendar.HOUR_OF_DAY, 1)
                endDateCalendar.add(Calendar.MINUTE, 30)
                updateEndDate()
            } else {
                Timber.d("not empty")
                startDateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                startDateCalendar.set(Calendar.MINUTE, minute)
                updateStartDate()
            }
        }

    private val endTimePickerListener = TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
        endDateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        endDateCalendar.set(Calendar.MINUTE, minute)
        updateEndDate()
    }

    private fun updateStartDate() {
        startDateCalendar.syncCalendarToUTC0()
        startTime = startDateCalendar.timeInMillis
        startTimeSimpleText = simpleDateFormat.format(
            Date(startDateCalendar.timeInMillis)
        )
        startTimeText = startTimeSimpleText
        viewAction = TimetableAddLessonAction.UpdateStartTime(startTimeText)
    }

    private fun updateEndDate() {
        //no need to sync to UTC0, because startDateCalendar already synced
        endTime = endDateCalendar.timeInMillis
        endTimeSimpleText = simpleDateFormat.format(
            Date(endDateCalendar.timeInMillis)
        )
        endTimeText = endTimeSimpleText
        viewAction = TimetableAddLessonAction.UpdateEndTime(endTimeText)
    }

    private fun isBodyType(): Boolean =
        title.isNotEmpty() && lecturers.isNotEmpty()

    private fun isEmptyLecturerType(): Boolean =
        title.isNotEmpty() && lecturers.isEmpty()

    private fun isEmptySubjectType(): Boolean =
        title.isEmpty() && lecturers.isNotEmpty()

    private fun isDifferentOneLesson(): Boolean =
        endDateCalendar.timeInMillis - startDateCalendar.timeInMillis == 90 * 60 * 1000L

    private fun convertStartTime(): Long = simpleDateFormat.parse(startTimeSimpleText).time

}