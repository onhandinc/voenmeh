package com.setnameinc.voenmeh.timetable.add.viewmodel

import com.setnameinc.voenmeh.timetable.add.domain.TimetableLessonType
import com.setnameinc.voenmeh.timetable.add.domain.TimetableWeekDay
import com.setnameinc.voenmeh.timetable.add.domain.TimetableWeekType

data class TimetableItem(
    var groupId: String,
    var groupNumber: Int,
    var groupName: String,
    var classroom: String,
    var discipline: String,
    var lecturers: String,
    var startTime: Long,
    var startTimeText: String,
    var endTimeText: String,
    var timetableWeekTypeList: List<TimetableWeekType>,
    val timetableWeekDayList: List<TimetableWeekDay>,
    val timetableLessonType: TimetableLessonType
)