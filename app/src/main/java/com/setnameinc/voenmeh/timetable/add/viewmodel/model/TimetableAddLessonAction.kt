package com.setnameinc.voenmeh.timetable.add.viewmodel.model

import android.app.TimePickerDialog

sealed class TimetableAddLessonAction {
    data class ShowStartTimePicker(
        val hour: Int,
        val minute: Int,
        val listener: TimePickerDialog.OnTimeSetListener
    ) : TimetableAddLessonAction()

    data class ShowEndTimePicker(
        val hour: Int,
        val minute: Int,
        val listener: TimePickerDialog.OnTimeSetListener
    ) : TimetableAddLessonAction()

    data class UpdateStartTime(
        val time: String
    ) : TimetableAddLessonAction()

    data class UpdateEndTime(
        val time: String
    ) : TimetableAddLessonAction()

    data class ShowSnackbar(
        val message: String
    ) : TimetableAddLessonAction()
}