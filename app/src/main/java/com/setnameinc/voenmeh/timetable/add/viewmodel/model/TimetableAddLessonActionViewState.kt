package com.setnameinc.voenmeh.timetable.add.viewmodel.model

data class TimetableAddLessonActionViewState(
    val fetchStatus: FetchStatus
)

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}
