package com.setnameinc.voenmeh.timetable.add.viewmodel.model

sealed class TimetableAddLessonEvent {
    object Save : TimetableAddLessonEvent()
    data class SaveTitle(val title: String) : TimetableAddLessonEvent()
    data class SaveLessonType(val viewId: Int) : TimetableAddLessonEvent()
    data class WeekDaySelected(val viewId: Int) : TimetableAddLessonEvent()
    data class WeekDayUnselected(val viewId: Int) : TimetableAddLessonEvent()
    data class WeekTypeSelected(val viewId: Int) : TimetableAddLessonEvent()
    data class WeekTypeUnselected(val viewId: Int) : TimetableAddLessonEvent()
    data class SaveClassroom(val classroom: String) : TimetableAddLessonEvent()
    data class SaveLecturer(val lecturers: String) : TimetableAddLessonEvent()
    object EditStartTime : TimetableAddLessonEvent()
    object EditEndTime : TimetableAddLessonEvent()
    data class RequestGroupInfo(val groupId: String) : TimetableAddLessonEvent()
}