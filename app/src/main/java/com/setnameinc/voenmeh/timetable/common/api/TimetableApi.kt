package com.setnameinc.voenmeh.timetable.common.api

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface TimetableApi {

    @GET("group/{groupId}")
    fun getTimetableAsync(
        @Path("groupId") groupId: String
    ): Deferred<Response<TimetableResponse>>
}