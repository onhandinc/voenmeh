package com.setnameinc.voenmeh.timetable.common.api

import com.google.gson.annotations.SerializedName

data class TimetableResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("number")
    val number: Int,
    @SerializedName("lessons")
    val lessons: List<LessonResponse>
)