package com.setnameinc.voenmeh.timetable.common.di

import com.setnameinc.voenmeh.app.api.RetrofitConst.VOENMEH_SERVER_RETROFIT_INJECTION_KEY
import com.setnameinc.voenmeh.app.database.AppDatabase
import com.setnameinc.voenmeh.timetable.common.api.TimetableApi
import com.setnameinc.voenmeh.timetable.lists.viewmodel.TimetableViewModel
import com.setnameinc.voenmeh.timetable.common.domain.TimetableInteractor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

object TimetableModule {
    val module = module {
        single {
            get<Retrofit>(
                named(VOENMEH_SERVER_RETROFIT_INJECTION_KEY)
            ).create(TimetableApi::class.java)
        }
        viewModel {
            TimetableViewModel(userInteractor = get(), timetableInteractor = get())
        }
        single {
            get<AppDatabase>().timetableRepository()
        }
        single {
            TimetableInteractor(timetableApi = get(), timetableRepository = get())
        }
    }
}