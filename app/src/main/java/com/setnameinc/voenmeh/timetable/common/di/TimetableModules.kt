package com.setnameinc.voenmeh.timetable.common.di

import com.setnameinc.voenmeh.timetable.add.di.TimetableAddLessonModule
import com.setnameinc.voenmeh.timetable.lists.di.TimetableCurrentWeekModule
import com.setnameinc.voenmeh.timetable.lists.di.TimetableLoadingModule
import com.setnameinc.voenmeh.timetable.lists.di.TimetableNextWeekModule
import com.setnameinc.voenmeh.timetable.edit.di.TimetableEditLessonModule
import com.setnameinc.voenmeh.timetable.widget.di.TimetableWidgetModule

object TimetableModules {
    val modules = listOf(
        TimetableAddLessonModule.module,
        TimetableEditLessonModule.module,
        TimetableCurrentWeekModule.module,
        TimetableNextWeekModule.module,
        TimetableLoadingModule.module,
        TimetableModule.module,
        TimetableWidgetModule.module
    )
}