package com.setnameinc.voenmeh.timetable.common.domain

data class Lecturer(
    val id: Int,
    val simpleName: String
)