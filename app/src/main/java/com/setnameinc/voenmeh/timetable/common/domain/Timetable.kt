package com.setnameinc.voenmeh.timetable.common.domain

data class Timetable(
    val id: String,
    val name: String,
    val number: Int,
    val lessons: List<Lesson>
)