package com.setnameinc.voenmeh.timetable.common.domain

import com.google.gson.Gson
import com.setnameinc.voenmeh.timetable.common.api.LessonResponse
import com.setnameinc.voenmeh.timetable.common.api.TimetableResponse
import com.setnameinc.voenmeh.timetable.common.repository.TimetableEntity

object TimetableConverter {

    fun convertFromNetwork(timetableResponse: TimetableResponse): Timetable =
        Timetable(
            id = timetableResponse.id,
            name = timetableResponse.name,
            number = timetableResponse.number,
            lessons = timetableResponse.lessons.map { convertLessonFromNetwork(it) }
        )

    private fun convertLessonFromNetwork(lessonResponse: LessonResponse): Lesson =
        Lesson(
            id = lessonResponse.id,
            classroom = lessonResponse.classroom,
            classroomName = lessonResponse.classroomName,
            numberDayInWeek = lessonResponse.numberDayInWeek,
            discipline = lessonResponse.discipline,
            disciplineName = lessonResponse.disciplineName,
            lecturers = lessonResponse.lecturers,
            lecturersId = lessonResponse.lecturersId,
            startTime = lessonResponse.startTime,
            startTimeText = lessonResponse.startTimeText,
            endTimeText = lessonResponse.endTimeText,
            groupsId = lessonResponse.groupsId,
            groups = lessonResponse.groups,
            weekCode = lessonResponse.weekCode,
            type = lessonResponse.type
        )

    fun List<TimetableEntity>.convertFromDatabase(): List<Lesson> =
        this.map { convertLessonFromDatabase(it) }

    private fun convertLessonFromDatabase(timetableEntity: TimetableEntity): Lesson =
        Lesson(
            id = timetableEntity.id,
            classroom = timetableEntity.classroom,
            classroomName = timetableEntity.classroomName,
            numberDayInWeek = timetableEntity.numberDayInWeek,
            discipline = timetableEntity.discipline,
            disciplineName = timetableEntity.disciplineName,
            lecturers = timetableEntity.lecturers,
            lecturersId = timetableEntity.lecturersIdStringArrayJson.fromJsonToStringArray(),
            startTime = timetableEntity.startTime,
            startTimeText = timetableEntity.startTimeText,
            endTimeText = timetableEntity.endTimeText,
            groups = timetableEntity.groups,
            groupsId = timetableEntity.groupsIdStringArrayJson.fromJsonToStringArray(),
            weekCode = timetableEntity.weekCode,
            type = timetableEntity.type
        )

    fun convertToDatabase(timetable: Timetable): List<TimetableEntity> =
        timetable.lessons.map { lesson ->
            TimetableEntity(
                id = lesson.id,
                groupId = timetable.id,
                groupNumber = timetable.number,
                groupName = timetable.name,
                classroom = lesson.classroom,
                classroomName = lesson.classroomName,
                numberDayInWeek = lesson.numberDayInWeek,
                discipline = lesson.discipline,
                disciplineName = lesson.disciplineName,
                lecturers = lesson.lecturers,
                lecturersIdStringArrayJson = lesson.lecturersId.toJsonStringArray(),
                startTime = lesson.startTime,
                startTimeText = lesson.startTimeText,
                endTimeText = lesson.endTimeText,
                groupsIdStringArrayJson = lesson.groupsId.toJsonStringArray(),
                groups = lesson.groups,
                weekCode = lesson.weekCode,
                type = lesson.type
            )
        }

    private fun List<String>.toJsonStringArray(): String = Gson().toJson(this)

    private fun String.fromJsonToStringArray(): List<String> =
        if (this.isNotEmpty()) {
            Gson().fromJson(
                this,
                Array<String>::class.java
            ).toList()
        } else {
            emptyList()
        }

}