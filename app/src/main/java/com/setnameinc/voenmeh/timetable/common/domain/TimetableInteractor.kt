package com.setnameinc.voenmeh.timetable.common.domain

import com.setnameinc.voenmeh.timetable.common.api.TimetableApi
import com.setnameinc.voenmeh.timetable.common.api.TimetableResponse
import com.setnameinc.voenmeh.timetable.common.domain.TimetableConverter.convertFromDatabase
import com.setnameinc.voenmeh.timetable.common.repository.TimetableRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response
import java.lang.Exception
import java.lang.IllegalArgumentException

class TimetableInteractor(
    private val timetableApi: TimetableApi,
    private val timetableRepository: TimetableRepository
) {

    fun getTimetable(
        groupId: String,
        weekCode: Int,
        complete: (List<Lesson>) -> Unit,
        error: (Throwable) -> Unit
    ) {
        CoroutineScope(Dispatchers.Default).launch {
            if (timetableRepository.isTimetableExists(groupId)) {
                complete(
                    timetableRepository.getTimetable(
                        groupId = groupId,
                        weekCode = weekCode
                    ).convertFromDatabase()
                )
            } else {
                loadTimetableFromApiAndCache(
                    groupId,
                    complete = {
                        complete(
                            timetableRepository.getTimetable(
                                groupId = groupId,
                                weekCode = weekCode
                            ).convertFromDatabase()
                        )
                    },
                    error = error
                )
            }
        }
    }

    fun isTimetableExists(groupId: String) = timetableRepository.isTimetableExists(groupId)

    suspend fun loadTimetableFromApiAndCache(
        groupId: String,
        complete: () -> Unit,
        error: (Throwable) -> Unit
    ) {
        getApiTimetable(
            groupId,
            complete = {
                saveTimetable(
                    it,
                    complete = complete,
                    error = error
                )
            },
            error = error
        )
    }

    suspend fun getApiTimetable(
        groupId: String,
        complete: suspend (Timetable) -> Unit,
        error: (Throwable) -> Unit
    ) {
        val response: Response<TimetableResponse> =
            withContext(Dispatchers.IO) {
                timetableApi.getTimetableAsync(groupId)
            }.await()
        val requestBody: TimetableResponse? = response.body()
        when {
            response.isSuccessful && requestBody != null -> {
                complete(TimetableConverter.convertFromNetwork(requestBody))
            }
            else -> {
                when (response.code()) {
                    500 -> {
                        error(IllegalArgumentException())
                    }
                    else -> {
                        response.errorBody()?.toString()?.let { error(it) }
                            ?: error(Exception("unknown"))
                    }
                }
            }
        }
    }

    private suspend fun saveTimetable(
        timetable: Timetable,
        complete: () -> Unit,
        error: (Throwable) -> Unit
    ) {
        if (!timetableRepository.isTimetableExists(timetable.id)) {
            withContext(Dispatchers.IO) {
                timetableRepository.saveAll(
                    TimetableConverter.convertToDatabase(timetable = timetable)
                )
                complete()
            }
        } else {
            error(Exception("unknown exception"))
        }
    }

    fun deleteLesson(
        id: String,
        complete: () -> Unit,
        error: (Throwable) -> Unit
    ) {
        CoroutineScope(Dispatchers.Default).launch {
            try {
                timetableRepository.delete(id)
                complete()
            } catch (e: Exception) {
                error(e)
            }
        }
    }

}