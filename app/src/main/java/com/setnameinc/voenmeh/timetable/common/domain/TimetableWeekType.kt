package com.setnameinc.voenmeh.timetable.common.domain

enum class TimetableWeekType(val weekCode: Int) {
    EVEN(0), //четная
    ODD(1) //нечетная
}