package com.setnameinc.voenmeh.timetable.common.repository

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "timetable")
open class TimetableEntity(
    @PrimaryKey
    var id: String = "",
    var classroom: String = "",
    var classroomName: String = "",
    var numberDayInWeek: Int = 0,
    var discipline: String = "",
    var disciplineName: String = "",
    val lecturers: String = "",
    val lecturersIdStringArrayJson: String = "",
    var startTime: Long = 0,
    var startTimeText: String = "",
    var endTimeText: String = "",
    val groupsIdStringArrayJson: String = "",
    var groupName: String = "",
    var groupId: String = "",
    var groupNumber: Int = 0,
    val groups: String = "",
    var weekCode: Int = 0,
    var type: String = "",
)