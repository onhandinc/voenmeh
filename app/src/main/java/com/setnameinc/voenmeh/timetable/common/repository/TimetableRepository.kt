package com.setnameinc.voenmeh.timetable.common.repository

import androidx.room.*
import com.setnameinc.voenmeh.timetable.edit.repository.TimetableEditUpdate
import com.setnameinc.voenmeh.user.common.domain.GroupInfoModel

@Dao
interface TimetableRepository {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAll(timetableEntity: List<TimetableEntity>)

    @Query("SELECT * FROM timetable WHERE groupId = :groupId AND weekCode = :weekCode ORDER BY numberDayInWeek")
    fun getTimetable(groupId: String, weekCode: Int): List<TimetableEntity>

    @Query("SELECT groupId, groupNumber, groupName FROM timetable WHERE groupId = :groupId ORDER BY numberDayInWeek")
    fun groupInfoByGroupId(groupId: String): GroupInfoModel

    @Query("DELETE FROM timetable")
    fun deleteAll()

    @Query("SELECT EXISTS(SELECT * FROM timetable WHERE groupId = :groupId)")
    fun isTimetableExists(groupId: String): Boolean

    @Query("DELETE FROM timetable WHERE id = :id")
    fun delete(id: String)

    @Query("SELECT * FROM timetable WHERE id = :id")
    fun getLessonById(id: String): TimetableEntity

    @Update(entity = TimetableEntity::class)
    fun update(updateEntity: TimetableEditUpdate)

}
