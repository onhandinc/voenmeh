package com.setnameinc.voenmeh.timetable.edit.di


import com.setnameinc.voenmeh.timetable.edit.domain.TimetableEditInteractor
import com.setnameinc.voenmeh.timetable.edit.ui.TimetableEditLessonActivity
import com.setnameinc.voenmeh.timetable.edit.viewmodel.TimetableEditViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object TimetableEditLessonModule {
    val module = module {
        scope<TimetableEditLessonActivity> {
            scoped {
                TimetableEditInteractor(timetableRepository = get())
            }
            viewModel {
                TimetableEditViewModel(interactor = get())
            }
        }
    }
}