package com.setnameinc.voenmeh.timetable.edit.domain

data class LessonEditLoad(
    val id: String,
    val classroom: String,
    val numberDayInWeekId: Int,
    val discipline: String,
    val lecturers: String,
    val lecturersId: List<String>,
    val startTime: Long,
    val startTimeText: String,
    val endTimeText: String,
    val weekTypeId: Int,
    val lessonTypeId: Int
)