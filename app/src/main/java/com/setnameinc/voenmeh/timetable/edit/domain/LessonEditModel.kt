package com.setnameinc.voenmeh.timetable.edit.domain

data class LessonEditModel(
    val id: String,
    val classroom: String,
    val classroomName: String,
    val numberDayInWeek: Int,
    val discipline: String,
    val disciplineName: String,
    val lecturers: String,
    val lecturersId: List<String>,
    val startTime: Long = 0,
    val startTimeText: String,
    val endTimeText: String,
    val weekCode: Int,
    val type: String,
)