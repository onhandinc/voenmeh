package com.setnameinc.voenmeh.timetable.edit.domain

import com.setnameinc.voenmeh.timetable.edit.domain.TimetableEditLessonConverter.fromDatabase
import com.setnameinc.voenmeh.timetable.edit.domain.TimetableEditLessonConverter.toDatabase
import com.setnameinc.voenmeh.timetable.edit.domain.TimetableEditLessonConverter.toLoadType
import com.setnameinc.voenmeh.timetable.common.repository.TimetableRepository
import com.setnameinc.voenmeh.timetable.edit.viewmodel.TimetableItem

class TimetableEditInteractor(
    private val timetableRepository: TimetableRepository
) {

    fun save(
        item: TimetableItem,
        complete: () -> Unit,
        error: () -> Unit
    ) {
        timetableRepository.update(item.toDatabase())
        complete()
    }

    fun lesson(
        id: String,
        complete: (LessonEditLoad) -> Unit,
        error: () -> Unit
    ) {
        complete(
            timetableRepository.getLessonById(id = id).fromDatabase().toLoadType()
        )
    }

}