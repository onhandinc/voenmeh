package com.setnameinc.voenmeh.timetable.edit.domain

import com.google.gson.Gson
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.timetable.edit.repository.TimetableEditUpdate
import com.setnameinc.voenmeh.timetable.edit.viewmodel.TimetableItem
import com.setnameinc.voenmeh.timetable.common.repository.TimetableEntity

object TimetableEditLessonConverter {

    fun TimetableItem.toDatabase(): TimetableEditUpdate =
        TimetableEditUpdate(
            id = this.id,
            classroom = this.classroom,
            classroomName = classroomName(
                this.classroom,
                this.lessonType.lessonName
            ),
            numberDayInWeek = weekDay.numberDayInWeek,
            discipline = this.discipline,
            disciplineName = this.discipline,
            lecturers = this.lecturers,
            lecturersIdStringArrayJson = this.lecturersId.toJsonStringArray(),
            startTime = this.startTime,
            startTimeText = this.startTimeText,
            endTimeText = this.endTimeText,
            weekCode = weekType.weekCode,
            type = this.lessonType.type
        )

    private fun classroomName(classroom: String, lessonName: String) =
        if (classroom.isNotEmpty()) {
            "$classroom $lessonName"
        } else {
            lessonName
        }

    fun TimetableEntity.fromDatabase() = LessonEditModel(
        id = this.id,
        classroom = this.classroom,
        classroomName = this.classroomName,
        numberDayInWeek = this.numberDayInWeek,
        discipline = this.discipline,
        disciplineName = this.disciplineName,
        lecturers = this.lecturers,
        lecturersId = this.lecturersIdStringArrayJson.fromJsonToStringArray(),
        startTime = this.startTime,
        startTimeText = this.startTimeText,
        endTimeText = this.endTimeText,
        weekCode = this.weekCode,
        type = this.type
    )

    fun LessonEditModel.toLoadType() = LessonEditLoad(
        id = this.id,
        classroom = this.classroom,
        discipline = this.discipline,
        lecturers = this.lecturers,
        lecturersId = this.lecturersId,
        startTime = this.startTime,
        startTimeText = this.startTimeText,
        endTimeText = this.endTimeText,
        weekTypeId = when (this.weekCode) {
            TimetableWeekType.EVEN.weekCode -> {
                TimetableWeekType.EVEN.id
            }
            else -> {
                TimetableWeekType.ODD.id
            }
        },
        lessonTypeId = when (this.type) {
            TimetableLessonType.LECTURE.type -> {
                TimetableLessonType.LECTURE.id
            }
            TimetableLessonType.PRACTISE.type -> {
                TimetableLessonType.PRACTISE.id
            }
            TimetableLessonType.LABORATORY.type -> {
                TimetableLessonType.LABORATORY.id
            }
            else -> {
                TimetableLessonType.CONSULTATION.id
            }
        },
        numberDayInWeekId = when (this.numberDayInWeek) {
            TimetableWeekDay.MONDAY.numberDayInWeek -> {
                TimetableWeekDay.MONDAY.id
                R.id.mondayChip
            }
            TimetableWeekDay.TUESDAY.numberDayInWeek -> {
                R.id.tuesdayChip
            }
            TimetableWeekDay.WEDNESDAY.numberDayInWeek -> {
                R.id.wednesdayChip
            }
            TimetableWeekDay.THURSDAY.numberDayInWeek -> {
                R.id.thursdayChip
            }
            TimetableWeekDay.FRIDAY.numberDayInWeek -> {
                R.id.fridayChip
            }
            TimetableWeekDay.SATURDAY.numberDayInWeek -> {
                R.id.saturdayChip
            }
            else -> {
                R.id.sundayChip
            }
        }
    )

    private fun List<String>.toJsonStringArray(): String = Gson().toJson(this)

    private fun String.fromJsonToStringArray(): List<String> =
        if (this.isNotEmpty()) {
            Gson().fromJson(
                this,
                Array<String>::class.java
            ).toList()
        } else {
            emptyList()
        }

}