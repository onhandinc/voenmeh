package com.setnameinc.voenmeh.timetable.edit.domain

import com.setnameinc.voenmeh.R

enum class TimetableLessonType(
    val id: Int,
    val type: String,
    val lessonName: String
) {
    LECTURE(R.id.lectureChip, "lecture", "лекция"),
    PRACTISE(R.id.practiseChip, "practise", "практика"),
    LABORATORY(R.id.laboratoryChip, "laboratory", "лабораторная"),
    CONSULTATION(R.id.consultationChip, "consultation", "консультация")
}