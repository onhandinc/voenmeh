package com.setnameinc.voenmeh.timetable.edit.domain

import com.setnameinc.voenmeh.R

enum class TimetableWeekDay(
    val id: Int,
    val dayName: String,
    val numberDayInWeek: Int
) {
    MONDAY(R.id.mondayChip, "Понедельник", 1),
    TUESDAY(R.id.tuesdayChip, "Вторник", 2),
    WEDNESDAY(R.id.wednesdayChip, "Среда", 3),
    THURSDAY(R.id.thursdayChip, "Четверг", 4),
    FRIDAY(R.id.fridayChip, "Пятница", 5),
    SATURDAY(R.id.saturdayChip, "Суббота", 6),
    SUNDAY(R.id.sundayChip, "Воскресенье", 7)
}