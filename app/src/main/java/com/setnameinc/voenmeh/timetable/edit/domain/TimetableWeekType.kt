package com.setnameinc.voenmeh.timetable.edit.domain

import com.setnameinc.voenmeh.R

enum class TimetableWeekType(
    val id: Int,
    val weekCode: Int
) {
    EVEN(R.id.evenChip, 0), //четная
    ODD(R.id.oddChip, 1) //нечетная
}