package com.setnameinc.voenmeh.timetable.edit.repository

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
open class TimetableEditUpdate(
    @PrimaryKey
    var id: String,
    var classroom: String,
    var classroomName: String,
    var numberDayInWeek: Int,
    var discipline: String,
    var disciplineName: String,
    val lecturers: String,
    val lecturersIdStringArrayJson: String,
    var startTime: Long,
    var startTimeText: String,
    var endTimeText: String,
    var weekCode: Int,
    var type: String
)