package com.setnameinc.voenmeh.timetable.edit.ui

import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.lifecycleScope
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.snackbar.Snackbar
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.timetable.edit.domain.LessonEditLoad
import com.setnameinc.voenmeh.timetable.edit.viewmodel.TimetableEditViewModel
import com.setnameinc.voenmeh.timetable.edit.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.timetable.edit.viewmodel.model.TimetableEditAction
import com.setnameinc.voenmeh.timetable.edit.viewmodel.model.TimetableEditEvent
import com.setnameinc.voenmeh.timetable.edit.viewmodel.model.TimetableEditViewState
import com.setnameinc.voenmeh.tools.utils.viewModel
import kotlinx.android.synthetic.main.activity_edit_lesson.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.scope.activityScope
import timber.log.Timber

class TimetableEditLessonActivity : AppCompatActivity(R.layout.activity_edit_lesson) {

    private val viewModel: TimetableEditViewModel by activityScope().viewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startTimeTextView.setDebounceClickListener {
            Timber.d("clicked startTimeTextView")
            viewModel.obtainEvent(TimetableEditEvent.EditStartTime)
        }
        endTimeTextView.setDebounceClickListener {
            Timber.d("clicked endTimeTextView")
            viewModel.obtainEvent(TimetableEditEvent.EditEndTime)
        }
        saveButton.setDebounceClickListener {
            viewModel.obtainEvent(TimetableEditEvent.Save)
        }
        closeImageButton.setDebounceClickListener {
            setResult(Activity.RESULT_CANCELED)
            finish()
        }
        lessonNameTextView.doAfterTextChanged {
            viewModel.obtainEvent(
                TimetableEditEvent.SaveTitle(it.toString())
            )
            errorSnackbar?.dismiss()
        }
        classroomTextView.doAfterTextChanged {
            viewModel.obtainEvent(
                TimetableEditEvent.SaveClassroom(it.toString())
            )
            errorSnackbar?.dismiss()
        }
        lecturerTextView.doAfterTextChanged {
            viewModel.obtainEvent(
                TimetableEditEvent.SaveLecturer(it.toString())
            )
        }
        lessonTypeChipGroup.setOnCheckedChangeListener { group, checkedId ->
            Timber.d("checkedId = $checkedId")
            viewModel.obtainEvent(TimetableEditEvent.SaveLessonType(checkedId))
        }
        weekDayChipGroup.setOnCheckedListener(
            checked = {
                viewModel.obtainEvent(TimetableEditEvent.WeekDaySelected(it))
            }
        )
        weekTypeChipGroup.setOnCheckedListener(
            checked = {
                viewModel.obtainEvent(TimetableEditEvent.WeekTypeSelected(it))
            }
        )
        lifecycleScope.launchWhenStarted {
            viewModel.viewActions()
                .filterNotNull()
                .collect {
                    bindViewAction(it)
                }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewStates(it)
                }
        }
        intent.extras
            ?.getString("lessonId")
            ?.let {
                Timber.d("lessonId = $it")
                viewModel.obtainEvent(TimetableEditEvent.LoadLesson(it))
            }
    }

    private fun bindViewAction(action: TimetableEditAction) {
        when (action) {
            is TimetableEditAction.ShowStartTimePicker -> {
                TimePickerDialog(
                    this,
                    action.listener,
                    action.hour,
                    action.minute,
                    true
                ).show()
            }
            is TimetableEditAction.ShowEndTimePicker -> {
                TimePickerDialog(
                    this,
                    action.listener,
                    action.hour,
                    action.minute,
                    true
                ).show()
            }
            is TimetableEditAction.UpdateStartTime -> {
                startTimeTextView.text = action.time
                errorSnackbar?.dismiss()
            }
            is TimetableEditAction.UpdateEndTime -> {
                endTimeTextView.text = action.time
                errorSnackbar?.dismiss()
            }
            is TimetableEditAction.ShowSnackbar -> {
                showSnackbar(action.message)
            }
        }
    }

    private fun bindViewStates(viewState: TimetableEditViewState) {
        when (viewState) {
            is TimetableEditViewState.SaveLesson -> {
                when (viewState.fetchStatus) {
                    FetchStatus.Success -> {
                        Timber.d("FetchStatus.Success")
                        //update list in mainActivity by passing back argument
                        setResult(
                            Activity.RESULT_OK,
                            Intent().apply {
                                putExtra("edited", true)
                            }
                        )
                        finish()
                    }
                    FetchStatus.Loading -> {
                        //show snackbar
                    }
                    is FetchStatus.Error -> {
                        //
                    }
                }
            }
            is TimetableEditViewState.LoadLesson -> {
                when (viewState.fetchStatus) {
                    FetchStatus.Loading -> {
                        loadingRelativeLayout.visibility = View.VISIBLE
                        rootNestedScrollView.visibility = View.INVISIBLE
                    }
                    FetchStatus.Success -> {
                        viewState.lesson?.let { fillViews(it) }
                        loadingRelativeLayout.visibility = View.GONE
                        rootNestedScrollView.visibility = View.VISIBLE
                    }
                    FetchStatus.Error -> {
                        loadingRelativeLayout.visibility = View.GONE
                        rootNestedScrollView.visibility = View.INVISIBLE
                        //show error message in full screen
                    }
                }
            }
        }
    }

    private fun fillViews(lesson: LessonEditLoad) {
        lifecycleScope.launchWhenStarted {
            Timber.d("lesson = $lesson")
            lessonNameTextView.setText(lesson.discipline)
            classroomTextView.setText(lesson.classroom)
            lecturerTextView.setText(lesson.lecturers)
            startTimeTextView.text = lesson.startTimeText
            endTimeTextView.text = lesson.endTimeText
            lessonTypeChipGroup.check(lesson.lessonTypeId)
            weekDayChipGroup.check(lesson.numberDayInWeekId)
            weekTypeChipGroup.check(lesson.weekTypeId)
        }
    }

    private fun ChipGroup.setOnCheckedListener(
        checked: (id: Int) -> Unit
    ) {
        for (i in 0 until this.childCount) {
            (this.getChildAt(i) as? Chip)
                ?.setOnCheckedChangeListener { view, isChecked ->
                    if (isChecked) {
                        checked(view.id)
                    }
                }
        }
    }

    private var errorSnackbar: Snackbar? = null

    private fun showSnackbar(message: String) {
        errorSnackbar?.dismiss()
        errorSnackbar = Snackbar.make(rootRelativeLayout, message, Snackbar.LENGTH_LONG)
        errorSnackbar?.show()
    }

}