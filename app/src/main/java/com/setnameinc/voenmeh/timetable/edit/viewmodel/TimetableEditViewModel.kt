package com.setnameinc.voenmeh.timetable.edit.viewmodel

import android.app.Application
import android.app.TimePickerDialog
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.timetable.edit.domain.TimetableLessonType
import com.setnameinc.voenmeh.timetable.edit.domain.TimetableWeekDay
import com.setnameinc.voenmeh.timetable.edit.domain.TimetableWeekType
import com.setnameinc.voenmeh.timetable.edit.domain.TimetableEditInteractor
import com.setnameinc.voenmeh.timetable.edit.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.timetable.edit.viewmodel.model.TimetableEditAction
import com.setnameinc.voenmeh.timetable.edit.viewmodel.model.TimetableEditEvent
import com.setnameinc.voenmeh.timetable.edit.viewmodel.model.TimetableEditViewState
import com.setnameinc.voenmeh.tools.utils.TimeUtils.calendarFactory
import com.setnameinc.voenmeh.tools.utils.TimeUtils.simpleDateFormat
import timber.log.Timber
import java.util.*

class TimetableEditViewModel(
    private val interactor: TimetableEditInteractor
) : BaseFlowViewModel<TimetableEditViewState, TimetableEditAction, TimetableEditEvent>() {

    private val startDateCalendar: Calendar = calendarFactory()
    private val endDateCalendar: Calendar = startDateCalendar.clone() as Calendar

    private var id: String = ""
    private var startTime: Long = -1
    private var startTimeSimpleText: String = ""
    private var startTimeText: String = ""
    private var endTime: Long = -1
    private var endTimeSimpleText: String = ""
    private var endTimeText: String = ""
    private var baseLecturers: String = ""
    private var lecturers: String = ""
    private val lecturersId: MutableList<String> = mutableListOf()
    private var title: String = ""
    private var classroom: String = ""
    private var lessonTypeId: Int = R.id.lectureChip

    private var weekDayId: Int = 0
    private var weekTypeId: Int = 0

    override suspend fun obtainInternalEvent(viewEvent: TimetableEditEvent) {
        when (viewEvent) {
            TimetableEditEvent.Save -> {
                if (isBodyType() || isEmptyLecturerType() || isEmptySubjectType()) {
                    when {
                        startTime == -1L || endTime == -1L -> {
                            viewState = TimetableEditViewState.SaveLesson(FetchStatus.Error)
                            viewAction = TimetableEditAction.ShowSnackbar(
                                "Укажите время начала и конца пары"
                            )
                            return
                        }
                    }
                    viewState = TimetableEditViewState.SaveLesson(FetchStatus.Loading)
                    val timetableItem = TimetableItem(
                        id = id,
                        classroom = classroom,
                        discipline = title,
                        lecturers = lecturers,
                        startTime = convertStartTime(),
                        startTimeText = startTimeSimpleText,
                        endTimeText = endTimeSimpleText,
                        weekType = TimetableWeekType.values()
                            .first {
                                it.id == weekTypeId
                            },
                        weekDay = TimetableWeekDay.values()
                            .first {
                                it.id == weekDayId
                            },
                        lessonType = TimetableLessonType.values()
                            .first {
                                it.id == lessonTypeId
                            },
                        lecturersId = if (lecturers == baseLecturers) lecturersId else listOf()
                    )
                    Timber.d("timetableItem = $timetableItem")
                    interactor.save(
                        timetableItem,
                        complete = {
                            Timber.d("InteractorFetchStatus.Success")
                            viewState = TimetableEditViewState.SaveLesson(FetchStatus.Success)
                        },
                        error = {
                            viewState = TimetableEditViewState.SaveLesson(FetchStatus.Error)
                        }
                    )
                } else {
                    when {
                        title.isEmpty() && lecturers.isEmpty() -> {
                            viewState = TimetableEditViewState.SaveLesson(FetchStatus.Error)
                            viewAction = TimetableEditAction.ShowSnackbar(
                                "Укажите преподавателя или название пары"
                            )
                            return
                        }
                    }
                }
            }
            is TimetableEditEvent.SaveTitle -> {
                title = viewEvent.title
            }
            is TimetableEditEvent.SaveLessonType -> {
                lessonTypeId = viewEvent.viewId
            }
            is TimetableEditEvent.WeekDaySelected -> {
                weekDayId = viewEvent.viewId
            }
            is TimetableEditEvent.WeekTypeSelected -> {
                weekTypeId = viewEvent.viewId
            }
            TimetableEditEvent.EditStartTime -> {
                viewAction = TimetableEditAction.ShowStartTimePicker(
                    startDateCalendar.get(Calendar.HOUR_OF_DAY),
                    startDateCalendar.get(Calendar.MINUTE),
                    startTimePickerListener
                )
            }
            TimetableEditEvent.EditEndTime -> {
                viewAction = TimetableEditAction.ShowEndTimePicker(
                    endDateCalendar.get(Calendar.HOUR_OF_DAY),
                    endDateCalendar.get(Calendar.MINUTE),
                    endTimePickerListener
                )
            }
            is TimetableEditEvent.SaveClassroom -> {
                classroom = viewEvent.classroom
            }
            is TimetableEditEvent.SaveLecturer -> {
                lecturers = viewEvent.lecturers
            }
            is TimetableEditEvent.LoadLesson -> {
                Timber.d("TimetableEditEvent.LoadLesson")
                viewState = TimetableEditViewState.LoadLesson(FetchStatus.Loading)
                interactor.lesson(
                    viewEvent.id,
                    complete = { lesson ->
                        id = lesson.id
                        title = lesson.discipline
                        classroom = lesson.classroom
                        lecturers = lesson.lecturers
                        baseLecturers = lesson.lecturers
                        lecturersId.apply {
                            clear()
                            addAll(lesson.lecturersId)
                        }
                        startTime = lesson.startTime
                        startTimeText = lesson.startTimeText
                        startTimeSimpleText = lesson.startTimeText
                        endTimeText = lesson.endTimeText
                        endTimeSimpleText = lesson.endTimeText
                        endTime = convertEndTime()
                        lessonTypeId = lesson.lessonTypeId
                        weekTypeId = lesson.weekTypeId
                        initStartAndEndTime()
                        obtainEvent(TimetableEditEvent.SaveLessonType(lessonTypeId))
                        obtainEvent(TimetableEditEvent.WeekTypeSelected(weekTypeId))
                        obtainEvent(TimetableEditEvent.WeekDaySelected(lesson.numberDayInWeekId))
                        viewState = TimetableEditViewState.LoadLesson(
                            FetchStatus.Success,
                            lesson
                        )
                        Timber.d("id = $id, lesson.id = ${lesson.id}")
                    },
                    error = {
                        viewState = TimetableEditViewState.LoadLesson(FetchStatus.Error)

                    }
                )
            }
        }
    }

    private fun initStartAndEndTime() {
        val tempCal = calendarFactory().apply {
            time = Date(startTime)
        }
        val tempEndCal = calendarFactory().apply {
            time = Date(endTime)
        }
        startDateCalendar.set(Calendar.HOUR_OF_DAY, tempCal.get(Calendar.HOUR_OF_DAY))
        startDateCalendar.set(Calendar.MINUTE, tempCal.get(Calendar.MINUTE))
        endDateCalendar.set(Calendar.HOUR_OF_DAY, tempEndCal.get(Calendar.HOUR_OF_DAY))
        endDateCalendar.set(Calendar.MINUTE, tempEndCal.get(Calendar.MINUTE))
    }

    private val startTimePickerListener =
        TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
            if (endTimeText.isEmpty() || isDifferentOneLesson()) {
                startDateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                startDateCalendar.set(Calendar.MINUTE, minute)
                updateStartDate()
                endDateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                endDateCalendar.set(Calendar.MINUTE, minute)
                endDateCalendar.add(Calendar.HOUR_OF_DAY, 1)
                endDateCalendar.add(Calendar.MINUTE, 30)
                updateEndDate()
            } else {
                Timber.d("not empty")
                startDateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
                startDateCalendar.set(Calendar.MINUTE, minute)
                updateStartDate()
            }
        }

    private val endTimePickerListener = TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
        endDateCalendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        endDateCalendar.set(Calendar.MINUTE, minute)
        updateEndDate()
    }

    private fun updateStartDate() {
        startTime = startDateCalendar.timeInMillis
        startTimeSimpleText = simpleDateFormat.format(
            Date(startDateCalendar.timeInMillis)
        )
        startTimeText = startTimeSimpleText
        viewAction = TimetableEditAction.UpdateStartTime(startTimeText)
    }

    private fun updateEndDate() {
        endTime = endDateCalendar.timeInMillis
        endTimeSimpleText = simpleDateFormat.format(
            Date(endDateCalendar.timeInMillis)
        )
        endTimeText = endTimeSimpleText
        viewAction = TimetableEditAction.UpdateEndTime(endTimeText)
    }

    private fun isBodyType(): Boolean =
        title.isNotEmpty() && lecturers.isNotEmpty()

    private fun isEmptyLecturerType(): Boolean =
        title.isNotEmpty() && lecturers.isEmpty()

    private fun isEmptySubjectType(): Boolean =
        title.isEmpty() && lecturers.isNotEmpty()

    private fun isDifferentOneLesson(): Boolean =
        endDateCalendar.timeInMillis - startDateCalendar.timeInMillis == 90 * 60 * 1000L

    private fun convertEndTime(): Long =
        simpleDateFormat.parse(endTimeSimpleText).time

    private fun convertStartTime(): Long =
        simpleDateFormat.parse(startTimeSimpleText).time

}