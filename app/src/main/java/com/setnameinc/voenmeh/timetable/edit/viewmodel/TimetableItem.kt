package com.setnameinc.voenmeh.timetable.edit.viewmodel

import com.setnameinc.voenmeh.timetable.edit.domain.TimetableLessonType
import com.setnameinc.voenmeh.timetable.edit.domain.TimetableWeekDay
import com.setnameinc.voenmeh.timetable.edit.domain.TimetableWeekType

data class TimetableItem(
    var id: String,
    var classroom: String,
    var discipline: String,
    var lecturers: String,
    var lecturersId: List<String>,
    var startTime: Long,
    var startTimeText: String,
    var endTimeText: String,
    var weekType: TimetableWeekType,
    val weekDay: TimetableWeekDay,
    val lessonType: TimetableLessonType
)