package com.setnameinc.voenmeh.timetable.edit.viewmodel.model

import android.app.TimePickerDialog

sealed class TimetableEditAction {
    data class ShowStartTimePicker(
        val hour: Int,
        val minute: Int,
        val listener: TimePickerDialog.OnTimeSetListener
    ) : TimetableEditAction()

    data class ShowEndTimePicker(
        val hour: Int,
        val minute: Int,
        val listener: TimePickerDialog.OnTimeSetListener
    ) : TimetableEditAction()

    data class UpdateStartTime(
        val time: String
    ) : TimetableEditAction()

    data class UpdateEndTime(
        val time: String
    ) : TimetableEditAction()

    data class ShowSnackbar(
        val message: String
    ) : TimetableEditAction()
}