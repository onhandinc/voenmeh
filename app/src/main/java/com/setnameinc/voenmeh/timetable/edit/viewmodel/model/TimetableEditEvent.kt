package com.setnameinc.voenmeh.timetable.edit.viewmodel.model

sealed class TimetableEditEvent {
    object Save : TimetableEditEvent()
    data class LoadLesson(val id: String) : TimetableEditEvent()
    data class SaveTitle(val title: String) : TimetableEditEvent()
    data class SaveLessonType(val viewId: Int) : TimetableEditEvent()
    data class WeekDaySelected(val viewId: Int) : TimetableEditEvent()
    data class WeekTypeSelected(val viewId: Int) : TimetableEditEvent()
    data class SaveClassroom(val classroom: String) : TimetableEditEvent()
    data class SaveLecturer(val lecturers: String) : TimetableEditEvent()
    object EditStartTime : TimetableEditEvent()
    object EditEndTime : TimetableEditEvent()
}