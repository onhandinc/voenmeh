package com.setnameinc.voenmeh.timetable.edit.viewmodel.model

import com.setnameinc.voenmeh.timetable.edit.domain.LessonEditLoad

sealed class TimetableEditViewState {
    data class SaveLesson(val fetchStatus: FetchStatus) : TimetableEditViewState()
    data class LoadLesson(
        val fetchStatus: FetchStatus,
        val lesson: LessonEditLoad? = null
    ) : TimetableEditViewState()
}

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}