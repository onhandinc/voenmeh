package com.setnameinc.voenmeh.timetable.lists

object Const {
    const val BODY_DIALOG_REQUEST_KEY = "body.dialogs.requestKey"

    const val BODY_EMPTY_LECTURER_DIALOG_REQUEST_KEY = "bodyEmptyLecturer.dialogs.requestKey"
    const val BODY_EMPTY_SUBJECT_DIALOG_REQUEST_KEY = "bodyEmptySubject.dialogs.requestKey"

    const val TIMETABLE_ACTIONS_ITEM_KEY = "timetable.actions.item"
    const val TIMETABLE_ACTIONS_EDIT_KEY = "body.dialogs.action.edit"
    const val TIMETABLE_ACTIONS_DELETE_KEY = "body.dialogs.action.delete"
}