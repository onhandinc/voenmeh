package com.setnameinc.voenmeh.timetable.lists.di

import com.setnameinc.voenmeh.timetable.lists.ui.TimetableCurrentWeekFragment
import com.setnameinc.voenmeh.timetable.lists.viewmodel.TimetableCurrentWeekViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object TimetableCurrentWeekModule {
    val module = module {
        scope<TimetableCurrentWeekFragment> {
            viewModel {
                TimetableCurrentWeekViewModel(
                    timetableInteractor = get(),
                    userInteractor = get()
                )
            }
        }
    }
}