package com.setnameinc.voenmeh.timetable.lists.di

import com.setnameinc.voenmeh.timetable.lists.ui.TimetableLoadingFragment
import com.setnameinc.voenmeh.timetable.lists.viewmodel.TimetableLoadingViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object TimetableLoadingModule {
    val module = module {
        scope<TimetableLoadingFragment> {
            viewModel {
                TimetableLoadingViewModel(
                    timetableInteractor = get(),
                    userInteractor = get()
                )
            }
        }
    }
}