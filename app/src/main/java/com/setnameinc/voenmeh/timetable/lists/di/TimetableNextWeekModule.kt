package com.setnameinc.voenmeh.timetable.lists.di

import com.setnameinc.voenmeh.timetable.lists.ui.TimetableNextWeekFragment
import com.setnameinc.voenmeh.timetable.lists.viewmodel.TimetableNextWeekViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object TimetableNextWeekModule {
    val module = module {
        scope<TimetableNextWeekFragment> {
            viewModel {
                TimetableNextWeekViewModel(
                    timetableInteractor = get(),
                    userInteractor = get()
                )
            }
        }
    }
}