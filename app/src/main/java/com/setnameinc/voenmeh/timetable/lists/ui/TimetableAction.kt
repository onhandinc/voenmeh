package com.setnameinc.voenmeh.timetable.lists.ui

enum class TimetableAction {
    EDIT,
    FIND_BY_LECTURER,
    FIND_BY_CLASSROOM,
    DELETE
}