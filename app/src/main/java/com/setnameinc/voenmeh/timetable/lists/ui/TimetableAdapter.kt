package com.setnameinc.voenmeh.timetable.lists.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.setnameinc.voenmeh.R

class TimetableAdapter(
    private val clickListener: (type: Int, item: TimetableItem) -> Unit
) : ListAdapter<TimetableItem, TimetableViewHolder>(
    object : DiffUtil.ItemCallback<TimetableItem>() {
        override fun areItemsTheSame(
            oldItem: TimetableItem,
            newItem: TimetableItem
        ): Boolean = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: TimetableItem,
            newItem: TimetableItem
        ): Boolean = newItem == oldItem

    }
) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TimetableViewHolder = when (viewType) {
        TimetableTypes.TITLE ->
            TimetableTitleViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_timetable_title, parent, false)
            )
        TimetableTypes.BODY ->
            TimetableBodyViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_timetable_body, parent, false)
            ) {
                getItem(it)?.let { item ->
                    clickListener(TimetableTypes.BODY, item)
                }
                return@TimetableBodyViewHolder false
            }
        TimetableTypes.BODY_WITH_EMPTY_SUBJECT -> {
            TimetableBodyWithEmptySubjectViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(
                        R.layout.item_timetable_body_empty_subject,
                        parent,
                        false
                    )
            ) {
                getItem(it)?.let { item ->
                    clickListener(TimetableTypes.BODY_WITH_EMPTY_SUBJECT, item)
                }
                return@TimetableBodyWithEmptySubjectViewHolder false
            }
        }
        else -> {
            TimetableBodyWithEmptyLecturersViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(
                        R.layout.item_timetable_body_empty_lecturers,
                        parent,
                        false
                    )
            ) {
                getItem(it)?.let { item ->
                    clickListener(TimetableTypes.BODY_WITH_EMPTY_LECTURERS, item)
                }
                return@TimetableBodyWithEmptyLecturersViewHolder false
            }
        }
    }

    override fun onBindViewHolder(holder: TimetableViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    override fun getItemViewType(position: Int): Int = currentList[position].type

}

