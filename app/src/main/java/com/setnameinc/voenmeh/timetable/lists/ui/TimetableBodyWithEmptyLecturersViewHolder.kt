package com.setnameinc.voenmeh.timetable.lists.ui

import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.item_timetable_body_empty_lecturers.view.*

class TimetableBodyWithEmptyLecturersViewHolder(
    private val view: View,
    private val onLongClickListener: (pos: Int) -> Boolean
) : TimetableViewHolder(view = view) {

    override fun bind(item: TimetableItem) {
        item as TimetableItem.BodyWithEmptyLecturers
        view.apply {
            setOnLongClickListener {
                onLongClickListener(absoluteAdapterPosition)
            }
            timeTextView.text = item.startTime
            subjectTextView.text = item.subjectName
            classroomTextView.background = ContextCompat.getDrawable(
                context,
                item.classroomBackgroundId
            )
            classroomTextView.text = item.classroomName
            endTimeTextView.text = item.endTime
        }
    }

}