package com.setnameinc.voenmeh.timetable.lists.ui

import android.view.View
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.item_timetable_body_empty_subject.view.*

class TimetableBodyWithEmptySubjectViewHolder(
    private val view: View,
    private val onLongClickListener: (pos: Int) -> Boolean
) : TimetableViewHolder(view = view) {

    override fun bind(item: TimetableItem) {
        item as TimetableItem.BodyWithEmptySubject
        view.apply {
            setOnLongClickListener {
                onLongClickListener(absoluteAdapterPosition)
            }
            timeTextView.text = item.startTime
            classroomTextView.background = ContextCompat.getDrawable(
                context,
                item.classroomBackgroundId
            )
            classroomTextView.text = item.classroomName
            lecturerTextView.text = item.lecturers
            endTimeTextView.text = item.endTime
        }
    }
}
