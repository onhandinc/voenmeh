package com.setnameinc.voenmeh.timetable.lists.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.timetable.lists.Const.BODY_DIALOG_REQUEST_KEY
import com.setnameinc.voenmeh.timetable.lists.Const.BODY_EMPTY_LECTURER_DIALOG_REQUEST_KEY
import com.setnameinc.voenmeh.timetable.lists.Const.BODY_EMPTY_SUBJECT_DIALOG_REQUEST_KEY
import com.setnameinc.voenmeh.timetable.lists.Const.TIMETABLE_ACTIONS_DELETE_KEY
import com.setnameinc.voenmeh.timetable.lists.Const.TIMETABLE_ACTIONS_EDIT_KEY
import com.setnameinc.voenmeh.timetable.lists.Const.TIMETABLE_ACTIONS_ITEM_KEY
import com.setnameinc.voenmeh.timetable.lists.viewmodel.TimetableCurrentWeekViewModel
import com.setnameinc.voenmeh.timetable.lists.viewmodel.currentmodel.FetchStatus
import com.setnameinc.voenmeh.timetable.lists.viewmodel.currentmodel.TimetableCurrentWeekEvent
import com.setnameinc.voenmeh.timetable.lists.viewmodel.currentmodel.TimetableCurrentWeekViewState
import com.setnameinc.voenmeh.timetable.edit.ui.TimetableEditLessonActivity
import com.setnameinc.voenmeh.tools.utils.viewModel
import kotlinx.android.synthetic.main.fragment_timetable_current_week.*
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.scope.fragmentScope
import timber.log.Timber
import kotlin.math.max

class TimetableCurrentWeekFragment(
    private val loadListener: CompletableDeferred<Boolean>,
    private val scrollListener: CompletableDeferred<Boolean>
) : Fragment(R.layout.fragment_timetable_current_week) {

    private val timetableViewModel: TimetableCurrentWeekViewModel by fragmentScope().viewModel(this)

    private val adapterTimetable: TimetableAdapter by lazy {
        TimetableAdapter { type, item ->
            when (type) {
                TimetableTypes.BODY -> {
                    findNavController().navigate(
                        R.id.toBodyActions,
                        bundleOf(
                            TIMETABLE_ACTIONS_ITEM_KEY to (item as TimetableItem.Body)
                        )
                    )
                }
                TimetableTypes.BODY_WITH_EMPTY_LECTURERS -> {
                    findNavController().navigate(
                        R.id.toBodyWithEmptyLecturerActions,
                        bundleOf(
                            TIMETABLE_ACTIONS_ITEM_KEY to (item as TimetableItem.BodyWithEmptyLecturers)
                        )
                    )
                }
                TimetableTypes.BODY_WITH_EMPTY_SUBJECT -> {
                    findNavController().navigate(
                        R.id.toBodyWithEmptySubjectActions,
                        bundleOf(
                            TIMETABLE_ACTIONS_ITEM_KEY to (item as TimetableItem.BodyWithEmptySubject)
                        )
                    )
                }

            }
        }.apply {
            stateRestorationPolicy = RecyclerView.Adapter.StateRestorationPolicy.PREVENT_WHEN_EMPTY
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            timetableViewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindViewState(it)
                }
        }
    }

    private fun bindViewState(viewState: TimetableCurrentWeekViewState) {
        when (viewState.fetchStatus) {
            FetchStatus.Success -> {
                loadListener.complete(true)
                adapterTimetable.submitList(viewState.timetable) {
                    loadListener.complete(true)
                    if (timetableViewModel.isNotFocusedToToday) {
                        val minDayOfWeek: Int = (
                                viewState.timetable.first {
                                    it is TimetableItem.Title
                                } as TimetableItem.Title
                                ).dayNumberInWeek
                        val maxDayOfWeek: Int? =
                            (viewState.timetable.findLast {
                                it is TimetableItem.Title
                            } as? TimetableItem.Title
                                    )?.dayNumberInWeek
                        val scrollToIndex: TimetableItem? =
                            viewState.timetable.firstOrNull {
                                it is TimetableItem.Title && it.isSelected
                            }
                        Timber.d("minDayOfWeek = $minDayOfWeek, maxDayOfWeek = $maxDayOfWeek")
                        (scrollToIndex as? TimetableItem.Title)?.let {
                            if (it.dayNumberInWeek == maxDayOfWeek) {
                                timetableRecycleView.scrollToPosition(viewState.timetable.lastIndex)
                                showTabLayout(false)
                            } else if (it.dayNumberInWeek == minDayOfWeek) {
                                showTabLayout(true)
                            } else {
                                showTabLayout(false)
                                timetableRecycleView.scrollToPosition(
                                    max(
                                        0,
                                        viewState.timetable.indexOf(it) - 3
                                    )
                                )
                            }
                            scrollListener.complete(true)
                        } ?: run {
                            //has no lessons for today
                            showTabLayout(true)
                            scrollListener.complete(true)
                        }
                        timetableViewModel.isNotFocusedToToday = false
                    }
                }
            }
            FetchStatus.Loading -> {

            }
            FetchStatus.Error -> {
                loadListener.complete(false)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        timetableViewModel.obtainEvent(TimetableCurrentWeekEvent.LoadTimetable)
        timetableRecycleView.apply {
            adapter = adapterTimetable
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
    }

    fun refresh() {
        timetableViewModel.obtainEvent(TimetableCurrentWeekEvent.LoadTimetable)
    }

    private fun showTabLayout(show: Boolean) {
        (parentFragment as TimetableFragment).showTabLayout(show)
    }

}