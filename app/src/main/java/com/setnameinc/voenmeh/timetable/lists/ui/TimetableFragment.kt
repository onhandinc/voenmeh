package com.setnameinc.voenmeh.timetable.lists.ui

import android.app.Activity
import android.appwidget.AppWidgetManager
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.tools.base.BsaeProgressFragment
import com.setnameinc.voenmeh.timetable.add.ui.TimetableAddLessonActivity
import com.setnameinc.voenmeh.timetable.add.ui.TimetableShowAvailableActionsBottomSheetDialogFragment
import com.setnameinc.voenmeh.timetable.lists.viewmodel.TimetableViewModel
import com.setnameinc.voenmeh.timetable.lists.viewmodel.model.TimetableViewState
import com.setnameinc.voenmeh.timetable.common.domain.TimetableWeekType
import com.setnameinc.voenmeh.timetable.edit.ui.TimetableEditLessonActivity
import com.setnameinc.voenmeh.timetable.lists.Const
import com.setnameinc.voenmeh.timetable.lists.viewmodel.currentmodel.TimetableCurrentWeekEvent
import com.setnameinc.voenmeh.timetable.lists.viewmodel.model.TimetableEvent
import com.setnameinc.voenmeh.timetable.widget.Container
import com.setnameinc.voenmeh.timetable.widget.TimetableWidgetProvider
import com.setnameinc.voenmeh.tools.utils.WeekType
import kotlinx.android.synthetic.main.fragment_timetable.*
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import java.util.*

class TimetableFragment : BsaeProgressFragment(R.layout.fragment_timetable) {

    private val ADD_LESSON_ACTIVITY_CODE = 108
    private val EDIT_LESSON_ACTIVITY_UPDATE_CURRENT_WEEK_CODE = 100
    private val EDIT_LESSON_ACTIVITY_UPDATE_NEXT_WEEK_CODE = 101

    private val NUMBER_OF_PAGES: Int = 2

    private val CURRENT_WEEK_POSITION: Int = 0
    private val NEXT_WEEK_POSITION: Int = 1

    /**
     * @see EDIT_LESSON_ACTIVITY_UPDATE_CURRENT_WEEK_CODE
     * @see EDIT_LESSON_ACTIVITY_UPDATE_NEXT_WEEK_CODE
     */
    private var pagePosition = 0
        set(value) {
            field = 100 + value
        }

    private var isTabLayoutContainerAppBarLayoutExpanded: Boolean = true

    private var groupName: String? = ""
    private var groupId: String? = ""

    private val currentWeekLoadListener: CompletableDeferred<Boolean> = CompletableDeferred()
    private val nextWeekLoadListener: CompletableDeferred<Boolean> = CompletableDeferred()
    private val scrollListener: CompletableDeferred<Boolean> = CompletableDeferred()
    private val tabLayoutListener: CompletableDeferred<Boolean> = CompletableDeferred()

    private val timetableCurrentWeekFragment: TimetableCurrentWeekFragment =
        TimetableCurrentWeekFragment(currentWeekLoadListener, scrollListener)
    private val timetableNextWeekFragment: TimetableNextWeekFragment =
        TimetableNextWeekFragment(nextWeekLoadListener)

    private val viewModel: TimetableViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    when (it) {
                        is TimetableViewState.UpdateGroupInfo -> {
                            if (groupIdHeaderTextView.text != it.groupName) {
                                groupName = it.groupName
                                groupId = it.groupId
                                groupIdHeaderTextView.text = it.groupName
                            }
                        }
                        TimetableViewState.RefreshBoth -> {
                            timetableCurrentWeekFragment.refresh()
                            timetableNextWeekFragment.refresh()
                        }
                    }
                }
        }
        setFragmentResultListener(Const.BODY_DIALOG_REQUEST_KEY) { key, bundle ->
            bindFragmentResult(key, bundle)
        }
        setFragmentResultListener(
            Const.BODY_EMPTY_LECTURER_DIALOG_REQUEST_KEY
        ) { key, bundle ->
            bindFragmentResult(key, bundle)
        }
        setFragmentResultListener(
            Const.BODY_EMPTY_SUBJECT_DIALOG_REQUEST_KEY
        ) { key, bundle ->
            bindFragmentResult(key, bundle)
        }
        lifecycleScope.launchWhenStarted {
            currentWeekLoadListener.await()
            nextWeekLoadListener.await()
            scrollListener.await()
            tabLayoutListener.await()
            hideProgressBar()
            viewModel.obtainEvent(TimetableEvent.RequestUpdateGroupTitle)
            updateWidget()
        }
    }

    private fun updateWidget() {
        requireContext().sendBroadcast(
            Intent(
                requireContext(),
                TimetableWidgetProvider::class.java
            ).apply {
                action = AppWidgetManager.ACTION_APPWIDGET_UPDATE
                putExtra(Container.UPDATED_GROUP_EXTRA_KEY, true)
            }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        groupIdHeaderTextView.text = groupName
        //refreshTimetableSwipeRefreshLayout.isEnabled = isTabLayoutContainerAppBarLayoutExpanded
        tabLayoutContainerAppBarLayout.setExpanded(isTabLayoutContainerAppBarLayoutExpanded)
        refreshTimetableSwipeRefreshLayout.apply {
            setProgressViewOffset(
                true,
                y.toInt(),
                y.toInt() + (40 * resources.displayMetrics.density).toInt()
            )
            setColorSchemeResources(
                R.color.burnt_sienna,
                R.color.atomic_tangerine,
                R.color.hippie_blue
            )
            setOnRefreshListener {
                lifecycleScope.launchWhenStarted {
                    timetableCurrentWeekFragment.refresh()
                    timetableNextWeekFragment.refresh()
                    currentWeekLoadListener.await()
                    nextWeekLoadListener.await()
                    viewModel.obtainEvent(TimetableEvent.RequestUpdateGroupTitle)
                    refreshTimetableSwipeRefreshLayout.isRefreshing = false
                }
            }
        }
        tabLayoutContainerAppBarLayout.addOnOffsetChangedListener(
            AppBarLayout.OnOffsetChangedListener { _, verticalOffset ->
                lifecycleScope.launchWhenStarted {
                    refreshTimetableSwipeRefreshLayout.isEnabled = verticalOffset == 0
                    isTabLayoutContainerAppBarLayoutExpanded = verticalOffset == 0
                }
            }
        )
        showActionFloatingButton.setDebounceClickListener {
            val bottomSheetDialog = TimetableShowAvailableActionsBottomSheetDialogFragment {
                startActivityForResult(
                    Intent(
                        requireActivity(),
                        TimetableAddLessonActivity::class.java
                    ).apply {
                        putExtra("groupId", groupId)
                    },
                    ADD_LESSON_ACTIVITY_CODE
                )
            }
            bottomSheetDialog.show(parentFragmentManager, bottomSheetDialog.tag)
        }
        setupViewPager()
        viewModel.obtainEvent(TimetableEvent.RequestUpdateGroupTitle)
        updateWidget()
    }

    fun showTabLayout(show: Boolean) {
        lifecycleScope.launchWhenStarted {
            isTabLayoutContainerAppBarLayoutExpanded = show
            tabLayoutContainerAppBarLayout
                .setExpanded(isTabLayoutContainerAppBarLayoutExpanded, false)
            refreshTimetableSwipeRefreshLayout.isEnabled = isTabLayoutContainerAppBarLayoutExpanded
            tabLayoutListener.complete(true)
        }
    }

    private fun setupViewPager() {
        timetableViewPager.apply {
            adapter = object : FragmentStateAdapter(this@TimetableFragment) {

                override fun getItemCount(): Int = NUMBER_OF_PAGES

                override fun createFragment(position: Int): Fragment =
                    when (position) {
                        CURRENT_WEEK_POSITION -> timetableCurrentWeekFragment
                        NEXT_WEEK_POSITION -> timetableNextWeekFragment
                        else -> throw Exception()
                    }

            }
            registerOnPageChangeCallback(
                object : ViewPager2.OnPageChangeCallback() {
                    override fun onPageScrolled(
                        position: Int,
                        positionOffset: Float,
                        positionOffsetPixels: Int
                    ) {
                        pagePosition = position
                        refreshTimetableSwipeRefreshLayout.isEnabled = (positionOffset <= 0.04)
                        super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                    }
                }
            )
            offscreenPageLimit = NUMBER_OF_PAGES
            (getChildAt(0) as RecyclerView).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
        }
        TabLayoutMediator(timetableTabLayout, timetableViewPager) { tab, position ->
            when (position) {
                CURRENT_WEEK_POSITION -> {
                    val weekTypeName =
                        if (WeekType.CURRENT_WEEK.code == TimetableWeekType.ODD.weekCode) {
                            "Нечетная"
                        } else {
                            "Четная"
                        }
                    tab.text = weekTypeName
                }
                NEXT_WEEK_POSITION -> {
                    val weekTypeName =
                        if (WeekType.NEXT_WEEK.code == TimetableWeekType.ODD.weekCode) {
                            "Нечетная"
                        } else {
                            "Четная"
                        }
                    tab.text = weekTypeName
                }
            }
        }.attach()
        Timber.d("WeekType.CURRENT_WEEK = ${WeekType.CURRENT_WEEK.code}, WeekType.CURRENT_WEEK = ${WeekType.NEXT_WEEK.code}")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Timber.d("onActivityResult")
        when (requestCode) {
            ADD_LESSON_ACTIVITY_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    Timber.d("ADD_LESSON_ACTIVITY_CODE Activity.RESULT_OK")
                    if (data?.getBooleanExtra("added", false) == true) {
                        Timber.d("added")
                        timetableCurrentWeekFragment.refresh()
                        timetableNextWeekFragment.refresh()
                    }
                }
            }
            EDIT_LESSON_ACTIVITY_UPDATE_CURRENT_WEEK_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    Timber.d("EDIT_LESSON_ACTIVITY_CODE Activity.RESULT_OK")
                    if (data?.getBooleanExtra("edited", false) == true) {
                        Timber.d("edited")
                        timetableCurrentWeekFragment.refresh()
                    }
                }
            }
            EDIT_LESSON_ACTIVITY_UPDATE_NEXT_WEEK_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    Timber.d("EDIT_LESSON_ACTIVITY_CODE Activity.RESULT_OK")
                    if (data?.getBooleanExtra("edited", false) == true) {
                        Timber.d("edited")
                        timetableNextWeekFragment.refresh()
                    }
                }
            }
        }
    }

    private fun bindFragmentResult(key: String, bundle: Bundle) {
        when (key) {
            Const.BODY_DIALOG_REQUEST_KEY -> {
                Timber.d("receive data")
                bundle.getString(Const.TIMETABLE_ACTIONS_DELETE_KEY)?.let {
                    viewModel.obtainEvent(
                        TimetableEvent.DeleteLesson(it)
                    )
                }
                bundle.getString(Const.TIMETABLE_ACTIONS_EDIT_KEY)?.let {
                    startActivityForResult(
                        Intent(
                            requireActivity(),
                            TimetableEditLessonActivity::class.java
                        ).apply {
                            putExtra("lessonId", it)
                        },
                        pagePosition
                    )
                }
            }
            Const.BODY_EMPTY_LECTURER_DIALOG_REQUEST_KEY -> {
                Timber.d("receive data")
                bundle.getString(Const.TIMETABLE_ACTIONS_DELETE_KEY)?.let {
                    viewModel.obtainEvent(
                        TimetableEvent.DeleteLesson(it)
                    )
                }
                bundle.getString(Const.TIMETABLE_ACTIONS_EDIT_KEY)?.let {
                    startActivityForResult(
                        Intent(
                            requireActivity(),
                            TimetableEditLessonActivity::class.java
                        ).apply {
                            putExtra("lessonId", it)
                        },
                        pagePosition
                    )
                }
            }
            Const.BODY_EMPTY_SUBJECT_DIALOG_REQUEST_KEY -> {
                Timber.d("receive data")
                bundle.getString(Const.TIMETABLE_ACTIONS_DELETE_KEY)?.let {
                    viewModel.obtainEvent(
                        TimetableEvent.DeleteLesson(it)
                    )
                }
                bundle.getString(Const.TIMETABLE_ACTIONS_EDIT_KEY)?.let {
                    startActivityForResult(
                        Intent(
                            requireActivity(),
                            TimetableEditLessonActivity::class.java
                        ).apply {
                            putExtra("lessonId", it)
                        },
                        pagePosition
                    )
                }
            }
        }
    }
}