package com.setnameinc.voenmeh.timetable.lists.ui

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

sealed class TimetableItem(
    open val id: String,
    open val type: Int
) {

    data class Title(
        val title: String,
        val dayNumberInWeek: Int,
        val isSelected: Boolean = false
    ) : TimetableItem(
        type = TimetableTypes.TITLE,
        id = dayNumberInWeek.toString()
    )

    @Parcelize
    data class Body(
        override val id: String,
        val classroom: String,
        val classroomName: String,
        val classroomBackgroundId: Int,
        val subject: String,
        val subjectName: String,
        val lecturers: String,
        val startTime: String,
        val endTime: String
    ) : TimetableItem(
        type = TimetableTypes.BODY,
        id = id
    ), Parcelable

    @Parcelize
    data class BodyWithEmptyLecturers(
        override val id: String,
        val classroom: String,
        val classroomName: String,
        val classroomBackgroundId: Int,
        val subject: String,
        val subjectName: String,
        val startTime: String,
        val endTime: String
    ) : TimetableItem(
        type = TimetableTypes.BODY_WITH_EMPTY_LECTURERS,
        id = id
    ), Parcelable

    @Parcelize
    data class BodyWithEmptySubject(
        override val id: String,
        val classroom: String,
        val classroomName: String,
        val classroomBackgroundId: Int,
        val lecturers: String,
        val startTime: String,
        val endTime: String
    ) : TimetableItem(
        type = TimetableTypes.BODY_WITH_EMPTY_SUBJECT,
        id = id
    ), Parcelable
}

object TimetableTypes {
    const val TITLE = 0
    const val BODY = 1
    const val BODY_WITH_EMPTY_LECTURERS = 2
    const val BODY_WITH_EMPTY_SUBJECT = 3
}