package com.setnameinc.voenmeh.timetable.lists.ui

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.tools.base.BaseBottomRoundSheetDialogFragment
import com.setnameinc.voenmeh.timetable.lists.Const.BODY_DIALOG_REQUEST_KEY
import com.setnameinc.voenmeh.timetable.lists.Const.TIMETABLE_ACTIONS_DELETE_KEY
import com.setnameinc.voenmeh.timetable.lists.Const.TIMETABLE_ACTIONS_EDIT_KEY
import com.setnameinc.voenmeh.timetable.lists.Const.TIMETABLE_ACTIONS_ITEM_KEY
import kotlinx.android.synthetic.main.dialog_timetable_item_actions.*
import kotlinx.android.synthetic.main.item_timetable_body.view.*

class TimetableItemActionsDialogFragment
    : BaseBottomRoundSheetDialogFragment(R.layout.dialog_timetable_item_actions) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getParcelable<TimetableItem.Body>(TIMETABLE_ACTIONS_ITEM_KEY)?.let { item ->
            itemRelativeLayout.timeTextView.text = item.startTime
            itemRelativeLayout.subjectTextView.text = item.subjectName
            itemRelativeLayout.classroomTextView.background = context?.let {
                ContextCompat.getDrawable(
                    it,
                    item.classroomBackgroundId
                )
            }
            itemRelativeLayout.classroomTextView.text = item.classroomName
            itemRelativeLayout.lecturerTextView.text = item.lecturers
            itemRelativeLayout.endTimeTextView.text = item.endTime
            editTextView.setDebounceClickListener {
                setFragmentResult(
                    BODY_DIALOG_REQUEST_KEY,
                    bundleOf(TIMETABLE_ACTIONS_EDIT_KEY to item.id)
                )
                dismiss()
            }
            findByLecturerTextView.setDebounceClickListener {
                /*clickListener(
                    TimetableAction.FIND_BY_LECTURER,
                    item.lecturers
                )*/
                dismiss()
            }
            findByClassroomTextView.setDebounceClickListener {
                /*clickListener(
                    TimetableAction.FIND_BY_CLASSROOM,
                    item.classroom
                )*/
                dismiss()
            }
            deleteTextView.setDebounceClickListener {
                setFragmentResult(
                    BODY_DIALOG_REQUEST_KEY,
                    bundleOf(TIMETABLE_ACTIONS_DELETE_KEY to item.id)
                )
                dismiss()
            }
        }
    }

}