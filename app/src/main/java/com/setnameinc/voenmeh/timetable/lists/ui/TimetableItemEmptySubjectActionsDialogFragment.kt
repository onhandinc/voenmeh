package com.setnameinc.voenmeh.timetable.lists.ui

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import com.setnameinc.voenmeh.tools.base.BaseBottomRoundSheetDialogFragment
import com.setnameinc.voenmeh.timetable.lists.Const
import kotlinx.android.synthetic.main.dialog_timetable_item_empty_subject_actions.*
import kotlinx.android.synthetic.main.item_timetable_body_empty_subject.view.*

class TimetableItemEmptySubjectActionsDialogFragment
    : BaseBottomRoundSheetDialogFragment(R.layout.dialog_timetable_item_empty_subject_actions) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getParcelable<TimetableItem.BodyWithEmptySubject>(Const.TIMETABLE_ACTIONS_ITEM_KEY)
            ?.let { item ->
                itemRelativeLayout.timeTextView.text = item.startTime
                itemRelativeLayout.lecturerTextView.text = item.lecturers
                itemRelativeLayout.classroomTextView.background = context?.let {
                    ContextCompat.getDrawable(
                        it,
                        item.classroomBackgroundId
                    )
                }
                itemRelativeLayout.classroomTextView.text = item.classroomName
                itemRelativeLayout.endTimeTextView.text = item.endTime
                editTextView.setDebounceClickListener {
                    setFragmentResult(
                        Const.BODY_EMPTY_SUBJECT_DIALOG_REQUEST_KEY,
                        bundleOf(Const.TIMETABLE_ACTIONS_EDIT_KEY to item.id)
                    )
                    dismiss()
                }
                findByClassroomTextView.setDebounceClickListener {
                    /*clickListener(
                        TimetableAction.FIND_BY_CLASSROOM,
                        item.classroom
                    )
                    dismiss()*/
                }
                deleteTextView.setDebounceClickListener {
                    setFragmentResult(
                        Const.BODY_EMPTY_SUBJECT_DIALOG_REQUEST_KEY,
                        bundleOf(Const.TIMETABLE_ACTIONS_DELETE_KEY to item.id)
                    )
                    dismiss()
                }
            }
    }

}