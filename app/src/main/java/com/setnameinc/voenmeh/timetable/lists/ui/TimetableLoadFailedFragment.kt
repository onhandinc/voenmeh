package com.setnameinc.voenmeh.timetable.lists.ui

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.app.ui.utils.DebounceClickListener.setDebounceClickListener
import kotlinx.android.synthetic.main.fragment_timetable_load_failed.*

class TimetableLoadFailedFragment : Fragment(R.layout.fragment_timetable_load_failed) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        loadFailedRootConstraintLayout.setDebounceClickListener {
            findNavController().popBackStack(R.id.timetableLoading, false)
        }
    }

}