package com.setnameinc.voenmeh.timetable.lists.ui

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.timetable.lists.viewmodel.TimetableLoadingViewModel
import com.setnameinc.voenmeh.timetable.lists.viewmodel.TimetableViewResponseFailed
import com.setnameinc.voenmeh.timetable.lists.viewmodel.loadingmodel.FetchStatus
import com.setnameinc.voenmeh.timetable.lists.viewmodel.loadingmodel.TimetableLoadingEvent
import com.setnameinc.voenmeh.timetable.lists.viewmodel.loadingmodel.TimetableLoadingViewState
import com.setnameinc.voenmeh.tools.utils.viewModel
import kotlinx.android.synthetic.main.fragment_timetable_loading.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import org.koin.androidx.scope.fragmentScope

class TimetableLoadingFragment : Fragment(R.layout.fragment_timetable_loading) {

    private val viewModel: TimetableLoadingViewModel by fragmentScope().viewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.viewStates()
                .filterNotNull()
                .collect {
                    bindState(it)
                }
        }
    }

    private fun bindState(state: TimetableLoadingViewState) {
        when (state.fetchStatus) {
            FetchStatus.Success -> {
                findNavController().navigate(R.id.timetableLoadingToLoadSuccess)
            }
            is FetchStatus.Error -> {
                when (state.fetchStatus.reason) {
                    TimetableViewResponseFailed.EMPTY_GROUP_ID -> {
                        if (findNavController().currentDestination?.id == R.id.timetableLoading) {
                            findNavController().navigate(R.id.timetableLoadingToLoadFailedEmptyGroupId)
                        }
                    }
                    TimetableViewResponseFailed.NO_FOUND_RECORDS -> {
                        if (findNavController().currentDestination?.id == R.id.timetableLoading) {
                            findNavController().navigate(R.id.timetableLoadingToLoadFailed)
                        }
                    }
                    TimetableViewResponseFailed.UNKNOWN -> {
                        if (findNavController().currentDestination?.id == R.id.timetableLoading) {
                            findNavController().navigate(R.id.timetableLoadingToLoadFailed)
                        }
                    }
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.obtainEvent(TimetableLoadingEvent.LoadTimetable)
        Handler().postDelayed(
            {
                if (isVisible) {
                    timeoutAlertTextView.visibility = View.VISIBLE
                }
            },
            10000
        )
    }

}