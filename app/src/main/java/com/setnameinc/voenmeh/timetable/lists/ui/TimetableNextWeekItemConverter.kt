package com.setnameinc.voenmeh.timetable.lists.ui

import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.tools.utils.DayTitleFromWeekNumber
import com.setnameinc.voenmeh.timetable.common.domain.Lesson
import timber.log.Timber

object TimetableNextWeekItemConverter {

    fun List<Lesson>.convert(): List<TimetableItem> =
        this
            .sortedBy {
                it.numberDayInWeek
            }
            .groupBy {
                Day(
                    DayTitleFromWeekNumber.getDayTitleFromWeekNumber(it.numberDayInWeek),
                    it.numberDayInWeek
                )
            }
            .also {
                Timber.d("$it")
            }
            .flatMap { pair ->
                createTimetableToDay(
                    pair.key,
                    pair.value.sortedBy { it.startTime }
                )
            }
            .toMutableList()

    private fun createTimetableToDay(day: Day, list: List<Lesson>): List<TimetableItem> =
        mutableListOf<TimetableItem>().apply {
            day.title?.let {
                add(TimetableItem.Title(it, day.numberDayInWeek))
            }
            addAll(
                list.map {
                    when {
                        it.discipline.isEmpty() -> {
                            convertLessonToBodyWithEmptySubject(it)
                        }
                        it.lecturers.isEmpty() -> {
                            convertLessonToBodyWithEmptyLecturers(it)
                        }
                        else -> {
                            convertLessonToBody(it)
                        }
                    }
                }
            )
        }

    private fun convertLessonToBody(
        lesson: Lesson
    ): TimetableItem.Body =
        TimetableItem.Body(
            id = lesson.id,
            classroom = lesson.classroom,
            classroomName = lesson.classroomName,
            subject = lesson.discipline,
            subjectName = lesson.disciplineName,
            startTime = lesson.startTimeText,
            endTime = lesson.endTimeText,
            lecturers = lesson.lecturers,
            classroomBackgroundId = generateClassroomBackgroundId(lesson.type)
        )

    private fun convertLessonToBodyWithEmptyLecturers(
        lesson: Lesson
    ): TimetableItem.BodyWithEmptyLecturers =
        TimetableItem.BodyWithEmptyLecturers(
            id = lesson.id,
            classroom = lesson.classroom,
            classroomName = lesson.classroomName,
            subject = lesson.discipline,
            subjectName = lesson.disciplineName,
            startTime = lesson.startTimeText,
            endTime = lesson.endTimeText,
            classroomBackgroundId = generateClassroomBackgroundId(lesson.type)
        )

    private fun convertLessonToBodyWithEmptySubject(
        lesson: Lesson
    ): TimetableItem.BodyWithEmptySubject =
        TimetableItem.BodyWithEmptySubject(
            id = lesson.id,
            classroom = lesson.classroom,
            classroomName = lesson.classroomName,
            startTime = lesson.startTimeText,
            endTime = lesson.endTimeText,
            lecturers = lesson.lecturers,
            classroomBackgroundId = generateClassroomBackgroundId(
                lesson.type
            )
        )

    private fun generateClassroomBackgroundId(subjectType: String): Int =
        when (subjectType) {
            "practise" -> {
                R.drawable.shape_timetable_class_type_practise_background
            }
            "laboratory" -> {
                R.drawable.shape_timetable_class_type_laboratory_background
            }
            "consultation" -> {
                R.drawable.shape_timetable_class_type_consultation_background
            }
            else -> {
                R.drawable.shape_timetable_class_type_lecture_background
            }
        }

    private data class Day(
        val title: String?,
        val numberDayInWeek: Int
    )

}