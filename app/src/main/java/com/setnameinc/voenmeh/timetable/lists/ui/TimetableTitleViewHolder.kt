package com.setnameinc.voenmeh.timetable.lists.ui

import android.view.View
import kotlinx.android.synthetic.main.item_timetable_title.view.*

class TimetableTitleViewHolder(
    private val view: View
) : TimetableViewHolder(view = view) {
    override fun bind(item: TimetableItem) {
        val localeItem = item as TimetableItem.Title
        view.apply {
            titleTextView.text = localeItem.title
            currentDayIndicatorView.visibility = if (item.isSelected) {
                View.VISIBLE
            } else {
                View.INVISIBLE
            }
        }
    }
}