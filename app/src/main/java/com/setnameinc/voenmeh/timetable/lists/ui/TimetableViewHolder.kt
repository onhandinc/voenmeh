package com.setnameinc.voenmeh.timetable.lists.ui

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class TimetableViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bind(item: TimetableItem)
}