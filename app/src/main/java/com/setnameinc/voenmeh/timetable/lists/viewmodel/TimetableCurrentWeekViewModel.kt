package com.setnameinc.voenmeh.timetable.lists.viewmodel

import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.timetable.lists.ui.TimetableCurrentItemConverter.convert
import com.setnameinc.voenmeh.timetable.lists.viewmodel.currentmodel.FetchStatus
import com.setnameinc.voenmeh.timetable.lists.viewmodel.currentmodel.TimetableCurrentWeekAction
import com.setnameinc.voenmeh.timetable.lists.viewmodel.currentmodel.TimetableCurrentWeekEvent
import com.setnameinc.voenmeh.timetable.lists.viewmodel.currentmodel.TimetableCurrentWeekViewState
import com.setnameinc.voenmeh.timetable.common.domain.TimetableInteractor
import com.setnameinc.voenmeh.user.common.domain.UserInteractor


import com.setnameinc.voenmeh.tools.utils.WeekType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import timber.log.Timber


class TimetableCurrentWeekViewModel(
    private val timetableInteractor: TimetableInteractor,
    private val userInteractor: UserInteractor
) : BaseFlowViewModel<TimetableCurrentWeekViewState, TimetableCurrentWeekAction, TimetableCurrentWeekEvent>() {

    override suspend fun obtainInternalEvent(viewEvent: TimetableCurrentWeekEvent) {
        when (viewEvent) {
            TimetableCurrentWeekEvent.LoadTimetable -> {
                Timber.d("UserInteractorEvent.RequestUserData")
                userInteractor.requestUserData(
                    complete = { userData ->
                        if (userData.groupId.isNotEmpty()) {
                            timetableInteractor.getTimetable(
                                groupId = userData.groupId,
                                weekCode = WeekType.CURRENT_WEEK.code,
                                complete = {
                                    viewState = TimetableCurrentWeekViewState(
                                        FetchStatus.Success,
                                        it.convert()
                                    )
                                },
                                error = {
                                    viewState = TimetableCurrentWeekViewState(
                                        FetchStatus.Error
                                    )
                                }
                            )
                        } else {
                            Timber.e("failure 2")
                        }
                    },
                    error = {
                        Timber.e("failure 1")
                    }
                )
            }
        }
    }

    var isNotFocusedToToday: Boolean = true

}