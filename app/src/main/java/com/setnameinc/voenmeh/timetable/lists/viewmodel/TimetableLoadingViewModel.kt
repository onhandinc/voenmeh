package com.setnameinc.voenmeh.timetable.lists.viewmodel

import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.timetable.lists.viewmodel.loadingmodel.FetchStatus
import com.setnameinc.voenmeh.timetable.lists.viewmodel.loadingmodel.TimetableLoadingAction
import com.setnameinc.voenmeh.timetable.lists.viewmodel.loadingmodel.TimetableLoadingEvent
import com.setnameinc.voenmeh.timetable.lists.viewmodel.loadingmodel.TimetableLoadingViewState
import com.setnameinc.voenmeh.timetable.common.domain.TimetableInteractor
import com.setnameinc.voenmeh.user.common.domain.UserInteractor
import timber.log.Timber

class TimetableLoadingViewModel(
    private val timetableInteractor: TimetableInteractor,
    private val userInteractor: UserInteractor
) : BaseFlowViewModel<TimetableLoadingViewState, TimetableLoadingAction, TimetableLoadingEvent>() {

    override suspend fun obtainInternalEvent(viewEvent: TimetableLoadingEvent) {
        when (viewEvent) {
            TimetableLoadingEvent.LoadTimetable -> {
                Timber.d("UserInteractorEvent.RequestUserData")
                userInteractor.requestUserData(
                    complete = { userData ->
                        if (userData.groupId.isNotEmpty()) {
                            val isExists = timetableInteractor.isTimetableExists(
                                groupId = userData.groupId
                            )
                            if (isExists) {
                                viewState = TimetableLoadingViewState(FetchStatus.Success)
                            } else {
                                timetableInteractor.loadTimetableFromApiAndCache(
                                    userData.groupId,
                                    complete = {
                                        viewState =
                                            TimetableLoadingViewState(FetchStatus.Success)
                                    },
                                    error = {
                                        viewState = TimetableLoadingViewState(
                                            FetchStatus.Error(TimetableViewResponseFailed.EMPTY_GROUP_ID)
                                        )
                                    }
                                )
                            }
                        } else {
                            Timber.e("timetable not exists")
                            viewState = TimetableLoadingViewState(
                                FetchStatus.Error(TimetableViewResponseFailed.EMPTY_GROUP_ID)
                            )
                        }
                    },
                    error = {
                        if (it is NullPointerException) {
                            viewState = TimetableLoadingViewState(
                                FetchStatus.Error(TimetableViewResponseFailed.NO_FOUND_RECORDS)
                            )
                        } else {
                            viewState = TimetableLoadingViewState(
                                FetchStatus.Error(TimetableViewResponseFailed.UNKNOWN)
                            )
                        }
                    }
                )
            }
        }
    }

}