package com.setnameinc.voenmeh.timetable.lists.viewmodel

import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.timetable.lists.ui.TimetableNextWeekItemConverter.convert
import com.setnameinc.voenmeh.timetable.lists.viewmodel.nextweekmodel.FetchStatus
import com.setnameinc.voenmeh.timetable.lists.viewmodel.nextweekmodel.TimetableNextWeekAction
import com.setnameinc.voenmeh.timetable.lists.viewmodel.nextweekmodel.TimetableNextWeekEvent
import com.setnameinc.voenmeh.timetable.lists.viewmodel.nextweekmodel.TimetableNextWeekViewState
import com.setnameinc.voenmeh.timetable.common.domain.TimetableInteractor
import com.setnameinc.voenmeh.user.common.domain.UserInteractor


import com.setnameinc.voenmeh.tools.utils.WeekType
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.launch
import timber.log.Timber

class TimetableNextWeekViewModel(
    private val timetableInteractor: TimetableInteractor,
    private val userInteractor: UserInteractor
) : BaseFlowViewModel<TimetableNextWeekViewState, TimetableNextWeekAction, TimetableNextWeekEvent>() {

    override suspend fun obtainInternalEvent(viewEvent: TimetableNextWeekEvent) {
        when (viewEvent) {
            TimetableNextWeekEvent.LoadTimetable -> {
                Timber.d("UserInteractorEvent.RequestUserData")
                userInteractor.requestUserData(
                    complete = { userData ->
                        if (userData.groupId.isNotEmpty()) {
                            timetableInteractor.getTimetable(
                                groupId = userData.groupId,
                                weekCode = WeekType.NEXT_WEEK.code,
                                complete = {
                                    viewState = TimetableNextWeekViewState(
                                        FetchStatus.Success,
                                        it.convert()
                                    )
                                },
                                error = {
                                    viewState = TimetableNextWeekViewState(FetchStatus.Error)
                                    Timber.e(it)
                                }
                            )
                        } else {
                            Timber.e("failure 2")
                            //onLoadTimetableFailed?(TimetableViewResponseFailed.EMPTY_GROUP_ID)
                        }
                    },
                    error = {

                    }
                )
            }
        }
    }

}