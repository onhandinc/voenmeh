package com.setnameinc.voenmeh.timetable.lists.viewmodel

import com.setnameinc.voenmeh.tools.base.BaseFlowViewModel
import com.setnameinc.voenmeh.loginsignup.signup.Const
import com.setnameinc.voenmeh.timetable.common.domain.TimetableInteractor
import com.setnameinc.voenmeh.timetable.lists.viewmodel.model.FetchStatus
import com.setnameinc.voenmeh.timetable.lists.viewmodel.model.TimetableAction
import com.setnameinc.voenmeh.timetable.lists.viewmodel.model.TimetableEvent
import com.setnameinc.voenmeh.timetable.lists.viewmodel.model.TimetableViewState
import com.setnameinc.voenmeh.user.common.domain.UserInteractor
import com.setnameinc.voenmeh.user.common.viewmodel.UserData
import timber.log.Timber

class TimetableViewModel(
    private val timetableInteractor: TimetableInteractor,
    private val userInteractor: UserInteractor
) : BaseFlowViewModel<TimetableViewState, TimetableAction, TimetableEvent>() {

    private val groupIdUpdatedListener: suspend ((UserData) -> Unit) = {
        Timber.d("groupId = ${it.groupId}")
        viewState = TimetableViewState.UpdateGroupInfo(
            FetchStatus.Success,
            findGroupNameById(it.groupId),
            groupId = it.groupId
        )
    }

    private fun findGroupNameById(id: String?): String? =
        Const.GROUPS_ID_TO_NAME.firstOrNull { it.id == id }?.name

    override suspend fun obtainInternalEvent(viewEvent: TimetableEvent) {
        when (viewEvent) {
            TimetableEvent.RequestUpdateGroupTitle -> {
                userInteractor.requestUserData(
                    complete = groupIdUpdatedListener,
                    error = {}
                )
            }
            is TimetableEvent.DeleteLesson -> {
                timetableInteractor.deleteLesson(
                    viewEvent.id,
                    complete = {
                        viewState = TimetableViewState.RefreshBoth
                    },
                    error = {
                        Timber.e(it)
                    }
                )
            }
        }
    }

}