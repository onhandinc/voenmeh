package com.setnameinc.voenmeh.timetable.lists.viewmodel

enum class TimetableViewResponseFailed {
    EMPTY_GROUP_ID,
    NO_FOUND_RECORDS,
    UNKNOWN
}