package com.setnameinc.voenmeh.timetable.lists.viewmodel.currentmodel

sealed class TimetableCurrentWeekEvent {
    object LoadTimetable : TimetableCurrentWeekEvent()
}