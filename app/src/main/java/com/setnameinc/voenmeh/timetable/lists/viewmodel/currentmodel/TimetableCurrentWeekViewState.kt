package com.setnameinc.voenmeh.timetable.lists.viewmodel.currentmodel

import com.setnameinc.voenmeh.timetable.lists.ui.TimetableItem

data class TimetableCurrentWeekViewState(
    val fetchStatus: FetchStatus,
    val timetable: List<TimetableItem> = listOf()
)

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}