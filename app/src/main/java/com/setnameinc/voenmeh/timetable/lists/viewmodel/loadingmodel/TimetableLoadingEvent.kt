package com.setnameinc.voenmeh.timetable.lists.viewmodel.loadingmodel

sealed class TimetableLoadingEvent {
    object LoadTimetable : TimetableLoadingEvent()
}