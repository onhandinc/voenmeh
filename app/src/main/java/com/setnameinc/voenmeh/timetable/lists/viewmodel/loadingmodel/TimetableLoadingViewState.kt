package com.setnameinc.voenmeh.timetable.lists.viewmodel.loadingmodel

import com.setnameinc.voenmeh.timetable.lists.viewmodel.TimetableViewResponseFailed

data class TimetableLoadingViewState(
    val fetchStatus: FetchStatus
)

sealed class FetchStatus {
    object Success : FetchStatus()
    data class Error(val reason: TimetableViewResponseFailed) : FetchStatus()
}