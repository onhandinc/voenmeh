package com.setnameinc.voenmeh.timetable.lists.viewmodel.model

sealed class TimetableEvent {
    object RequestUpdateGroupTitle : TimetableEvent()
    object RequestLoadTimetable : TimetableEvent()
    data class DeleteLesson(val id: String) : TimetableEvent()
}