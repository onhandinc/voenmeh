package com.setnameinc.voenmeh.timetable.lists.viewmodel.model

sealed class TimetableViewState {
    data class UpdateGroupInfo(
        val fetchStatus: FetchStatus,
        val groupName: String? = null,
        val groupId: String? = null
    ) : TimetableViewState()
    object RefreshBoth : TimetableViewState()
}

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}