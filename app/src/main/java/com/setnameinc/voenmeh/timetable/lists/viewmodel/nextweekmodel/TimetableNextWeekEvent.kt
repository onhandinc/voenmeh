package com.setnameinc.voenmeh.timetable.lists.viewmodel.nextweekmodel

sealed class TimetableNextWeekEvent {
    object LoadTimetable : TimetableNextWeekEvent()
}