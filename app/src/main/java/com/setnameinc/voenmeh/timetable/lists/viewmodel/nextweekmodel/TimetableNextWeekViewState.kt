package com.setnameinc.voenmeh.timetable.lists.viewmodel.nextweekmodel

import com.setnameinc.voenmeh.timetable.lists.ui.TimetableItem

data class TimetableNextWeekViewState(
    val fetchStatus: FetchStatus,
    val timetable: List<TimetableItem> = listOf()
)

sealed class FetchStatus {
    object Success : FetchStatus()
    object Loading : FetchStatus()
    object Error : FetchStatus()
}