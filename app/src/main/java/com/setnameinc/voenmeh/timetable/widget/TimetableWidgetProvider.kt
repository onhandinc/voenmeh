package com.setnameinc.voenmeh.timetable.widget

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.RemoteViews
import com.setnameinc.voenmeh.R
import com.setnameinc.voenmeh.timetable.widget.Container.UPDATED_GROUP_EXTRA_KEY
import com.setnameinc.voenmeh.timetable.widget.Container.widgetIds
import com.setnameinc.voenmeh.timetable.widget.interactor.TimetableWidgetInteractor
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import timber.log.Timber


class TimetableWidgetProvider : AppWidgetProvider(), KoinComponent {

    private val timetableInteractor: TimetableWidgetInteractor by inject()

    override fun onReceive(context: Context?, intent: Intent?) {
        Timber.d("onReceive, context = ${context}, intent = $intent")
        if (intent?.extras?.getBoolean(UPDATED_GROUP_EXTRA_KEY) == true) {
            Timber.d("groups updated")
            notifyWidgetUpdated(AppWidgetManager.getInstance(context))
        } else {
            Timber.d("just onReceive")
            super.onReceive(context, intent)
        }
    }

    override fun onUpdate(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetIds: IntArray
    ) {
        Timber.d("onUpdate, ${appWidgetIds.toList()}")
        super.onUpdate(context, appWidgetManager, appWidgetIds)
        widgetIds = appWidgetIds
        for (i in appWidgetIds) {
            updateWidget(context, appWidgetManager, i)
        }
    }

    private fun notifyWidgetUpdated(
        appWidgetManager: AppWidgetManager
    ) {
        Timber.d("widgets = ${widgetIds.toList()}")
        timetableInteractor.updateData {
            appWidgetManager.notifyAppWidgetViewDataChanged(widgetIds, R.id.timetableListView)
        }
    }

    private fun updateWidget(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetId: Int
    ) {
        val rv = RemoteViews(
            context.packageName,
            R.layout.widget_timetable
        )
        timetableInteractor.updateData {
            setList(rv, context, appWidgetId)
            appWidgetManager.updateAppWidget(appWidgetId, rv)
        }
    }

    private fun setList(rv: RemoteViews, context: Context?, appWidgetId: Int) {
        rv.setRemoteAdapter(
            R.id.timetableListView,
            Intent(context, TimetableWidgetRemoteViewsService::class.java)
        )
    }

    override fun onAppWidgetOptionsChanged(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetId: Int,
        newOptions: Bundle
    ) {
        val rv = RemoteViews(
            context.packageName,
            R.layout.widget_timetable
        )
        timetableInteractor.updateData {
            setList(rv, context, appWidgetId)
            appWidgetManager.updateAppWidget(appWidgetId, rv)
        }
    }

    /*private fun resizeWidget(appWidgetOptions: Bundle, views: RemoteViews) {
        val minWidth = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH)
        val maxWidth = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH)
        val minHeight = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT)
        val maxHeight = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT)
        if (maxHeight > 100) {
            views.setViewVisibility(R.id.example_widget_text, View.VISIBLE)
        } else {
            views.setViewVisibility(R.id.example_widget_text, View.GONE)
        }
    }*/

}

object Container {
    const val UPDATED_GROUP_EXTRA_KEY = "groupIdUpdated"

    var widgetIds: IntArray = intArrayOf()
}