package com.setnameinc.voenmeh.timetable.widget

import com.setnameinc.voenmeh.R
import android.content.Context
import android.graphics.Typeface
import android.text.SpannableString
import android.text.style.StyleSpan
import android.widget.RemoteViews
import android.widget.RemoteViewsService
import com.setnameinc.voenmeh.timetable.lists.ui.TimetableItem
import com.setnameinc.voenmeh.timetable.widget.interactor.TimetableWidgetInteractor
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import timber.log.Timber

class TimetableWidgetRemoteViewFactory(
    private val context: Context
) : RemoteViewsService.RemoteViewsFactory, KoinComponent {

    private val timetableInteractor: TimetableWidgetInteractor by inject()

    override fun onCreate() {
        Timber.d("onCreate")
    }

    override fun onDataSetChanged() {
        Timber.d("onDataSetChanged")
    }

    override fun onDestroy() {
        Timber.d("onDestroy")
    }

    override fun getCount(): Int = timetableInteractor.dataList.size

    override fun getViewAt(position: Int): RemoteViews {
        Timber.d("getViewAt, position $position, item = ${timetableInteractor.dataList[position]}")
        val item = timetableInteractor.dataList[position]
        val rootView: RemoteViews
        if (timetableInteractor.emptyLessonForToday) {
            item as TimetableItem.Title
            rootView = RemoteViews(
                context.packageName,
                R.layout.widget_item_timetable_no_lessons
            )
            with(rootView) {
                setTextViewText(R.id.titleTextView, item.title.makeBold())
            }
        } else if (item is TimetableItem.Body) {
            rootView = RemoteViews(
                context.packageName,
                R.layout.widget_item_timetable_body
            )
            with(rootView) {
                setTextViewText(R.id.timeTextView, item.startTime)
                setInt(R.id.classroomTextView, "setBackgroundResource", item.classroomBackgroundId)
                setTextViewText(R.id.classroomTextView, item.classroomName)
                setTextViewText(R.id.subjectTextView, item.subjectName)
                setTextViewText(R.id.lecturerTextView, item.lecturers)
                setTextViewText(R.id.endTimeTextView, item.endTime)
            }
        } else if (item is TimetableItem.Title) {
            rootView = RemoteViews(
                context.packageName,
                R.layout.widget_item_timetable_title
            )
            with(rootView) {
                setTextViewText(R.id.titleTextView, item.title.makeBold())
            }
        } else {
            item as TimetableItem.BodyWithEmptyLecturers
            rootView = RemoteViews(
                context.packageName,
                R.layout.widget_item_timetable_body_empty_lecturers
            )
            with(rootView) {
                setTextViewText(R.id.timeTextView, item.startTime)
                setInt(R.id.classroomTextView, "setBackgroundResource", item.classroomBackgroundId)
                setTextViewText(R.id.classroomTextView, item.classroomName)
                setTextViewText(R.id.subjectTextView, item.subjectName)
                setTextViewText(R.id.endTimeTextView, item.endTime)
            }
        }
        return rootView
    }

    override fun getLoadingView(): RemoteViews = RemoteViews(
        context.packageName,
        R.layout.widget_item_loading
    ).also {
        Timber.d("getLoadingView")
    }

    override fun getViewTypeCount(): Int = 3

    override fun getItemId(position: Int): Long =
        timetableInteractor.dataList[position].type.toLong()

    override fun hasStableIds(): Boolean = false

    private fun String.makeBold(): SpannableString = SpannableString(this).apply {
        setSpan(StyleSpan(Typeface.BOLD), 0, this.length, 0)
    }

/*
    private fun insertMockData() {
        dataList.clear()
        dataList.addAll(
            listOf(
                TimetableItem.Title(
                    "Понедельник",
                    1
                ),
                TimetableItem.Body(
                    "1",
                    "clas",
                    "clas 1",
                    R.drawable.shape_timetable_class_type_practise_background,
                    "sub",
                    "Математика 3",
                    "lectu",
                    "09:00",
                    "10:00"
                ),
                TimetableItem.Body(
                    "2",
                    "clas",
                    "clas 1",
                    R.drawable.shape_timetable_class_type_consultation_background,
                    "sub",
                    "Математика 3",
                    "lectu",
                    "09:00",
                    "10:00"
                ),
                TimetableItem.Body(
                    "3",
                    "clas",
                    "clas 1",
                    R.drawable.shape_timetable_class_type_practise_background,
                    "sub",
                    "Математика 3",
                    "lectu",
                    "09:00",
                    "10:00"
                ),
                TimetableItem.Body(
                    "4",
                    "clas",
                    "clas 1",
                    R.drawable.shape_timetable_class_type_practise_background,
                    "sub",
                    "Математика 3",
                    "lectu",
                    "09:00",
                    "10:00"
                ),
                TimetableItem.Body(
                    "5",
                    "clas",
                    "clas 1",
                    R.drawable.shape_timetable_class_type_practise_background,
                    "sub",
                    "Математика 3",
                    "lectu",
                    "09:00",
                    "10:00"
                ),
                TimetableItem.Body(
                    "6",
                    "clas",
                    "clas 1",
                    R.drawable.shape_timetable_class_type_practise_background,
                    "sub",
                    "Математика 3",
                    "lectu",
                    "09:00",
                    "10:00"
                ),
                TimetableItem.Body(
                    "7",
                    "clas",
                    "clas 1",
                    R.drawable.shape_timetable_class_type_practise_background,
                    "sub",
                    "Математика 3",
                    "lectu",
                    "09:00",
                    "10:00"
                )
            )
        )

        timetableRepository.deleteAll()
        timetableRepository.saveAll(
            listOf(
                TimetableEntity(
                    id = "613a464a696d6225e1b4de86",
                    classroom = "",
                    classroomName = "практика",
                    numberDayInWeek = 1,
                    discipline = "Эк по фк и спорту",
                    disciplineName = "Эк по фк и спорту",
                    lecturers = "",
                    lecturersIdStringArrayJson = "",
                    startTime = 32400000,
                    startTimeText = "09:00",
                    endTimeText = "10:30",
                    groups = "А101С",
                    groupId = "613a464a696d6225e1b507f2",
                    type = "practise",
                    weekCode = 1
                ),
                TimetableEntity(
                    id = "613a464a696d6225e1b4de87",
                    classroom = "106*",
                    classroomName = "106* лабораторная",
                    numberDayInWeek = 1,
                    discipline = "Сопр.материалов",
                    disciplineName = "Сопр.материалов",
                    lecturers = "Спиридонов Д.В.",
                    lecturersIdStringArrayJson = "",
                    startTime = 39000000,
                    startTimeText = "10:50",
                    endTimeText = "12:20",
                    groups = "А101С",
                    groupId = "613a464a696d6225e1b507f2",
                    type = "laboratory",
                    weekCode = 1
                ),
                TimetableEntity(
                    id = "613a464a696d6225e1b4de8a",
                    classroom = "434*",
                    classroomName = "434* практика",
                    numberDayInWeek = 1,
                    discipline = "Физика",
                    disciplineName = "Физика",
                    lecturers = "",
                    lecturersIdStringArrayJson = "",
                    startTime = 45600000,
                    startTimeText = "12:40",
                    endTimeText = "14:10",
                    groups = "А101С",
                    groupId = "613a464a696d6225e1b507f2",
                    type = "practise",
                    weekCode = 1
                ),
                TimetableEntity(
                    id = "613a464a696d6225e1b4de8b",
                    classroom = "326*",
                    classroomName = "326* лабораторная",
                    numberDayInWeek = 1,
                    discipline = "Физика",
                    disciplineName = "Физика",
                    lecturers = "",
                    lecturersIdStringArrayJson = "",
                    startTime = 45600000,
                    startTimeText = "12:40",
                    endTimeText = "14:10",
                    groups = "А101С",
                    groupId = "613a464a696d6225e1b507f2",
                    type = "laboratory",
                    weekCode = 1
                ),
                TimetableEntity(
                    id = "613a464a696d6225e1b4de8e",
                    classroom = "502*",
                    classroomName = "502* практика",
                    numberDayInWeek = 1,
                    discipline = "Инж.и комп. граф",
                    disciplineName = "Инж.и комп. граф",
                    lecturers = "",
                    lecturersIdStringArrayJson = "",
                    startTime = 45600000,
                    startTimeText = "12:40",
                    endTimeText = "14:10",
                    groups = "А101С",
                    groupId = "613a464a696d6225e1b507f2",
                    type = "practise",
                    weekCode = 1
                )
            )
        )
    }
*/

}