package com.setnameinc.voenmeh.timetable.widget

import android.content.Intent
import android.widget.RemoteViewsService

class TimetableWidgetRemoteViewsService : RemoteViewsService() {
    override fun onGetViewFactory(intent: Intent): RemoteViewsFactory =
        TimetableWidgetRemoteViewFactory(applicationContext)
}