package com.setnameinc.voenmeh.timetable.widget.di

import com.setnameinc.voenmeh.timetable.widget.interactor.TimetableWidgetInteractor
import org.koin.dsl.module

object TimetableWidgetModule {
    val module = module {
        single {
            TimetableWidgetInteractor(
                timetableRepository = get(),
                userInteractor = get()
            )
        }
    }
}