package com.setnameinc.voenmeh.timetable.widget.interactor

import com.setnameinc.voenmeh.timetable.common.domain.TimetableConverter.convertFromDatabase
import com.setnameinc.voenmeh.timetable.common.repository.TimetableRepository
import com.setnameinc.voenmeh.timetable.lists.ui.TimetableItem
import com.setnameinc.voenmeh.timetable.lists.ui.TimetableNextWeekItemConverter.convert
import com.setnameinc.voenmeh.tools.utils.DayTitleFromWeekNumber
import com.setnameinc.voenmeh.tools.utils.TimeUtils.TODAY_NUMBER_DAY_IN_WEEK
import com.setnameinc.voenmeh.tools.utils.WeekType
import com.setnameinc.voenmeh.tools.utils.startOnUi
import com.setnameinc.voenmeh.user.common.domain.UserInteractor
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import timber.log.Timber

/**
 * TODO(remove and change all to ContentProvider)
 */
class TimetableWidgetInteractor(
    private val timetableRepository: TimetableRepository,
    private val userInteractor: UserInteractor
) {

    val dataList: MutableList<TimetableItem> = mutableListOf()
    val mutex: Mutex = Mutex()

    var emptyLessonForToday: Boolean = false

    fun updateData(complete: () -> Unit) {
        CoroutineScope(Dispatchers.Default).launch {
            mutex.withLock(dataList) {
                userInteractor.requestUserGroup(
                    complete = { groupId ->
                        Timber.d("complete $groupId")
                        dataList.clear()
                        val items = timetableRepository.getTimetable(
                            groupId,
                            WeekType.CURRENT_WEEK.code
                        )
                            .filter {
                                it.numberDayInWeek == TODAY_NUMBER_DAY_IN_WEEK
                            }
                            .convertFromDatabase()
                            .convert()
                        emptyLessonForToday = items.isEmpty()
                        if (emptyLessonForToday) {
                            Timber.d("empty list")
                            dataList.add(
                                TimetableItem.Title(
                                    DayTitleFromWeekNumber.getDayTitleFromWeekNumber(
                                        TODAY_NUMBER_DAY_IN_WEEK
                                    ),
                                    TODAY_NUMBER_DAY_IN_WEEK
                                )
                            )
                        } else {
                            Timber.d("add all items $items")
                            dataList.addAll(items)
                        }
                        startOnUi {
                            complete.invoke()
                        }
                    },
                    error = {
                        Timber.e("show dialog loaded wrong :(")
                    }
                )
            }
        }
    }

}