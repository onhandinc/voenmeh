package com.setnameinc.voenmeh.tools.base

import android.content.DialogInterface
import android.graphics.Color
import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * always shown in STATE_EXPANDED
 */
open class BaseBottomRoundSheetDialogFragment(
    @LayoutRes
    val layoutId: Int
) : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentView = inflater.inflate(layoutId, container, false)
        fragmentView?.rootView?.setBackgroundColor(Color.TRANSPARENT)
        if (activity != null) {
            val decorView = activity?.window?.decorView
            decorView?.viewTreeObserver?.addOnGlobalLayoutListener {
                val displayFrame = Rect()
                decorView.getWindowVisibleDisplayFrame(displayFrame)
                val height =
                    decorView.context.resources.displayMetrics.heightPixels
                val heightDifference: Int = height - displayFrame.bottom
                if (heightDifference != 0) {
                    if (fragmentView.paddingBottom != heightDifference) {
                        fragmentView.setPadding(0, 0, 0, heightDifference)
                    }
                } else {
                    if (fragmentView.paddingBottom != 0) {
                        fragmentView.setPadding(0, 0, 0, 0)
                    }
                }
            }
        }
        dialog?.setOnShowListener { dialog: DialogInterface ->
            val bottomSheetDialog = dialog as? BottomSheetDialog
            val bottomSheetInternal =
                bottomSheetDialog?.findViewById<View>(
                    com.google.android.material.R.id.design_bottom_sheet
                ) ?: return@setOnShowListener
            val behavior = BottomSheetBehavior.from(
                bottomSheetInternal
            )
            behavior.apply {
                skipCollapsed = true
                state = BottomSheetBehavior.STATE_EXPANDED
                setBottomSheetCallback(
                    object : BottomSheetBehavior.BottomSheetCallback() {
                        override fun onStateChanged(bottomSheet: View, newState: Int) {
                            when (newState) {
                                BottomSheetBehavior.STATE_HIDDEN -> {
                                    dismiss()
                                }
                                else -> {
                                    //ignore
                                }
                            }
                        }

                        override fun onSlide(bottomSheet: View, slideOffset: Float) {}
                    }
                )
            }
        }
        return fragmentView
    }

}