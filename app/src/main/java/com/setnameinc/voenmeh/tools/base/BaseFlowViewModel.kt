package com.setnameinc.voenmeh.tools.base

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlin.coroutines.CoroutineContext

/**
 * S - state
 * A - action
 * E - event
 */
abstract class BaseFlowViewModel<S, A, E> : ViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext = Job() + Dispatchers.Default

    private val TAG = BaseFlowViewModel::class.java.simpleName
    private val _viewStates: MutableStateFlow<S?> = MutableStateFlow(null)
    fun viewStates(): StateFlow<S?> = _viewStates

    protected var viewState: S
        get() = _viewStates.value
            ?: throw UninitializedPropertyAccessException("\"viewState\" was queried before being initialized")
        set(value) {
            /** StateFlow doesn't work with same values */
            synchronized(this) {
                if (_viewStates.value == value) {
                    _viewStates.value = null
                }
                launch {
                    withContext(Dispatchers.Main) {
                        _viewStates.value = value
                    }
                }
            }
        }

    private val _viewActions: MutableStateFlow<A?> = MutableStateFlow(null)
    fun viewActions(): StateFlow<A?> = _viewActions

    protected var viewAction: A
        get() = _viewActions.value
            ?: throw UninitializedPropertyAccessException("\"viewAction\" was queried before being initialized")
        set(value) {
            /** StateFlow doesn't work with same values */
            synchronized(this) {
                if (_viewActions.value == value) {
                    _viewActions.value = null
                }

                launch {
                    withContext(Dispatchers.Main) {
                        _viewActions.value = value
                    }
                }
            }
        }

    open fun obtainEvent(viewEvent: E) {
        launch {
            withContext(Dispatchers.Default) {
                obtainInternalEvent(viewEvent)
            }
        }
    }

    protected abstract suspend fun obtainInternalEvent(viewEvent: E)

}