package com.setnameinc.voenmeh.tools.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.setnameinc.voenmeh.R
import kotlinx.android.synthetic.main.fragment_loading.*
import timber.log.Timber

open class BsaeProgressFragment(
    @LayoutRes open val contentLayoutId: Int
) : Fragment(R.layout.fragment_loading) {

    private var skipProgressBar: Boolean = false

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (skipProgressBar) {
            hideProgressBar()
        }
        containerFrameLayout.addView(
            LayoutInflater.from(context).inflate(contentLayoutId, null)
        )
    }

    protected fun showProgressBar() {
        lifecycleScope.launchWhenStarted {
            containerFrameLayout.visibility = View.INVISIBLE
            loadingProgressBar.visibility = View.VISIBLE
        }
    }

    protected fun hideProgressBar() {
        skipProgressBar = true
        lifecycleScope.launchWhenStarted {
            containerFrameLayout.visibility = View.VISIBLE
            loadingProgressBar.visibility = View.INVISIBLE
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Timber.d("onDestroyView")
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.d("onDestroy")
    }

}