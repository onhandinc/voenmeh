package com.setnameinc.voenmeh.tools.base

import androidx.annotation.LayoutRes
import org.koin.androidx.scope.fragmentScope
import org.koin.core.scope.Scope

open class BsaeScopedProgressFragment(
    @LayoutRes override val contentLayoutId: Int
) : BsaeProgressFragment(contentLayoutId) {

    val fragmentScope: Scope by lazy { fragmentScope() }

    override fun onDestroy() {
        super.onDestroy()
        fragmentScope.close()
    }

}