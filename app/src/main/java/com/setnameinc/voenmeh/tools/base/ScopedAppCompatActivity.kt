package com.setnameinc.voenmeh.tools.base

import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import org.koin.androidx.scope.activityScope
import org.koin.core.scope.Scope

open class ScopedAppCompatActivity(
    @LayoutRes val contentLayoutId: Int
) : AppCompatActivity(contentLayoutId) {

    val activityScope: Scope by lazy { activityScope() }

    override fun onDestroy() {
        super.onDestroy()
        activityScope.close()
    }

}