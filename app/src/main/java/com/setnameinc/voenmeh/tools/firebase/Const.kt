package com.setnameinc.voenmeh.tools.firebase

object Const {
    const val USERS_COLLECTION_KEY: String = "users"
    const val BLOCKED_USERS_COLLECTION_KEY: String = "blockedUsers"

    const val USERS_GROUP_ID_FIELD_KEY: String = "groupId"
    const val USERS_FIRST_NAME_FIELD_KEY: String = "firstName"
    const val USERS_LAST_NAME_FIELD_KEY: String = "lastName"
    const val USERS_IS_BLOCKED_FIELD_KEY: String = "isBlocked"
}