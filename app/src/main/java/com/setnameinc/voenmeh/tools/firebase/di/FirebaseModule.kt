package com.setnameinc.voenmeh.tools.firebase.di

import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import org.koin.dsl.module

object FirebaseModule {
    val module = module {
        single { Firebase.auth }
        single { Firebase.firestore }
    }
}