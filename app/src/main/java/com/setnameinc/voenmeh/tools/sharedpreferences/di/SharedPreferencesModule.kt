package com.setnameinc.voenmeh.tools.sharedpreferences.di

import android.content.Context
import android.content.SharedPreferences
import org.koin.dsl.module

object SharedPreferencesModule {
    val module = module {
        single<SharedPreferences> {
            get<Context>().getSharedPreferences("application", Context.MODE_PRIVATE)
        }
    }
}