package com.setnameinc.voenmeh.tools.utils

object DayTitleFromWeekNumber {
    fun getDayTitleFromWeekNumber(number: Int) = when (number) {
        1 -> "Понедельник"
        2 -> "Вторник"
        3 -> "Среда"
        4 -> "Четверг"
        5 -> "Пятница"
        6 -> "Суббота"
        else -> "Воскресенье"
    }
}