package com.setnameinc.voenmeh.tools.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.SparseArray;

import androidx.annotation.NonNull;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;

import java.security.MessageDigest;

import timber.log.Timber;

public class FaceCenterCrop extends BitmapTransformation {

    private final GlideFaceDetector glideFaceDetector;

    public FaceCenterCrop(com.setnameinc.voenmeh.tools.utils.GlideFaceDetector glideFaceDetector) {
        this.glideFaceDetector = glideFaceDetector;
    }

    /**
     * @param bitmapPool A {@link com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool} that can be used to obtain and
     *                   return intermediate {@link Bitmap}s used in this transformation. For every
     *                   {@link android.graphics.Bitmap} obtained from the pool during this transformation, a
     *                   {@link android.graphics.Bitmap} must also be returned.
     * @param original   The {@link android.graphics.Bitmap} to transform
     * @param width      The ideal width of the transformed bitmap
     * @param height     The ideal height of the transformed bitmap
     * @return a transformed bitmap with face being in center.
     */
    @Override
    protected Bitmap transform(BitmapPool bitmapPool, Bitmap original, int width, int height) {

        float scaleX = (float) width / original.getWidth();
        float scaleY = (float) height / original.getHeight();

        Timber.d("width = %d, height = %d", width, height);
        Timber.d("original.width = %d, original.height = %d", original.getWidth(), original.getHeight());

        if (scaleX != scaleY) {

            Bitmap.Config config =
                    original.getConfig() != null ? original.getConfig() : Bitmap.Config.ARGB_8888;
            Bitmap result = bitmapPool.get(width, height, config);
            if (result == null) {
                result = Bitmap.createBitmap(width, height, config);
            }

            PointF focusPoint = new PointF();

            detectFace(original, focusPoint);

            Canvas canvas = new Canvas(result);
            Paint paint = new Paint();
            paint.setShader(
                    new BitmapShader(
                            original,
                            BitmapShader.TileMode.CLAMP,
                            BitmapShader.TileMode.CLAMP
                    )
            );
            paint.setAntiAlias(true);
            float radius = Math.min(canvas.getWidth() / 2, canvas.getHeight() / 2) * 0.65f;
            canvas.drawCircle(
                    focusPoint.x,
                    focusPoint.y,
                    radius,
                    paint
            );
            Timber.d("radius = %f", radius);
            //No need to recycle() original Bitmap as Glide will take care of returning our original Bitmap to the BitmapPool
            return Bitmap.createBitmap(
                    result,
                    (int) (focusPoint.x - radius),
                    (int) (focusPoint.y - radius),
                    (int) (radius * 2),
                    (int) (radius * 2)
            );
        } else {
            return original;
        }
    }

    /**
     * Calculates a point (focus point) in the bitmap, around which cropping needs to be performed.
     *
     * @param bitmap           Bitmap in which faces are to be detected.
     * @param centerOfAllFaces To store the center point.
     */
    private void detectFace(Bitmap bitmap, PointF centerOfAllFaces) {
        FaceDetector faceDetector = glideFaceDetector.getFaceDetector();
        if (!faceDetector.isOperational()) {
            centerOfAllFaces.set(bitmap.getWidth() / 2, bitmap.getHeight() / 2); // center crop
            return;
        }
        Frame frame = new Frame.Builder().setBitmap(bitmap).build();
        SparseArray<Face> faces = faceDetector.detect(frame);
        final int totalFaces = faces.size();
        if (totalFaces > 0) {
            float sumX = 0f;
            float sumY = 0f;
            for (int i = 0; i < totalFaces; i++) {
                PointF faceCenter = new PointF();
                getFaceCenter(faces.get(faces.keyAt(i)), faceCenter);
                sumX = sumX + faceCenter.x;
                sumY = sumY + faceCenter.y;
            }
            centerOfAllFaces.set(sumX / totalFaces, sumY / totalFaces);
            return;
        }
        centerOfAllFaces.set(bitmap.getWidth() / 2, bitmap.getHeight() / 2); // center crop
    }

    /**
     * Calculates center of a given face
     *
     * @param face   Face
     * @param center Center of the face
     */
    private void getFaceCenter(Face face, PointF center) {
        float x = face.getPosition().x;
        float y = face.getPosition().y;
        float width = face.getWidth();
        float height = face.getHeight();
        center.set(x + (width / 2), y + (height / 2)); // face center in original bitmap
    }

    private float getTopPoint(int height, float scaledHeight, float faceCenterY) {
        if (faceCenterY <= height / 2) { // Face is near the top edge
            return 0f;
        } else if ((scaledHeight - faceCenterY) <= height / 2) { // face is near bottom edge
            return height - scaledHeight;
        } else {
            return (height / 2) - faceCenterY;
        }
    }

    private float getLeftPoint(int width, float scaledWidth, float faceCenterX) {
        if (faceCenterX <= width / 2) { // face is near the left edge.
            return 0f;
        } else if ((scaledWidth - faceCenterX) <= width / 2) {  // face is near right edge
            return (width - scaledWidth);
        } else {
            return (width / 2) - faceCenterX;
        }
    }

    @Override
    public void updateDiskCacheKey(@NonNull MessageDigest messageDigest) {

    }
}

