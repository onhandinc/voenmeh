package com.setnameinc.voenmeh.tools.utils;

import android.content.Context;

import com.google.android.gms.vision.face.FaceDetector;

import java.lang.ref.WeakReference;

public class GlideFaceDetector {

    private volatile FaceDetector faceDetector;
    private WeakReference<Context> mContext;

    public Context getContext() {
        if (mContext == null) {
            throw new RuntimeException("Initialize GlideFaceDetector by calling GlideFaceDetector.initialize(context).");
        }
        return mContext.get();
    }

    public void initialize(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Context must not be null.");
        }
        mContext = new WeakReference(context.getApplicationContext());
    }

    private void initDetector() {
        if (faceDetector == null) {
            synchronized ((GlideFaceDetector.class)) {
                if (faceDetector == null) {
                    faceDetector = new
                            FaceDetector.Builder(getContext())
                            .setTrackingEnabled(false)
                            .build();
                }
            }
        }
    }

    public FaceDetector getFaceDetector() {
        initDetector();
        return faceDetector;
    }

    public void releaseDetector() {
        if (faceDetector != null) {
            faceDetector.release();
            faceDetector = null;
        }
        mContext.clear();
        mContext = null;
    }
}

