package com.setnameinc.voenmeh.tools.utils

import java.util.*

object Id {

    fun generateStringId(): String = UUID.randomUUID().toString().replace("-", "")

}