package com.setnameinc.voenmeh.tools.utils

suspend fun <T, R> (suspend (T) -> Unit).flatMap(
    transform: suspend (R) -> T
): (suspend (R) -> Unit) = {
    this(transform(it))
}

suspend fun <T> (suspend (T) -> Unit).map(): (suspend (T) -> Unit) = {
    this(it)
}