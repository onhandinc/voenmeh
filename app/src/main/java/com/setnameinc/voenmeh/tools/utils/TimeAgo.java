package com.setnameinc.voenmeh.tools.utils;

public class TimeAgo {
    private static final int SECOND_MILLIS = 1000;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public static String getTimeAgo(long time) {
        if (time < 1000000000000L) {
            time *= 1000;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0) {
            return null;
        }

        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "недавно";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return " минута назад";
        } else if (diff < 5 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " минуты назад";
        } else if (diff < 21 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " минут назад";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " минута назад";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "1 час назад";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " часа назад";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "вчера";
        } else {
            return diff / DAY_MILLIS + " дней назад";
        }
    }
}
