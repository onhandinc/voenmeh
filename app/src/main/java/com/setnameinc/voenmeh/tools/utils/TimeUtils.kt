package com.setnameinc.voenmeh.tools.utils

import java.text.SimpleDateFormat
import java.util.*

object TimeUtils {

    val TODAY_NUMBER_DAY_IN_WEEK: Int = let {
        val numberDayInWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
        if (numberDayInWeek == Calendar.SUNDAY) {
            7
        } else {
            numberDayInWeek - 1
        }
    }

    val simpleDateFormat = SimpleDateFormat("HH:mm", Locale.ENGLISH).apply {
        timeZone = TimeZone.getTimeZone("UTC")
    }

    fun calendarFactory(): Calendar = Calendar.getInstance(
        TimeZone.getTimeZone("UTC"),
        Locale.ENGLISH
    )

    fun Calendar.syncCalendarToCurrentUTC() {
        timeZone = TimeZone.getDefault()
    }

    fun Calendar.syncCalendarToUTC0() {
        timeZone = TimeZone.getTimeZone("UTC")
    }

}