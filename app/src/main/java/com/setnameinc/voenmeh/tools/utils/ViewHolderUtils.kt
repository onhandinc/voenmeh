package com.setnameinc.voenmeh.tools.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes

fun ViewGroup.inflateHolder(@LayoutRes layout: Int): View =
    LayoutInflater.from(this.context).inflate(layout, this, false)