package com.setnameinc.voenmeh.tools.utils

import java.util.*

enum class WeekType(val code: Int) {
    CURRENT_WEEK(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR) % 2),
    NEXT_WEEK((Calendar.getInstance().get(Calendar.WEEK_OF_YEAR) + 1) % 2)
}