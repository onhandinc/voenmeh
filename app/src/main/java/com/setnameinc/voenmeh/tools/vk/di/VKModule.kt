package com.setnameinc.voenmeh.tools.vk.di

import com.setnameinc.voenmeh.tools.vk.domain.VKInteractor
import com.setnameinc.voenmeh.tools.vk.preferences.VKSharedPreferences
import org.koin.dsl.module

object VKModule {
    val module = module {
        single {
            VKSharedPreferences(sharedPreferences = get())
        }
        single {
            VKInteractor(vkSharedPreferences = get())
        }
    }
}