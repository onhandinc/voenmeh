package com.setnameinc.voenmeh.tools.vk.domain

import com.setnameinc.voenmeh.tools.vk.preferences.VKSharedPreferences

class VKInteractor(
    private val vkSharedPreferences: VKSharedPreferences
) {

    fun saveToken(token: String) {
        vkSharedPreferences.saveToken(token)
    }

    fun getToken(): String = vkSharedPreferences.getToken()

    fun deleteToken() = vkSharedPreferences.deleteToken()

}