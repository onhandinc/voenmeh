package com.setnameinc.voenmeh.tools.vk.preferences

import android.content.SharedPreferences
import com.setnameinc.voenmeh.tools.vk.preferences.VKSharedPreferencesConst.TOKEN_KEY_SHARED_PREFERENCES
import timber.log.Timber

class VKSharedPreferences(
    private val sharedPreferences: SharedPreferences
) {

    fun saveToken(token: String) {
        Timber.d("save token = $token")
        sharedPreferences.edit()
            .putString(TOKEN_KEY_SHARED_PREFERENCES, token)
            .apply()
    }

    fun getToken(): String =
        (sharedPreferences.getString(TOKEN_KEY_SHARED_PREFERENCES, null) ?: "").also {
            Timber.d("token = $it")
        }

    fun deleteToken() {
        sharedPreferences.edit()
            .remove(TOKEN_KEY_SHARED_PREFERENCES)
            .apply()
    }

}