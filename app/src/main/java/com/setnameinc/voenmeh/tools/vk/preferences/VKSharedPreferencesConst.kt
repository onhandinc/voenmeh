package com.setnameinc.voenmeh.tools.vk.preferences

object VKSharedPreferencesConst {
    const val TOKEN_KEY_SHARED_PREFERENCES: String = "vk.token"
}