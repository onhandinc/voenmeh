package com.setnameinc.voenmeh.user.blocked.api

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.setnameinc.voenmeh.tools.firebase.Const.BLOCKED_USERS_COLLECTION_KEY
import kotlinx.coroutines.tasks.await
import java.lang.Exception

class UserBlockedApi(
    private val firebaseAuth: FirebaseAuth,
    private val firebaseFirestore: FirebaseFirestore
) {

    suspend fun isUserBlocked(
        complete: suspend (Boolean) -> Unit,
        error: suspend (Throwable) -> Unit
    ) {
        try {
            val response = firebaseAuth.currentUser
                ?.uid
                ?.let {
                    firebaseFirestore.collection(BLOCKED_USERS_COLLECTION_KEY)
                        .document(it)
                        .get()
                        .await()
                        ?.exists()
                }
            if (response != null) {
                complete(response)
            } else {
                throw NullPointerException()
            }
        } catch (e: Exception) {
            error(e)
        }
    }

}