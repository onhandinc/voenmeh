package com.setnameinc.voenmeh.user.blocked.di

import com.setnameinc.voenmeh.user.blocked.api.UserBlockedApi
import com.setnameinc.voenmeh.user.blocked.domain.UserBlockedInteractor
import org.koin.dsl.module

object UserBlockedModule {
    val module = module {
        single { UserBlockedApi(firebaseAuth = get(), firebaseFirestore = get()) }
        single { UserBlockedInteractor(userBlockedApi = get()) }
    }
}