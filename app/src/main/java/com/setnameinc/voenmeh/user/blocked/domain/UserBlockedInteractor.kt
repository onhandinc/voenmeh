package com.setnameinc.voenmeh.user.blocked.domain

import com.setnameinc.voenmeh.user.blocked.api.UserBlockedApi

class UserBlockedInteractor(
    private val userBlockedApi: UserBlockedApi
) {

    suspend fun isUserBlocked(
        complete: suspend (Boolean) -> Unit,
        error: suspend (Throwable) -> Unit
    ) {
        userBlockedApi.isUserBlocked(complete, error)
    }
}