package com.setnameinc.voenmeh.user.common.api

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface UserApi {

    @POST("user/create")
    fun create(@Body user: UserPostRequest): Deferred<Response<UserResponse>>

    @POST("user/updateGroupId")
    fun updateGroupId(@Body user: UserUpdateGroupId): Deferred<Response<String>>

    @GET("user/findById/{id}")
    fun findById(@Path("id") id: String): Deferred<Response<UserResponse>>

    @GET("user/findByUid/{uid}")
    fun findByUid(@Path("uid") uid: String): Deferred<Response<UserResponse>>

}