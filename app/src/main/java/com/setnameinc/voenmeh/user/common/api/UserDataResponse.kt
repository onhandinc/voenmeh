package com.setnameinc.voenmeh.user.common.api

data class UserDataResponse(
    val uid: String,
    val email: String,
    val firstName: String,
    val lastName: String,
    val groupId: String,
    val isBlocked: Boolean
)