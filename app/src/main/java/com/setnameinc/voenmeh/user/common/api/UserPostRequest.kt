package com.setnameinc.voenmeh.user.common.api

data class UserPostRequest(
    val authUid: String,
    val firstName: String,
    val lastName: String,
    val groupId: String
)