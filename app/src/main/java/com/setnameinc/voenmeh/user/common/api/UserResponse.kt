package com.setnameinc.voenmeh.user.common.api

import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("authUid")
    val authUid: String,
    @SerializedName("firstName")
    val firstName: String = "",
    @SerializedName("lastName")
    val lastName: String = "",
    @SerializedName("groupId")
    val groupId: String = "",
    @SerializedName("blocked")
    val isBlocked: Boolean = false
)