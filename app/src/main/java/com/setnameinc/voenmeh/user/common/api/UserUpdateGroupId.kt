package com.setnameinc.voenmeh.user.common.api

data class UserUpdateGroupId(
    val id: String,
    val groupId: String
)