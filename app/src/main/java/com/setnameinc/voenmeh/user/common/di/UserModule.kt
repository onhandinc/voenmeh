package com.setnameinc.voenmeh.user.common.di

import com.setnameinc.voenmeh.app.api.RetrofitConst
import com.setnameinc.voenmeh.user.common.api.UserApi
import com.setnameinc.voenmeh.user.common.domain.UserInteractor
import com.setnameinc.voenmeh.user.common.repository.UserRepository
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit

object UserModule {
    val module = module {
        single {
            get<Retrofit>(
                named(RetrofitConst.VOENMEH_SERVER_RETROFIT_INJECTION_KEY)
            ).create(UserApi::class.java)
        }
        single { UserRepository(sharedPreferences = get()) }
        single {
            UserInteractor(
                userRepository = get(),
                firebaseAuth = get(),
                userApi = get()
            )
        }
    }
}