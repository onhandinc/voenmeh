package com.setnameinc.voenmeh.user.common.domain

data class GroupInfoModel(
    val groupId: String,
    val groupNumber: Int,
    val groupName: String
)