package com.setnameinc.voenmeh.user.common.domain

import com.setnameinc.voenmeh.user.common.viewmodel.UserData
import com.setnameinc.voenmeh.user.common.api.UserDataResponse
import com.setnameinc.voenmeh.user.common.repository.UserEntity

object UserConverter {

    fun fromNetwork(
        userDataResponse: UserDataResponse
    ): UserData =
        UserData(
            uid = userDataResponse.uid,
            email = userDataResponse.email,
            firstName = userDataResponse.firstName,
            lastName = userDataResponse.lastName,
            groupId = userDataResponse.groupId,
            isBlocked = userDataResponse.isBlocked
        )

    fun toDatabase(
        userData: UserData
    ): UserEntity = UserEntity(
        uid = userData.uid,
        email = userData.email,
        firstName = userData.firstName,
        lastName = userData.lastName,
        groupId = userData.groupId,
        isBlocked = userData.isBlocked
    )

    fun fromDatabase(
        userEntity: UserEntity
    ): UserData =
        UserData(
            uid = userEntity.uid,
            email = userEntity.email,
            firstName = userEntity.firstName,
            lastName = userEntity.lastName,
            groupId = userEntity.groupId,
            isBlocked = userEntity.isBlocked
        )
}