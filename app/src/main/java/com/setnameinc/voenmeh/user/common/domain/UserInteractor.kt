package com.setnameinc.voenmeh.user.common.domain

import com.google.firebase.auth.FirebaseAuth
import com.setnameinc.voenmeh.tools.utils.flatMap
import com.setnameinc.voenmeh.user.common.api.UserApi
import com.setnameinc.voenmeh.user.common.api.UserUpdateGroupId
import com.setnameinc.voenmeh.user.common.repository.UserEntity
import com.setnameinc.voenmeh.user.common.repository.UserRepository
import com.setnameinc.voenmeh.user.common.viewmodel.UserData
import timber.log.Timber

class UserInteractor(
    private val userApi: UserApi,
    private val userRepository: UserRepository,
    private val firebaseAuth: FirebaseAuth
) {

    suspend fun requestUserData(
        complete: suspend (UserData) -> Unit,
        error: suspend (Throwable) -> Unit
    ) {
        Timber.d("RequestUserData, stackTrace = ${Throwable().stackTrace.toList()}")
        userRepository.getUserData(
            complete = complete.flatMap { UserConverter.fromDatabase(it) }
        )
        //TODO(error.invoke) in which cases???
    }

    suspend fun updateUserGroupId(
        groupId: String,
        complete: suspend (String) -> Unit
    ) {
        Timber.d("UserInteractorEvent.UpdateUserGroupId")
        userRepository.getUserId(
            complete = { userId: String ->
                Timber.d("UserInteractor UserInteractorEvent.UpdateUserGroupId Success, userId = ${userId}")
                userApi.updateGroupId(
                    UserUpdateGroupId(userId, groupId)
                )
                //TODO(Exception on userApi.updateGroupId().await())
                userRepository.groupIdChangedTo(groupId)
                complete(groupId)
            },
            error = {
                Timber.d("UserInteractor UserInteractorEvent.UpdateUserGroupId Failed")
                firebaseAuth.uid?.let {
                    Timber.d("UserInteractor UserInteractorEvent.UpdateUserGroupId Success, uid = ${it}")
                    val response = userApi.findByUid(it).await()
                    Timber.d("response = ${response.code()}, ${response.isSuccessful}, ${response.errorBody()}")
                    response.body()?.let { userResponse ->
                        Timber.d("UserInteractor UserInteractorEvent.UpdateUserGroupId Success, userResponse = ${userResponse}")
                        UserEntity(
                            id = userResponse.id,
                            firstName = userResponse.firstName,
                            lastName = userResponse.lastName,
                            isBlocked = userResponse.isBlocked
                        )
                    }?.let { it1 ->
                        userRepository.updateUserData(it1)
                        updateUserGroupId(groupId, complete)
                    } ?: Timber.d("error :(")
                } ?: Timber.d("UserInteractor UserInteractorEvent.UpdateUserGroupId Error, null")
            }
        )
    }

    suspend fun updateUserData(
        complete: (() -> Unit)? = null
    ) {
        firebaseAuth.uid?.let {
            userApi.findByUid(it).await().body()?.let { userResponse ->
                UserEntity(
                    id = userResponse.id,
                    firstName = userResponse.firstName,
                    lastName = userResponse.lastName,
                    groupId = userResponse.groupId,
                    isBlocked = userResponse.isBlocked
                )
            }?.let { it1 ->
                userRepository.updateUserData(it1)
            }
        }
        complete?.invoke()
    }

    suspend fun requestUserBlocked(
        complete: suspend (Boolean) -> Unit,
        error: () -> Unit
    ) {
        userRepository.isUserBlocked(complete)
    }

    suspend fun requestUserGroup(
        complete: (String) -> Unit,
        error: (() -> Unit)? = null
    ) {
        Timber.d("RequestUserData, stackTrace = ${Throwable().stackTrace.toList()}")
        userRepository.getUserData {
            Timber.d("model = $it")
            if (it.groupId.isNotEmpty()) {
                complete(it.groupId)
            } else {
                error?.invoke()
            }
        }
    }

}