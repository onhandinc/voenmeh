package com.setnameinc.voenmeh.user.common.domain

sealed class UserModelResponse<T> {
    data class Success<T>(val data: T) : UserModelResponse<T>()
    data class Failed<T>(val reason: UserModelResponseFailed) : UserModelResponse<T>()
    data class Error<T>(val throwable: Throwable) : UserModelResponse<T>()
}