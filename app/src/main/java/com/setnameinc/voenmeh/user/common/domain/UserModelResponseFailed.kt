package com.setnameinc.voenmeh.user.common.domain

enum class UserModelResponseFailed {
    EMPTY_USER_DATA
}