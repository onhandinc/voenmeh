package com.setnameinc.voenmeh.user.common.repository

data class UserEntity(
    var id: String = "",
    var uid: String = "",
    var email: String = "",
    var firstName: String = "",
    var lastName: String = "",
    var groupId: String = "",
    var isBlocked: Boolean = false
)