package com.setnameinc.voenmeh.user.common.repository

import android.content.SharedPreferences
import com.setnameinc.voenmeh.user.common.repository.UserRepositoryKeys.EMAIL_KEY
import com.setnameinc.voenmeh.user.common.repository.UserRepositoryKeys.FIRST_NAME_KEY
import com.setnameinc.voenmeh.user.common.repository.UserRepositoryKeys.GROUP_ID_KEY
import com.setnameinc.voenmeh.user.common.repository.UserRepositoryKeys.ID_KEY
import com.setnameinc.voenmeh.user.common.repository.UserRepositoryKeys.IS_BLOCKED_KEY
import com.setnameinc.voenmeh.user.common.repository.UserRepositoryKeys.LAST_NAME_KEY
import com.setnameinc.voenmeh.user.common.repository.UserRepositoryKeys.UID_KEY
import java.lang.NullPointerException

class UserRepository(private val sharedPreferences: SharedPreferences) {

    fun updateUserData(userEntity: UserEntity) {
        sharedPreferences.edit().apply {
            putString(ID_KEY, userEntity.id)
            putString(UID_KEY, userEntity.uid)
            putString(EMAIL_KEY, userEntity.email)
            putString(FIRST_NAME_KEY, userEntity.firstName)
            putString(LAST_NAME_KEY, userEntity.lastName)
            putString(GROUP_ID_KEY, userEntity.groupId)
            putBoolean(IS_BLOCKED_KEY, userEntity.isBlocked)
        }.apply()
    }

    suspend fun isUserBlocked(complete: suspend (Boolean) -> Unit) {
        complete(sharedPreferences.getBoolean(IS_BLOCKED_KEY, false))
    }

    suspend fun getUserId(complete: suspend (String) -> Unit, error: suspend (Throwable) -> Unit) {
        val response = sharedPreferences.getString(ID_KEY, "")
        if (response != null && response.isNotEmpty()) {
            complete(response)
        } else {
            error(NullPointerException())
        }
    }

    fun groupIdChangedTo(groupId: String) {
        sharedPreferences.edit().apply {
            putString(GROUP_ID_KEY, groupId)
        }.apply()
    }

    suspend fun getUserData(complete: suspend (UserEntity) -> Unit) {
        complete(
            UserEntity(
                uid = sharedPreferences.getString(UID_KEY, "") ?: "",
                email = sharedPreferences.getString(EMAIL_KEY, "") ?: "",
                firstName = sharedPreferences.getString(FIRST_NAME_KEY, "") ?: "",
                lastName = sharedPreferences.getString(LAST_NAME_KEY, "") ?: "",
                groupId = sharedPreferences.getString(GROUP_ID_KEY, "") ?: "",
                isBlocked = sharedPreferences.getBoolean(IS_BLOCKED_KEY, false)
            )
        )
    }

}
