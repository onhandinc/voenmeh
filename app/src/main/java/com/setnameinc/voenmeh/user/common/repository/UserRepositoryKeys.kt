package com.setnameinc.voenmeh.user.common.repository

object UserRepositoryKeys {
    const val ID_KEY = "user.id"
    const val UID_KEY = "user.uid"
    const val EMAIL_KEY = "user.email"
    const val FIRST_NAME_KEY = "user.firstName"
    const val LAST_NAME_KEY = "user.lastName"
    const val GROUP_ID_KEY = "user.groupId"
    const val IS_BLOCKED_KEY = "user.isUserBlocked"
}