package com.setnameinc.voenmeh.user.common.viewmodel

data class UserData(
    val uid: String,
    val email: String,
    val firstName: String,
    val lastName: String,
    val groupId: String,
    val isBlocked: Boolean
)